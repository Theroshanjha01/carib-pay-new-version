import React, { PureComponent } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  View,
  PanResponder,
  ViewPropTypes,
  BackHandler,
  TouchableOpacity
} from 'react-native';
// import PropTypes from 'prop-types';
// import AsyncStorage from '@react-native-community/async-storage';
import AppContainer from './App/routes.js';
// // import Login from './App/Components/Login.js'
// import Modal from 'react-native-modal';
// import metrics from './App/Themes/Metrics.js'
// import colors from './App/Themes/Colors.js';


// class UserInactivity extends PureComponent {
//   static propTypes = {
//     timeForInactivity: PropTypes.number,
//     checkInterval: PropTypes.number,
//     children: PropTypes.node.isRequired,
//     style: ViewPropTypes.style,
//     onAction: PropTypes.func.isRequired,
//   };

//   static defaultProps = {
//     timeForInactivity: 10000,
//     checkInterval: 2000,
//     style: {
//       flex: 1,
//     },
//   };

//   state = {
//     active: true,
//   };

//   componentWillMount() {
//     this.panResponder = PanResponder.create({
//       onMoveShouldSetPanResponderCapture: this.onMoveShouldSetPanResponderCapture,
//       onStartShouldSetPanResponderCapture: this.onMoveShouldSetPanResponderCapture,
//       onResponderTerminationRequest: this.handleInactivity
//     });
//     this.handleInactivity();
//   }

//   componentWillUnmount() {
//     clearInterval(this.inactivityTimer);
//   }

//   handleInactivity = () => {
//     clearTimeout(this.timeout);
//     this.setState({
//       active: true,
//     }, () => {
//       this.props.onAction(this.state.active); // true
//     });
//     this.resetTimeout();
//   }

//   timeoutHandler = () => {
//     this.setState({
//       active: false,
//     }, () => {
//       this.props.onAction(this.state.active); // false
//     });
//   }

//   resetTimeout = () => {
//     this.timeout = setTimeout(this.timeoutHandler, this.props.timeForInactivity);
//   }

//   onMoveShouldSetPanResponderCapture = () => {
//     this.handleInactivity();
//     return false;
//   }

//   render() {
//     const {
//       style,
//       children,
//     } = this.props;
//     return (
//       <View
//         style={style}
//         collapsable={false}
//         {...this.panResponder.panHandlers}
//       >
//         {children}
//       </View>
//     );
//   }
// }
export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      active: true,
      text: '', passcode_device: '', DeviceId: ''
    };

    console.disableYellowBox = true;
  }


  // async componentDidMount() {
  //   let passcode = await AsyncStorage.getItem("passcode")
  //   let device_id = await AsyncStorage.getItem("DeviceId")
  //   passcode = JSON.parse(passcode)
  //   this.setState({
  //     passcode_device: passcode,
  //     DeviceId: device_id
  //   })
  // }

  // onAction = (active) => {
  //   this.setState({
  //     active,
  //   });
  //   console.log("clikced")
  // }

  // async onLogout() {
  //   if (this.state.passcode_device == null) {
  //     await AsyncStorage.clear();
  //     BackHandler.exitApp()
  //     return true
  //   } else {
  //     await AsyncStorage.clear();
  //     await AsyncStorage.setItem("passcode", JSON.stringify(this.state.passcode_device))
  //     await AsyncStorage.setItem("DeviceId", this.state.DeviceId)
  //     await AsyncStorage.setItem("logoutHavingPasscode", "1")
  //     BackHandler.exitApp()
  //     return true
  //   }
  // }



  render() {
    // const { active } = this.state;
    // if (this.state.active) {
      return (
        // <UserInactivity
        //   timeForInactivity={300000}
        //   checkInterval={1000}
        //   onAction={this.onAction}
        // >
          <AppContainer />
        // {/* </UserInactivity> */}
      )
    // } else {
    //   return (
    //     <View style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }}>
    //       <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={!this.state.active}>
    //         <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 260, justifyContent: 'center' }}>
    //           <View style={{ flex: 1 }}>
    //             <View>
    //               <Image style={{ height: 120, width: 120, alignSelf: 'center', resizeMode: 'contain', marginTop: 20 }}
    //                 source={require("./App/Images/danger.png")}></Image>
    //               <Text style={{ fontSize: metrics.text_normal, color: colors.black, textAlign: 'center', marginTop: 10, fontWeight: 'bold' }}>{"Your session is expired \n Please Login Again"}</Text>
    //             </View>
    //             <View style={styles.bottomview2}>
    //               <TouchableOpacity onPress={() => this.onLogout()}>
    //                 <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center' }}>OK</Text>
    //               </TouchableOpacity>
    //             </View>
    //           </View>
    //         </View>
    //       </Modal>
    //     </View>
    //   )
    // }
  }
}


// const styles = StyleSheet.create({
//   bottomview2: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 }
// })
