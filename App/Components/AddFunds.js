import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  BackHandler,
  TouchableOpacity,
  Image,
} from "react-native";
import metrics from "../Themes/Metrics.js";
import colors from "../Themes/Colors.js";
import AsyncStorage from "@react-native-community/async-storage";
var Spinner = require("react-native-spinkit");
import { Input, CheckBox } from "react-native-elements";
import Toast, { DURATION } from "react-native-easy-toast";
const axios = require("axios");
import Modal from "react-native-modal";
// import RNPaypal from "react-native-paypal-lib";
import { getRandomInt } from "./CaribMall/validations.js";
import { cancelUrl, returnUrl } from "./CaribMall/Utils/api.constants.js";
import { paypal_basictoken, paypal_gettoken_live, paypal_payment_live } from "../Utils.js";
import fetchPost from "../fetchPost.js";

export default class AddFunds extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: this.props.navigation.state.params.amount,
      paymentMethodId: this.props.navigation.state.params.paymentMethodId,
      currency_id: this.props.navigation.state.params.currency_id,
      token: this.props.navigation.state.params.token,
      txn_fee: "...",
      total_amount_pay: "...",
      visible: true,
      checked: false,
      isVisibleAlert: false,
      note: "",
    };
  }

  componentDidMount() {
    console.log("direct parset flot -- >", this.state.currency_id);
    this.setState({
      amount: parseFloat(this.state.amount).toFixed(2),
    });

    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    let postData = {
      paymentMethodId: this.state.paymentMethodId,
      currencyId: this.state.currency_id,
    };
    axios
      .get(
        "https://sandbox.caribpayintl.com/api/get-fees-list-by-payment-method",
        { params: postData, headers: { Authorization: this.state.token } }
      )
      .then((response) => {
        let fees_charge_in_percentage =
          response.data.success.feesdetails[0].charge_percentage;
        let feees = (fees_charge_in_percentage / 100) * this.state.amount;
        // let total_amount =
        console.log(this.state.amount, feees);
        this.setState({
          txn_fee: parseFloat(feees).toFixed(2),
          total_amount_pay: parseFloat(this.state.amount) + parseFloat(feees),
          visible: false,
        });
      })
      .catch((error) => {
        console.log(error);
      });

    this.getPayPalInfo();
  }

  getPayPalInfo = () => {
    console.log("getPaypalinfo", this.state.token);
    let postData = { method_id: 3, currency_id: this.state.currency_id };
    axios({
      method: "post",
      url: "https://sandbox.caribpayintl.com/api/deposit/get-paypal-info",
      data: postData,
      headers: { Authorization: this.state.token },
    })
      .then((response) => {
        // console.log(response.data)
        if (response.data.success.status == 200) {
          console.log("paypalInfo ---> ", response.data);
          AsyncStorage.setItem(
            "clientIdPaypal",
            response.data.success.method_info.client_id
          );
          AsyncStorage.setItem(
            "clientSecretIdPaypal",
            response.data.success.method_info.client_secret
          );
        }
      })
      .catch((err) => {
        alert("roshan");
      });
  };

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  onPayPalBtnClick = async () => {
    if (this.state.paymentMethodId == 3) {
      // this.setState({
      //     isComingSoon: true
      // })
      let client_id = await AsyncStorage.getItem("clientIdPaypal");

      console.log("client id is --> ", client_id);
      let user_id = await AsyncStorage.getItem("id");
      this.setState({
        visible: true,
      });


      let invoice_number = getRandomInt();
      console.log('invoice_number', invoice_number);
      let cp_id = await AsyncStorage.getItem('caribpay_id');      
      var dataDetail=JSON.stringify({
        intent: "sale",
        payer: {
            payment_method: "paypal"
        },
        transactions: [
        {
          amount: {
              total: this.state.total_amount_pay.toFixed(2).toString(),
              currency: "USD"
          },
          description: this.state.note == "" ? "Payment for invoices "+ invoice_number :this.state.note,
          custom: "Mobile topup",
          invoice_number: invoice_number,
          payment_options: {
              allowed_payment_method: "INSTANT_FUNDING_SOURCE"
          },
          soft_descriptor: cp_id,
          }
        ],
        note_to_payer: "Contact us for any questions on your order.",
        redirect_urls: {
          return_url: returnUrl,
          cancel_url: cancelUrl
          }
        });
      let body = 'grant_type=client_credentials';
      const response = await fetchPost(paypal_gettoken_live, body, paypal_basictoken);
      console.log(response);


      axios.post(paypal_payment_live, dataDetail, // you can get data details from https://developer.paypal.com/docs/api/payments/v1/
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${response.access_token}`
            }
        }
    ).then(response5 => {
        console.log(response5);
        const { id, links } = response5.data
        const approvalUrl = links.find(data => data.rel == "approval_url")
        this.setState({ visible: false });

        let urltosend = `https://sandbox.caribpayintl.com/api/deposit/paypal-payment-store`; 
        let postData = {
          amount: this.state.amount,
          currencyID: 1,
          userId: user_id,
          methodID: 3,
          details: {
            status: "COMPLETED",
          },
        };
        this.props.navigation.navigate("WebPage",{
            approvalUrl: approvalUrl.href,
            accessToken: response.access_token,
            url: urltosend,
            data: postData,
            paymentId: id,
            token: this.state.token,
            from: 'Addfund', 
            requestData: '',
            amount: this.state.total_amount_pay.toFixed(2),
            note:  this.state.note
        })

      }).catch(err => {
          console.log({ ...err })
      })

      // end of code
      // RNPaypal.paymentRequest({
      //   clientId: client_id,
      //   environment: RNPaypal.ENVIRONMENT.PRODUCTION,
      //   intent: RNPaypal.INTENT.SALE,
      //   price: this.state.total_amount_pay,
      //   currency: "USD",
      //   description: this.state.note == "" ? "Payment for invoices "+Math.floor(100000 + Math.random() * 900000 +" "):this.state.note,
      //   acceptCreditCards: true,
      // })
      //   .then((response) => {
      //     console.log("response paypal ---> ", response);
      //     if (response.response.state == "approved") {
      //       console.log("yes it is approved");
      //       let postData = {
      //         amount: this.state.amount,
      //         currencyID: 1,
      //         userId: user_id,
      //         methodID: 3,
      //         details: {
      //           status: "COMPLETED",
      //         },
      //       };
      //       axios({
      //         method: "post",
      //         url:
      //           "https://sandbox.caribpayintl.com/api/deposit/paypal-payment-store",
      //         data: postData,
      //         headers: { Authorization: this.state.token },
      //       })
      //         .then((response) => {
      //           if (response.data.success.status == 200) {
      //             let createdOn = response.data.success.transaction.created_at;
      //             let postData = { user_id: user_id };
      //             axios
      //               .get(
      //                 "https://sandbox.caribpayintl.com/api/get-default-wallet-balance",
      //                 {
      //                   params: postData,
      //                   headers: { Authorization: this.state.token },
      //                 }
      //               )
      //               .then((response) => {
      //                 if (response.data.success.status == 200) {
      //                   let balance =
      //                     response.data.success.defaultWalletBalance;
      //                   let available_bal = parseFloat(balance).toFixed(2);
      //                   this.props.navigation.navigate("SuccessFull", {
      //                     amount: this.state.amount,
      //                     remarks: this.state.note,
      //                     updated_balance: available_bal,
      //                     createdOn: createdOn,
      //                     payment_mode: "PayPal",
      //                     user_id: user_id,
      //                     token: this.state.token,
      //                   });
      //                   this.setState({ visible: false });
      //                 }
      //               })
      //               .catch((error) => {
      //                 console.log("error updated bal---", error);
      //               });
      //           }
      //         })
      //         .catch((err) => {
      //           console.log("error in paypal payment store ", err);
      //         });
      //     } else {
      //       this.setState({ visible: false });
      //       alert("Payment Request Failed!");
      //     }
      //   })
      //   .catch((error) => {
      //     this.setState({ visible: false });
      //     console.log("error in paymentrequest", error);
      //   });
    } else {
      let user_id = await AsyncStorage.getItem("id");
      this.props.navigation.navigate("AddMoneyStripe", {
        amount: this.state.amount,
        token: this.state.token,
        user_id: user_id,
        fromTopup: false,
        currency_id: this.state.currency_id,
      });
    }
  };

  // onPayPalBtnClick = () => {
  //     console.log("cdwcdsjkcbjdsk")
  // }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
        <View
          style={{
            backgroundColor: colors.theme_caribpay,
            height: metrics.newView.upperview,
          }}
        >
          <View style={{ margin: metrics.dimen_20 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
            >
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                  }}
                  source={require("../Images/leftarrow.png")}
                />
              </View>
            </TouchableOpacity>
            {/* <Image style={styles.imageStyle} source={require('../Images/logo.png')}></Image> */}
            <Text
              style={{
                fontSize: metrics.text_heading,
                color: colors.white,
                fontWeight: "bold",
              }}
            >
              Add Fund
            </Text>
          </View>
        </View>

        <View style={styles.curveview}>
          <View style={{ flex: 1 }}>
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                height:
                  this.state.paymentMethodId == 3
                    ? metrics.dimen_170
                    : metrics.dimen_100,
                borderRadius: metrics.dimen_8,
                marginTop: metrics.dimen_40,
                borderColor: colors.carib_light_grey,
                borderWidth: 0.4,
              }}
            >
              {this.state.paymentMethodId == 3 && (
                <Text
                  style={{
                    fontSize: metrics.text_16,
                    color: colors.theme_caribpay,
                    textAlign: "center",
                    marginTop: metrics.dimen_35,
                    fontFamily:metrics.quicksand_semibold
                  }}
                >
                  You are about to deposit money via
                </Text>
              )}
              {this.state.paymentMethodId == 2 && (
                <Text
                  style={{
                    fontSize: metrics.text_16,
                    color: colors.theme_caribpay,
                    textAlign: "center",
                    marginTop: metrics.dimen_35,
                    fontFamily:metrics.quicksand_semibold

                  }}
                >
                  {"You are about to Deposit money via \n Debit/Credit Card"}
                </Text>
              )}
              {this.state.paymentMethodId == 3 && (
                <Image
                  style={{
                    height: metrics.dimen_80,
                    width: metrics.dimen_160,
                    alignSelf: "center",
                    resizeMode: "contain",
                  }}
                  source={require("../Images/paypal.png")}
                />
              )}
              {this.state.paymentMethodId == 3 && (
                <View
                  style={{
                    width: "80%",
                    height: 50,
                    alignSelf: "center",
                    marginBottom: 10,
                  }}
                >
                  <View
                    style={{
                      margin: metrics.dimen_7,
                      alignSelf: "center",
                      flexDirection: "row",
                    }}
                  >
                    <Image
                      style={{
                        height: metrics.dimen_20,
                        width: metrics.dimen_20,
                        resizeMode: "contain",
                        alignSelf: "center",
                      }}
                      source={require("../Images/addnote.png")}
                    />
                    <Input
                      placeholder={"Add Note"}
                      containerStyle={{ alignSelf: "center", height: 30 }}
                      value={this.state.note}
                      multiline={false}
                      inputContainerStyle={{ borderBottomWidth: 0 }}
                      inputStyle={{ fontSize: metrics.text_description }}
                      onChangeText={(text) => this.setState({ note: text })}
                    />
                  </View>
                </View>
              )}
              <View
                style={{
                  height: 40,
                  width: metrics.dimen_150,
                  backgroundColor: colors.theme_caribpay,
                  position: "absolute",
                  top: -20,
                  alignSelf: "center",
                  justifyContent: "center",
                  borderRadius: metrics.dimen_8,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_16,
                    color: colors.white,
                    fontFamily:metrics.quicksand_bold,
                    textAlign: "center",
                  }}
                >
                  {this.state.currency_id == 1
                    ? "$ " + this.state.amount
                    : "EC$ " + this.state.amount}
                </Text>
              </View>
            </View>

            <View style={{ flexDirection: "column", margin: 5 }}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: metrics.dimen_15,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    color: colors.app_gray,
                    fontFamily:metrics.quicksand_semibold

                  }}
                >
                  Deposit Amount
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    color: colors.black,
                    textAlign: "center",
                    fontFamily:metrics.quicksand_semibold

                  }}
                >
                  {this.state.currency_id == 1
                    ? "$ " + this.state.amount
                    : "EC$ " + this.state.amount}
                </Text>
              </View>
              <View
                style={{
                  borderBottomColor: "#D3D3D3",
                  borderBottomWidth: 1,
                  width: "90%",
                  alignSelf: "center",
                }}
              />

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: metrics.dimen_15,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    color: colors.app_gray,
                    fontFamily:metrics.quicksand_semibold

                  }}
                >
                  Fee
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    color: colors.black,
                    textAlign: "center",
                    fontFamily:metrics.quicksand_semibold

                  }}
                >
                  {this.state.currency_id == 1
                    ? "$ " + this.state.txn_fee
                    : "EC$ " + this.state.txn_fee}
                </Text>
              </View>
              <View
                style={{
                  borderBottomColor: "#D3D3D3",
                  borderBottomWidth: 1,
                  width: "90%",
                  alignSelf: "center",
                }}
              />

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: metrics.dimen_15,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    color: colors.app_gray,
                    fontFamily:metrics.quicksand_semibold

                  }}
                >
                  Total
                </Text>
                {this.state.total_amount_pay != NaN && (
                  <Text
                    style={{
                      fontSize: metrics.text_header,
                      color: colors.carib_pay_blue,
                      textAlign: "center",
                    fontFamily:metrics.quicksand_semibold

                    }}
                  >
                    {this.state.currency_id == 1
                      ? "$ " +
                        parseFloat(this.state.total_amount_pay).toFixed(2)
                      : "EC$ " +
                        parseFloat(this.state.total_amount_pay).toFixed(2)}
                  </Text>
                )}
              </View>
              <View
                style={{
                  borderBottomColor: "#D3D3D3",
                  borderBottomWidth: 1,
                  width: "90%",
                  alignSelf: "center",
                }}
              />
            </View>

            <Modal
              style={{ alignSelf: "center", width: "80%" }}
              isVisible={this.state.visible}
            >
              <View
                style={{
                  backgroundColor: "white",
                  borderRadius: metrics.dimen_10,
                }}
              >
                <View
                  style={{ flexDirection: "column", margin: metrics.dimen_20 }}
                >
                  <Spinner
                    style={{ alignSelf: "center" }}
                    isVisible={this.state.visible}
                    size={70}
                    type={"ThreeBounce"}
                    color={colors.black}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_header,
                      color: colors.black,
                      textAlign: "center",
                      marginTop: 20,
                    }}
                  >
                    We are processing now, Please wait a while!{" "}
                  </Text>
                </View>
              </View>
            </Modal>

            <Toast
              ref="toast"
              style={{ backgroundColor: "black" }}
              position="center"
              positionValue={200}
              fadeInDuration={200}
              fadeOutDuration={1000}
              opacity={0.8}
              textStyle={{ color: "white" }}
            />

            <Modal
              style={{ alignSelf: "center", width: "80%" }}
              isVisible={this.state.isVisibleAlert}
            >
              <View
                style={{
                  backgroundColor: "white",
                  borderRadius: metrics.dimen_7,
                }}
              >
                <View
                  style={{
                    flexDirection: "column",
                    height: metrics.dimen_60,
                    backgroundColor: colors.theme_caribpay,
                    justifyContent: "center",
                  }}
                >
                  <Text
                    style={{
                      fontSize: metrics.text_heading,
                      color: colors.white,
                      padding: 20,
                      fontWeight: "bold",
                      textAlign: "center",
                    }}
                  >
                    Add Funds
                  </Text>
                </View>
                <View
                  style={{ flexDirection: "column", margin: metrics.dimen_10 }}
                >
                  <Image
                    style={{
                      height: metrics.dimen_80,
                      width: metrics.dimen_100,
                      resizeMode: "contain",
                      alignSelf: "center",
                      marginTop: 20,
                    }}
                    source={require("../Images/warning.png")}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      color: colors.heading_black_text,
                      textAlign: "center",
                      marginTop: 20,
                    }}
                  >
                    Please agree to our terms and condition.
                  </Text>
                </View>

                <View
                  style={{
                    margin: metrics.dimen_15,
                    flexDirection: "row",
                    justifyContent: "center",
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.setState({ isVisibleAlert: false })}
                  >
                    <View
                      style={{
                        backgroundColor: colors.theme_caribpay,
                        borderRadius: metrics.dimen_7,
                        height: metrics.dimen_40,
                        width: 150,
                        marginLeft: 10,
                        justifyContent: "center",
                      }}
                    >
                      <Text
                        style={{
                          paddingHorizontal: metrics.dimen_10,
                          fontSize: metrics.text_header,
                          color: colors.white,
                          textAlign: "center",
                          fontWeight: "900",
                        }}
                      >
                        {"OK "}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
          </View>
        </View>

        <View
          style={{
            ...styles.bottomview,
            backgroundColor: colors.transparent,
            bottom: 55,
          }}
        >
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <CheckBox
              checkedIcon={
                <Image
                  source={require("../Images/tick.png")}
                  style={{
                    height: metrics.dimen_20,
                    width: metrics.dimen_20,
                    borderRadius: 3,
                  }}
                />
              }
              uncheckedIcon={
                <Image
                  source={require("../Images/unchecked.png")}
                  style={{ height: metrics.dimen_20, width: metrics.dimen_20 }}
                />
              }
              containerStyle={{
                backgroundColor: "transparent",
                borderWidth: 0,
                marginLeft: -10,
              }}
              checked={this.state.checked}
              onPress={() => this.setState({ checked: !this.state.checked })}
            />
            <Text
              style={{
                color: colors.heading_black_text,
                marginLeft: -10,
                fontSize: metrics.text_description,
                fontFamily:metrics.quicksand_semibold

              }}
              onPress={() => {
                console.warn("clicked");
              }}
            >
              I Agree to Terms {"\u0026"} Conditions
            </Text>
          </View>
        </View>

        {this.state.paymentMethodId == 3 ? (
          <View style={styles.bottomview}>
            <TouchableOpacity
              onPress={() =>
                this.state.checked
                  ? this.onPayPalBtnClick()
                  : this.setState({ isVisibleAlert: true })
              }
            >
              <Image
                style={{
                  height: metrics.dimen_30,
                  width: metrics.dimen_120,
                  alignSelf: "center",
                }}
                source={require("../Images/paypal1.png")}
              />
            </TouchableOpacity>
          </View>
        ) : (
          <View
            style={{
              ...styles.bottomview,
              backgroundColor: colors.theme_caribpay,
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this.state.checked
                  ? this.onPayPalBtnClick()
                  : this.setState({ isVisibleAlert: true })
              }
            >
              <Text
                style={{
                  fontSize: 15,
                  color: "white",
                  fontFamily:metrics.quicksand_bold,
                  textAlign: "center",
                  alignSelf: "center",
                  marginLeft: 10,
                }}
              >
                {"Proceed "}
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  curveview: {
    height: metrics.newView.curveview,
    position: "absolute",
    top: metrics.newView.curvetop - metrics.dimen_60,
    width: "100%",
    backgroundColor: colors.white,
    borderRadius: 40,
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 25,
  },
  bottomview2: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_5,
  },
});
