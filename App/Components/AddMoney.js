import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  Dimensions,
  BackHandler,
  TouchableOpacity,
  Image,
  FlatList,
  TextInput,
} from "react-native";
import metrics from "../Themes/Metrics.js";
import colors from "../Themes/Colors.js";
import ProgressDialog from "../ProgressDialog.js";
import { Input } from "react-native-elements";
import RadioForm from "react-native-simple-radio-button";
const axios = require("axios");
import Modal from "react-native-modal";
import Toast, { DURATION } from "react-native-easy-toast";
// import { AmountInput } from "react-native-amount-input";
import CurrencyInput from "react-currency-masked-input";

let Facilities_data = [
  {
    id: 111,
    amount: "20",
  },
  {
    id: 222,
    amount: "50",
  },
  {
    id: 333,
    amount: "100",
  },
  {
    id: 444,
    amount: "150",
  },
  {
    id: 555,
    amount: "200",
  },
  {
    id: 666,
    amount: "300",
  },
];

export default class AddMoney extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      available_Balance: this.props.navigation.state.params.available_Balance,
      token: this.props.navigation.state.params.token,
      amount: this.props.navigation.state.params.amount,
      isVisible: false,
      value: "Select One",
      paymentMethods: [],
      depositCurrency: [],
      pay_method: "",
      isPaymentVisible: false,
      amount: "0.00",
      currency_id: 0,
      fromOnchage: false,
      maxLimit: "",
      minLimit: "",
      limitsAvailable: false,
      symbol: this.props.navigation.state.params.symbol,
      amountsuggestions: [],
    };
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    console.log("token", this.props.navigation.state.params.amount);
    this.getDepositCurrencies();

    axios
      .get("https://sandbox.caribpayintl.com/api/get-payment-method-list", {
        headers: { Authorization: this.state.token },
      })
      .then((response) => {
        if (response.data.status == 200) {
          let respponse = response.data.success.data;
          console.log(
            "response payemnt methods --- > ",
            response.data.success.data
          );
          let payment_Methods = [];
          let i;
          for (i = 0; i < respponse.length; i++) {
            let id = respponse[i].id;
            let name = respponse[i].name;
            var add_methods = { value: name, label: name };
            payment_Methods.push(add_methods);
          }
          // console.log(payment_Methods)

          let payment_Methods_data = payment_Methods.map((item, index) => {
            item.id = ++index;
            return { ...item };
          });
          this.setState({
            paymentMethods: payment_Methods,
          });

          console.log(
            "response status  paymentmethods ---> ",
            this.state.paymentMethods
          );
        }
      })
      .catch((error) => {
        console.log("roshan", error);
      });

    if (this.state.symbol === "USD") {
      this.setState({
        value: "USD",
      });
    } else {
      this.setState({
        value: "XCD",
      });
    }
    let data = Facilities_data.map((item, index) => {
      item.id = ++index;
      return { ...item };
    });
    this.setState({
      amountsuggestions: data,
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  getDepositCurrencies = () => {
    axios
      .get("https://sandbox.caribpayintl.com/api/get-deposit-currency-list", {
        headers: { Authorization: this.state.token },
      })
      .then((response) => {
        console.log("response status ---> ", response.data.success.currencies);

        if (response.data.status == 200) {
          let respponse = response.data.success.currencies;
          let deposit_crncy = [];
          let i;
          for (i = 0; i < respponse.length; i++) {
            let id = respponse[i].id;
            let name = respponse[i].code;
            var add_crncy = { value: name, label: name };
            deposit_crncy.push(add_crncy);
          }
          this.setState({
            depositCurrency: deposit_crncy,
          });
          // console.log("value coming --- ", this.state.depositCurrency)
        }
        // console.log(response.data)
      })
      .catch((error) => {
        console.log("roshan", error);
      });
  };

  onProceedToAddMoney = () => {
    if (this.state.value == "Select One") {
      this.refs.toast.show("Please Select Currency");
    } else if (this.state.pay_method == "") {
      this.refs.toast.show("Please Select Payment Method.");
    } else if (this.state.pay_method == "Bank") {
      this.setState({
        isComingSoonBank: true,
      });
    } else if (this.state.amount == "") {
      this.refs.toast.show("Please enter amount to add in your wallet");
    } else if (parseFloat(this.state.amount) <= 0.0) {
      this.refs.toast.show("Amount can't be empty");
    } else if (
      parseFloat(this.state.maxLimit) < parseFloat(this.state.amount)
    ) {
      this.setState({
        limitExceed:true
      })
    } else {
      this.props.navigation.navigate("AddFunds", {
        amount: this.state.fromOnchage
          ? this.state.amount
          : this.props.navigation.state.params.amount,
        token: this.state.token,
        paymentMethodId: this.state.pay_method == "Paypal" ? 3 : 2,
        currency_id: this.state.value == "USD" ? 1 : 5,
      });
    }
  };

  knowLimits = () => {
    let postData = {
      paymentMethodId: this.state.pay_method == "Stripe" ? 2 : 3,
      currencyId: this.state.symbol == "USD" ? 1 : 5,
    };
    axios
      .get(
        "https://sandbox.caribpayintl.com/api/get-fees-list-by-payment-method",
        { params: postData, headers: { Authorization: this.state.token } }
      )
      .then((response) => {
        let maxlimit = response.data.success.feesdetails[0].max_limit;
        let minlimit = response.data.success.feesdetails[0].min_limit;
        let parseFloatMaxlimit;
        let parseFloatMinlimit;
        if (maxlimit != null) {
          parseFloatMaxlimit = parseFloat(maxlimit).toFixed(2);
        }
        if (minlimit != null) {
          parseFloatMinlimit = parseFloat(minlimit).toFixed(2);
        }
        this.setState({
          maxLimit: maxlimit == null ? "No Limit" : parseFloatMaxlimit,
          minLimit: minlimit == null ? "No Limit" : parseFloatMinlimit,
          limitsAvailable: true,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  onChangeTextFormatted = (text) => {
    let numberFormatted = text.replace(/\./g, "").replace(/^0+/, "");

    if (numberFormatted?.length > 2) {
      numberFormatted = [
        numberFormatted.slice(0, numberFormatted.length - 2),
        ".",
        numberFormatted.slice(numberFormatted.length - 2),
      ].join("");
    } else if (numberFormatted?.length > 1) {
      numberFormatted = "0." + numberFormatted;
    } else if (numberFormatted?.length > 0) {
      numberFormatted = "0.0" + numberFormatted;
    }

    this.setState({ amount: numberFormatted, fromOnchage: true });
  };

  handleSelection = (item, id) => {
    var selectedid = this.state.selectedID;
    if (selectedid === id) {
      this.setState({
        selectedID: null,
        amount: item.amount + ".00",
        fromOnchage: true,
      });
    } else {
      this.setState({
        selectedID: id,
        amount: item.amount + ".00",
        fromOnchage: true,
      });
    }
  };

  handleSelectionPayments = (item, id) => {
    var selectedid = this.state.selectedIDS;
    if (selectedid === id) {
      this.setState(
        {
          selectedIDS: null,
          pay_method: item.value,
          isPaymentVisible: false,
        },
        () => this.knowLimits()
      );
    } else {
      this.setState(
        {
          selectedIDS: id,
          pay_method: item.value,
          isPaymentVisible: false,
        },
        () => this.knowLimits()
      );
    }
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ProgressDialog visible={this.state.visible} />
        <View
          style={{
            backgroundColor: colors.theme_caribpay,
            height: metrics.newView.upperview,
          }}
        >
          <View style={{ margin: metrics.dimen_20 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
            >
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                  }}
                  source={require("../Images/leftarrow.png")}
                />
              </View>
            </TouchableOpacity>
            {/* <Image style={styles.imageStyle} source={require('../Images/logo.png')}></Image> */}
            <Text
              style={{
                fontSize: metrics.text_heading,
                color: colors.white,
                fontFamily: metrics.quicksand_bold,
              }}
            >
              Add Fund
            </Text>
          </View>
        </View>

        <View style={styles.curveview}>
          <View style={{ flex: 1 }}>
            <View
              style={{
                margin: metrics.dimen_25,
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <View style={{ flexDirection: "column" }}>
                <Text
                  style={{
                    color: colors.theme_caribpay,
                    fontFamily: metrics.quicksand_semibold,
                    fontSize: metrics.text_normal,
                    marginTop: metrics.dimen_10,
                  }}
                >
                  {"Available Balance " +
                    this.state.symbol +
                    " " +
                    this.state.available_Balance}
                </Text>
                <Text
                  style={{
                    color: colors.app_gray,
                    fontFamily: metrics.quicksand_semibold,
                    fontSize: metrics.text_normal,
                    // marginTop: metrics.dimen_10,
                  }}
                >
                  {"Add money & make one click payment"}
                </Text>
              </View>

              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  borderRadius: metrics.dimen_40 / 2,
                  backgroundColor: "#f2f2f2",
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_22,
                    width: metrics.dimen_22,
                    alignSelf: "center",
                  }}
                  source={require("../Images/balance_wallet.png")}
                />
              </View>
            </View>
            <TouchableOpacity
              onPress={() => this.setState({ isVisible: true })}
            >
              <View style={styles.boxContainer}>
                <Text
                  style={{
                    color: colors.black,
                    fontSize: metrics.text_header,
                    padding: 10,
                    fontFamily: metrics.quicksand_semibold,
                  }}
                >
                  Select Currency
                </Text>
                <View style={{ flexDirection: "row" }}>
                  <Text
                    style={{
                      color: colors.app_gray,
                      fontSize: metrics.text_description,
                      padding: 10,
                      alignSelf: "center",
                    }}
                  >
                    {this.state.value}
                  </Text>
                  <Image
                    style={{
                      height: metrics.dimen_18,
                      width: metrics.dimen_18,
                      alignSelf: "center",
                      marginRight: 10,
                      tintColor: colors.app_gray,
                    }}
                    source={require("../Images/dropdown.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.setState({ isPaymentVisible: true })}
            >
              <View
                style={{ ...styles.boxContainer, marginTop: metrics.dimen_7 }}
              >
                <Text
                  style={{
                    color: colors.black,
                    fontSize: metrics.text_header,
                    padding: 10,
                    fontFamily: metrics.quicksand_semibold,
                  }}
                >
                  Select Payment Method
                </Text>
                <View style={{ flexDirection: "row" }}>
                  <Text
                    style={{
                      color: colors.app_gray,
                      fontSize: metrics.text_description,
                      padding: 10,
                      alignSelf: "center",
                    }}
                  >
                    {this.state.pay_method == "Stripe"
                      ? " Visa/Master Card "
                      : this.state.pay_method}
                  </Text>
                  <Image
                    style={{
                      height: metrics.dimen_18,
                      width: metrics.dimen_18,
                      alignSelf: "center",
                      marginRight: 10,
                      tintColor: colors.app_gray,
                    }}
                    source={require("../Images/dropdown.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            <Text
              style={{
                fontSize: metrics.text_normal,
                color: colors.app_light_gray,
                fontFamily: metrics.quicksand_semibold,
                marginHorizontal: metrics.dimen_20,
                marginTop: 10,
              }}
            >
              Amount
            </Text>

            <View
              style={{ ...styles.boxContainer, marginTop: metrics.dimen_7 }}
            >
              <Text
                style={{
                  color: colors.black,
                  fontSize: metrics.text_header,
                  padding: 10,
                  fontFamily: metrics.quicksand_semibold,
                }}
              />

              {/* <View style={{ flex: 1 }}>
                <Input
                  placeholder={"$0.00"}
                  containerStyle={{ alignSelf: "center" }}
                  keyboardType="number-pad"
                  value={
                    this.state.fromOnchage
                      ? this.state.amount
                      : this.props.navigation.state.params.amount
                  }
                  multiline={false}
                  maxLength={7}
                  inputContainerStyle={{ borderBottomWidth: 0 }}
                  inputStyle={{
                    fontSize: this.state.fromOnchage
                      ? metrics.text_large
                      : metrics.text_normal,
                    textAlign: "right",
                    color: this.state.fromOnchage ? colors.app_blueColor : null,
                    fontWeight: this.state.fromOnchage ? "bold" : "200",
                  }}
                  onChangeText={(text) =>
                    this.setState({
                      fromOnchage: true,
                      amount: text,
                    })
                  }
                />
              </View> */}

              <View style={{ flex: 1 }}>
                <TextInput
                  placeholder={"$ 10"}
                  style={{ flex: 1, textAlign: "right", marginRight: 7 }}
                  keyboardType="number-pad"
                  value={this.state.amount}
                  maxLength={7}
                  multiline={false}
                  inputContainerStyle={{ borderBottomWidth: 0 }}
                  inputStyle={{
                    fontSize: this.state.fromOnchage
                      ? metrics.text_large
                      : metrics.text_normal,
                    textAlign: "right",
                    color: this.state.fromOnchage ? colors.app_blueColor : null,
                    fontWeight: this.state.fromOnchage ? "bold" : "200",
                  }}
                  onChangeText={(text) => this.onChangeTextFormatted(text)}
                />
              </View>

              {/* <View>
                <AmountInput
                  currency="USD"
                  defaultQuantity={0.00}
                  // onChangeQuantity={handleChangeQuantity}
                />
              </View> */}

              {/* <CurrencyInput 
    /> */}
            </View>

            {/* {this.state.limitsAvailable && (
              <View
                style={{
                  margin: metrics.dimen_25,
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <Text
                  style={{
                    color: colors.app_gray,
                    fontWeight: "500",
                    fontSize: metrics.text_description,
                  }}
                >
                  {"Min Limit : " + this.state.minLimit}
                </Text>
                <Text
                  style={{
                    color: colors.app_gray,
                    fontWeight: "500",
                    fontSize: metrics.text_description,
                  }}
                >
                  {"Max Limit : " + this.state.maxLimit}
                </Text>
              </View>
            )} */}

            <View
              style={{
                justifyContent: "center",
                alignSelf: "center",
                backgroundColor: colors.white,
                width: "90%",
                borderRadius: metrics.dimen_15,
                marginTop: metrics.dimen_7,
              }}
            >
              <Text
                style={{
                  fontSize: metrics.text_normal,
                  color: colors.app_light_gray,
                  fontFamily: metrics.quicksand_semibold,
                }}
              >
                Recommended
              </Text>

              <FlatList
                style={{ marginTop: 7 }}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={this.state.amountsuggestions}
                extraData={this.state.selectedID}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <View
                    style={{
                      backgroundColor:
                        item.id == this.state.selectedID
                          ? colors.theme_caribpay
                          : colors.light_grey_backgroud,
                      margin: metrics.dimen_5,
                      justifyContent: "center",
                      width: 70,
                      height: metrics.dimen_35,
                      borderRadius: metrics.dimen_20,
                      borderColor: colors.app_gray,
                      borderWidth: 0.5,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => this.handleSelection(item, item.id)}
                    >
                      <Text
                        style={{
                          fontSize: metrics.text_normal,
                          color:
                            item.id == this.state.selectedID
                              ? "white"
                              : colors.heading_black_text,
                          textAlign: "center",
                          alignSelf: "center",

                          fontFamily: metrics.quicksand_semibold,
                        }}
                      >
                        {"$ " + item.amount}
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              />
            </View>
          </View>

          <View>
            <Modal
              style={{ borderRadius: metrics.dimen_10 }}
              isVisible={this.state.isVisible}
            >
              <View
                style={{
                  width: "90%",
                  alignSelf: "center",
                  backgroundColor: colors.white,
                  borderRadius: metrics.dimen_10,
                  flexDirection: "column",
                  height: 230,
                }}
              >
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isVisible: false,
                    })
                  }
                >
                  <Image
                    style={{
                      height: metrics.dimen_16,
                      width: metrics.dimen_16,
                      resizeMode: "contain",
                      margin: 15,
                    }}
                    source={require("../Images/cross-sign.png")}
                  />
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: metrics.text_heading,
                    color: colors.theme_caribpay,
                    fontFamily: metrics.quicksand_semibold,
                    textAlign: "center",
                  }}
                >
                  Select Currency
                </Text>
                <View style={{ flexDirection: "column" }}>
                  <View
                    style={{
                      marginLeft: metrics.dimen_20,
                      marginTop: metrics.dimen_20,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          isVisible: false,
                          value: "USD",
                        })
                      }
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: metrics.quicksand_semibold,
                            fontSize: metrics.text_header,
                            margin: 5,
                          }}
                        >
                          USD
                        </Text>

                        <Image
                          style={{
                            height: metrics.dimen_16,
                            width: metrics.dimen_16,
                            resizeMode: "contain",
                            margin: 5,
                            marginRight: 20,
                          }}
                          source={require("../Images/rightarrow.png")}
                        />
                      </View>
                    </TouchableOpacity>

                    <View style={styles.horizontalLine} />

                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          isVisible: false,
                          value: "XCD",
                        })
                      }
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: metrics.quicksand_semibold,
                            fontSize: metrics.text_header,
                            margin: 5,
                          }}
                        >
                          XCD
                        </Text>
                        <Image
                          style={{
                            height: metrics.dimen_16,
                            width: metrics.dimen_16,
                            resizeMode: "contain",
                            margin: 5,
                            marginRight: 20,
                          }}
                          source={require("../Images/rightarrow.png")}
                        />
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
          </View>

          <Toast
            ref="toast"
            style={{ backgroundColor: "black" }}
            position="center"
            positionValue={200}
            fadeInDuration={200}
            fadeOutDuration={1000}
            opacity={0.8}
            textStyle={{ color: "white" }}
          />

          <View>
            <Modal
              style={{ borderRadius: metrics.dimen_10 }}
              isVisible={this.state.isPaymentVisible}
            >
              <View
                style={{
                  width: "105%",
                  alignSelf: "center",
                  backgroundColor: colors.white,
                  borderRadius: metrics.dimen_10,
                  flexDirection: "column",
                }}
              >
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isPaymentVisible: false,
                    })
                  }
                >
                  <Image
                    source={require("../Images/cross-sign.png")}
                    style={{
                      height: metrics.dimen_18,
                      width: metrics.dimen_18,
                      resizeMode: "contain",
                      marginLeft: 10,
                      marginTop: 10,
                    }}
                  />
                </TouchableOpacity>

                <Text
                  style={{
                    fontSize: metrics.text_heading,
                    color: colors.theme_caribpay,
                    margin: 15,
                    fontFamily: metrics.quicksand_semibold,
                    textAlign: "center",
                  }}
                >
                  Select Payment Method
                </Text>

                <View style={{ flexDirection: "column" }}>
                  <View
                    style={{
                      marginTop: 10,
                      marginLeft: metrics.dimen_15,
                      height: Dimensions.get("screen").height / 1.5,
                    }}
                  >
                    <FlatList
                      showsVerticalScrollIndicator={false}
                      data={this.state.paymentMethods}
                      extraData={this.state.selectedIDS}
                      keyExtractor={(item, index) => index.toString()}
                      renderItem={({ item }) => (
                        <TouchableOpacity
                          onPress={() =>
                            this.handleSelectionPayments(item, item.id)
                          }
                        >
                          <View
                            style={{
                              margin: 5,
                              flexDirection: "row",
                              justifyContent: "space-between",
                              height: 50,
                              borderBottomColor:
                                item.id == this.state.selectedIDS
                                  ? colors.app_black_text
                                  : colors.light_grey_backgroud,
                              width: "90%",
                              marginTop: 5,
                              borderBottomWidth: 1,
                            }}
                          >
                            {item.value === "Mts" && (
                              <Image
                                source={require("../Images/MTS-01.png")}
                                style={{
                                  height: metrics.dimen_30,
                                  width: metrics.dimen_30,
                                  resizeMode: "contain",
                                  alignSelf: "center",
                                  marginLeft: 5,
                                }}
                              />
                            )}
                            {item.value === "Stripe" && (
                              <Image
                                source={require("../Images/credit-card.png")}
                                style={{
                                  height: metrics.dimen_30,
                                  width: metrics.dimen_30,
                                  resizeMode: "contain",
                                  alignSelf: "center",
                                  marginLeft: 5,
                                }}
                              />
                            )}
                            {item.value === "Paypal" && (
                              <Image
                                source={require("../Images/paypallogo.png")}
                                style={{
                                  height: metrics.dimen_30,
                                  width: metrics.dimen_30,
                                  resizeMode: "contain",
                                  alignSelf: "center",
                                  marginLeft: 5,
                                }}
                              />
                            )}
                            {item.value === "2Checkout" && (
                              <Image
                                source={require("../Images/2checkout.png")}
                                style={{
                                  height: metrics.dimen_30,
                                  width: metrics.dimen_30,
                                  resizeMode: "contain",
                                  alignSelf: "center",
                                  marginLeft: 5,
                                }}
                              />
                            )}

                            {item.value === "PayUmoney" && (
                              <Image
                                source={require("../Images/payment.png")}
                                style={{
                                  height: metrics.dimen_30,
                                  width: metrics.dimen_30,
                                  resizeMode: "contain",
                                  alignSelf: "center",
                                  marginLeft: 5,
                                }}
                              />
                            )}

                            {item.value === "Bank" && (
                              <Image
                                source={require("../Images/bank.png")}
                                style={{
                                  height: metrics.dimen_30,
                                  width: metrics.dimen_30,
                                  resizeMode: "contain",
                                  alignSelf: "center",
                                  marginLeft: 5,
                                }}
                              />
                            )}

                            {item.value === "Coinpayments" && (
                              <Image
                                source={require("../Images/coinpay.png")}
                                style={{
                                  height: metrics.dimen_30,
                                  width: metrics.dimen_30,
                                  resizeMode: "contain",
                                  alignSelf: "center",
                                  marginLeft: 5,
                                  tintColor: colors.theme_caribpay,
                                }}
                              />
                            )}
                            {item.value === "BlockIo" && (
                              <Image
                                source={require("../Images/blockio.png")}
                                style={{
                                  height: metrics.dimen_30,
                                  width: metrics.dimen_30,
                                  resizeMode: "contain",
                                  alignSelf: "center",
                                  marginLeft: 5,
                                }}
                              />
                            )}
                            {/* {item.value == "Stripe" &&
                                                            
                                                        }  {item.value == "Paypal" &&
                                                            <Image source={require("../Images/paypallogo.png")}
                                                                style={{ height: metrics.dimen_24, width: metrics.dimen_24, resizeMode: 'contain', alignSelf: "center", marginLeft: 5 }}></Image>
                                                        }
                                                         */}
                            <Text
                              style={{
                                fontSize: 16,
                                paddingLeft: 10,
                                alignSelf: "center",
                                marginTop: 2,
                                color: colors.black,
                                fontFamily: metrics.quicksand_semibold,
                              }}
                            >
                              {item.value == "Stripe"
                                ? "Visa/Master Card"
                                : item.value}
                            </Text>

                            <Image
                              source={require("../Images/rightarrow.png")}
                              style={{
                                height: metrics.dimen_18,
                                width: metrics.dimen_18,
                                resizeMode: "contain",
                                alignSelf: "center",
                                marginRight: 5,
                              }}
                            />
                          </View>
                        </TouchableOpacity>
                      )}
                    />
                  </View>
                </View>
              </View>
            </Modal>
          </View>

          <View>
            <Modal
              style={{ borderRadius: metrics.dimen_10 }}
              isVisible={this.state.isComingSoon}
            >
              <View
                style={{
                  width: "90%",
                  alignSelf: "center",
                  backgroundColor: colors.white,
                  borderRadius: metrics.dimen_10,
                  margin: metrics.dimen_20,
                  flexDirection: "column",
                  height: 200,
                  justifyContent: "center",
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Image
                      style={{
                        height: 150,
                        width: 150,
                        resizeMode: "cover",
                        alignSelf: "center",
                        marginTop: -30,
                      }}
                      source={require("../Images/soon.png")}
                    />
                    <Text
                      style={{
                        fontSize: metrics.text_heading,
                        color: colors.black,
                        fontWeight: "bold",
                        textAlign: "center",
                        marginTop: -20,
                      }}
                    >
                      {"Add Fund Via Paypal"}
                    </Text>
                  </View>
                  <View style={styles.bottomview2}>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          isComingSoon: false,
                        })
                      }
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          color: "white",
                          fontWeight: "bold",
                          textAlign: "center",
                          alignSelf: "center",
                          marginLeft: 10,
                        }}
                      >
                        OK
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
          </View>

          <View>
            <Modal
              style={{ borderRadius: metrics.dimen_10 }}
              isVisible={this.state.limitExceed}
            >
              <View
                style={{
                  width: "90%",
                  alignSelf: "center",
                  backgroundColor: colors.white,
                  borderRadius: metrics.dimen_10,
                  margin: metrics.dimen_20,
                  flexDirection: "column",
                  height: 250,
                  justifyContent: "center",
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          limitExceed: false,
                        })
                      }
                    >
                      <Image
                        style={{
                          height: metrics.dimen_16,
                          width: metrics.dimen_16,
                          margin: 15,
                        }}
                        source={require("../Images/cross-sign.png")}
                      />
                    </TouchableOpacity>

                    <Image
                      style={{
                        height: 70,
                        width: 70,
                        resizeMode: "cover",
                        alignSelf: "center",
                      }}
                      source={require("../Images/danger.png")}
                    />
                    <Text
                      style={{
                        fontSize: 16,
                        color: colors.black,
                        fontFamily: metrics.quicksand_bold,
                        textAlign: "center",
                        margin: 20,
                      }}
                    >
                      {"Try with lesser amount!\n Maximum Limit for this payment method is " +
                        parseFloat(this.state.maxLimit).toFixed(2) +"!"}
                    </Text>
                  </View>
                </View>
              </View>
            </Modal>
          </View>

          <View>
            <Modal
              style={{ borderRadius: metrics.dimen_10 }}
              isVisible={this.state.isComingSoonBank}
            >
              <View
                style={{
                  width: "90%",
                  alignSelf: "center",
                  backgroundColor: colors.white,
                  borderRadius: metrics.dimen_10,
                  margin: metrics.dimen_20,
                  flexDirection: "column",
                  height: 280,
                  justifyContent: "center",
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          isComingSoonBank: false,
                        })
                      }
                    >
                      <Image
                        style={{
                          height: metrics.dimen_16,
                          width: metrics.dimen_16,
                          margin: 15,
                        }}
                        source={require("../Images/cross-sign.png")}
                      />
                    </TouchableOpacity>

                    <Image
                      style={{
                        height: 70,
                        width: 150,
                        resizeMode: "cover",
                        alignSelf: "center",
                      }}
                      source={require("../Images/soon.png")}
                    />
                    <Text
                      style={{
                        fontSize: 16,
                        color: colors.black,
                        fontFamily: metrics.quicksand_bold,
                        textAlign: "center",
                        marginTop: 20,
                      }}
                    >
                      {
                        "Sorry\n This method is not available at the moment.\nPlease try again later!"
                      }
                    </Text>
                  </View>
                </View>
              </View>
            </Modal>
          </View>
        </View>
        <View style={styles.bottomview}>
          <TouchableOpacity onPress={() => this.onProceedToAddMoney()}>
            <Text
              style={{
                fontSize: 15,
                color: "white",
                fontFamily: metrics.quicksand_bold,
                textAlign: "center",
                alignSelf: "center",
                marginLeft: 10,
              }}
            >
              {"Proceed to Add Fund "}
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: metrics.dimen_70,
    backgroundColor: colors.theme_caribpay,
    flexDirection: "row",
    paddingHorizontal: metrics.dimen_20,
  },
  headerleftImage: {
    height: metrics.dimen_24,
    width: metrics.dimen_24,
    alignSelf: "center",
    resizeMode: "contain",
    tintColor: colors.whitesmoke,
    marginBottom: metrics.dimen_15,
  },
  headertextStyle: {
    fontSize: metrics.text_16,
    fontWeight: "bold",
    color: colors.white,
    marginBottom: metrics.dimen_15,
    fontFamily: metrics.quicksand_bold,
    textAlignVertical: "center",
    paddingHorizontal: metrics.dimen_20,
  },
  absouluteview: {
    position: "absolute",
    top: 51,
    borderTopRightRadius: metrics.dimen_20,
    borderTopLeftRadius: metrics.dimen_20,
    width: "100%",
    height: Dimensions.get("screen").height,
    backgroundColor: colors.white,
  },
  container: { flexDirection: "row", margin: metrics.dimen_20 },
  image_style: {
    height: metrics.dimen_30,
    width: metrics.dimen_30,
    resizeMode: "contain",
  },
  headingg: { fontSize: metrics.text_header, color: colors.black },
  descriiption: {
    fontSize: metrics.text_description,
    color: "#D3D3D3",
    fontWeight: "800",
  },
  arrowstyle: {
    height: metrics.dimen_15,
    width: metrics.dimen_15,
    resizeMode: "contain",
  },
  horizontalLine: {
    borderBottomColor: "#D3D3D3",
    marginBottom: 10,
    borderBottomWidth: 1,
    width: "90%",
    alignSelf: "center",
    marginTop: 10,
    marginLeft: -10,
  },
  boxContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#f2f2f2",
    borderRadius: metrics.dimen_5,
    height: metrics.dimen_50,
    alignSelf: "center",
    width: "90%",
  },
  buttonStyle: {
    flexDirection: "row",
    width: "80%",
    borderRadius: 5,
    justifyContent: "center",
    alignSelf: "center",
    marginBottom: 30,
    height: 40,
    backgroundColor: colors.theme_caribpay,
    shadowColor: "#f2f2f2",
    shadowOpacity: 1,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1,
    },
  },
  imageStyle: {
    height: metrics.dimen_60,
    width: metrics.dimen_60,
    resizeMode: "contain",
    marginTop: 5,
  },
  curveview: {
    height: metrics.newView.curveview,
    position: "absolute",
    top: metrics.newView.curvetop - metrics.dimen_60,
    width: "100%",
    backgroundColor: colors.white,
    borderRadius: 40,
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 25,
  },
  bottomview2: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 20,
  },
});
