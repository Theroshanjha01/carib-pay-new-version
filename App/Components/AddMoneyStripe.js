import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from "react-native";
import metrics from "../Themes/Metrics.js";
import colors from "../Themes/Colors.js";
var Spinner = require("react-native-spinkit");
import Toast, { DURATION } from "react-native-easy-toast";
import Stripe from "react-native-stripe-api";
import Modal from "react-native-modal";
const axios = require("axios");
import { CreditCardInput } from "react-native-credit-card-input";

export default class AddMoneyStripe extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: this.props.navigation.state.params.amount,
      fromCard: true,
      fromBanking: false,
      user_id: this.props.navigation.state.params.user_id,
      token: this.props.navigation.state.params.token,
      currency_id: this.props.navigation.state.params.currency_id,
      cvv: "",
      expiryMonth: "",
      expiryYear: "",
      visible: false,
      cardNumber: "",
      stripeKey: "",
      updated_balance: "",
      isVisible: false,
      isVisiblePayment: false,
      card_details: "",
      isAmexCard: false,
    };
  }

  componentDidMount() {
    let postData = { method_id: "2", currency_id: "1" };
    axios({
      method: "post",
      url: "https://sandbox.caribpayintl.com/api/deposit/get-stripe-info",
      data: postData,
      headers: { Authorization: this.state.token },
    })
      .then((response) => {
        if (response.data.success.status == 200) {
          this.setState({
            stripeKey: response.data.success.stripe_keys.secret_key,
          });
          console.log(this.state.stripeKey);
        }
      })
      .catch((err) => {
        console.log("error ", err);
      });
  }

  OnProccede = async () => {
    let month = this.state.card_details.expiry.substr(0, 2);
    let year = this.state.card_details.expiry.substr(3, 5);
    this.setState({ visible: true });
    const apiKey = this.state.stripeKey;
    const client = new Stripe(apiKey);
    const token = await client
      .createToken({
        number: this.state.card_details.number,
        exp_month: month,
        exp_year: year,
        cvc: this.state.card_details.cvc,
        address_zip: "12345",
      })
      .then((response) => {
        if (response) {
          console.log("response", response);
          let postData = {
            amount: this.state.amount,
            totalAmount: this.state.amount,
            currency_id: this.state.currency_id,
            user_id: this.state.user_id,
            deposit_payment_id: 2,
            stripeToken: response.id,
          };
          axios({
            method: "post",
            url:
              "https://sandbox.caribpayintl.com/api/deposit/stripe-payment-store",
            headers: { Authorization: this.state.token },
            data: postData,
          })
            .then((response) => {
              console.log("stripe payment store --", response.data);
              if (response.data.status == 200) {
                let createdOn = response.data.success.created_at;
                let postData = { user_id: this.state.user_id };
                axios
                  .get(
                    "https://sandbox.caribpayintl.com/api/get-default-wallet-balance",
                    {
                      params: postData,
                      headers: { Authorization: this.state.token },
                    }
                  )
                  .then((response) => {
                    if (response.data.success.status == 200) {
                      let balance = response.data.success.defaultWalletBalance;
                      let available_bal = parseFloat(balance).toFixed(2);
                      this.setState({
                        updated_balance: available_bal,
                        visible: false,
                      });
                    }
                    this.props.navigation.navigate("SuccessFull", {
                      amount: this.state.amount,
                      remarks: "Funds Added via Debit/Credit Card",
                      updated_balance: this.state.updated_balance,
                      createdOn: createdOn,
                      payment_mode: "Debit/Credit Card",
                      user_id: this.state.user_id,
                      token: this.state.token,
                    });
                  })
                  .catch((error) => {
                    console.log("error updated bal---", error);
                  });
              }
            })
            .catch((err) => {
              alert(err);
              this.setState({ visible: false });
            });
        } else {
          alert(response.error.message);
          this.setState({ visible: false });
        }
      })
      .catch((err) => {
        console.log(err);
        this.setState({ visible: false });
      });
  };

  _onChange = (form) => {
    let card_details = form.values;

    if (
      card_details.number.startsWith("34") ||
      card_details.number.startsWith("37")
    ) {
      this.setState({
        isAmexCard: true,
      });
    }
    if (
      card_details.number != "" &&
      card_details.expiry !== "" &&
      card_details.cvc.length == 3
    ) {
      this.setState({
        creditInputDone: true,
        card_details: card_details,
      });
    }
    console.log(card_details);
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.header}>
          <TouchableOpacity
            style={styles.headerleftImage}
            onPress={() => this.props.navigation.goBack(null)}
          >
            <Image
              style={styles.headerleftImage}
              source={require("../Images/leftarrow.png")}
            />
          </TouchableOpacity>
          <Text style={styles.headertextStyle}>Add Fund</Text>
        </View>
        <View style={styles.absouluteview}>
          {/* <LinearGradient colors={['#b4e6f1', '#d7f3f7', '#f9fdff']} style={{ flex: 1 }}> */}
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              backgroundColor: colors.light_grey_backgroud,
              height: metrics.dimen_35,
            }}
          >
            <Text
              style={{
                color: colors.black,
                fontSize: metrics.text_normal,
                fontFamily: metrics.quicksand_semibold,
                marginLeft: metrics.dimen_10,
                textAlignVertical: "center",
              }}
            >
              Add Fund to Wallet
            </Text>
            <Text
              style={{
                color: colors.black,
                fontSize: metrics.text_normal,
                fontFamily: metrics.quicksand_semibold,
                marginRight: metrics.dimen_10,
                textAlignVertical: "center",
              }}
            >
              {this.state.currency_id == 1
                ? "$ " + this.state.amount
                : "EC$ " + this.state.amount}
            </Text>
          </View>
          <View style={{ flex: 1, margin: metrics.dimen_15 }}>
            <View
              style={{
                flexDirection: "row",
                backgroundColor: colors.theme_caribpay,
                height: metrics.dimen_35,
                alignSelf: "center",
                width: "100%",
                margin: 2,
                borderRadius: metrics.dimen_10,
              }}
            >
              <TouchableOpacity
                style={{
                  justifyContent: "center",
                  flex: 1 / 2,
                  borderRadius: metrics.dimen_10,
                  margin: 2,
                  backgroundColor: this.state.fromCard
                    ? colors.white
                    : colors.theme_caribpay,
                }}
                onPress={() =>
                  this.setState({
                    fromCard: true,
                    fromBanking: false,
                    isVisibleAlert: false,
                  })
                }
              >
                <View
                  style={{
                    justifyContent: "center",
                    flex: 1 / 2,
                    borderRadius: metrics.dimen_10,
                    margin: 1,
                    backgroundColor: this.state.fromCard
                      ? colors.white
                      : colors.theme_caribpay,
                  }}
                >
                  <Text
                    style={{
                      fontSize: metrics.text_description,
                      color: this.state.fromCard ? colors.black : colors.white,
                      textAlign: "center",
                      fontFamily: metrics.quicksand_bold,
                    }}
                  >
                    Credit/Debit Card
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  justifyContent: "center",
                  flex: 1 / 2,
                  borderRadius: metrics.dimen_10,
                  margin: 1,
                  backgroundColor: this.state.fromBanking
                    ? colors.white
                    : colors.theme_caribpay,
                }}
                onPress={() =>
                  this.setState({
                    fromCard: false,
                    fromBanking: true,
                    isVisibleAlert: true,
                  })
                }
              >
                <View
                  style={{
                    justifyContent: "center",
                    flex: 1 / 2,
                    borderRadius: metrics.dimen_10,
                    margin: 1,
                    backgroundColor: this.state.fromBanking
                      ? colors.white
                      : colors.theme_caribpay,
                  }}
                >
                  <Text
                    style={{
                      fontSize: metrics.text_description,
                      color: this.state.fromBanking
                        ? colors.black
                        : colors.white,
                      textAlign: "center",
                      fontFamily: metrics.quicksand_semibold,
                    }}
                  >
                    Online Banking
                  </Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={{ margin: 20 }}>
              <CreditCardInput
                onChange={this._onChange}
                placeholderColor={colors.app_light_gray}
              />
            </View>

            <Modal
              style={{ alignSelf: "center", width: "80%" }}
              isVisible={this.state.visible}
            >
              <View
                style={{
                  backgroundColor: "white",
                  borderRadius: metrics.dimen_10,
                }}
              >
                <View
                  style={{ flexDirection: "column", margin: metrics.dimen_20 }}
                >
                  <Spinner
                    style={{ alignSelf: "center" }}
                    isVisible={this.state.visible}
                    size={70}
                    type={"ThreeBounce"}
                    color={colors.black}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_header,
                      color: colors.black,
                      textAlign: "center",
                      marginTop: 20,
                    }}
                  >
                    We are processing now, Please wait a while!{" "}
                  </Text>
                </View>
              </View>
            </Modal>

            <Toast
              ref="toast"
              style={{ backgroundColor: "black" }}
              position="center"
              positionValue={200}
              fadeInDuration={200}
              fadeOutDuration={1000}
              opacity={0.8}
              textStyle={{ color: "white" }}
            />
          </View>
        </View>

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.isVisibleAlert}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                margin: metrics.dimen_20,
                flexDirection: "column",
                height: 200,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isVisibleAlert: false,
                      fromBanking: false,
                      fromCard: true,
                    })
                  }
                >
                  <Image
                    style={{
                      height: metrics.dimen_16,
                      width: metrics.dimen_16,
                      resizeMode: "contain",
                      margin: 15,
                    }}
                    source={require("../Images/cross-sign.png")}
                  />
                </TouchableOpacity>

                <View style={{ justifyContent: "center", flex: 1 }}>
                  <Image
                    style={{
                      height: metrics.dimen_80,
                      width: metrics.dimen_80,
                      resizeMode: "contain",
                      alignSelf: "center",
                    }}
                    source={require("../Images/logo.png")}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      color: colors.black,
                      fontFamily: metrics.quicksand_bold,
                      textAlign: "center",
                    }}
                  >
                    {
                      "Banking is not available at the moment. Please add fund using Debit / Credit Card"
                    }
                  </Text>
                </View>
              </View>
            </View>
          </Modal>
        </View>
        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.isAmexCard}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                margin: metrics.dimen_20,
                flexDirection: "column",
                height: 250,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <View>
                  <Image
                    style={{
                      height: metrics.dimen_120,
                      width: metrics.dimen_80,
                      resizeMode: "contain",
                      alignSelf: "center",
                      marginTop: 20,
                    }}
                    source={require("../Images/danger.png")}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      color: colors.black,
                      fontWeight: "bold",
                      textAlign: "center",
                    }}
                  >
                    {
                      "Sorry! AMEX CARD is not Valid/Unsupported,Please try other card!"
                    }
                  </Text>
                </View>
                <View style={styles.bottomview2}>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        isAmexCard: false,
                      })
                    }
                  >
                    <Text
                      style={{
                        fontSize: 15,
                        color: "white",
                        fontWeight: "bold",
                        textAlign: "center",
                        alignSelf: "center",
                        marginLeft: 10,
                      }}
                    >
                      Try Again
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </View>

        {this.state.creditInputDone && (
          <View
            style={{
              ...styles.bottomview,
              backgroundColor: colors.carib_pay_blue,
            }}
          >
            <TouchableOpacity onPress={() => this.OnProccede()}>
              <Text
                style={{
                  fontSize: 15,
                  color: "white",
                  fontFamily: metrics.quicksand_semibold,
                  textAlign: "center",
                  alignSelf: "center",
                  marginLeft: 10,
                }}
              >
                {"Proceed "}
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: metrics.dimen_70,
    backgroundColor: colors.carib_pay_blue,
    flexDirection: "row",
    paddingHorizontal: metrics.dimen_20,
  },
  headerleftImage: {
    height: metrics.dimen_24,
    width: metrics.dimen_24,
    alignSelf: "center",
    resizeMode: "contain",
    tintColor: colors.whitesmoke,
    marginBottom: metrics.dimen_15,
  },
  headertextStyle: {
    fontSize: metrics.text_17,
    fontFamily: metrics.quicksand_semibold,
    color: colors.white,
    marginBottom: metrics.dimen_15,
    textAlignVertical: "center",
    paddingHorizontal: metrics.dimen_20,
  },
  absouluteview: {
    position: "absolute",
    top: 51,
    borderTopRightRadius: metrics.dimen_20,
    borderTopLeftRadius: metrics.dimen_20,
    width: "100%",
    height: Dimensions.get("screen").height,
    backgroundColor: colors.white,
  },
  container: { flexDirection: "row", margin: metrics.dimen_20 },
  image_style: {
    height: metrics.dimen_30,
    width: metrics.dimen_30,
    resizeMode: "contain",
  },
  headingg: { fontSize: metrics.text_header, color: colors.black },
  descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
  arrowstyle: {
    height: metrics.dimen_15,
    width: metrics.dimen_15,
    resizeMode: "contain",
  },
  horizontalLine: {
    borderBottomColor: "#D3D3D3",
    borderBottomWidth: 0.4,
    width: "90%",
    alignSelf: "center",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 20,
  },
  bottomview2: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_5,
  },
});
