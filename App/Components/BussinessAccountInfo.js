import React from 'react';
import { SafeAreaView, Text, View, Image, Dimensions, StyleSheet, TouchableOpacity, BackHandler, FlatList, Linking } from 'react-native';
import metrics from '../Themes/Metrics';
import colors from '../Themes/Colors';

const sliderdata = [
    {
        id: 1,
        name: 'Multi Currency and Merchant Payments Features',
        image: require("../Images/itsimple.png"),
        status: 'Its Simple',
        description: 'Provide seamless International purchases in most countries',
    },
    {
        id: 2,
        name: 'Sell on CaribMall at the Lowest Cost',
        status: 'Its Fast',
        image: require("../Images/itfast.png"),
        description: 'You can start operating your ecommerce business almost immediately and hassle free',
    },
    {
        id: 3,
        name: 'Peace of Mind with Secure & Dedicated Support',
        status: 'Its Safe',
        image: require("../Images/itsafe.png"),
        description: 'Over the past years has resulted in fraud ratio far below the industry threshold',
    },

]

export default class BussinessAccountInfo extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View>
                    <Image
                        source={require("../Images/kychead.png")}
                        style={{ height: 150, width: Dimensions.get('window').width, resizeMode: 'contain', marginTop: -20 }}></Image>

                    <Text style={{ fontSize: metrics.text_large, color: colors.theme_caribpay, fontWeight: 'bold', marginHorizontal: 10, fontFamily: metrics.quicksand_bold  }}>Why Choose Us ? </Text>


                    <FlatList
                        data={sliderdata}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) =>
                            <View style={{ backgroundColor: colors.light_grey_backgroud, borderRadius: 10, flexDirection: 'column', margin: 20, width: 240, height: 230, fontFamily: metrics.quicksand_bold }}>

                                <Image style={{
                                    height: 120,
                                    width: 200,
                                    resizeMode: 'contain',
                                    alignSelf: 'center',
                                }} source={item.image}></Image>

                                <Text style={{ fontWeight: 'bold',margin:5, fontFamily: metrics.quicksand_bold, textAlign:"center" }}>{item.name}</Text>
                                {/* <Text style={{ position: 'absolute', top: 25, marginLeft: 115, alignSelf: 'center', fontWeight: 'bold', textDecorationLine: 'underline' }}>{item.status}</Text> */}
                                <Text style={{ margin:5,textAlign:'center' }}>{item.description}</Text>
                            </View>
                        }>

                    </FlatList>


                    <View style={{ ...styles.horizontalLine, width: "80%" }}></View>


                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 40, alignSelf: 'center', marginRight: 10 }}>
                        <View style={{ flexDirection: 'column', flex: 1 / 3 }}>
                            <View style={{ height: 30, width: 30, borderRadius: 15, justifyContent: 'center', backgroundColor: colors.theme_caribpay, alignSelf: 'center' }}>
                                <Text style={{ fontSize: metrics.text_medium, color: colors.white, fontWeight: 'bold', alignSelf: 'center' }}>1</Text>
                            </View>
                            <Text style={{ fontSize: metrics.text_medium, color: colors.theme_caribpay, alignSelf: 'center', textAlign: 'center', marginTop: 5, fontWeight: 'bold', fontFamily: metrics.quicksand_bold  }}>Register</Text>
                        </View>
                        <View style={{ ...styles.horizontalLine, marginTop: -40 }}></View>
                        <View style={{ flexDirection: 'column', flex: 1 / 3 }}>
                            <View style={{ height: 30, width: 30, borderRadius: 15, justifyContent: 'center', backgroundColor: colors.theme_caribpay, alignSelf: 'center' }}>
                                <Text style={{ fontSize: metrics.text_medium, color: colors.white, fontWeight: 'bold', alignSelf: 'center' }}>2</Text>
                            </View>
                            <Text style={{ fontSize: metrics.text_medium, color: colors.theme_caribpay, alignSelf: 'center', textAlign: 'center', marginTop: 5, fontWeight: 'bold', fontFamily: metrics.quicksand_bold  }}>Select Subscription Plan</Text>
                        </View>
                        <View style={{ ...styles.horizontalLine, marginTop: -40 }}></View>
                        <View style={{ flexDirection: 'column', flex: 1 / 3 }}>
                            <View style={{ height: 30, width: 30, borderRadius: 15, justifyContent: 'center', backgroundColor: colors.theme_caribpay, alignSelf: 'center' }}>
                                <Text style={{ fontSize: metrics.text_medium, color: colors.white, fontWeight: 'bold', alignSelf: 'center' }}>3</Text>
                            </View>
                            <Text style={{ fontSize: metrics.text_medium, color: colors.theme_caribpay, alignSelf: 'center', textAlign: 'center', marginTop: 5, fontWeight: 'bold', fontFamily: metrics.quicksand_bold  }}>Upload Document</Text>
                        </View>

                    </View>

                    {/* <View style={{ flexDirection: 'row', marginTop: 50, marginLeft: 20 }}>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={{ height: metrics.dimen_30, width: metrics.dimen_30, alignSelf: 'center' }} source={require('../Images/shield.png')}></Image>
                        </View>
                        <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                            <Text style={{ fontSize: metrics.text_header, color: colors.black, fontWeight: 'bold', }}>Feel Safe</Text>
                            <Text style={{ ...styles.descriiption, width: 300, alignSelf: 'center' }}>Rest assured that your transactions and data are kept secure and confidential</Text>
                        </View>
                    </View> */}

                </View>
                <View style={styles.bottomview}>
                    <TouchableOpacity onPress={() => Linking.openURL('https://sandbox.caribpayintl.com/register').catch(err => console.error('An error occurred', err))}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10, fontFamily: metrics.quicksand_bold  }}>Register Now</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView >
        )
    }


}

const styles = StyleSheet.create({
    headingg: { fontSize: metrics.text_header, color: colors.black, fontWeight: 'bold', marginBottom: 5 },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_30 },
    horizontalLine: { borderBottomColor: colors.theme_caribpay, borderBottomWidth: 0.4, width: 40, alignSelf: 'center', justifyContent: 'center' },


})