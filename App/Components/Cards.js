import React from 'react';
import { SafeAreaView, Text, View, Image, Dimensions, StyleSheet, } from 'react-native';
import metrics from '../Themes/Metrics';
import colors from '../Themes/Colors';

export default class Cards extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isCheckbalanceEnabled: false,
            isCarNumberEnabled: false
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.header}>
                    <Text style={styles.headertextStyle}>Card</Text>
                </View>

                <View style={styles.absouluteview}>
                    <View style={{flex:0.8,justifyContent:'center', alignSelf:'center'}}>
                        <Image source={require("../Images/comingsoon.png")}
                            style={{ height: 200, width: 200, alignSelf: 'center', resizeMode: 'center' }}></Image>
                    </View>
                </View>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_70,
        backgroundColor: colors.carib_pay_blue,
        paddingHorizontal: metrics.dimen_20,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        marginBottom: metrics.dimen_15,
        paddingHorizontal: metrics.dimen_5,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center'
    },
    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_15 },
    image_style: { height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black, textAlignVertical: 'center' },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' }
})