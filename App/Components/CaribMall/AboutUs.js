import React, { Component } from "react";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  BackHandler,
  Image,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import metrics from "../../Themes/Metrics";
import colors from "../../Themes/Colors";
const axios = require("axios");
import HTML from "react-native-render-html";
var Spinner = require("react-native-spinkit");
import Modal from "react-native-modal";

export default class AboutUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      htmlContent: "",
      spinvisible: false,
      fromaboutus: this.props.navigation.state.params.fromaboutus,
    };
  }

  componentDidMount() {
    this.setState({
      spinvisible: true,
    });
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    let data =
      this.state.fromaboutus == "1"
        ? "about-us"
        : this.state.fromaboutus == "2"
        ? "privacy-policy"
        : this.state.fromaboutus == "3"
        ? "terms-of-use-customer"
        : this.state.fromaboutus == "10"
        ? "terms-of-use"
        : this.state.fromaboutus == "11"
        ? "return-and-refund-policy"
        : this.state.fromaboutus == "12"
        ? "intellectual-property-rights"
        : "faq";
    axios({
      method: "get",
      url: "https://caribmall.com/api/page/" + data,
    })
      .then((response) => {
        console.log("About us response -- > ", response.data);
        this.setState({
          htmlContent: response.data.data.content,
          spinvisible: false,
        });
      })
      .catch((err) => {
        alert(err);
      });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity
            style={styles.headerleftImage}
            onPress={() => this.props.navigation.goBack(null)}
          >
            <Image
              style={styles.headerleftImage}
              source={require("../../Images/leftarrow.png")}
            />
          </TouchableOpacity>
          <Text style={styles.headertextStyle}>
            { this.state.fromaboutus == "1"
        ? "About Us"
        : this.state.fromaboutus == "2"
        ? "Privacy Policy"
        : this.state.fromaboutus == "3"
        ? "Terms & Conditions"
        : this.state.fromaboutus == "10"
        ? "Terms of Use"
        : this.state.fromaboutus == "11"
        ? "Return and refund policy"
        : this.state.fromaboutus == "12"
        ? "Intellectual Property Rights"
        : "FAQs"}
          </Text>
        </View>
        <ScrollView style={{ flex: 1 }}>
          <HTML
            html={this.state.htmlContent}
            containerStyle={{
              flex: 1,
              marginTop: 10,
              margin: 15,
            }}
            imagesMaxWidth={Dimensions.get("window").width}
          />
        </ScrollView>

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.spinvisible}
          >
            <View
              style={{
                width: "50%",
                alignSelf: "center",
                justifyContent: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_5,
                margin: metrics.dimen_20,
                height: 100,
              }}
            >
              <Spinner
                style={{ alignSelf: "center" }}
                isVisible={this.state.spinvisible}
                size={70}
                type={"ThreeBounce"}
                color={colors.black}
              />
            </View>
          </Modal>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: metrics.dimen_60,
    backgroundColor: colors.theme_caribpay,
    paddingHorizontal: metrics.dimen_15,
    flexDirection: "row",
  },
  headerleftImage: {
    height: metrics.dimen_24,
    width: metrics.dimen_24,
    alignSelf: "center",
    resizeMode: "contain",
    tintColor: colors.whitesmoke,
  },
  headertextStyle: {
    fontSize: metrics.text_17,
    fontWeight: "200",
    color: colors.white,
    paddingHorizontal: metrics.dimen_20,
    fontFamily: metrics.quicksand_bold,
    textAlignVertical: "center",
    textAlign: "center",
  },
});
