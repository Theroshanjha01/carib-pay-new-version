import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, FlatList, Image, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../../Themes/Metrics.js';
import colors from '../../Themes/Colors.js';
import { Input } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
const axios = require('axios');
import Modal from 'react-native-modal';
var Spinner = require('react-native-spinkit');
import DropdownAlert from 'react-native-dropdownalert';




export default class AccountDetails extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            starCount: 3.5,
            username: this.props.navigation.state.params.name,
            addressData: [],
            api_token: ''
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.getUserAddress();
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                this.getUserAddress();
            },
        );
    }


    componentWillUnmount() {
        this.willFocusSubscription.remove();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }


    getUserAddress = async () => {
        this.setState({
            spinvisible: true
        })
        let caribmall_data = await AsyncStorage.getItem("carib_mall_data")
        let data = JSON.parse(caribmall_data)
        console.log("token", data.api_token)
        this.setState({
            api_token: data.api_token
        })
        axios({
            method: 'get',
            url: 'https://caribmall.com/api/addresses?api_token=' + data.api_token,
        }).then(response => {
            // console.log("response address data", response.data.data[0].country)
            this.setState({
                addressData: response.data.data,
                spinvisible: false
            })
        }).catch((error) => {
            console.log("response coupons errors ", error)
            this.setState({
                // addressData: response.data.data,
                spinvisible: false
            })
        })
    }


    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    renderStatesAndCountry = (item) => {
        console.log(item)
        let stateName
        let countryName
        console.log(item.state == null ? stateName = "" : stateName = item.state['name'])
        console.log(item.country == null ? countryName = "" : countryName = item.country['name'])

        return (
            <Text style={{ fontSize: metrics.text_description, color: colors.app_gray, color: colors.black }}>{stateName + "" + item.zip_code + ", " + countryName}</Text>
        )
    }

    onDeleteAddress = (item) => {
        console.log("adrres item -->", item)
        this.setState({
            spinvisibleAddress: true
        })
        axios({
            method: 'delete',
            url: 'https://caribmall.com/api/address/' + item.id + '?api_token=' + this.state.api_token,
        }).then(response => {
            console.log("address delete response --- > ", response.data)

            this.setState({
                spinvisibleAddress: false
            })
            this.dropDownAlertRef.alertWithType('success', "Your " + item.address_type + " address is removed")
            this.getUserAddress();
        }).catch(error => {
            if (error.response) {
                console.log('response addresss response store error ---> ', error.response.data);
            }
        })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }} >
                <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                    <View style={{ flexDirection: 'row', margin: 10 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white' }} source={require('../../Images/leftarrow.png')}></Image>
                            </View>
                        </TouchableOpacity>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold', marginTop: 2, marginLeft: 10 }}>Account Details</Text>
                    </View>
                </View>
                <View style={styles.curveview}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        style={{ marginBottom: 20, marginTop: 20 }}>
                        <View style={{ flex: 1, margin: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ color: colors.app_black_text, fontSize: metrics.text_normal, fontWeight: 'bold', marginLeft: 5, alignSelf: 'center' }}>Basic Info</Text>
                                <Text style={styles.curvyview}>Update</Text>
                            </View>
                            <View style={{ backgroundColor: colors.light_grey_backgroud, height: 50, justifyContent: 'center', borderRadius: 10, marginTop: 10, marginLeft: 5, marginRight: 5 }}>
                                <Text style={{ color: colors.app_black_text, fontSize: metrics.text_normal, fontWeight: 'bold', marginLeft: metrics.dimen_12 }}>Customer Name</Text>
                            </View>

                            <View style={{ marginTop: 5, flexDirection: 'column' }}>
                                <Input
                                    placeholder={'Adam Voges'}
                                    placeholderTextColor={colors.app_gray}
                                    value={this.state.username}
                                    onChangeText={(text) => this.setState({ username: text })}
                                    containerStyle={{ width: "97%", height: 50, backgroundColor: colors.light_grey_backgroud, borderRadius: 10, marginLeft: 5 }}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    inputStyle={{ fontSize: 14, color: colors.app_gray, fontWeight: 'bold' }}>
                                </Input>

                                <Input
                                    placeholder={'Tell us about your self'}
                                    placeholderTextColor={colors.app_gray}
                                    containerStyle={{ height: metrics.dimen_150, borderColor: colors.app_gray, borderWidth: 0.5, borderRadius: metrics.dimen_7, marginTop: metrics.dimen_10, marginLeft: 5, width: '97%' }}
                                    value={this.state.description}
                                    multiline={true}
                                    numberOfLines={10}
                                    inputContainerStyle={{ borderBottomWidth: 0, justifyContent: 'center', alignItems: this.multiline ? 'flex-start' : 'center' }}
                                    inputStyle={{ fontSize: 14, color: colors.app_gray, fontWeight: 'bold', textAlign: 'center' }}
                                    onChangeText={(text) => this.setState({ description: text })}>
                                </Input>
                            </View>


                            <View style={styles.horizontalLine}></View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ color: colors.app_black_text, fontSize: metrics.text_normal, fontWeight: 'bold', marginLeft: 5, alignSelf: 'center' }}>Address</Text>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate("AddAddress")}>
                                    <Text style={styles.curvyview}>Add Address</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ marginTop: 10, flex: 1 }}>
                                <FlatList
                                    style={{ marginTop: metrics.dimen_10, marginBottom: 40 }}
                                    showsVerticalScrollIndicator={false}
                                    data={this.state.addressData}
                                    ItemSeparatorComponent={() => <View style={styles.horizontalLine}></View>}
                                    keyExtractor={(item, index) => index.toString()}
                                    renderItem={({ item }) => (
                                        <View style={{ flexDirection: 'row', borderRadius: 10 }}>
                                            <View style={{ flex: 0.2 }}>
                                                <Text style={{ fontSize: metrics.text_description, color: colors.app_gray, color: colors.black, marginLeft: 5 }}>{item.address_type}</Text>
                                            </View>
                                            <View style={{ flex: 0.78 }}>
                                                <Text style={{ fontSize: metrics.text_description, color: colors.app_gray, color: colors.black }}>{item.address_title}</Text>
                                                <Text style={{ fontSize: metrics.text_description, color: colors.app_gray, color: colors.black }}>{item.address_line_1 + "," + item.address_line_2}</Text>
                                                <Text style={{ fontSize: metrics.text_description, color: colors.app_gray, color: colors.black }}>{item.city + " - "}{this.renderStatesAndCountry(item)}</Text>
                                            </View>
                                            <TouchableOpacity onPress={() => this.onDeleteAddress(item)}>
                                                <Image source={require('../../Images/add_cut.png')}
                                                    style={{ height: 28, width: 28, resizeMode: 'contain', marginTop: 10 }}></Image>
                                            </TouchableOpacity>
                                        </View>
                                    )} />
                            </View>

                        </View>
                    </ScrollView>
                </View>

                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.spinvisible}>
                    <View style={{ width: "50%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 100 }}>
                        <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinvisible} size={70} type={"ThreeBounce"} color={colors.black} />
                    </View>
                </Modal>

                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.spinvisibleAddress}>
                    <View style={{ width: "50%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 100 }}>
                        <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinvisibleAddress} size={70} type={"ThreeBounce"} color={colors.black} />
                    </View>
                </Modal>

                <DropdownAlert ref={ref => this.dropDownAlertRef = ref}></DropdownAlert>


            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    horizontalLine: { borderBottomColor: '#f2f2f2', borderBottomWidth: 8, width: '100%', marginTop: 30, marginBottom: 20 },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    curveview: { height: metrics.newView.curveview + 160, position: "absolute", top: metrics.newView.curvetop - 100, width: "100%", backgroundColor: colors.white, borderRadius: 40 },
    curvyview: { backgroundColor: colors.theme_caribpay, color: colors.white, fontSize: metrics.text_normal, fontWeight: 'bold', marginRight: 5, padding: 6, borderRadius: 20 }
})