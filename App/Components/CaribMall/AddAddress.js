import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  Dimensions,
  BackHandler,
  TouchableOpacity,
  Image,
  FlatList,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
import { Input } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
const axios = require("axios");
import Modal from "react-native-modal";
import AsyncStorage from "@react-native-community/async-storage";
var Spinner = require("react-native-spinkit");
import DropdownAlert from "react-native-dropdownalert";
import RBSheet from "react-native-raw-bottom-sheet";
import CountryData from "country-data";

export default class AddAddress extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      countryData: [],
      statesData: [],
      addressTypeData: [],
      api_token: "",
      addressType: "",
      customer_name: "",
      address1: "",
      address2: "",
      city: "",
      zipcode: "",
      country_id: "",
      stateName: "",
      contact_number: "",
      arrayholder: [],
      arrayholder2: [],
      text: "",
      countryDataSource: [],
      arrayholderStates: [],
    };
  }

  componentDidMount() {
    this.getCountries();
    this.getCountryPicker();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  getCountryPicker = () => {
    axios
      .get("https://topups.reloadly.com/countries")
      .then((response) => {
        this.setState({
          countryDataSource: response.data,
        });
        this.state.arrayholder = response.data;
        // console.log("respnse couuntry code picker", response.data);
      })
      .catch((err) => {
        console.warn(err);
      });

    axios
      .get("https://ipinfo.io/json")
      .then((response) => {
        let country_code = response.data.country;
        let countrycallingcode = JSON.stringify(
          CountryData.countries[country_code].countryCallingCodes[0]
        );
        this.setState({
          currencycode: countrycallingcode.replace(/"/g, ""),
          isoName: country_code,
          inshort: country_code.toString().toLocaleLowerCase(),
        });

        console.log(
          "info coming ---> ",
          this.state.currencycode,
          this.state.isoName
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };

  getCountries = () => {
    axios
      .get("https://caribmall.com/api/countries")
      .then((response) => {
        this.setState({
          countryData: response.data.data,
          arrayholder2: response.data.data,
        });
        // console.log("countryData  ---> ", this.state.countryData);
      })
      .catch((err) => {
        console.log("roshan", err);
      });

    this.getAddresstype();
  };

  getAddresstype = async () => {
    this.setState({
      spinvisible: true,
    });
    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    console.log("token", data.api_token);
    this.setState({
      api_token: data.api_token,
    });

    axios
      .get(
        "https://caribmall.com/api/address/create?api_token=" + data.api_token
      )
      .then((response) => {
        console.log("getAddresstype  ---> ", response.data.address_types);
        let arr = [];
        for (var key in response.data.address_types) {
          var value = response.data.address_types[key];
          console.log(value);
          if (value != "Primary Address") {
            const elements = { name: value };
            arr.push(elements);
          }
          console.log("array-->", arr);
        }

        this.setState({
          addressTypeData: arr.reverse(),
          spinvisible: false,
        });
      })
      .catch((err) => {
        console.log("roshan", err);
        this.setState({
          spinvisible: false,
        });
      });
  };

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  onSelectCountry = (item) => {
    this.setState({
      country: item.name,
      openCountryModal: false,
      country_id: item.id,
      text: "",
    });
    axios
      .get("https://caribmall.com/api/states/" + item.id)
      .then((response) => {
        this.setState({
          statesData: response.data.data,
          arrayholderStates:response.data.data
        });
        console.log("states Data ---> ", this.state.statesData);
        console.log(
          "states Data lengthssss ---> ",
          this.state.statesData.length
        );
      })
      .catch((err) => {
        console.log("roshan", err);
      });
  };

  onSelectState = (item) => {
    this.setState({
      stateName: item.name,
      state_id: item.id,
      openStatesModal: false,
    });
  };

  storeAddress = () => {
    if (this.state.addressType == "") {
      this.dropDownAlertRef.alertWithType(
        "error",
        "Please let us know your Address Type."
      );
    } else if (this.state.customer_name == "") {
      this.dropDownAlertRef.alertWithType("error", "Please enter your name");
    } else if (this.state.contact_number == "") {
      this.dropDownAlertRef.alertWithType(
        "error",
        "Required your Mobile Number"
      );
    } else if ((this.state.country = "")) {
      this.dropDownAlertRef.alertWithType("error", "Please enter country name");
    } else if (this.state.zipcode == "") {
      this.dropDownAlertRef.alertWithType("error", "Please enter area zipcode");
    } else if (this.state.address1 == "") {
      this.dropDownAlertRef.alertWithType("error", "Please enter your Address");
    } else if (this.state.address2 == "") {
      this.dropDownAlertRef.alertWithType("error", "Please enter your Address");
    } else {
      this.setState({
        spinvisibleAddress: true,
      });
      let postdata = {
        address_type: this.state.addressType,
        address_title: this.state.customer_name,
        address_line_1: this.state.address1,
        address_line_2: this.state.address2,
        city: this.state.city,
        zip_code: this.state.zipcode,
        country_id: this.state.country_id,
        state_id: this.state.state_id,
        phone: this.state.currencycode + this.state.contact_number,
      };

      console.log("params add address --> ", postdata);

      axios({
        method: "post",
        url:
          "https://caribmall.com/api/address/store?api_token=" +
          this.state.api_token,
        data: postdata,
      })
        .then((response) => {
          console.log("add addresss response store --- > ", response.data);
          this.setState({
            spinvisibleAddress: false,
          });
          this.dropDownAlertRef.alertWithType(
            "success",
            "Your address is successfully Saved!"
          );
          this.setState({
            addressType: "",
            customer_name: "",
            address1: "",
            address2: "",
            city: "",
            zipcode: "",
            country_id: "",
            stateName: "",
            contact_number: "",
          });
          this.props.navigation.goBack();
        })
        .catch((error) => {
          if (error.response) {
            console.log(
              "response addresss response store error ---> ",
              error.response.data
            );
          }
        });
    }
  };

  SearchFilterFunction(text) {
    const newData = this.state.arrayholder.filter((item) => {
      const itemData = `${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({ countryDataSource: newData, text: text });
  }

  SearchFilterFunction2(text) {
    const newData = this.state.arrayholder2.filter((item) => {
      const itemData = `${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({ countryData: newData, text: text });
  }


  SearchFilterFunctionStates(text) {
    const newData = this.state.arrayholderStates.filter((item) => {
      const itemData = `${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({ statesData: newData, text: text });
  }

  renderCountries = () => {
    return (
      <View>
        <View
          style={{
            height: metrics.dimen_60,
            backgroundColor: colors.carib_pay_blue,
            flexDirection: "row",
          }}
        >
          <TouchableOpacity
            style={{
              height: metrics.dimen_25,
              width: metrics.dimen_25,
              marginStart: metrics.dimen_10,
              alignSelf: "center",
            }}
            onPress={() => this.RBSheetAlertCountryPicker.close()}
          >
            <Image
              style={{
                height: metrics.dimen_25,
                width: metrics.dimen_25,
                alignSelf: "center",
                paddingHorizontal: 10,
              }}
              source={require("../../Images/leftarrow.png")}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: metrics.text_16,
              color: "white",
              textAlign: "center",
              alignSelf: "center",
              marginLeft: 10,
            }}
          >
            Select Country
          </Text>
        </View>
        <View
          style={{
            marginTop: metrics.dimen_5,
            width: "95%",
            alignSelf: "center",
          }}
        >
          <Input
            containerStyle={{
              alignSelf: "center",
              backgroundColor: "#DCDCDC",
              borderRadius: 10,
              height: 45,
            }}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            placeholder="Search"
            placeholderTextColor="#9D9D9F"
            inputStyle={{ color: colors.black, fontSize: metrics.text_16 }}
            value={this.state.text}
            onChangeText={(text) => this.SearchFilterFunction(text)}
            leftIcon={
              <Image
                style={{
                  width: metrics.dimen_25,
                  height: metrics.dimen_25,
                  resizeMode: "contain",
                }}
                source={require("../../Images/search.png")}
              />
            }
          />
        </View>

        <FlatList
          style={{ marginTop: metrics.dimen_7 }}
          showsVerticalScrollIndicator={false}
          data={this.state.countryDataSource}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => this.renderItemCountries(item)}
        />
      </View>
    );
  };

  getCodesByList = (item) => {
    this.setState({
      currencycode: item.callingCodes[0],
      isoName: item.isoName,
      inshort: item.isoName.toString().toLocaleLowerCase(),
    });
    this.RBSheetAlertCountryPicker.close();
  };

  renderItemCountries = (item) => {
    return (
      <View>
        <TouchableOpacity onPress={() => this.getCodesByList(item)}>
          <View style={{ flexDirection: "row", margin: metrics.dimen_15 }}>
            <Image
              style={{ height: 30, width: 30, resizeMode: "contain" }}
              source={{
                uri:
                  "https://www.countryflags.io/" +
                  item.isoName.toLocaleLowerCase() +
                  "/flat/64.png",
              }}
            />
            <Text
              style={{
                color: colors.app_black_text,
                fontSize: metrics.text_normal,
                paddingHorizontal: 10,
              }}
            >
              {item.name}
            </Text>
            <Text
              style={{
                color: colors.app_black_text,
                fontSize: metrics.text_description,
              }}
            >
              {" (" + item.callingCodes[0] + ")"}
            </Text>
          </View>
          <View
            style={{
              borderBottomColor: colors.light_grey_backgroud,
              borderBottomWidth: 1,
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <KeyboardAwareScrollView>
          <View
            style={{
              backgroundColor: colors.theme_caribpay,
              height: metrics.dimen_60,
            }}
          >
            <View style={{ margin: metrics.dimen_10, flexDirection: "row" }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.goBack(null)}
              >
                <View
                  style={{
                    height: metrics.dimen_40,
                    width: metrics.dimen_40,
                    justifyContent: "center",
                  }}
                >
                  <Image
                    style={{
                      height: metrics.dimen_25,
                      width: metrics.dimen_25,
                      tintColor: "white",
                    }}
                    source={require("../../Images/leftarrow.png")}
                  />
                </View>
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: metrics.text_heading,
                  color: colors.white,
                  fontWeight: "bold",
                  marginTop: 4,
                }}
              >
                Add Address
              </Text>
            </View>
          </View>

          <View
            style={{
              ...styles.curveview,
              margin: 10,
              justifyContent: "center",
              alignSelf: "center",
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  openAddressType: true,
                })
              }
            >
              <View style={{ ...styles.boxContainer }}>
                <Input
                  placeholder={"Address Type"}
                  placeholderTextColor={colors.app_gray}
                  disabled={true}
                  value={this.state.addressType}
                  onChangeText={(text) => this.setState({ addressType: text })}
                  containerStyle={{
                    width: "100%",
                    height: 50,
                    borderColor: colors.theme_caribpay,
                    borderWidth: 0.5,
                    borderRadius: 10,
                  }}
                  inputContainerStyle={{ borderBottomWidth: 0 }}
                  inputStyle={{ fontSize: 14 }}
                />
              </View>
            </TouchableOpacity>

            <View style={{ ...styles.boxContainer, marginTop: 5 }}>
              <Input
                placeholder={"Full Name"}
                placeholderTextColor={colors.app_gray}
                value={this.state.customer_name}
                onChangeText={(text) => this.setState({ customer_name: text })}
                containerStyle={{
                  width: "100%",
                  height: 50,
                  borderColor: colors.theme_caribpay,
                  borderWidth: 0.5,
                  borderRadius: 10,
                }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                // keyboardType="number-pad"
                inputStyle={{ fontSize: 14 }}
              />
            </View>

            <View style={{ ...styles.boxContainer, marginTop: 5 }}>
              <Input
                placeholder={"Address Line 1"}
                placeholderTextColor={colors.app_gray}
                value={this.state.address1}
                onChangeText={(text) => this.setState({ address1: text })}
                containerStyle={{
                  width: "100%",
                  height: 50,
                  borderColor: colors.theme_caribpay,
                  borderWidth: 0.5,
                  borderRadius: 10,
                }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={{ fontSize: 14 }}
              />
            </View>
            <View style={{ ...styles.boxContainer, marginTop: 5 }}>
              <Input
                placeholder={"Address Line 2"}
                placeholderTextColor={colors.app_gray}
                value={this.state.address2}
                onChangeText={(text) => this.setState({ address2: text })}
                containerStyle={{
                  width: "100%",
                  height: 50,
                  borderColor: colors.theme_caribpay,
                  borderWidth: 0.5,
                  borderRadius: 10,
                }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={{ fontSize: 14 }}
              />
            </View>

            <View style={{ ...styles.boxContainer, marginTop: 5 }}>
              <Input
                placeholder={"Enter City"}
                placeholderTextColor={colors.app_gray}
                value={this.state.city}
                onChangeText={(text) => this.setState({ city: text })}
                containerStyle={{
                  width: "100%",
                  height: 50,
                  borderColor: colors.theme_caribpay,
                  borderWidth: 0.5,
                  borderRadius: 10,
                }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={{ fontSize: 14 }}
              />
            </View>

            <TouchableOpacity
              onPress={() =>
                this.setState({
                  openCountryModal: true,
                })
              }
            >
              <View style={{ ...styles.boxContainer, marginTop: 5 }}>
                <Input
                  placeholder={"Select Country"}
                  placeholderTextColor={colors.app_gray}
                  value={this.state.country}
                  disabled={true}
                  // onChangeText={(text) => this.setState({ email: text })}
                  containerStyle={{
                    width: "100%",
                    height: 50,
                    borderColor: colors.theme_caribpay,
                    borderWidth: 0.5,
                    borderRadius: 10,
                  }}
                  inputContainerStyle={{ borderBottomWidth: 0 }}
                  inputStyle={{ fontSize: 14 }}
                />
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.setState({
                  openStatesModal: true,
                })
              }
            >
              <View style={{ ...styles.boxContainer, marginTop: 5 }}>
                <Input
                  placeholder={"Select State"}
                  placeholderTextColor={colors.app_gray}
                  value={this.state.stateName}
                  disabled={true}
                  containerStyle={{
                    width: "100%",
                    height: 50,
                    borderColor: colors.theme_caribpay,
                    borderWidth: 0.5,
                    borderRadius: 10,
                  }}
                  inputContainerStyle={{ borderBottomWidth: 0 }}
                  inputStyle={{ fontSize: 14 }}
                />
              </View>
            </TouchableOpacity>

            <View style={{ ...styles.boxContainer, marginTop: 5 }}>
              <Input
                placeholder={"Enter Zip Code"}
                placeholderTextColor={colors.app_gray}
                value={this.state.zipcode}
                onChangeText={(text) => this.setState({ zipcode: text })}
                containerStyle={{
                  width: "100%",
                  height: 50,
                  borderColor: colors.theme_caribpay,
                  borderWidth: 0.5,
                  borderRadius: 10,
                }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={{ fontSize: 14 }}
              />
            </View>

            <View style={{ ...styles.boxContainer, marginTop: 5 }}>
              <Input
                placeholder={"Select Country Code"}
                placeholderTextColor={colors.app_gray}
                value={this.state.currencycode}
                onChangeText={(text) => this.setState({ contact_number: text })}
                containerStyle={{
                  width: "100%",
                  height: 50,
                  borderColor: colors.theme_caribpay,
                  borderWidth: 0.5,
                  borderRadius: 10,
                }}
                leftIcon={
                  <TouchableOpacity
                    onPress={() => this.RBSheetAlertCountryPicker.open()}
                  >
                    {this.state.isoName == "" ? (
                      <Image
                        style={{ height: 20, width: 20 }}
                        source={require("../../Images/globe.png")}
                      />
                    ) : (
                      <Image
                        style={{ height: 30, width: 30 }}
                        source={{
                          uri:
                            "https://www.countryflags.io/" +
                            this.state.inshort +
                            "/flat/64.png",
                        }}
                      />
                    )}
                  </TouchableOpacity>
                }
                inputContainerStyle={{ borderBottomWidth: 0 }}
                keyboardType="number-pad"
                inputStyle={{ fontSize: 14 }}
              />
            </View>

            <View style={{ ...styles.boxContainer, marginTop: 5 }}>
              <Input
                placeholder={"Enter Contact Number"}
                placeholderTextColor={colors.app_gray}
                value={this.state.contact_number}
                containerStyle={{
                  width: "100%",
                  height: 50,
                  borderColor: colors.theme_caribpay,
                  borderWidth: 0.5,
                  borderRadius: 10,
                }}
                onChangeText={(text) => this.setState({ contact_number: text })}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                keyboardType="number-pad"
                inputStyle={{ fontSize: 14 }}
              />
            </View>

            <View style={{ height: 100 }} />
          </View>
        </KeyboardAwareScrollView>

        <Modal
          onBackdropPress={() => this.setState({ openCountryModal: false })}
          style={{ alignSelf: "center", width: "70%" }}
          isVisible={this.state.openCountryModal}
        >
          <View
            style={{ backgroundColor: "white", borderRadius: metrics.dimen_10 }}
          >
            <View style={{ flexDirection: "column", margin: metrics.dimen_20 }}>

            <Text
                style={{
                  fontSize: metrics.text_large,
                  color: colors.theme_caribpay,
                  textAlign: "center",
                  marginTop: metrics.dimen_10,
                  marginBottom:metrics.dimen_10,
                  fontWeight: "bold",
                }}
              >
                Select Country
              </Text>
            
              <Input
                containerStyle={{
                  alignSelf: "center",
                  backgroundColor: "#DCDCDC",
                  borderRadius: 10,
                  height: 45,
                }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                placeholder="Search"
                placeholderTextColor="#9D9D9F"
                inputStyle={{ color: colors.black, fontSize: metrics.text_16 }}
                value={this.state.text}
                onChangeText={(text) => this.SearchFilterFunction2(text)}
                leftIcon={
                  <Image
                    style={{
                      width: metrics.dimen_25,
                      height: metrics.dimen_25,
                      resizeMode: "contain",
                    }}
                    source={require("../../Images/search.png")}
                  />
                }
              />
              <FlatList
                style={{ marginTop: metrics.dimen_10, height: 300 }}
                showsVerticalScrollIndicator={false}
                data={this.state.countryData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <TouchableOpacity onPress={() => this.onSelectCountry(item)}>
                    <View
                      style={{
                        flexDirection: "row",
                        margin: 15,
                        backgroundColor: colors.white,
                        borderRadius: 10,
                      }}
                    >
                      <Text
                        style={{
                          fontSize: metrics.text_normal,
                          color: colors.theme_caribpay,
                        }}
                      >
                        {item.name}
                      </Text>
                      <Text
                        style={{
                          fontSize: metrics.text_normal,
                          color: colors.black,
                        }}
                      >
                        {" ( " + item.iso_code + " ) "}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </Modal>

        <Modal
          onBackdropPress={() => this.setState({ openStatesModal: false })}
          style={{ alignSelf: "center", width: "70%" }}
          isVisible={this.state.openStatesModal}
        >
          <View
            style={{ backgroundColor: "white", borderRadius: metrics.dimen_10 }}
          >
            <View style={{ flexDirection: "column", margin: metrics.dimen_20 }}>
              <Text
                style={{
                  fontSize: metrics.text_large,
                  color: colors.theme_caribpay,
                  textAlign: "center",
                  marginTop: 10,
                  fontWeight: "bold",
                }}
              >
                Select State
              </Text>
              <View
                style={{
                  marginTop: metrics.dimen_10,
                  width: "95%",
                  alignSelf: "center",
                }}
              >
                <Input
                  containerStyle={{
                    alignSelf: "center",
                    backgroundColor: "#DCDCDC",
                    borderRadius: 10,
                    height: 45,
                  }}
                  inputContainerStyle={{ borderBottomWidth: 0 }}
                  placeholder="Search"
                  placeholderTextColor="#9D9D9F"
                  inputStyle={{
                    color: colors.black,
                    fontSize: metrics.text_16,
                  }}
                  value={this.state.text}
                  onChangeText={(text) => this.SearchFilterFunctionStates(text)}
                  leftIcon={
                    <Image
                      style={{
                        width: metrics.dimen_25,
                        height: metrics.dimen_25,
                        resizeMode: "contain",
                      }}
                      source={require("../../Images/search.png")}
                    />
                  }
                />
              </View>
              <FlatList
                style={{ marginTop: metrics.dimen_10, height: 300 }}
                showsVerticalScrollIndicator={false}
                data={this.state.statesData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <TouchableOpacity onPress={() => this.onSelectState(item)}>
                    <View
                      style={{
                        flexDirection: "row",
                        margin: 15,
                        backgroundColor: colors.white,
                        borderRadius: 10,
                      }}
                    >
                      <Text
                        style={{
                          fontSize: metrics.text_normal,
                          color: colors.theme_caribpay,
                        }}
                      >
                        {item.name}
                      </Text>
                      <Text
                        style={{
                          fontSize: metrics.text_normal,
                          color: colors.black,
                        }}
                      >
                        {" ( " + item.iso_code + " ) "}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </Modal>

        <Modal
          onBackdropPress={() => this.setState({ openAddressType: false })}
          style={{ alignSelf: "center", width: "70%" }}
          isVisible={this.state.openAddressType}
        >
          <View
            style={{ backgroundColor: "white", borderRadius: metrics.dimen_10 }}
          >
            <View style={{ flexDirection: "column", margin: metrics.dimen_20 }}>
              <Text
                style={{
                  fontSize: metrics.text_large,
                  color: colors.theme_caribpay,
                  textAlign: "center",
                  marginTop: 10,
                  fontWeight: "bold",
                }}
              >
                Select Address Type
              </Text>
              <FlatList
                style={{ marginTop: metrics.dimen_10, height: 150 }}
                showsVerticalScrollIndicator={false}
                data={this.state.addressTypeData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        addressType: item.name,
                        openAddressType: false,
                      })
                    }
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        margin: 15,
                        backgroundColor: colors.white,
                        borderRadius: 10,
                      }}
                    >
                      <Text
                        style={{
                          fontSize: metrics.text_normal,
                          color: colors.theme_caribpay,
                          fontFamily: metrics.quicksand_medium,
                        }}
                      >
                        {item.name}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </Modal>

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisibleAddress}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisibleAddress}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>

        <View style={styles.bottomview}>
          <TouchableOpacity onPress={() => this.storeAddress()}>
            <Text
              style={{
                fontSize: 15,
                color: "white",
                fontWeight: "bold",
                textAlign: "center",
                alignSelf: "center",
                marginLeft: 10,
              }}
            >
              {"Save "}
            </Text>
          </TouchableOpacity>
        </View>

        <DropdownAlert ref={(ref) => (this.dropDownAlertRef = ref)} />

        <RBSheet
          ref={(ref) => {
            this.RBSheetAlertCountryPicker = ref;
          }}
          height={Dimensions.get("screen").height}
          duration={0}
        >
          {this.renderCountries()}
        </RBSheet>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: metrics.dimen_70,
    backgroundColor: colors.theme_caribpay,
    flexDirection: "row",
    paddingHorizontal: metrics.dimen_20,
  },
  headerleftImage: {
    height: metrics.dimen_24,
    width: metrics.dimen_24,
    alignSelf: "center",
    resizeMode: "contain",
    tintColor: colors.whitesmoke,
    marginBottom: metrics.dimen_15,
  },
  headertextStyle: {
    fontSize: metrics.text_16,
    fontWeight: "bold",
    color: colors.white,
    marginBottom: metrics.dimen_15,
    fontFamily: metrics.quicksand_bold,
    textAlignVertical: "center",
    paddingHorizontal: metrics.dimen_20,
  },
  absouluteview: {
    position: "absolute",
    top: 51,
    borderTopRightRadius: metrics.dimen_20,
    borderTopLeftRadius: metrics.dimen_20,
    width: "100%",
    height: Dimensions.get("screen").height,
    backgroundColor: colors.white,
  },
  container: { flexDirection: "row", margin: metrics.dimen_20 },
  image_style: {
    height: metrics.dimen_30,
    width: metrics.dimen_30,
    resizeMode: "contain",
  },
  headingg: { fontSize: metrics.text_header, color: colors.black },
  descriiption: {
    fontSize: metrics.text_description,
    color: "#D3D3D3",
    fontWeight: "800",
  },
  arrowstyle: {
    height: metrics.dimen_15,
    width: metrics.dimen_15,
    resizeMode: "contain",
  },
  horizontalLine: {
    borderBottomColor: "#D3D3D3",
    marginBottom: 10,
    borderBottomWidth: 1,
    width: "100%",
    alignSelf: "center",
    marginTop: 20,
  },
  boxContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: colors.white,
    borderRadius: metrics.dimen_5,
    alignSelf: "center",
  },
  buttonStyle: {
    flexDirection: "row",
    width: "80%",
    borderRadius: 5,
    justifyContent: "center",
    alignSelf: "center",
    marginBottom: 30,
    height: 40,
    backgroundColor: colors.theme_caribpay,
    shadowColor: "#f2f2f2",
    shadowOpacity: 1,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1,
    },
  },
  imageStyle: {
    height: metrics.dimen_60,
    width: metrics.dimen_60,
    resizeMode: "contain",
    marginTop: 5,
  },
  curveview: { width: "90%", backgroundColor: colors.white },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 25,
  },
  bottomview2: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_5,
  },
});
