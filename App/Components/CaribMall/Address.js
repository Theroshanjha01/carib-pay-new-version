import React from "react";
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  BackHandler
} from "react-native";
import Modal from "react-native-modal";
import metrics from "../../Themes/Metrics";
import colors from "../../Themes/Colors";
var Spinner = require("react-native-spinkit");
const axios = require("axios");

export default class Address extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.navigation.state.params.token,
      addressData: [],
      spinvisible: false,
    };
  }

  componentDidMount() {
    console.log("api token", this.state.token);
    this.getUserAddress();
    this.willFocusSubscription = this.props.navigation.addListener(
        "willFocus",
        () => {
          this.getUserAddress();
        }
      );
  }


  componentWillUnmount() {
    this.willFocusSubscription.remove();
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  getUserAddress = async () => {
    this.setState({
      spinvisible: true,
    });
    axios({
      method: "get",
      url: "https://caribmall.com/api/addresses?api_token=" + this.state.token,
    })
      .then((response) => {
        let data = response.data.data;
        console.log("response data --> ", data);
        this.setState({
          addressData: data,
          spinvisible: false,
        });
      })
      .catch((error) => {
        console.log("response coupons errors ", error);
      });
  };

  onDeleteAddress = (item) => {
    console.log("adrres item -->", item);
    this.setState({
      spinvisible: true,
    });
    axios({
      method: "delete",
      url:
        "https://caribmall.com/api/address/" +
        item.id +
        "?api_token=" +
        this.state.token,
    })
      .then((response) => {
        console.log("address delete response --- > ", response.data);

        this.setState({
          spinvisible: false,
        });

        this.getUserAddress();
      })
      .catch((error) => {
        if (error.response) {
          console.log(
            "response addresss response store error ---> ",
            error.response.data
          );
        }
      });
  };

  renderItem = (item) => {
    return (
      <View>
        <TouchableOpacity
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            backgroundColor: colors.white,
            width: "95%",
            alignSelf: "center",
            borderRadius: 10,
          }}
          onPress={() =>
            this.props.navigation.navigate("UpdateAddress", {
              token: this.state.token,
              data: item,
            })
          }
        >
          <View>
            <Text style={styles.heading}>{item.address_type}</Text>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.subHeading}>{item.address_title + " ,"}</Text>
              <Text style={styles.subHeading}>{item.phone}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.subHeading}>
                {item.address_line_1 + " ,"}
              </Text>
              <Text style={styles.subHeading}>{item.address_line_2}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.subHeading}>{item.city + " ,"}</Text>
              <Text style={styles.subHeading}>{item.state.name}</Text>
            </View>
            <Text style={{ ...styles.subHeading, marginBottom: 10 }}>
              {item.country.name}
            </Text>
          </View>
          <View style={{ justifyContent: "center" }}>

          <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("UpdateAddress", {token:this.state.token , data:item})
                  }>
            <View
              style={{
                justifyContent: "center",
                backgroundColor: colors.light_grey_backgroud,
                width: 36,
                height: 36,
                borderRadius: 18,
                alignSelf: "center",
                marginRight: 10,
              }}
            >
              <Image
                style={{
                  height: metrics.dimen_22,
                  width: metrics.dimen_22,
                  resizeMode: "contain",
                  alignSelf: "center",
                }}
                source={require("../../Images/pencil.png")}
              />
            </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.onDeleteAddress(item)}>
              <View
                style={{
                  justifyContent: "center",
                  backgroundColor: colors.light_grey_backgroud,
                  width: 36,
                  height: 36,
                  borderRadius: 18,
                  alignSelf: "center",
                  marginRight: 10,
                  marginTop: 15,
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_22,
                    width: metrics.dimen_22,
                    resizeMode: "contain",
                    alignSelf: "center",
                  }}
                  source={require("../../Images/delete.png")}
                />
              </View>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={{ backgroundColor: colors.light_grey_backgroud, flex: 1 }}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("AddAddress")}>
          <View
            style={{
              flexDirection: "row",
              height: metrics.dimen_50,
              justifyContent: "space-between",
              marginTop: 10,
              backgroundColor: "white",
              borderRadius: 10,
              alignSelf: "center",
              width: "95%",
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                margin: 10,
              }}
            >
              <Image
                style={{
                  height: 20,
                  width: 20,
                  resizeMode: "contain",
                  alignSelf: "center",
                }}
                source={require("../../Images/location.png")}
              />
              <Text
                style={{
                  alignSelf: "center",
                  fontSize: metrics.text_normal,
                  color: colors.theme_caribpay,
                  fontWeight: "bold",
                  marginLeft: 10,
                }}
              >
                Add Address
              </Text>
            </View>

            <Image
              style={{
                height: 20,
                width: 20,
                resizeMode: "contain",
                alignSelf: "center",
                tintColor: colors.black,
                marginRight: 15,
              }}
              source={require("../../Images/plus.png")}
            />
          </View>
        </TouchableOpacity>

        {this.state.addressData.length > 0 && this.state.spinvisible == false && 
          <FlatList
            style={{
              marginTop: metrics.dimen_10,
              marginBottom: metrics.dimen_10,
            }}
            showsVerticalScrollIndicator={false}
            data={this.state.addressData}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => this.renderItem(item)}
          />
        }

        {this.state.addressData.length == 0 &&
            this.state.spinvisible == false && 
              <View style={{ flex: 1, justifyContent: "center" }}>
                <Image
                  style={{
                    height: 150,
                    width: 150,
                    resizeMode: "contain",
                    alignSelf: "center",
                  }}
                  source={require("../../Images/noadd.png")}
                />
                <Text
                  style={{
                    fontFamily: metrics.quicksand_bold,
                    fontSize: metrics.text_large,
                    color: colors.black,
                    marginTop:5,
                    alignSelf:'center'
                  }}>No Address Found!</Text>
              </View>
        }

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisible}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  heading: {
    fontSize: metrics.text_heading,
    fontFamily: metrics.quicksand_bold,
    marginLeft: 10,
  },
  subHeading: {
    fontSize: metrics.text_normal,
    fontFamily: metrics.quicksand_semibold,
    marginLeft: 10,
  },
});
