import React from "react";
import { View, Text } from "react-native";
import metrics from "../../Themes/Metrics";

export default class CancelPayPalTransactions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignSelf: "center" }}>
        <Text style={{ fontFamily: metrics.quicksand_semibold, fontSize: 20 , alignSelf:'center'}}>
          {" "}
          Transaction Cancalled
        </Text>
      </View>
    );
  }
}
