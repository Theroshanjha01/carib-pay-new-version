import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ImageBackground,
  BackHandler,
  ActivityIndicator,
} from "react-native";
import metrics from "../../Themes/Metrics";
import colors from "../../Themes/Colors";
import Slideshow from "../../Slideshow.js";
var Spinner = require("react-native-spinkit");
const axios = require("axios");
import { Input, Icon } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
import StarRating from "react-native-star-rating";
import RBSheet from "react-native-raw-bottom-sheet";
import Modal from "react-native-modal";

export default class CaribMallHome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      position: 0,
      responseData: [],
      dataBannerSource: [],
      categories: [],
      TrendingData: [],
      RecentData: [],
      extraData: [],
      data_cartItems: [],
      cartData: [],
      spinvisible: false,
      caribMallData: [],
      fetching_from_server: false,
      page: 1,
      filters : {
        condition: 'new',
        offers: true,
        price : {
          mininum : '',
          maximum : ''
        }
      }
    };
    console.disableYellowBox = true;
  }

  componentDidMount() {
    this.setState({
      spinvisible: true,
    });
    this.getSliders();
    this.getCategories();
    this.getBannerData();
    this.getCaribMallData();
    this.getCartItems();

    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getCartItems();
      }
    );

    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  getCaribMallData = async () => {
    let caribMallData = await AsyncStorage.getItem("carib_mall_data");
    this.setState({
      caribMallData: JSON.parse(caribMallData),
    });
    // console.log("caribmall Data-->", this.state.caribMallData)
  };

  componentWillUnmount() {
    this.willFocusSubscription.remove();
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  insertAt(array, index, data) {
    array.splice(index, 0, data);
  }

  getCategories() {
    axios
      .get("https://caribmall.com/api/category-grps")
      .then((response) => {
        this.setState({
          categories: response.data.data,
        });
        let arr = this.state.categories.map((item, index) => {
          item.idNum = ++index;
          return { ...item };
        });

        this.setState({
          categories: arr.splice(0, 8),
          fullcategories: response.data.data,
        });

        // console.log("category data ---> ", response.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getSliders() {
    let bannerSource = this.state.dataBannerSource;
    let i;
    axios
      .get("https://caribmall.com/api/sliders")
      .then((response) => {
        // console.log("banner slider --- > ", response.data.data)
        for (i = 0; i < response.data.data.length; i++) {
          let value = response.data.data[i].image.path;
          const bannerData = {
            title: response.data.data[i].title,
            caption: response.data.data[i].sub_title,
            url: value,
          };
          bannerSource.push(bannerData);
          this.setState({
            dataBannerSource: bannerSource,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getBannerData() {
    axios
      .get("https://caribmall.com/api/banners")
      .then((response) => {
        this.setState({
          responseData: response.data.data,
        });
        // console.log("banner data -- > ", response.data.data)
      })
      .catch((err) => {
        console.log(err);
      });

    this.getTrendingProducts();
  }

  getTrendingProducts = () => {
    axios
      .get("https://caribmall.com/api/listings/trending")
      .then((response) => {
        console.log("trending datat list --> ", response.data.data);
        this.setState({
          TrendingData: response.data.data,
          spinvisible: false,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    this.getRecentData();
  };

  getRecentData = () => {
    axios
      .get("https://caribmall.com/api/listings/latest")
      .then((response) => {
        this.setState({
          RecentData: response.data.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    this.getAdditionalProducts();
  };

  getAdditionalProducts = () => {
    axios
      .get("https://caribmall.com/api/listings/random?page=1")
      .then((response) => {
        // console.log(response.data.data)
        // console.log(response.data.links.next)
        this.setState({
          extraData: [...this.state.extraData, ...response.data.data],
          pagination_url: response.data.links.next,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getCartItems = async () => {
    this.setState({
      spinvisible: true,
    });
    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    console.log("token", data.api_token);
    axios
      .get("https://caribmall.com/api/carts?api_token=" + data.api_token)
      .then((response) => {
        this.setState({
          data_cartItems:
          response.data.data.length == 0 ? [] : response.data.data[0].items,
          cartData: response.data.data.length == 0 ? [] : response.data.data,
          spinvisible: false,
        });

      })
      .catch((err) => {
        console.log(err);
        this.setState({
          spinvisible: false,
        });
      });
  };
  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position:
            this.state.position === this.state.dataBannerSource.length
              ? 0
              : this.state.position + 1,
        });
      }, 2000),
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

  loadMoreData = () => {
    this.setState({ fetching_from_server: true }, () => {
      axios
        .get(this.state.pagination_url)
        .then((response) => {
          this.setState({
            extraData: [...this.state.extraData, ...response.data.data],
            pagination_url: response.data.links.next,
            fetching_from_server: false,
          });
          // console.log("Next Page --->", this.state.pagination_url)
        })
        .catch((error) => {
          console.error(error);
        });
    });
  };

  renderFooter() {
    return this.state.pagination_url != null ? (
      <View style={styles.footer}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={this.loadMoreData}
          //On Click of button calling loadMoreData function to load more data
          style={styles.loadMoreBtn}
        >
          <Text style={styles.btnText}>Load More</Text>
          {this.state.fetching_from_server ? (
            <ActivityIndicator color="white" style={{ marginLeft: 8 }} />
          ) : null}
        </TouchableOpacity>
      </View>
    ) : null;
  }
  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }}
      >
        <View style={styles.header}>
          <TouchableOpacity
            style={{
              height: 24,
              width: 24,
              tintColor: "white",
              alignSelf: "center",
            }}
            onPress={() => this.props.navigation.goBack(null)}
          >
            <Image
              style={{
                height: 24,
                width: 24,
                tintColor: colors.white,
                alignSelf: "center",
                marginLeft: 15,
                marginTop: 1,
              }}
              source={require("../../Images/back-button.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              height: 35,
              flexDirection: "row",
              width: "87%",
              borderRadius: 20,
              alignSelf: "center",
              marginLeft: 7,
            }}
            onPress={() => this.props.navigation.navigate("Search")
          }
          >
            <Input
              containerStyle={{
                alignSelf: "center",
                backgroundColor: colors.white,
                height: 35,
                width: "100%",
                borderRadius: 20,
                marginLeft: 10,
              }}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              placeholder="Type item name to search"
              editable={false}
              leftIcon={
                <Image
                  source={require("../../Images/find.png")}
                  style={{
                    height: 24,
                    width: 24,
                    resizeMode: "contain",
                    tintColor: colors.app_gray,
                    marginTop: -10,
                  }}
                />
              }
              placeholderTextColor="#9D9D9F"
              inputStyle={{
                color: colors.black,
                fontSize: metrics.text_normal,
                marginTop: -10,
              }}
              value={this.state.text}
              onChangeText={(text) => console.log(text)}
            />
          </TouchableOpacity>
        </View>

        {this.state.spinvisible ? (
          <View
            style={{ flex: 1, justifyContent: "center", alignSelf: "center" }}
          >
            <View style={{ width: 100, borderRadius: metrics.dimen_5 }}>
              <Spinner
                style={{ alignSelf: "center" }}
                isVisible={this.state.spinvisible}
                size={70}
                type={"ThreeBounce"}
                color={colors.black}
              />
            </View>
          </View>
        ) : (
          <View style={{ flex: 1 }}>
            <View
              style={{
                flexDirection: "row",
                marginRight: 10,
                justifyContent: "space-between",
                marginLeft: 10,
              }}
            >
              <View style={{ flexDirection: "row", justifyContent: "center" }}>
                <Text
                  style={{
                    color: colors.theme_caribpay,
                    fontSize: metrics.text_header,
                    textAlignVertical: "center",
                    fontWeight: "bold",
                  }}
                >
                  Best Match
                </Text>
                <Image
                  style={{
                    height: metrics.dimen_20,
                    width: metrics.dimen_20,
                    tintColor: colors.theme_caribpay,
                    alignSelf: "center",
                    marginLeft: 7,
                  }}
                  source={require("../../Images/dropdown.png")}
                />
              </View>

              <View
                style={{
                  flexDirection: "row",
                  height: 50,
                  backgroundColor: colors.light_grey_backgroud,
                  marginRight: 5,
                }}
              >
                {/* <Image
                  style={{ height: 30, width: 30, alignSelf: "center" }}
                  source={require("../../Images/newArr.png")}
                />
                <Image
                  style={{
                    height: 30,
                    width: 30,
                    alignSelf: "center",
                    marginLeft: 30,
                  }}
                  source={require("../../Images/free.png")}
                />
                <TouchableOpacity
                  style={{
                    height: 30,
                    width: 30,
                    tintColor: "white",
                    alignSelf: "center",
                  }}
                  onPress={() => this.RBSheetAlert.open()}
                >
                  <Image
                    style={{
                      height: 22,
                      width: 22,
                      alignSelf: "center",
                      marginLeft: 30,
                      marginTop: 4,
                    }}
                    source={require("../../Images/filter.png")}
                  />
                </TouchableOpacity> */}
              </View>
            </View>

            <ScrollView showsVerticalScrollIndicator={false}>
              <View>
                <Slideshow
                  height={120}
                  resizeMode="contain"
                  dataSource={this.state.dataBannerSource}
                  indicatorColor={colors.white}
                  indicatorSelectedColor={colors.app_gray}
                  arrowSize={0}
                  position={this.state.position}
                  onPositionChanged={(position) => this.setState({ position })}
                  captionStyle={{
                    color: "white",
                    fontWeight: "400",
                    fontSize: metrics.text_normal,
                  }}
                  titleStyle={{
                    color: "white",
                    fontWeight: "800",
                    fontSize: metrics.text_medium,
                  }}
                  containerStyle={{ height: 150 }}
                />

                {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={{ fontSize: metrics.text_header, color: colors.heading_black_text, marginLeft: 10, fontWeight: 'bold' }}>All Categories</Text>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate("ShopMore", { data: this.state.categories })}>
                                        <Text style={{ fontSize: metrics.text_normal, color: colors.heading_black_text, marginRight: 10 }}>Shop More</Text>
                                    </TouchableOpacity>
                                </View> */}

                {/* </View> */}

                <View
                  style={{
                    justifyContent: "center",
                    alignSelf: "center",
                    backgroundColor: colors.white,
                    width: "95%",
                    height: 220,
                    borderRadius: metrics.dimen_5,
                    marginTop: 10,
                    borderRadius: 7,
                    shadowColor: "white",
                    shadowOffset: { width: 1, height: 2 },
                    shadowOpacity: 0.9,
                    shadowRadius: 3,
                    elevation: 1,
                  }}
                >
                  <FlatList
                    style={{ marginTop: 7 }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.categories}
                    numColumns={4}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                      <View
                        style={{
                          backgroundColor: colors.white,
                          margin: 10,
                          flex: 1 / 4,
                        }}
                      >
                        <TouchableOpacity
                          onPress={() =>
                            item.idNum == 8
                              ? this.props.navigation.navigate("ShopMore", {
                                  data: this.state.fullcategories,
                                })
                              : this.props.navigation.navigate(
                                  "CategoryProduct",
                                  {
                                    titleName: item.name,
                                    id: item.id,
                                    slug: item.slug,
                                  }
                                )
                          }
                        >
                          {item.id == 3 && (
                            <Image
                              style={{
                                height: 45,
                                width: 45,
                                resizeMode: "contain",
                                alignSelf: "center",
                              }}
                              source={require("../../Images/kids.png")}
                            />
                          )}
                          {item.id == 6 && (
                            <Image
                              style={{
                                height: 45,
                                width: 45,
                                resizeMode: "contain",
                                alignSelf: "center",
                              }}
                              source={require("../../Images/sports.png")}
                            />
                          )}
                          {item.id == 7 && (
                            <Image
                              style={{
                                height: 45,
                                width: 45,
                                resizeMode: "contain",
                                alignSelf: "center",
                              }}
                              source={require("../../Images/jewels.png")}
                            />
                          )}
                          {item.id == 8 && (
                            <Image
                              style={{
                                height: 45,
                                width: 45,
                                resizeMode: "contain",
                                alignSelf: "center",
                              }}
                              source={require("../../Images/dog.png")}
                            />
                          )}
                          {item.id == 9 && (
                            <Image
                              style={{
                                height: 45,
                                width: 45,
                                resizeMode: "contain",
                                alignSelf: "center",
                              }}
                              source={require("../../Images/hobbies.png")}
                            />
                          )}
                          {item.id == 10 && (
                            <Image
                              style={{
                                height: 45,
                                width: 45,
                                resizeMode: "contain",
                                alignSelf: "center",
                              }}
                              source={require("../../Images/mens.png")}
                            />
                          )}

                          {item.id == 14 && (
                            <Image
                              style={{
                                height: 45,
                                width: 45,
                                resizeMode: "contain",
                                alignSelf: "center",
                              }}
                              source={require("../../Images/hk.png")}
                            />
                          )}

                          {item.idNum == 8 && (
                            <Image
                              style={{
                                height: 45,
                                width: 45,
                                resizeMode: "contain",
                                alignSelf: "center",
                              }}
                              source={require("../../Images/morebtn.png")}
                            />
                          )}

                          <Text
                            style={{
                              fontSize: 12,
                              fontFamily: metrics.quicksand_semibold,
                              color: colors.heading_black_text,
                              marginTop: 2,
                              alignSelf: "center",
                              marginLeft:
                                item.id == 14 || item.id == 9 || item.id == 3
                                  ? 15
                                  : 0,
                            }}
                          >
                            {item.idNum == 8 ? "More" : item.name}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                  />
                </View>

                <View style={{ marginTop: 5, marginLeft: 10 }}>
                  <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={this.state.responseData}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                      <View
                        style={{
                          margin: metrics.dimen_5,
                          justifyContent: "center",
                          width: Dimensions.get("window").width - 25,
                          height: 100,
                          alignSelf: "center",
                        }}
                      >
                        <ImageBackground
                          style={{
                            width: "100%",
                            height: 100,
                          }}
                          source={{ uri: item.bg_image }}
                          imageStyle={{ borderRadius: 15 }}
                        >
                          <TouchableOpacity
                            onPress={() => console.log("cdjhfbdh")}
                          >
                            <View
                              style={{
                                flexDirection: "row",
                                justifyContent: "space-between",
                                marginVertical: 10,
                              }}
                            >
                              <View
                                style={{
                                  flexDirection: "column",
                                  marginLeft: 10,
                                  justifyContent: "center",
                                }}
                              >
                                <Text
                                  style={{
                                    fontSize: metrics.text_large,
                                    color: colors.whitesmoke,
                                    marginTop: 2,
                                  }}
                                >
                                  {item.title}
                                </Text>
                                <Text
                                  style={{
                                    fontSize: metrics.text_large,
                                    color: colors.whitesmoke,
                                    marginTop: 2,
                                  }}
                                >
                                  {item.description}
                                </Text>
                                <Text
                                  style={{
                                    fontSize: metrics.text_header,
                                    color: colors.whitesmoke,
                                    marginTop: 2,
                                  }}
                                >
                                  {item.link_label}
                                </Text>
                              </View>
                              <View
                                style={{
                                  alignSelf: "center",
                                  marginRight: 10,
                                  justifyContent: "center",
                                }}
                              >
                                <Image
                                  style={{
                                    height: 85,
                                    width: 85,
                                    resizeMode: "contain",
                                    alignSelf: "center",
                                  }}
                                  source={{ uri: item.image }}
                                />
                              </View>
                            </View>
                          </TouchableOpacity>
                        </ImageBackground>
                      </View>
                    )}
                  />
                </View>

                <View
                  style={{
                    backgroundColor: colors.white,
                    marginTop: 10,
                    marginLeft: 10,
                    marginRight: 10,
                    borderRadius: 10,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        color: "#696969",
                        marginLeft: 7,
                        marginTop: 15,
                        fontWeight: "bold",
                      }}
                    >
                      {"Trending Now"}
                    </Text>
                   
                  </View>
                  <FlatList
                    style={{
                      marginTop: metrics.dimen_10,
                      marginBottom: metrics.dimen_30,
                      backgroundColor: colors.white,
                    }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.TrendingData}
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                      <View
                        style={{
                          flex: 1 / 2,
                          backgroundColor: colors.white,
                          borderRadius: 10,
                          margin: 5,
                          flexDirection: "column",
                          borderColor: colors.light_grey_backgroud,
                          borderWidth: 1,
                        }}
                      >
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate("ProductDetails", {
                              slug: item.slug,
                            })
                          }
                        >
                          <View
                            style={{
                              borderTopLeftRadius: 10,
                              borderTopRightRadius: 10,
                            }}
                          >
                            <Image
                              source={{ uri: item.image }}
                              style={{
                                height: metrics.dimen_200,
                                width: "100%",
                                borderTopLeftRadius: 10,
                                borderTopRightRadius: 10,
                              }}
                            />
                            <View
                              style={{
                                position: "absolute",
                                top: -7,
                                alignSelf: "flex-end",
                              }}
                            >
                              <Image
                                source={require("../../Images/tag.png")}
                                style={{ height: 30, width: 35 }}
                              />
                              <Text
                                style={{
                                  position: "absolute",
                                  top: 1,
                                  fontSize: 10,
                                  color: colors.white,
                                  alignSelf: "center",
                                  marginLeft: 10,
                                  marginTop: 7,
                                }}
                              >
                                {item.discount == null
                                  ? "0%"
                                  : item.discount.substring(0, 4)}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              marginLeft: 5,
                              marginBottom: 5,
                              marginTop: 10,
                            }}
                          >
                            <Text
                              style={{ color: colors.app_gray }}
                              numberOfLines={1}
                            >
                              {item.title}
                            </Text>
                            <View style={{ flexDirection: "row" }}>
                              {item.offer_price != null && (
                                <Text
                                  style={{
                                    alignSelf: "center",
                                    fontSize: 15,
                                    color: colors.app_orange_color,
                                    marginTop: 2,
                                    fontWeight: "bold",
                                  }}
                                >
                                  {item.offer_price}
                                </Text>
                              )}
                              <Text
                                style={{
                                  alignSelf: "center",
                                  color:
                                    item.offer_price == null
                                      ? colors.app_orange_color
                                      : colors.app_gray,
                                  fontSize: 12,
                                  textDecorationLine:
                                    item.offer_price == null
                                      ? null
                                      : "line-through",
                                  marginLeft: item.offer_price == null ? 0 : 10,
                                }}
                              >
                                <Text
                                  style={{
                                    alignSelf: "center",
                                    fontSize:
                                      item.offer_price == null ? 15 : 12,
                                    color:
                                      item.offer_price == null
                                        ? colors.app_orange_color
                                        : colors.app_gray,
                                    marginTop: 2,
                                    fontWeight: "bold",
                                    textDecorationLine:
                                      item.offer_price == null
                                        ? null
                                        : "line-through",
                                    marginLeft:
                                      item.offer_price == null ? 0 : 10,
                                  }}
                                >
                                  {item.price}
                                </Text>
                              </Text>
                            </View>
                            <View style={{ width: 40, marginTop: 3 }}>
                              <StarRating
                                disabled={false}
                                maxStars={5}
                                starSize={10}
                                buttonStyle={{ margin: 1 }}
                                halfStarEnabled={true}
                                fullStarColor={"#FEC750"}
                                rating={item.rating == null ? 0 : item.rating}
                              />
                            </View>

                            {/* <Text style={{ color: colors.app_gray }}>{"St. Lucia"}</Text> */}
                          </View>
                        </TouchableOpacity>
                      </View>
                    )}
                  />
                </View>

                <View
                  style={{
                    backgroundColor: "white",
                    marginTop: 10,
                    marginLeft: 10,
                    marginRight: 10,
                    borderRadius: 10,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        color: "#696969",
                        marginLeft: 7,
                        marginTop: 15,
                        fontWeight: "bold",
                      }}
                    >
                      {"Recently Added"}
                    </Text>
                  
                  </View>
                  <FlatList
                    style={{
                      marginTop: metrics.dimen_10,
                      marginBottom: metrics.dimen_30,
                    }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.RecentData}
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                      <View
                        style={{
                          flex: 1 / 2,
                          backgroundColor: colors.white,
                          borderRadius: 10,
                          margin: 5,
                          flexDirection: "column",
                          borderColor: colors.light_grey_backgroud,
                          borderWidth: 1,
                        }}
                      >
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate("ProductDetails", {
                              slug: item.slug,
                            })
                          }
                        >
                          <View
                            style={{
                              borderTopLeftRadius: 10,
                              borderTopRightRadius: 10,
                            }}
                          >
                            <Image
                              source={{ uri: item.image }}
                              style={{
                                height: metrics.dimen_200,
                                width: "100%",
                                borderTopLeftRadius: 10,
                                borderTopRightRadius: 10,
                              }}
                            />
                            <View
                              style={{
                                position: "absolute",
                                top: -7,
                                alignSelf: "flex-end",
                              }}
                            >
                              <Image
                                source={require("../../Images/tag.png")}
                                style={{ height: 30, width: 35 }}
                              />
                              <Text
                                style={{
                                  position: "absolute",
                                  top: 1,
                                  fontSize: 10,
                                  color: colors.white,
                                  alignSelf: "center",
                                  marginLeft: 10,
                                  marginTop: 7,
                                }}
                              >
                                {item.discount == null
                                  ? "0%"
                                  : item.discount.substring(0, 4)}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              marginLeft: 5,
                              marginBottom: 5,
                              marginTop: 10,
                            }}
                          >
                            <Text
                              style={{ color: colors.app_gray }}
                              numberOfLines={1}
                            >
                              {item.title}
                            </Text>
                            <View style={{ flexDirection: "row" }}>
                              {item.offer_price != null && (
                                <Text
                                  style={{
                                    alignSelf: "center",
                                    fontSize: 15,
                                    color: colors.app_orange_color,
                                    marginTop: 2,
                                    fontWeight: "bold",
                                  }}
                                >
                                  {item.offer_price}
                                </Text>
                              )}
                              <Text
                                style={{
                                  alignSelf: "center",
                                  color:
                                    item.offer_price == null
                                      ? colors.app_orange_color
                                      : colors.app_gray,
                                  fontSize: 12,
                                  textDecorationLine:
                                    item.offer_price == null
                                      ? null
                                      : "line-through",
                                  marginLeft: item.offer_price == null ? 0 : 10,
                                }}
                              >
                                <Text
                                  style={{
                                    alignSelf: "center",
                                    fontSize:
                                      item.offer_price == null ? 15 : 12,
                                    color:
                                      item.offer_price == null
                                        ? colors.app_orange_color
                                        : colors.app_gray,
                                    marginTop: 2,
                                    fontWeight: "bold",
                                    textDecorationLine:
                                      item.offer_price == null
                                        ? null
                                        : "line-through",
                                    marginLeft:
                                      item.offer_price == null ? 0 : 10,
                                  }}
                                >
                                  {item.price}
                                </Text>
                              </Text>
                            </View>
                            <View style={{ width: 40, marginTop: 3 }}>
                              <StarRating
                                disabled={false}
                                maxStars={5}
                                starSize={10}
                                buttonStyle={{ margin: 1 }}
                                halfStarEnabled={true}
                                fullStarColor={"#FEC750"}
                                rating={item.rating == null ? 0 : item.rating}
                              />
                            </View>

                            {/* <Text style={{ color: colors.app_gray }}>{"St. Lucia"}</Text> */}
                          </View>
                        </TouchableOpacity>
                      </View>
                    )}
                  />
                </View>

                <View
                  style={{
                    backgroundColor: "white",
                    marginTop: 10,
                    marginLeft: 10,
                    marginRight: 10,
                    borderRadius: 10,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        color: "#696969",
                        marginLeft: 7,
                        marginTop: 15,
                        fontWeight: "bold",
                      }}
                    >
                      {"Additional Item to Explore"}
                    </Text>
               
                  </View>
                  <FlatList
                    style={{
                      marginTop: metrics.dimen_10,
                      marginBottom: metrics.dimen_30,
                    }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.extraData}
                    numColumns={2}
                    ListFooterComponent={this.renderFooter.bind(this)}
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                      <View
                        style={{
                          flex: 1 / 2,
                          backgroundColor: colors.white,
                          borderRadius: 10,
                          margin: 5,
                          flexDirection: "column",
                          borderColor: colors.light_grey_backgroud,
                          borderWidth: 1,
                        }}
                      >
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate("ProductDetails", {
                              slug: item.slug,
                            })
                          }
                        >
                          <View
                            style={{
                              borderTopLeftRadius: 10,
                              borderTopRightRadius: 10,
                            }}
                          >
                            <Image
                              source={{ uri: item.image }}
                              style={{
                                height: metrics.dimen_200,
                                width: "100%",
                                borderTopLeftRadius: 10,
                                borderTopRightRadius: 10,
                              }}
                            />
                            <View
                              style={{
                                position: "absolute",
                                top: -7,
                                alignSelf: "flex-end",
                              }}
                            >
                              <Image
                                source={require("../../Images/tag.png")}
                                style={{ height: 30, width: 35 }}
                              />
                              <Text
                                style={{
                                  position: "absolute",
                                  top: 1,
                                  fontSize: 10,
                                  color: colors.white,
                                  alignSelf: "center",
                                  marginLeft: 10,
                                  marginTop: 7,
                                }}
                              >
                                {item.discount == null
                                  ? "0%"
                                  : item.discount.substring(0, 4)}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              marginLeft: 5,
                              marginBottom: 5,
                              marginTop: 10,
                            }}
                          >
                            <Text
                              style={{ color: colors.app_gray }}
                              numberOfLines={1}
                            >
                              {item.title}
                            </Text>
                            <View style={{ flexDirection: "row" }}>
                              {item.offer_price != null && (
                                <Text
                                  style={{
                                    alignSelf: "center",
                                    fontSize: 15,
                                    color: colors.app_orange_color,
                                    marginTop: 2,
                                    fontWeight: "bold",
                                  }}
                                >
                                  {item.offer_price}
                                </Text>
                              )}
                              <Text
                                style={{
                                  alignSelf: "center",
                                  color:
                                    item.offer_price == null
                                      ? colors.app_orange_color
                                      : colors.app_gray,
                                  fontSize: 12,
                                  textDecorationLine:
                                    item.offer_price == null
                                      ? null
                                      : "line-through",
                                  marginLeft: item.offer_price == null ? 0 : 10,
                                }}
                              >
                                <Text
                                  style={{
                                    alignSelf: "center",
                                    fontSize:
                                      item.offer_price == null ? 15 : 12,
                                    color:
                                      item.offer_price == null
                                        ? colors.app_orange_color
                                        : colors.app_gray,
                                    marginTop: 2,
                                    fontWeight: "bold",
                                    textDecorationLine:
                                      item.offer_price == null
                                        ? null
                                        : "line-through",
                                    marginLeft:
                                      item.offer_price == null ? 0 : 10,
                                  }}
                                >
                                  {item.price}
                                </Text>
                              </Text>
                            </View>
                            <View style={{ width: 40, marginTop: 3 }}>
                              <StarRating
                                disabled={false}
                                maxStars={5}
                                starSize={10}
                                buttonStyle={{ margin: 1 }}
                                halfStarEnabled={true}
                                fullStarColor={"#FEC750"}
                                rating={item.rating == null ? 0 : item.rating}
                              />
                            </View>

                            {/* <Text style={{ color: colors.app_gray }}>{"St. Lucia"}</Text> */}
                          </View>
                        </TouchableOpacity>
                      </View>
                    )}
                  />
                </View>

                {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 16, color: '#696969', marginLeft: 7, marginTop: 15, fontWeight: 'bold' }}>{"Additional Items to Explore"}</Text>
                                    <Text style={{ borderWidth: 0.5, fontSize: metrics.text_normal, color: colors.app_blue, marginRight: 10, marginTop: 15, borderColor: colors.theme_caribpay, borderRadius: 15, padding: 5 }}>{"Shop More"}</Text>
                                </View> */}

                {/* <View style={{ alignSelf: 'center', borderRadius: 10, width: '90%' }}>
                                    <FlatList
                                        style={{ marginTop: metrics.dimen_10, marginBottom: metrics.dimen_70 }}
                                        showsVerticalScrollIndicator={false}
                                        data={this.state.extraData}
                                        ListFooterComponent={this.renderFooter.bind(this)}
                                        numColumns={2}
                                        keyExtractor={(item, index) => index.toString()}
                                        renderItem={({ item }) => (
                                            <View style={{
                                                flex: 1 / 2, backgroundColor: colors.white, borderRadius: 10, margin: 5, flexDirection: 'column', borderColor: colors.light_grey_backgroud, borderWidth: 1
                                            }}>
                                                <TouchableOpacity onPress={() => this.props.navigation.navigate("ProductDetails", { slug: item.slug })}>
                                                    <View style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                                                        <Image source={{ uri: item.image }}
                                                            style={{ height: metrics.dimen_200, width: "100%", borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                                                        </Image>
                                                        <View style={{ position: 'absolute', top: -7, alignSelf: 'flex-end' }}>
                                                            <Image source={require('../../Images/tag.png')}
                                                                style={{ height: 30, width: 35 }}>
                                                            </Image>
                                                            <Text style={{ position: 'absolute', top: 1, fontSize: 10, color: colors.white, alignSelf: 'center', marginLeft: 10, marginTop: 7 }}>{item.discount == null ? "0%" : item.discount.substring(0, 4)}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ marginLeft: 5, marginBottom: 5, marginTop: 10 }}>
                                                        <Text style={{ color: colors.app_gray }} numberOfLines={1}>{item.title}</Text>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            {item.offer_price != null && <Text style={{ color: colors.app_orange_color, fontSize: 12 }}>EC$<Text style={{ alignSelf: 'center', fontSize: 15, color: colors.app_orange_color, marginTop: 2, fontWeight: 'bold' }}>{item.offer_price.substring(3)}</Text></Text>}
                                                            <Text style={{ alignSelf: 'center', color: item.offer_price == null ? colors.app_orange_color : colors.app_gray, fontSize: 12, textDecorationLine: item.offer_price == null ? null : 'line-through', marginLeft: item.offer_price == null ? 0 : 10 }}>EC$<Text style={{ alignSelf: 'center', fontSize: item.offer_price == null ? 15 : 12, color: item.offer_price == null ? colors.app_orange_color : colors.app_gray, marginTop: 2, fontWeight: 'bold', textDecorationLine: item.offer_price == null ? null : 'line-through', marginLeft: item.offer_price == null ? 0 : 10 }}>{item.price.substring(3)}</Text></Text>
                                                        </View>
                                                        <View style={{ width: 40, marginTop: 3 }}>
                                                            <StarRating
                                                                disabled={false}
                                                                maxStars={5}
                                                                starSize={10}
                                                                buttonStyle={{ margin: 1 }}
                                                                halfStarEnabled={true}
                                                                fullStarColor={'#FEC750'}
                                                                rating={item.rating == null ? 0 : item.rating} />
                                                        </View>

                                                        {/* <Text style={{ color: colors.app_gray }}>{"St. Lucia"}</Text> */}

                {/* </View>
                                                </TouchableOpacity>
                                            </View>
                                        )} />
                                // </View> */}
              </View>
            </ScrollView>

            <View
              style={{
                backgroundColor: colors.theme_caribpay,
                height: metrics.dimen_55,
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                }}
              >
                <TouchableOpacity
                  style={styles.bottomTabView}
                  onPress={() => this.props.navigation.navigate("Orders")}
                >
                  <View>
                    <Image
                      style={styles.bottomtabImage}
                      source={require("../../Images/ordersss.png")}
                    />
                    <Text style={styles.bottomtabText}>Orders</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.bottomTabView}
                  onPress={() =>
                    this.setState({
                      isComingSoon: true,
                    })
                  }
                >
                  <View>
                    <Image
                      style={styles.bottomtabImage}
                      source={require("../../Images/comment.png")}
                    />
                    <Text style={styles.bottomtabText}>Chat</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.bottomTabView}
                  onPress={() =>
                    this.props.navigation.navigate("Cart", {
                      item: this.state.data_cartItems,
                      shopSlug:
                        this.state.cartData.length > 0
                          ? this.state.cartData[0].shop.slug
                          : "",
                    })
                  }
                >
                  <View>
                    <Image
                      style={styles.bottomtabImage}
                      source={require("../../Images/supermarket.png")}
                    />
                    <Text style={styles.bottomtabText}>Cart</Text>
                    {this.state.data_cartItems.length > 0 && (
                      <View style={styles.bottomcartCounts}>
                        <Text
                          style={{ color: colors.white, textAlign: "center" }}
                        >
                          {this.state.data_cartItems.length}
                        </Text>
                      </View>
                    )}
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.bottomTabView}
                  onPress={() =>
                    this.props.navigation.navigate("CaribMallProfile")
                  }
                >
                  <View>
                    <Image
                      style={styles.bottomtabImage}
                      source={require("../../Images/gear.png")}
                    />
                    <Text style={styles.bottomtabText}>Settings</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        )}

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.isComingSoon}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                // margin: metrics.dimen_10,
                flexDirection: "column",
                height: 200,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isComingSoon: false,
                    })
                  }
                >
                  <Image
                    style={{
                      height: metrics.dimen_16,
                      width: metrics.dimen_16,
                      resizeMode: "contain",
                      margin: 15,
                    }}
                    source={require("../../Images/cross-sign.png")}
                  />
                </TouchableOpacity>
                <Image
                  style={{
                    height: 150,
                    width: 150,
                    resizeMode: "cover",
                    alignSelf: "center",
                    marginTop: -30,
                  }}
                  source={require("../../Images/soon.png")}
                />
                <Text
                  style={{
                    fontSize: metrics.text_heading,
                    color: colors.black,
                    fontFamily: metrics.quicksand_bold,
                    textAlign: "center",
                    marginTop: -20,
                  }}
                >
                  {"Chat Services"}
                </Text>
              </View>
            </View>
          </Modal>
        </View>

        {/* {!this.state.spinvisible && (
          <View>
            <TouchableOpacity
              style={{
                position: "absolute",
                alignItems: "flex-end",
                height: 60,
                width: 60,
                borderRadius: 30,
                backgroundColor: colors.theme_caribpay,
                bottom: 35,
                right: 20,
                borderWidth: 3,
                borderColor: "#ddd",
                shadowColor: colors.app_gray,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.9,
                justifyContent: "center",
              }}
              onPress={() =>
                this.props.navigation.navigate("Cart", {
                  item: this.state.data_cartItems,
                  shopSlug:
                    this.state.cartData.length > 0
                      ? this.state.cartData[0].shop.slug
                      : "",
                })
              }
            >
              <Image
                style={{
                  height: 24,
                  width: 24,
                  resizeMode: "contain",
                  alignSelf: "center",
                  tintColor: colors.white,
                }}
                source={require("../../Images/supermarket.png")}
              />
              {this.state.data_cartItems.length > 0 && (
                <View
                  style={{
                    position: "absolute",
                    backgroundColor: "red",
                    height: 20,
                    width: 20,
                    borderRadius: 10,
                    marginLeft: 15,
                    top: -5,
                  }}
                >
                  <Text style={{ color: colors.white, textAlign: "center" }}>
                    {this.state.data_cartItems.length}
                  </Text>
                </View>
              )}
            </TouchableOpacity>
          </View>
        )} */}

      
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: metrics.dimen_60,
    backgroundColor: colors.carib_pay_blue,
    flexDirection: "row",
    // justifyContent: 'center'
  },
  headerleftImage: {
    height: metrics.dimen_24,
    width: metrics.dimen_24,
    alignSelf: "center",
    resizeMode: "contain",
    tintColor: colors.whitesmoke,
  },
  headertextStyle: {
    fontSize: metrics.text_17,
    fontWeight: "200",
    color: colors.white,
    alignSelf: "center",
    marginHorizontal: 100,
  },

  absouluteview: {
    position: "absolute",
    top: 51,
    borderTopRightRadius: metrics.dimen_20,
    borderTopLeftRadius: metrics.dimen_20,
    width: "100%",
    height: Dimensions.get("screen").height,
    backgroundColor: colors.white,
  },
  container: { flexDirection: "row", margin: metrics.dimen_15 },
  image_style: {
    height: metrics.dimen_25,
    width: metrics.dimen_25,
    resizeMode: "contain",
  },
  headingg: {
    fontSize: metrics.text_header,
    color: colors.black,
    textAlignVertical: "center",
  },
  descriiption: {
    fontSize: metrics.text_description,
    color: colors.app_gray,
    fontWeight: "300",
  },
  arrowstyle: {
    height: metrics.dimen_15,
    width: metrics.dimen_15,
    resizeMode: "contain",
  },
  horizontalLine: {
    borderBottomColor: "#D3D3D3",
    borderBottomWidth: 0.4,
    width: "90%",
    alignSelf: "center",
  },
  footer: {
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: colors.theme_caribpay,
    borderRadius: 20,
    width: 120,
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  btnText: {
    color: "white",
    fontSize: 15,
    textAlign: "center",
  },
  bottomTabView: { flex: 1 / 4, justifyContent: "center", margin: 5 },
  bottomtabImage: {
    height: 24,
    width: 24,
    resizeMode: "contain",
    tintColor: colors.white,
    alignSelf: "center",
  },
  bottomtabText: {
    fontFamily: metrics.quicksand_bold,
    fontSize: metrics.text_medium,
    color: colors.white,
    marginLeft: -1,
    alignSelf: "center",
  },
  bottomcartCounts: {
    position: "absolute",
    backgroundColor: "red",
    height: 20,
    width: 20,
    borderRadius: 10,
    marginLeft: 50,
    top: -5,
  },
});
