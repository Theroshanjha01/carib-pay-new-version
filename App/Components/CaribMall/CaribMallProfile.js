import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
import AsyncStorage from "@react-native-community/async-storage";
const axios = require("axios");
import Modal from "react-native-modal";
var Spinner = require("react-native-spinkit");

export default class CaribMallProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      caribMallData: [],
      name: "",
    };
  }

  componentDidMount() {
    this.getCaribMallData();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  getCaribMallData = async () => {
    let caribMallData = await AsyncStorage.getItem("carib_mall_data");
    this.setState({
      caribMallData: JSON.parse(caribMallData),
      spinvisible: true,
    });
    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    console.log("token", data.api_token);
    this.setState({
      token: data.api_token,
    });
    axios({
      method: "get",
      url: "https://caribmall.com/api/dashboard?api_token=" + data.api_token,
    })
      .then((response) => {
        console.log("response--->", response.data.data);
        this.setState({
          wishlisht_length: response.data.data.wishlists_count,
          coupons_length: response.data.data.coupons_count,
          disputes_length: response.data.data.disputes_count,
          orders_length: response.data.data.orders_count,
          spinvisible: false,
        });
      })
      .catch((error) => {
        console.log("response coupons errors ", error);
      });
  };

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }}
      >
        <View
          style={{
            backgroundColor: colors.theme_caribpay,
            height: metrics.newView.upperview,
          }}
        >
          <View style={{ flexDirection: "row", margin: 10 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
            >
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                  }}
                  source={require("../../Images/leftarrow.png")}
                />
              </View>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: metrics.text_heading,
                color: colors.white,
                fontWeight: "bold",
                marginTop: 2,
                marginLeft: 10,
              }}
            >
              Settings
            </Text>
          </View>
        </View>
        <View style={styles.curveview}>
          <View
            style={{
              flexDirection: "row",
              backgroundColor: colors.white,
              height: 100,
              width: "95%",
              alignSelf: "center",
              borderRadius: 10,
              marginTop: 15,
            }}
          >
            <TouchableOpacity
              style={{
                flexDirection: "column",
                height: 100,
                flex: 1 / 4,
                alignSelf: "center",
                justifyContent: "center",
              }}
              onPress={() => this.props.navigation.navigate("Orders")}
            >
              <View
                style={{
                  flexDirection: "column",
                  height: 100,
                  flex: 1 / 4,
                  alignSelf: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_26,
                    color: colors.theme_caribpay,
                    fontWeight: "bold",
                    textAlign: "center",
                  }}
                >
                  {this.state.orders_length}
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    color: colors.black,
                    textAlign: "center",
                  }}
                >
                  Orders
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                flexDirection: "column",
                height: 100,
                flex: 1 / 4,
                alignSelf: "center",
                justifyContent: "center",
              }}
              onPress={() => this.props.navigation.navigate("Coupons")}
            >
              <View
                style={{
                  flexDirection: "column",
                  height: 100,
                  flex: 1 / 4,
                  alignSelf: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_26,
                    color: colors.theme_caribpay,
                    fontWeight: "bold",
                    textAlign: "center",
                  }}
                >
                  {this.state.coupons_length}
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    color: colors.black,
                    textAlign: "center",
                  }}
                >
                  Coupons
                </Text>
              </View>
            </TouchableOpacity>

            {/* <TouchableOpacity style={{ flexDirection: 'column', height: 100, flex: 1 / 4, alignSelf: 'center', justifyContent: 'center' }}
                            onPress={() => this.props.navigation.navigate("Disputes")}>
                            <View style={{ flexDirection: 'column', height: 100, flex: 1 / 4, alignSelf: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: metrics.text_26, color: colors.theme_caribpay, fontWeight: 'bold', textAlign: 'center' }}>{this.state.disputes_length}</Text>
                                <Text style={{ fontSize: metrics.text_header, color: colors.black, textAlign: 'center' }}>Disputes</Text>
                            </View>
                        </TouchableOpacity> */}

            <TouchableOpacity
              style={{
                flexDirection: "column",
                height: 100,
                flex: 1 / 4,
                alignSelf: "center",
                justifyContent: "center",
              }}
              onPress={() => this.props.navigation.navigate("Wishlist")}
            >
              <View
                style={{
                  flexDirection: "column",
                  height: 100,
                  flex: 1 / 4,
                  alignSelf: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_26,
                    color: colors.theme_caribpay,
                    fontWeight: "bold",
                    textAlign: "center",
                  }}
                >
                  {this.state.wishlisht_length}
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    color: colors.black,
                    textAlign: "center",
                  }}
                >
                  Wishlist
                </Text>
              </View>
            </TouchableOpacity>


            <TouchableOpacity
              style={{
                flexDirection: "column",
                height: 100,
                flex: 1 / 4,
                alignSelf: "center",
                justifyContent: "center",
              }}
              onPress={() => this.props.navigation.navigate("Disputes", {token:this.state.token})}
            >
              <View
                style={{
                  flexDirection: "column",
                  height: 100,
                  flex: 1 / 4,
                  alignSelf: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_26,
                    color: colors.theme_caribpay,
                    fontWeight: "bold",
                    textAlign: "center",
                  }}
                >
                  {this.state.disputes_length}
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    color: colors.black,
                    textAlign: "center",
                  }}
                >
                  Disputes
                </Text>
              </View>
            </TouchableOpacity>


          </View>

          {/* <TouchableOpacity onPress={() => this.props.navigation.navigate("AccountDetails", { name: this.state.caribMallData.name })}>
                        <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', marginTop: 10, backgroundColor: colors.white, borderRadius: 10, width: '95%', alignSelf: 'center' }}>
                            <View style={{ height: 60, width: 60, justifyContent: 'center', alignSelf: 'center', marginLeft: 10 }}>
                                <Image style={{ height: 50, width: 50, alignSelf: 'center', borderRadius: 25 }} source={{ uri: this.state.caribMallData.avatar }}></Image>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', marginLeft: 10, marginTop: 5 }}>
                                <Text style={{ color: colors.black }}>{this.state.caribMallData.name}</Text>
                                <Text style={styles.descriiption}>User</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={{ ...styles.arrowstyle, tintColor: colors.app_gray, marginRight: 20 }} source={require('../../Images/rightarrow.png')}></Image>
                            </View>
                        </View>
                    </TouchableOpacity> */}

          <View
            style={{
              backgroundColor: colors.white,
              marginTop: 10,
              borderRadius: 10,
              width: "95%",
              alignSelf: "center",
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("Address", {
                  token: this.state.token,
                })
              }
            >
              <View style={{ flexDirection: "row", margin: metrics.dimen_20 }}>
                <View style={{ justifyContent: "center", flex: 1 }}>
                  <Text
                    style={{
                      color: colors.black,
                      fontSize: metrics.text_header,
                    }}
                  >
                    Address
                  </Text>
                </View>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.arrowstyle}
                    source={require("../../Images/rightarrow.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("Chat", {
                  customer_id: "",
                  shop_id: "",
                  shopName: "Conversation",
                })
              }
            >
              <View style={{ flexDirection: "row", margin: metrics.dimen_20 }}>
                <View style={{ justifyContent: "center", flex: 1 }}>
                  <Text
                    style={{
                      color: colors.black,
                      fontSize: metrics.text_header,
                    }}
                  >
                    Conversations
                  </Text>
                </View>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.arrowstyle}
                    source={require("../../Images/rightarrow.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("AboutUs", { fromaboutus: "1" })
              }
            >
              <View style={{ flexDirection: "row", margin: metrics.dimen_20 }}>
                <View style={{ justifyContent: "center", flex: 1 }}>
                  <Text
                    style={{
                      color: colors.black,
                      fontSize: metrics.text_header,
                    }}
                  >
                    About Us
                  </Text>
                </View>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.arrowstyle}
                    source={require("../../Images/rightarrow.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("AboutUs", { fromaboutus: "2" })
              }
            >
              <View style={{ flexDirection: "row", margin: metrics.dimen_20 }}>
                <View style={{ justifyContent: "center", flex: 1 }}>
                  <Text
                    style={{
                      color: colors.black,
                      fontSize: metrics.text_header,
                    }}
                  >
                    Privacy Policy
                  </Text>
                </View>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.arrowstyle}
                    source={require("../../Images/rightarrow.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("AboutUs", { fromaboutus: "3" })
              }
            >
              <View style={{ flexDirection: "row", margin: metrics.dimen_20 }}>
                <View style={{ justifyContent: "center", flex: 1 }}>
                  <Text
                    style={{
                      color: colors.black,
                      fontSize: metrics.text_header,
                    }}
                  >
                    {"Terms & Conditions"}
                  </Text>
                </View>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.arrowstyle}
                    source={require("../../Images/rightarrow.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("AboutUs", { fromaboutus: "13" })
              }
            >
              <View style={{ flexDirection: "row", margin: metrics.dimen_20 }}>
                <View style={{ justifyContent: "center", flex: 1 }}>
                  <Text
                    style={{
                      color: colors.black,
                      fontSize: metrics.text_header,
                    }}
                  >
                    {"FAQs"}
                  </Text>
                </View>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.arrowstyle}
                    source={require("../../Images/rightarrow.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("AboutUs", { fromaboutus: "10" })
              }
            >
              <View style={{ flexDirection: "row", margin: metrics.dimen_20 }}>
                <View style={{ justifyContent: "center", flex: 1 }}>
                  <Text
                    style={{
                      color: colors.black,
                      fontSize: metrics.text_header,
                    }}
                  >
                    {"Terms of Use"}
                  </Text>
                </View>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.arrowstyle}
                    source={require("../../Images/rightarrow.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("AboutUs", { fromaboutus: "11" })
              }
            >
              <View style={{ flexDirection: "row", margin: metrics.dimen_20 }}>
                <View style={{ justifyContent: "center", flex: 1 }}>
                  <Text
                    style={{
                      color: colors.black,
                      fontSize: metrics.text_header,
                    }}
                  >
                    {"Return & Refund Policy"}
                  </Text>
                </View>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.arrowstyle}
                    source={require("../../Images/rightarrow.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("AboutUs", { fromaboutus: "12" })
              }
            >
              <View style={{ flexDirection: "row", margin: metrics.dimen_20 }}>
                <View style={{ justifyContent: "center", flex: 1 }}>
                  <Text
                    style={{
                      color: colors.black,
                      fontSize: metrics.text_header,
                    }}
                  >
                    {"Intellectual Property Rights"}
                  </Text>
                </View>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.arrowstyle}
                    source={require("../../Images/rightarrow.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            {/* <View style={{ flexDirection: 'row', margin: metrics.dimen_20 }}>
                            <View style={{ justifyContent: 'center', flex: 1 }}>
                                <Text style={{ color: colors.app_red, fontSize: metrics.text_header, fontWeight: 'bold' }}>Signout</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={styles.arrowstyle} source={require('../../Images/rightarrow.png')}></Image>
                            </View>
                        </View> */}
          </View>
        </View>
        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisible}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  horizontalLine: {
    borderBottomColor: "#dcdcdc",
    borderBottomWidth: 8,
    width: "100%",
    alignSelf: "center",
    marginTop: 10,
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
  },
  curveview: {
    height: metrics.newView.curveview,
    position: "absolute",
    top: metrics.newView.curvetop - 100,
    width: "100%",
    backgroundColor: colors.light_grey_backgroud,
    borderRadius: 40,
  },
  image_style: {
    height: metrics.dimen_40,
    width: metrics.dimen_40,
    resizeMode: "contain",
  },
  headingg: {
    fontSize: metrics.text_header,
    color: colors.white,
    fontWeight: "bold",
  },
  descriiption: {
    fontSize: metrics.text_description,
    color: colors.app_gray,
    fontWeight: "300",
  },
  arrowstyle: {
    height: metrics.dimen_15,
    width: metrics.dimen_15,
    resizeMode: "contain",
    tintColor: colors.app_gray,
  },
});
