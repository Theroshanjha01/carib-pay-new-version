import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
import InputSpinner from "react-native-input-spinner";
import AsyncStorage from "@react-native-community/async-storage";
const axios = require("axios");
import Modal from "react-native-modal";
var Spinner = require("react-native-spinkit");
import DropdownAlert from "react-native-dropdownalert";

export default class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.navigation.state.params.item,
      shopSlug: this.props.navigation.state.params.shopSlug,
      netTotal: "",
      data_cartItems: [],
      cartData: [],
    };
  }

  componentDidMount() {
    console.log("cart items", this.state.shopSlug, this.state.data);
    this.getCartItems();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  getCartItems = async () => {
    this.setState({
      spinvisible: true,
    });
    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    console.log("token", data.api_token);

    axios
      .get("https://caribmall.com/api/carts?api_token=" + data.api_token)
      .then((response) => {
        console.log("response cartitems ---> ", response.data.data[0].items);
        this.setState({
          data_cartItems:
            response.data.data.length == 0 ? [] : response.data.data[0].items,
          cartData: response.data.data.length == 0 ? [] : response.data.data,
          netTotal:
            response.data.data.length == 0 ? "" : response.data.data[0].total,
          spinvisible: false,
        });
        console.log("cartData", this.state.cartData);
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          spinvisible: false,
        });
      });
  };

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  onDeleteFromCart = (item, data) => {
    console.log(data);
    this.setState({
      opencartModal: true,
      spincart: true,
    });
    let postData = {
      cart: data[0].id,
      item: item.id,
    };
    console.log("posdata----> ", postData);
    axios({
      method: "delete",
      url:
        "https://caribmall.com/api/cart/removeItem?api_token=" +
        this.state.apiToken,
      data: postData,
    })
      .then((response) => {
        console.log("response cart removed data", response.data);
        this.setState({
          spincart: false,
          opencartModal: false,
          data_cartItems: response.data.cart.items,
        });
        this.dropDownAlertRef.alertWithType("success", response.data.message);
      })
      .catch((error) => {
        if (error.response) {
          console.log("response", error.response.data);
          this.setState({
            spincart: false,
            opencartModal: false,
          });
          // this.dropDownAlertRef.alertWithType('error', error.response.data.message);
        }
      });
  };

  setItemQuantity(data) {
    console.log('hi');
    this.setState({
      product_Quantity: data,
    });
  }

  calculateTotalAmount() {
  // total count for cart page 
  let cartItems = [...this.state.data_cartItems];
  let amount = 0;
  
  for (const iterator of cartItems) {
    amount += iterator.quantity * parseFloat(iterator.unit_price.substring(1));
  }
  
  // grand_total for checkoutPage
      let newCardData = [...this.state.cartData];
      let cardDataNew = {
        ...this.state.cartData[0],
        total: `$${amount.toFixed(2)}`,
        grand_total: `$${amount.toFixed(2)}`,
      };
      newCardData[0] = cardDataNew;

      this.setState({
        netTotal: `$${amount.toFixed(2)}`,
        cartData: newCardData,
      });
  } 
  updateCardQuantity(itemId, quantity){
    let newCardData = [...this.state.data_cartItems];
    var cardItemfoundIndex = this.state.data_cartItems.findIndex(
      (x) => x.id === itemId,
    );
    let tempObj = newCardData[cardItemfoundIndex];

    const tempObj2 = {
      ...tempObj,
      quantity : quantity
    }
    
    newCardData[cardItemfoundIndex] = tempObj2;
    this.setState({data_cartItems: newCardData}, () => {
      this.calculateTotalAmount();
  });
  }
  renderItemCart = (item) => {
    return (
      <View
        style={{
          flexDirection: "row",
          margin: 10,
          backgroundColor: colors.white,
          borderRadius: 10,
          justifyContent: "center",
          alignSelf: "center",
          height: 120,
        }}
      >
        <Image
          style={{
            height: 50,
            width: 50,
            resizeMode: "contain",
            marginTop: 10,
            marginLeft: 7,
          }}
          source={{ uri: item.image }}
        />
        <View
          style={{
            flexDirection: "column",
            marginLeft: 15,
            flex: 1,
            marginTop: 10,
          }}
        >
          <Text
            numberOfLines={2}
            style={{
              fontSize: metrics.text_normal,
              color: colors.black,
            }}
          >
            {item.title}
          </Text>
          <Text
            style={{
              fontSize: metrics.text_normal,
              color: colors.app_red,
              marginTop: 5,
            }}
          >
            {item.unit_price}
            <Text
              style={{
                fontSize: metrics.text_normal,
                color: colors.app_gray,
              }}
            >
              {" x " + item.quantity}
            </Text>
          </Text>
        </View>
        <View>
          <InputSpinner
            // max={20}
            min={1}
            step={1}
            height={35}
            width={110}
            editable={false}
            rounded={false}
            inputStyle={{ height: 35 }}
            style={{
              height: 50,
              marginLeft: 30,
              marginTop: 15,
              marginRight: 10,
            }}
            value={item.quantity}
            onChange={(num) =>{ 
              console.log(num);
              console.log(item.unit_price);
              this.updateCardQuantity(item.id, num);
              // this.setState({
              //   netTotal : num * parseFloat(item.unit_price),
              //   product_Quantity: num,
              // })
            }
            }
          />
          <TouchableOpacity
            onPress={() => this.onDeleteFromCart(item, this.state.cartData)}
          >
            <View style={{ alignItems: "flex-end", marginRight: 10 }}>
              <Image
                style={{
                  height: 30,
                  width: 30,
                  resizeMode: "contain",
                  marginTop: 5,
                }}
                source={require("../../Images/delete.png")}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor:
            this.state.data.length == 0
              ? colors.white
              : colors.light_grey_backgroud,
        }}
      >
        <View style={{ backgroundColor: colors.theme_caribpay, height: 55 }}>
          <View style={{ flexDirection: "row", margin: 10 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
            >
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                  }}
                  source={require("../../Images/leftarrow.png")}
                />
              </View>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: metrics.text_heading,
                color: colors.white,
                fontWeight: "bold",
                marginTop: 2,
                marginLeft: 10,
              }}
            >
              Cart
            </Text>
          </View>
        </View>

        <View style={{ flex: 1 }}>
          {this.state.data_cartItems.length > 0 &&
            this.state.spinvisible == false && (
              <FlatList
                style={{ marginBottom: 20 }}
                showsVerticalScrollIndicator={false}
                data={this.state.data_cartItems}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => this.renderItemCart(item)}
              />
            )}

          {this.state.data_cartItems.length == 0 &&
            this.state.spinvisible == false && (
              <View>
                <Image
                  source={require("../../Images/cartemp.png")}
                  style={{
                    height: 200,
                    width: 250,
                    resizeMode: "contain",
                    alignSelf: "center",
                    marginTop: 200,
                  }}
                />
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: metrics.text_heading,
                    color: colors.black,
                    fontWeight: "bold",
                    marginTop: 2,
                    marginLeft: 10,
                  }}
                >
                  No Item in Cart!
                </Text>
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: metrics.text_header,
                    color: colors.app_gray,
                    fontWeight: "bold",
                    marginTop: 2,
                    marginLeft: 10,
                  }}
                >
                  Explore More and Add items in your cart
                </Text>
              </View>
            )}

          {this.state.data_cartItems.length == 0 &&
            this.state.spinvisible == false && (
              <TouchableOpacity
                style={{
                  position: "absolute",
                  bottom: 10,
                  height: 40,
                  backgroundColor: colors.theme_caribpay,
                  borderRadius: 20,
                  width: "90%",
                  alignSelf: "center",
                  justifyContent: "center",
                  marginTop: 20,
                }}
                onPress={() => this.props.navigation.navigate("CaribMallHome")}
              >
                <View>
                  <Text
                    style={{
                      fontSize: metrics.text_heading,
                      color: colors.white,
                      fontWeight: "bold",
                      textAlignVertical: "center",
                      textAlign: "center",
                    }}
                  >
                    Start Shopping
                  </Text>
                </View>
              </TouchableOpacity>
            )}

          {this.state.data_cartItems.length > 0 && (
            <View
              style={{
                position: "absolute",
                bottom: 70,
                width: "90%",
                left: 20,
                right: 20,
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <Text
                  style={{
                    marginLeft: 10,
                    fontWeight: "bold",
                    fontSize: 16,
                    color: colors.black,
                    alignSelf: "center",
                  }}
                >
                  Net Total
                </Text>
                <Text
                  style={{
                    marginRight: 10,
                    fontWeight: "bold",
                    fontSize: metrics.text_normal,
                    color: colors.app_red,
                    alignSelf: "center",
                    fontSize: 16,
                  }}
                >
                  {"   " + this.state.netTotal}
                </Text>
              </View>
            </View>
          )}

          {this.state.data_cartItems.length > 0 && (
            <View style={styles.shadowEffect}>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("Checkout", {
                    shopSlug: this.state.shopSlug,
                    cartItems: this.state.data_cartItems,
                    allCartData: this.state.cartData,
                  })
                }
              >
                <View
                  style={{
                    backgroundColor: colors.theme_caribpay,
                    height: 50,
                    borderRadius: 25,
                    justifyContent: "center",
                  }}
                >
                  <Text
                    style={{
                      marginRight: 10,
                      fontWeight: "bold",
                      fontSize: 16,
                      color: colors.white,
                      alignSelf: "center",
                    }}
                  >
                    {"Checkout"}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.opencartModal}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spincart}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisible}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>

        <DropdownAlert ref={(ref) => (this.dropDownAlertRef = ref)} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  horizontalLine: {
    borderBottomColor: "#D3D3D3",
    borderBottomWidth: 0.4,
    width: "90%",
    alignSelf: "center",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
  },
  curveview: {
    height: metrics.newView.curveview + 110,
    position: "absolute",
    top: metrics.newView.curvetop - 110,
    width: "100%",
    backgroundColor: colors.white,
    borderRadius: 40,
  },
  shadowEffect: {
    // justifyContent: 'center',
    alignSelf: "center",
    backgroundColor: colors.white,
    width: "90%",
    height: 50,
    borderRadius: 1,
    position: "absolute",
    bottom: 15,
    borderWidth: 1,
    borderRadius: 25,
    borderColor: "#ddd",
    borderBottomWidth: 1,
    shadowColor: "black",
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.9,
    shadowRadius: 3,
    elevation: 5,
  },
});
