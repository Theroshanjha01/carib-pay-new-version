import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, FlatList, Image, TouchableOpacity, BackHandler, ActivityIndicator } from 'react-native';
import metrics from '../../Themes/Metrics.js';
import colors from '../../Themes/Colors.js';
const axios = require('axios');
var Spinner = require('react-native-spinkit');
import Modal from 'react-native-modal';
import DropdownAlert from 'react-native-dropdownalert';
import StarRating from 'react-native-star-rating';



export default class CategoryProduct extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            titleName: this.props.navigation.state.params.titleName,
            id: this.props.navigation.state.params.id,
            Subcategories: [],
            products: [],
            sub_cat_sub_data: [],
            spinvisible: false,
            slug: this.props.navigation.state.params.slug,
            fetching_from_server: false,
            page: 1,
            pagination_url_limit: false
        }
    }

    componentDidMount() {
        console.log("slug", this.state.slug)
        this.setState({
            spinvisible: true
        })
        this.getSubcategories();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }


    getSubcategories = () => {
        axios.get('https://caribmall.com/api/category-subgrps/' + this.state.id).then((response) => {
            let sub_cat_data = response.data.data.map((item, index) => {
                item.idNum = ++index
                return { ...item };
            })
            this.setState({
                Subcategories: sub_cat_data
            })
        }).catch((err) => {
            console.log(err)
        })




        this.getProducts()
    }


    getProducts = () => {
        axios.get("https://caribmall.com/api/listing/category-grp/" + this.state.slug).then((response) => {
            this.setState({
                products: response.data.data, spinvisible: false
            })
        }).catch((err) => {
            console.log(err)
        })
    }



    handleSelection = (item, id) => {
        var selectedid = this.state.selectedID
        if (selectedid === id) {
            this.setState({
                selectedID: null,
            })
        } else {
            this.setState({
                selectedID: id,
            })
            axios.get("https://caribmall.com/api/listing/category-subgrp/" + item.slug).then((response) => {
                this.setState({
                    products: response.data.data, spinvisible: false,
                })
            }).catch((err) => {
                console.log(err)
            })
            console.log("slug is >>>", item.slug)
            axios.get('https://caribmall.com/api/categories/' + item.id).then((response) => {
                let sub_data = response.data.data.map((item, index) => {
                    item.idNum = ++index
                    return { ...item };
                })
                this.setState({
                    sub_cat_sub_data: sub_data,
                    selectedIDs: null
                })
            }).catch((err) => {
                console.log(err)
            })
        }
    }






    handleSelectionSubs = (item, id) => {
        var selectedid = this.state.selectedIDs
        if (selectedid === id) {
            this.setState({
                selectedIDs: null,
            })
        } else {
            this.setState({
                selectedIDs: id,
            })

            axios.get("https://caribmall.com/api/listing/category/" + item.slug).then((response) => {
                this.setState({
                    products: response.data.data, spinvisible: false,
                })
            }).catch((err) => {
                console.log(err)
            })


            // aut-adipisci-ut-illum-et-qui-illum-autem
        }
    }




    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    addToCart = (item) => {
        axios.post('https://caribmall.com/api/addToCart/' + item.slug)
            .then(response => {
                if (response.data.message == "Item added to cart") {
                    this.dropDownAlertRef.alertWithType('success', 'Item added to cart', item.title);
                }
            })
            .catch(error => {
                // this.refs.toast.show(this.state.data.title + " " + "Item is Already Added in your cart!")
                this.dropDownAlertRef.alertWithType('error', 'Item is Already Added in your cart!');
            });
    }


    renderFooter() {
        return (
            this.state.pagination_url_limit == false && this.state.products.length > 7 ?
                <View style={styles.footer}>
                    <TouchableOpacity
                        activeOpacity={0.9}
                        onPress={() => this.loadMoreData()}
                        //On Click of button calling loadMoreData function to load more data
                        style={styles.loadMoreBtn}>
                        <Text style={styles.btnText}>Load More</Text>
                        {this.state.fetching_from_server ? (
                            <ActivityIndicator color="white" style={{ marginLeft: 8 }} />
                        ) : null}
                    </TouchableOpacity>
                </View>
                :
                null
        );
    }

    loadMoreData = () => {
        this.setState({
            page: this.state.page + 1
        })
        this.setState({ fetching_from_server: true }, () => {
            axios.get("https://caribmall.com/api/listing/category-grp/" + this.state.slug + '?page=' + this.state.page).then((response) => {
                this.setState({
                    products: [...this.state.products, ...response.data.data],
                    pagination_url_limit: response.data.data.length > 0 ? false : true,
                    fetching_from_server: false
                })
                console.log("Next Page --->", this.state.pagination_url)
            })
                .catch(error => {
                    console.error(error);
                });
        });
    };




    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.lightWhite }} >
                <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                    <View style={{ flexDirection: 'row', margin: 10 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white' }} source={require('../../Images/leftarrow.png')}></Image>
                            </View>
                        </TouchableOpacity>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold', marginTop: 2, marginLeft: 10 }}>{this.state.titleName}</Text>
                    </View>
                </View>
                <View style={{ ...styles.curveview, height: this.state.Subcategories.length > 0 ? metrics.newView.curveview + 120 : metrics.newView.curveview + 110 }}>
                    <View style={{ flexDirection: 'column', margin: 12, color: colors.light_grey_backgroud }}>
                        <View>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={this.state.Subcategories}
                                extraData={this.state.selectedID}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item }) => (
                                    <View style={{
                                        backgroundColor: item.idNum == this.state.selectedID ? colors.theme_caribpay : colors.white, margin: metrics.dimen_5, justifyContent: 'center', width: 100, height: 50,
                                        borderWidth: 2.5,
                                        borderRadius: 30,
                                        borderColor: '#ddd',
                                        borderBottomWidth: 0.5,
                                        shadowColor: '#000000',
                                        shadowOffset: { width: 0, height: 5 },
                                        shadowOpacity: 0.9,
                                        shadowRadius: 7,
                                        elevation: 6,
                                    }}>
                                        <TouchableOpacity onPress={() => this.handleSelection(item, item.idNum)}>
                                            <Text style={{ fontSize: metrics.text_medium, color: item.idNum == this.state.selectedID ? colors.white : colors.heading_black_text, textAlign: 'center', marginTop: 2 }}>{item.name}</Text>
                                        </TouchableOpacity>
                                    </View>
                                )} />
                        </View>

                    </View>

                    <View style={{ flexDirection: 'column', marginTop: -10, marginLeft: 13, color: 'red' }}>
                        <View>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={this.state.sub_cat_sub_data}
                                extraData={this.state.selectedIDs}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item }) => (
                                    <View style={{
                                        backgroundColor: item.idNum == this.state.selectedIDs ? colors.theme_caribpay : colors.white, margin: metrics.dimen_5, justifyContent: 'center', width: 100, height: 50,
                                        borderWidth: 2.5,
                                        borderRadius: 30,
                                        borderColor: '#ddd',
                                        borderBottomWidth: 0.5,
                                        shadowColor: '#000000',
                                        shadowOffset: { width: 0, height: 5 },
                                        shadowOpacity: 0.9,
                                        shadowRadius: 7,
                                        elevation: 6,
                                    }}>
                                        <TouchableOpacity onPress={() => this.handleSelectionSubs(item, item.idNum)}>
                                            <Text style={{ fontSize: metrics.text_medium, color: item.idNum == this.state.selectedIDs ? colors.white : colors.heading_black_text, textAlign: 'center', marginTop: 2 }}>{item.name}</Text>
                                        </TouchableOpacity>
                                    </View>
                                )} />
                        </View>

                    </View>



                    <View style={{ alignSelf: 'center', borderRadius: 10, width: '90%', backgroundColor: this.state.products.length == 0 ? colors.white : colors.light_grey_backgroud }}>
                        {this.state.spinvisible == false && this.state.products.length > 0 && <FlatList
                            style={{ marginTop: metrics.dimen_15, marginBottom: this.state.sub_cat_sub_data.length > 0 ? 120 : 80 }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.products}
                            ListFooterComponent={this.renderFooter.bind(this)}
                            numColumns={2}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item }) => (
                                <View style={{
                                    flex: 1 / 2, backgroundColor: colors.white, borderRadius: 10, margin: 5, flexDirection: 'column', borderColor: colors.light_grey_backgroud, borderWidth: 1
                                }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate("ProductDetails", { slug: item.slug })}>
                                        <View style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                                            <Image source={{ uri: item.image }}
                                                style={{ height: metrics.dimen_200, width: "100%", borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                                            </Image>
                                            <View style={{ position: 'absolute', top: -7, alignSelf: 'flex-end' }}>
                                                <Image source={require('../../Images/tag.png')}
                                                    style={{ height: 30, width: 35 }}>
                                                </Image>
                                                <Text style={{ position: 'absolute', top: 1, fontSize: 10, color: colors.white, alignSelf: 'center', marginLeft: 10, marginTop: 7 }}>{item.discount == null ? "0%" : item.discount.substring(0, 4)}</Text>
                                            </View>
                                        </View>
                                        <View style={{ marginLeft: 5, marginBottom: 5, marginTop: 10 }}>
                                            <Text style={{ color: colors.app_gray }} numberOfLines={1}>{item.title}</Text>
                                            <View style={{ flexDirection: 'row' }}>
                                                {item.offer_price != null && <Text style={{ color: colors.app_orange_color, fontSize: 12 }}><Text style={{ alignSelf: 'center', fontSize: 15, color: colors.app_orange_color, marginTop: 2, fontWeight: 'bold' }}>{item.offer_price}</Text></Text>}
                                                <Text style={{ alignSelf: 'center', color: item.offer_price == null ? colors.app_orange_color : colors.app_gray, fontSize: 12, textDecorationLine: item.offer_price == null ? null : 'line-through', marginLeft: item.offer_price == null ? 0 : 10 }}><Text style={{ alignSelf: 'center', fontSize: item.offer_price == null ? 15 : 12, color: item.offer_price == null ? colors.app_orange_color : colors.app_gray, marginTop: 2, fontWeight: 'bold', textDecorationLine: item.offer_price == null ? null : 'line-through', marginLeft: item.offer_price == null ? 0 : 10 }}>{item.price}</Text></Text>
                                            </View>
                                            <View style={{ width: 40, marginTop: 3 }}>
                                                <StarRating
                                                    disabled={false}
                                                    maxStars={5}
                                                    starSize={10}
                                                    buttonStyle={{ margin: 1 }}
                                                    halfStarEnabled={true}
                                                    fullStarColor={'#FEC750'}
                                                    rating={item.rating == null ? 0 : item.rating} />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>


                                // <View style={{
                                //     margin: 7, flex: 1 / 2, width: 200, height: 200, marginLeft: 5, borderRadius: 10, justifyContent: 'center', backgroundColor: 'white'
                                // }}>
                                //     <TouchableOpacity onPress={() => this.props.navigation.navigate("ProductDetails", { slug: item.slug })}>
                                //         <Image source={{ uri: item.image }} style={{
                                //             height: metrics.dimen_120, width: metrics.dimen_120, alignSelf: 'center', margin: 5
                                //         }}></Image>
                                //         <Text numberOfLines={2} style={{ fontSize: metrics.text_normal, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, marginTop: 2, marginLeft: 5 }}>{item.title}</Text>
                                //         <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 2, justifyContent: 'space-between' }}>
                                //             <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 0.5 }}>
                                //                 {item.offer_price != null && <Text style={{ fontSize: metrics.text_normal, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, marginTop: 2, fontWeight: 'bold', marginLeft: 5 }}>{item.offer_price}</Text>}
                                //                 <Text style={{ fontSize: item.offer_price != null ? metrics.text_medium : metrics.text_normal, color: item.offer_price != null ? colors.app_gray : colors.heading_black_text, marginTop: 1.5, fontWeight: 'bold', marginLeft: 5, textDecorationLine: item.offer_price != null ? 'line-through' : 'none', alignSelf: 'center', textAlignVertical: 'center' }}>{item.price}</Text>
                                //             </View>
                                //             <TouchableOpacity onPress={() => this.addToCart(item)}>
                                //                 <Image source={require('../../Images/supermarket.png')} style={{
                                //                     height: metrics.dimen_15, width: metrics.dimen_15, alignSelf: 'center', marginRight: 10
                                //                 }}>
                                //                 </Image>
                                //             </TouchableOpacity>
                                //         </View>
                                //     </TouchableOpacity>
                                // </View>
                            )} />
                        }
                        {this.state.spinvisible == false && this.state.products.length == 0 &&
                            <View style={{ marginBottom: 100 }}>
                                <Image source={require('../../Images/noData.png')}
                                    style={{ height: 300, width: 300, resizeMode: 'contain', alignSelf: 'center', marginTop: 150, borderColor: 'red' }}>
                                </Image>
                                <Text style={{ fontSize: metrics.text_header, color: colors.heading_black_text, marginTop: 2, alignSelf: 'center' }}>{"No Product Found"}</Text>
                            </View>
                        }
                    </View>
                </View>
                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.spinvisible}>
                    <View style={{ width: "50%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 100 }}>
                        <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinvisible} size={70} type={"ThreeBounce"} color={colors.black} />
                    </View>
                </Modal>

                <DropdownAlert ref={ref => this.dropDownAlertRef = ref}
                    successImageSrc={require('../../Images/addcart.png')}></DropdownAlert>
            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    curveview: { position: "absolute", top: metrics.newView.curvetop - 110, width: "100%", backgroundColor: colors.lightWhite, borderRadius: 40 },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    loadMoreBtn: {
        padding: 10,
        width: "40%",
        height: 50,
        alignSelf: 'center',
        backgroundColor: '#800000',
        borderRadius: 4,
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10
    },
    btnText: {
        color: 'white',
        fontSize: 15,
        textAlign: 'center',
    },
})