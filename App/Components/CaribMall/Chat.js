import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
import { Input } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
const axios = require("axios");
import Modal from "react-native-modal";
import { FaceDetector } from "react-native-camera";
var Spinner = require("react-native-spinkit");

export default class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customer_id: this.props.navigation.state.params.customer_id,
      shop_id: this.props.navigation.state.params.shop_id,
      shopName: this.props.navigation.state.params.shopName,
      ChatWithSellerData: [],
      type_message: "",
      product_name:this.props.navigation.state.params.product_name
    };
  }

  async componentDidMount() {
    this.getProductMessage();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  getProductMessage = async () => {
    {
      this.state.shopName == "Conversation" &&
        this.setState({
          spinvisible: true,
        });
      let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
      let data = JSON.parse(caribmall_data);
      this.setState({
        apiToken: data.api_token,
      });
      axios({
        method: "get",
        url:
          "https://caribmall.com/api/shop/" +
          this.state.shop_id +
          "/contact?api_token=" +
          this.state.apiToken,
      })
        .then((response) => {
          console.log("response chats data  --> ", response.data.data);
          let newArrayData = [];
          newArrayData.push(response.data.data);
          this.setState({
            spinvisible: false,
            ChatWithSellerData: newArrayData,
          });
        })
        .catch((error) => {
          if (error.response) {
            console.log("response coupons errors ", error.response.data);
            this.setState({
              noConversationFound:
                error.response.data === "api.no_conversation" ? true : false,
              spinvisible: false,
            });
          }
        });
    }
  };

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  renderItem = (item) => {
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
            margin: 10,
            backgroundColor: colors.light_grey_backgroud,
            borderRadius: 10,
          }}
        >
          <Image
            style={{
              height: metrics.dimen_45,
              width: metrics.dimen_45,
              resizeMode: "contain",
              alignSelf: "center",
              margin: 5,
            }}
            source={{ uri: item.customer.avatar }}
          />

          <View style={{ marginLeft: 10 }}>
            <Text
              style={{
                fontFamily: metrics.quicksand_bold,
                fontSize: metrics.text_17,
                color: colors.black,
                marginTop: -2,
              }}
            >
              {item.subject == null ? this.state.product_name : item.subject}
            </Text>
            <Text
              style={{
                fontFamily: metrics.quicksand_regular,
                fontSize: metrics.text_normal,
                color: colors.black,
                marginTop: 2,
              }}
            >
              {item.message == null ? "---" : item.message}
            </Text>
          </View>
        </View>

        {item.replies.length > 0 && (
          <FlatList
            style={{
              marginTop: 5,
            }}
            showsVerticalScrollIndicator={false}
            data={item.replies}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => this.renderRepliesItem(item)}
          />
        )}
      </View>
    );
  };

  renderRepliesItem = (item) => {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "flex-end",
          margin: 10,
          backgroundColor: colors.light_grey_backgroud,
          borderRadius: 10,
        }}
      >
        <View style={{ marginLeft: 10 }}>
          <Text
            style={{
              fontFamily: metrics.quicksand_bold,
              fontSize: metrics.text_header,
              color: colors.black,
              marginTop: -2,
            }}
          >
            {item.customer.name}
          </Text>
          <Text
            style={{
              fontFamily: metrics.quicksand_regular,
              fontSize: metrics.text_normal,
              color: colors.black,
              marginTop: 2,
            }}
          >
            {item.reply}
          </Text>
        </View>
        <Image
          style={{
            height: metrics.dimen_45,
            width: metrics.dimen_45,
            resizeMode: "contain",
            alignSelf: "center",
            margin: 5,
          }}
          source={{ uri: item.customer.avatar }}
        />
      </View>
    );
  };

  onSendMessage = () => {
    if (this.state.noConversationFound) {
      this.setState({
        spinvisible: true,
      });
      let postData = {
        customer_id: this.state.customer_id,
        message: this.state.type_message,
      };

      console.log(postData, this.state.apiToken);
      axios({
        method: "post",
        url:
          "https://caribmall.com/api/shop/" +
          this.state.shop_id +
          "/contact?api_token=" +
          this.state.apiToken,
        data: postData,
      })
        .then((response) => {
          console.log("response chats data no convo  --> ", response.data.data);
          let newArrayData = [];
          newArrayData.push(response.data.data);
          this.setState({
            spinvisible: false,
            ChatWithSellerData: newArrayData,
            type_message: "",
          });

          this.getProductMessage();
        })
        .catch((error) => {
          if (error.response) {
            console.log("response coupons errors ", error.response.data);
            this.setState({
              spinvisible: false,
            });
          }
        });
    } else {
      this.setState({
        spinvisible: true,
      });
      let postData = {
        customer_id: this.state.customer_id,
        shop_id: this.state.shop_id,
        message: this.state.type_message,
      };

      console.log(postData, this.state.apiToken);
      axios({
        method: "post",
        url:
          "https://caribmall.com/api/shop/" +
          this.state.shop_id +
          "/contact?api_token=" +
          this.state.apiToken,
        data: postData,
      })
        .then((response) => {
          console.log("response chats data  --> ", response.data.data);
          let newArrayData = [];
          newArrayData.push(response.data.data);
          this.setState({
            spinvisible: false,
            type_message: "",
            ChatWithSellerData: newArrayData,
          });
          
          this.getProductMessage();
        })
        .catch((error) => {
          if (error.response) {
            console.log("response coupons errors ", error.response.data);
            this.setState({
              spinvisible: false,
            });
          }
        });
    }
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
        <View
          style={{
            backgroundColor: colors.theme_caribpay,
            height: metrics.newView.upperview,
          }}
        >
          <View style={{ flexDirection: "row", margin: 10 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
            >
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                  }}
                  source={require("../../Images/leftarrow.png")}
                />
              </View>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: metrics.text_heading,
                color: colors.white,
                fontWeight: "bold",
                marginTop: 2,
                marginLeft: 10,
              }}
            >
              {this.state.shopName}
            </Text>
          </View>
        </View>
        <View style={styles.curveview}>
          {this.state.ChatWithSellerData.length > 0 &&
            this.state.spinvisible == false && (
              <FlatList
                style={{
                  marginTop: metrics.dimen_10,
                  marginBottom: metrics.dimen_10,
                }}
                showsVerticalScrollIndicator={false}
                data={this.state.ChatWithSellerData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => this.renderItem(item)}
              />
            )}

          {this.state.ChatWithSellerData.length == 0 &&
            this.state.spinvisible == false && (
              <View style={{ flex: 1, justifyContent: "center" }}>
                <Image
                  style={{
                    height: 150,
                    width: 150,
                    resizeMode: "contain",
                    alignSelf: "center",
                  }}
                  source={require("../../Images/nocov.png")}
                />
                <Text
                  style={{
                    fontFamily: metrics.quicksand_bold,
                    fontSize: metrics.text_large,
                    color: colors.black,
                    marginTop: 5,
                    alignSelf: "center",
                  }}
                >
                  No Conversation Found!
                </Text>
              </View>
            )}
        </View>

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisible}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>

        <View
          style={{
            position: "absolute",
            bottom: 2,
            flexDirection: "row",
            flex: 1,
          }}
        >
          <View style={{ flex: 0.94, flexDirection: "row" }}>
            <Input
              placeholder={"Type Message"}
              placeholderTextColor={colors.place_holder_color}
              value={this.state.type_message}
              onChangeText={(text) => this.setState({ type_message: text })}
              containerStyle={{
                width: "100%",
                height: 40,
                backgroundColor: colors.light_grey_backgroud,
                borderRadius: 25,
              }}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              inputStyle={{ fontSize: 14 }}
            />

            <View>
              <TouchableOpacity
                style={{
                  flex: 0.15,
                  alignSelf: "center",
                  justifyContent: "center",
                }}
                onPress={() => this.onSendMessage()}
              >
                <Image
                  style={{
                    height: 24,
                    width: 24,
                    resizeMode: "contain",
                    alignSelf: "center",
                    marginLeft: 10,
                    marginTop: 5,
                  }}
                  source={require("../../Images/sendding.png")}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  horizontalLine: {
    borderBottomColor: "#f2f2f2",
    borderBottomWidth: 8,
    width: "100%",
    alignSelf: "center",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
  },
  curveview: {
    height: metrics.newView.curveview,
    position: "absolute",
    top: metrics.newView.curvetop - 110,
    width: "100%",
    backgroundColor: colors.white,
    borderRadius: 40,
  },
});
