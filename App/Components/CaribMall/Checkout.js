import React from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  FlatList,
  Alert,
  BackHandler,
} from "react-native";
import colors from "../../Themes/Colors.js";
import metrics from "../../Themes/Metrics.js";
import { Input } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
const axios = require("axios");
import Modal from "react-native-modal";
import WebView from "react-native-webview";
import fetchPost from "../../fetchPost.js";
import {
  cancelUrl,
  maxValue,
  paypal_basictoken,
  paypal_gettoken,
  paypal_gettoken_live,
  paypal_payment,
  paypal_payment_live,
  returnUrl,
  expressmerchantwalletsreview,
  expressmerchantamountreview,
  expressmerchantcodesubmit,
} from "./Utils/api.constants";
var Spinner = require("react-native-spinkit");


export default class Checkout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      free: true,
      ipsa: false,
      elite: false,
      cod: false,
      wire: false,
      standard: true,
      addressData: [],
      api_token: "",
      shopSlug: this.props.navigation.state.params.shopSlug,
      cartItems: this.props.navigation.state.params.cartItems,
      allCartData: this.props.navigation.state.params.allCartData,
      packgingData: [],
      paymentOptionsData: [],
      address_id: "",
      phone_number: "",
      address: {},
      payment_id: "",
      packing_id: "",
      couponsData: [],
      shop_id: "",
      shipping_zone_id: "",
      description: "",
      userWalletbalance: 0,
      userWalletSymbol: "",
      caribpayToken: "",
      activeWallet: false,
      defaultCurrencyId: 0,
      accessToken: null,
      approvalUrl: null,
      approvalUrlFound: false,
      paymentId: null,
      carbpayFinalObj: {},
      grandTotalAmountToPay: 0,
      coupon_data: "",
      // isOrderPlaced: true,
    };

  }

  async componentDidMount() {
    console.log(
      "shop_slug---",
      this.state.cartItems,
      this.state.allCartData[0].id,
      this.state.allCartData[0].shop.id,
      this.state.allCartData[0].shipping_zone_id
    );
    this.setState({
      shop_id: this.state.allCartData[0].shipping_option_id,
      shipping_zone_id: this.state.allCartData[0].shipping_zone_id,
    });

    this.getUserAddress();
    this.getPackagingOptions();
    this.getPaymentOptions();
    this.getShippingOptions();
    this.getGrandtotal();

    let balance = await AsyncStorage.getItem("available_balance");
    let symbol = await AsyncStorage.getItem("symbol");
    let user_id = await AsyncStorage.getItem("id");
    let token = await AsyncStorage.getItem("token");
    let cp_id = await AsyncStorage.getItem("caribpay_id");

    this.setState({
      userWalletSymbol: symbol,
      userWalletbalance: balance,
      caribpay_user_id: user_id,
      caribID: cp_id,
      caribpayToken: token,
    });

    console.log(
      "info coming --> ",
      this.state.userWalletSymbol,
      this.state.userWalletbalance
    );

    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getUserAddress();
      }
    );
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  componentWillUnmount() {
    this.willFocusSubscription.remove();
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  getUserAddress = async () => {
    this.setState({
      spinvisible: true,
    });
    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    console.log("token", data.api_token);
    this.setState({
      api_token: data.api_token,
    });
    axios({
      method: "get",
      url: "https://caribmall.com/api/addresses?api_token=" + data.api_token,
    })
      .then((response) => {
        let data = response.data.data.map((item, index) => {
          item.idNum = ++index;
          return { ...item };
        });
        this.setState({
          addressData: data,
          spinvisible: false,
        });
      })
      .catch((error) => {
        console.log("response coupons errors ", error);
      });
  };

  getPackagingOptions = () => {
    axios({
      method: "get",
      url:
        "https://caribmall.com/api/packaging/" +
        this.state.shopSlug +
        "?api_token=" +
        this.state.api_token,
    })
      .then((response) => {
        let data = response.data.map((item, index) => {
          item.idNum = ++index;
          return { ...item };
        });
        this.setState({
          packgingData: data,
        });

        console.log("packing options are ---> ", this.state.packgingData);
      })
      .catch((error) => {
        console.log("response packaging datat errors ", error);
      });
  };

  getPaymentOptions = () => {
    axios({
      method: "get",
      url:
        "https://caribmall.com/api/paymentOptions/" +
        this.state.shopSlug +
        "?api_token=" +
        this.state.api_token,
    })
      .then((response) => {
        console.log("response paymentOptions data---> ", response.data);
        let data = response.data.data.map((item, index) => {
          item.idNum = ++index;
          return { ...item };
        });

        // let paypal = {
        //     code: "paypal",
        //     id: 1,
        //     idNum: 10,
        //     name: "paypal",
        //     order: 10,
        //     type: "Paypal"
        // }
        // data.push(paypal);
        this.setState({
          paymentOptionsData: data,
          spinvisible: false,
        });
      })
      .catch((error) => {
        console.log("response packaging datat errors ", error);
      });
  };

  getShippingOptions = () => {
    let postData = { zone: this.state.allCartData[0].shipping_zone_id };
    console.log(
      postData,
      this.state.api_token,
      this.state.allCartData[0].shop.id
    );
    axios({
      method: "post",
      url:
        "https://caribmall.com/api/shipping/" +
        this.state.allCartData[0].shop.id +
        "?api_token=" +
        this.state.api_token,
      data: postData,
    })
      .then((response) => {
        console.log("response Shiipinng data---> ", response.data.data);
        let data = response.data.data.map((item, index) => {
          item.idNum = ++index;
          return { ...item };
        });
        this.setState({
          shippingOptionsData: data,
          spinvisible: false,
        });
      })
      .catch((error) => {
        console.log("response Shiipinng data errors ", error);
        this.setState({
          spinvisible: false,
        });
      });
  };

  getGrandtotal = () => {
    let discount = this.state.allCartData[0].discount.slice(1);
    let shipping = this.state.allCartData[0].shipping.slice(1);
    let packaging = this.state.allCartData[0].packaging.slice(1);
    let handling = this.state.allCartData[0].handling.slice(1);
    let tax = this.state.allCartData[0].taxes.slice(1);
    let product_price = this.state.allCartData[0].total.slice(1);

    console.log(
      "info coming ---> ",
      discount,
      shipping,
      packaging,
      handling,
      tax,
      product_price
    );

    let grandTotalAmountToPay =
      parseFloat(shipping) +
      parseFloat(packaging) +
      parseFloat(handling) +
      parseFloat(tax) +
      parseFloat(product_price) -
      parseFloat(discount);

    console.log(
      "grandtotal amount --> ",
      parseFloat(grandTotalAmountToPay).toFixed(2)
    );

    this.setState({
      grandTotalAmountToPay: parseFloat(grandTotalAmountToPay).toFixed(2),
      itemProductPrice: this.state.allCartData[0].total,
      itemDiscountPrice: this.state.allCartData[0].discount,
      itemShippingPrice: this.state.allCartData[0].shipping,
      itemPackagingPrice: this.state.allCartData[0].packaging,
      itemhandlingPrice: this.state.allCartData[0].handling,
      itemTaxesPrice: this.state.allCartData[0].taxes,
    });
  };

  getGrandtotalAfterCoupounsApplied = () => {
    let discount = this.state.itemDiscountPrice.slice(1);
    let shipping = this.state.itemShippingPrice.slice(1);
    let packaging = this.state.itemPackagingPrice.slice(1);
    let handling = this.state.itemhandlingPrice.slice(1);
    let tax = this.state.itemTaxesPrice.slice(1);
    let product_price = this.state.itemProductPrice.slice(1);

    console.log(
      "info coming ---> ",
      discount,
      shipping,
      packaging,
      handling,
      tax,
      product_price
    );

    let grandTotalAmountToPay =
      parseFloat(shipping) +
      parseFloat(packaging) +
      parseFloat(handling) +
      parseFloat(tax) +
      parseFloat(product_price) -
      parseFloat(discount);

    console.log(
      "grandtotal amount --> ",
      parseFloat(grandTotalAmountToPay).toFixed(2)
    );

    this.setState({
      grandTotalAmountToPay: parseFloat(grandTotalAmountToPay).toFixed(2),
      itemProductPrice: this.state.allCartData[0].total,
      itemDiscountPrice: this.state.allCartData[0].discount,
      itemShippingPrice: this.state.allCartData[0].shipping,
      itemPackagingPrice: this.state.allCartData[0].packaging,
      itemhandlingPrice: this.state.allCartData[0].handling,
      itemTaxesPrice: this.state.allCartData[0].taxes,
    });
  };

  renderStatesAndCountry = (item) => {
    console.log("itesmsDDREASS --?>", item.state);
    let stateName;
    let countryName;
    countryName =
      item.country?.name == null || undefined ? "" : item.country?.name;
    stateName = item.state?.name == null || undefined ? "" : item.state?.name;
    return (
      <Text
        style={{
          fontSize: metrics.text_description,
          color: colors.app_gray,
          color: colors.black,
        }}
      >
        {stateName + "" + item.zip_code + ", " + countryName}
      </Text>
    );
  };

  onSendMessage = () => {
    console.log("called on send messge");
    axios({
      method: "post",
      url:
        "https://www.experttexting.com/ExptRestApi/sms/json/Message/Send?username=caribpay&api_key=6ljxqccz53x23ns&api_secret=2bukt1ihaefc34w&from=DEFAULT&to=" +
        this.state.phone_number +
        "&text=" +
        "Your Order has been placed Successfully!" +
        "&type=text",
    })
      .then((response) => {
        console.log(" response message ---> ", response.data);
        this.setState({
          isSMSVisible: false,
          message: "",
        });
      })
      .catch((error) => {
        console.log("response message  errors ", error);
      });
  };

  handleSelection = (item, id) => {
    console.log("address items ---> ", item.phone);
    var selectedid = this.state.selectedID;
    if (selectedid === id) {
      this.setState({
        selectedID: null,
        address_id: item.id,
        phone_number: item.phone,
        address: item,
      });
    } else {
      this.setState({
        selectedID: id,
        address_id: item.id,
        phone_number: item.phone,
        address: item,
      });
    }
  };

  handleSelectionPackage = (item, id) => {
    console.log("package items ---> ", item);
    var selectedid = this.state.selectedIDPackage;
    if (selectedid === id) {
      this.setState({
        selectedIDPackage: null,
        packing_id: item.id,
        spinvisible: true,
      });
      let postData2 = {
        packaging_id: item.id,
      };

      console.log("postdata ---", postData2);

      axios({
        method: "put",
        url:
          "https://caribmall.com/api/cart/" +
          this.state.allCartData[0].id +
          "/update?api_token=" +
          this.state.api_token,
        data: postData2,
      })
        .then((response) => {
          console.log("response updated  data---> ", response.data.cart);
          let newCartData = [];
          newCartData.push(response.data.cart);
          this.setState({
            allCartData: newCartData,
            spinvisible: false,
          });
          console.log("new aLL CART data--->", this.state.allCartData);
          this.getGrandtotal();
        })
        .catch((error) => {
          console.log("response updated cart errors ", error);
          this.setState({
            spinvisible: false,
          });
        });
    } else {
      this.setState({
        selectedIDPackage: id,
        packing_id: item.id,
        spinvisible: true,
      });
      let postData1 = {
        packaging_id: item.id,
      };
      console.log("postdata--->", postData1);
      axios({
        method: "put",
        url:
          "https://caribmall.com/api/cart/" +
          this.state.allCartData[0].id +
          "/update?api_token=" +
          this.state.api_token,
        data: postData1,
      })
        .then((response) => {
          console.log("response updated  data---> ", response.data.cart);
          let newCartData = [];
          newCartData.push(response.data.cart);
          this.setState({
            allCartData: newCartData,
            spinvisible: false,
          });
          this.getGrandtotal();
          console.log("new aLL CART data--->", this.state.allCartData);
        })
        .catch((error) => {
          console.log("response updated cart errors ", error);
          this.setState({
            spinvisible: false,
          });
        });
    }
  };

  handleSelectionPayment = async (item, id) => {
    console.log("address items ---> ", item);
    var selectedid = this.state.selectedIDPayment;
    this.setState({
      selectedIDPayment: selectedid === id ? null : id,
      payment_id: item.id,
    });

    let postData2 = {
      user_id: this.state.caribpay_user_id,
    };
    console.log("info coming --->", postData2, this.state.caribpayToken);

    item.id == 9
      ? this.setState({
          spinvisible: true,
        })
      : null;

    if (item.id == 9) {
      if (
        parseFloat(this.state.userWalletbalance) <
        parseFloat(this.state.grandTotalAmountToPay)
      ) {
        this.setState({
          invalid: true,
          spinvisible: false,
        });
      } else {
        const requestedParams = {
          expressMerchantId: item.merchant_id,
          expressMerchantPaymentCurrencyCode: "USD",
          user_id: this.state.caribpay_user_id,
        };
        let merchantDetails_res = await fetchPost(
          expressmerchantwalletsreview,
          requestedParams,
          this.state.caribpayToken,
          true
        );
        console.log("Merchant details -->", merchantDetails_res);
        this.setState({
          spinvisible: false,
        });

        if (merchantDetails_res.error || merchantDetails_res.status !== 200) {
          return;
        } else if (merchantDetails_res.status == 200) {
          this.checkAmountReview(merchantDetails_res, item.merchant_id);
        }
      }
    }
  };

  checkAmountReview = async (merchantObj, merchantID) => {
    console.log(
      "check amount review ... cliked",
      merchantID,
      this.state.grandTotalAmountToPay
    );

    const requestedParams = {
      expressMerchantPaymentAmount: this.state.grandTotalAmountToPay,
      user_id: this.state.caribpay_user_id,
      expressMerchantActualFee: merchantObj.expressMerchantActualFee,
      expressMerchantPaymentCurrencyId:
        merchantObj.expressMerchantPaymentCurrencyId,
      expressMerchantUserId: merchantObj.expressMerchantUserId,
      expressMerchantId: merchantID,
    };
    let merchantAmountReview_res = await fetchPost(
      expressmerchantamountreview,
      requestedParams,
      this.state.caribpayToken,
      true
    );
    console.log("merchantAmountReview_res ::: ", merchantAmountReview_res);
    const fees =
      merchantAmountReview_res.expressMerchantCalculatedChargePercentageFee;
    this.sendmoeny(merchantObj, fees, merchantID);
  };

  sendmoeny = async (merchantObj, fees, id) => {
    console.log("send Money clicked...");
    console.log("merchant object--->", merchantObj, "merchant-id ---->".id);
    const requestedParams = {
      expressMerchantPaymentAmount: this.state.grandTotalAmountToPay,
      user_id: this.state.caribpay_user_id,
      expressMerchantActualFee: fees,
      expressMerchantPaymentCurrencyId:
        merchantObj.expressMerchantPaymentCurrencyId,
      expressMerchantUserId: merchantObj.expressMerchantUserId,
      expressMerchantId: id,
    };

    console.log("Requested Params SendMoney --->", requestedParams);
    this.setState({
      carbpayFinalObj: requestedParams,
      spinvisible: false,
    });

    let merchantAmountReview_res = await fetchPost(
      expressmerchantcodesubmit,
      requestedParams,
      this.state.caribpayToken,
      true
    );
    console.log(
      "Merchant amount review response ---> ",
      merchantAmountReview_res
    );
  };

  handleSelectionShipping = (item, id) => {
    console.log("address items ---> ", item);
    var selectedid = this.state.selectedIDShipping;
    if (selectedid === id) {
      this.setState({
        selectedIDShipping: null,
        shipping_id: item.id,
      });
    } else {
      this.setState({
        selectedIDShipping: id,
        shipping_id: item.id,
      });
    }
  };
  getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
  };

  onPlaceOrders = async () => {
    if (this.state.payment_id == "") {
      if (this.state.userWalletbalance < this.state.grandTotalAmountToPay) {
        this.setState({
          invalid: true,
        });
      } else {
        Alert.alert(
          "Address Required",
          "Please select your address/payment mode for this order!",
          [{ text: "OK", onPress: () => console.log("Yes Pressed") }],
          { cancelable: false }
        );
      }
      // 7 - cod, 2- stripe, 1- paypal, 9 - caribpay wallet // 8 - bank wire transfer
    } else if (this.state.payment_id == 2) {
      if (this.state.address_id == "") {
        Alert.alert(
          "Address Required",
          "Please select your address , so that we can deliver your order!",
          [{ text: "OK", onPress: () => console.log("Yes Pressed") }],
          { cancelable: false }
        );
      } else if (this.state.payment_id == "") {
        Alert.alert(
          "Payment Method Required",
          "Please select your payment mode!",
          [{ text: "OK", onPress: () => console.log("Yes Pressed") }],
          { cancelable: false }
        );
      } else {
        this.props.navigation.navigate("StripePlaceOrder", {
          amount: this.state.grandTotalAmountToPay,
          token: this.state.api_token,
          shipping_option_id: this.state.allCartData[0].shipping_option_id,
          shipping_address: this.state.address_id,
          payment_method_id: this.state.payment_id,
          address_id: this.state.address_id,
          buyer_note: this.state.description,
          cart_id: this.state.allCartData[0].id,
          phone_number: this.state.phone_number,
        });
      }
    } else if (this.state.payment_id == 7) {
      if (this.state.address_id == "") {
        Alert.alert(
          "Address Required",
          "Please select your address , so that we can deliver your order!",
          [{ text: "OK", onPress: () => console.log("Yes Pressed") }],
          { cancelable: false }
        );
      } else if (this.state.payment_id == "") {
        Alert.alert(
          "Payment Method Required",
          "Please select your payment mode!",
          [{ text: "OK", onPress: () => console.log("Yes Pressed") }],
          { cancelable: false }
        );
      } else {
        this.setState({
          isOrderPlaced: true,
          spinOrderlacedvisible: true,
        });

        let postData1 = {
          shipping_option_id: this.state.allCartData[0].shipping_option_id,
          shipping_address: this.state.address_id,
          payment_method_id: this.state.payment_id,
          address_id: this.state.address_id,
          buyer_note: this.state.description,
        };

        console.log("posdata placed orders  ---> ", postData1);

        axios({
          method: "post",
          url:
            "https://caribmall.com/api/cart/" +
            this.state.allCartData[0].id +
            "/checkout?api_token=" +
            this.state.api_token,
          data: postData1,
        })
          .then((response) => {
            console.log("response order placed ---> ", response.data.data);
            console.log("order number", response.data.data.order_number);
            this.setState({
              spinOrderlacedvisible: false,
              order_number: response.data.data.order_number,
            });
            this.onSendMessage();
          })
          .catch((error) => {
            console.log("response order placed errors ", error);
            this.setState({
              spinvisible: false,
            });
          });
      }
    } else if (this.state.payment_id == 1) {
      var dataDetail = JSON.stringify({
        intent: "sale",
        payer: {
          payment_method: "paypal",
        },
        transactions: [
          {
            amount: {
              total: this.state.grandTotalAmountToPay.toString(),
              currency: "USD",
              details: {
                subtotal: this.state.grandTotalAmountToPay.toString(),
                tax: "0.00",
                shipping: "0.00",
                handling_fee: "0.00",
                shipping_discount: "0.00",
                insurance: "0.00",
              },
            },
            description: "Payment for Order",
            custom: "CARIB_MALL",
            invoice_number: this.getRandomInt(maxValue).toString(),
            payment_options: {
              allowed_payment_method: "INSTANT_FUNDING_SOURCE",
            },
            soft_descriptor: this.state.caribID,
          },
        ],
        note_to_payer: "Contact us for any questions on your order.",
        redirect_urls: {
          return_url: returnUrl,
          cancel_url: cancelUrl,
        },
      });
      let body = "grant_type=client_credentials";
      const response = await fetchPost(
        paypal_gettoken_live,
        body,
        paypal_basictoken
      );
      console.log(response);

      this.setState({
        accessToken: response.access_token,
      });

      axios
        .post(
          paypal_payment_live,
          dataDetail, // you can get data details from https://developer.paypal.com/docs/api/payments/v1/
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${response.access_token}`,
            },
          }
        )
        .then((response5) => {
          console.log(response5);
          const { id, links } = response5.data;
          const approvalUrl = links.find((data) => data.rel == "approval_url");
          this.setState({
            paymentId: id,
            approvalUrl: approvalUrl.href,
            approvalUrlFound: true,
          });
          let urltosend = `https://caribmall.com/api/cart/${
            this.state.allCartData[0].id
          }/checkout?api_token=${this.state.api_token}`;

          let postData2 = {
            shipping_option_id: this.state.allCartData[0].shipping_option_id,
            shipping_address: this.state.address_id,
            payment_method_id: this.state.payment_id,
            address_id: this.state.address_id,
            buyer_note: this.state.description,
          };
          this.props.navigation.navigate("WebPage", {
            approvalUrl: approvalUrl.href,
            accessToken: response.access_token,
            url: urltosend,
            data: postData2,
            paymentId: id,
            from: "orders",
          });
        })
        .catch((err) => {
          console.log({ ...err });
        });
    } else {
      if (
        parseFloat(this.state.userWalletbalance) <
        parseFloat(this.state.grandTotalAmountToPay)
      ) {
        this.setState({
          invalid: true,
        });
      } else {
        if (this.state.address_id == "") {
          Alert.alert(
            "Please select your Address.",
            [{ text: "Ok", onPress: () => console.log("Yes Pressed") }],
            { cancelable: false }
          );
        } else {
          this.setState({
            isOrderPlaced: true,
            spinOrderlacedvisible: true,
          });
          let postData1 = {
            shipping_option_id: this.state.allCartData[0].shipping_option_id,
            shipping_address: this.state.address_id,
            payment_method_id: this.state.payment_id,
            address_id: this.state.address_id,
            buyer_note: this.state.description,
          };
          console.log("posdata placed orders  ---> ", postData1);
          axios({
            method: "post",
            url:
              "https://caribmall.com/api/cart/" +
              this.state.allCartData[0].id +
              "/checkout?api_token=" +
              this.state.api_token,
            data: postData1,
          })
            .then((response) => {
              console.log("response order placed ---> ", response.data.data);
              console.log("order number", response.data.data.order_number);
              this.setState({
                spinOrderlacedvisible: false,
                order_number: response.data.data.order_number,
              });
              this.onSendMessage();
            })
            .catch((error) => {
              console.log("response order placed errors ", error);
              this.setState({
                spinvisible: false,
              });
            });
        }
      }
    }
  };

  onCouponsApply = () => {
    console.log("clicked");
    this.setState({
      spinvisible: true,
    });
    let postdata = { coupon: this.state.coupon_data };
    axios({
      method: "post",
      url:
        "https://caribmall.com/api/cart/" +
        this.state.allCartData[0].id +
        "/applyCoupon?api_token=" +
        this.state.api_token,
      data: postdata,
    })
      .then((response) => {
        console.log("response coupons", response.data);
        this.setState({
          itemProductPrice: response.data.total,
          itemDiscountPrice: response.data.discount,
          itemShippingPrice: response.data.shipping,
          itemPackagingPrice: response.data.packaging,
          itemhandlingPrice: response.data.handling,
          itemTaxesPrice: response.data.taxes,
          spinvisible: false,
        });
        this.getGrandtotalAfterCoupounsApplied();
      })
      .catch((error) => {
        if (error.response) {
          console.log("response coupons errors  ---> ", error.response.data);
          this.setState({
            spinvisible: false,
            isCoupounInvalid:
              error.response.data.message == "Coupon doesn't exist!"
                ? true
                : true,
          });
        }
      });
  };

  renderPackaging = (item) => {
    let modifyCost = parseFloat(item.cost).toFixed(2);
    return (
      <TouchableOpacity
        onPress={() => this.handleSelectionPackage(item, item.idNum)}
      >
        <View
          style={{
            flexDirection: "column",
            width: 150,
            // borderRadius: 10,
            backgroundColor:
              item.idNum == this.state.selectedIDPackage
                ? "#ffe4b9"
                : "#5cd6f326",
            marginLeft: 10,
            borderWidth: 1,
            borderTopLeftRadius: 5,
            borderBottomLeftRadius: 5,
            borderLeftWidth: 5,
            borderTopWidth: 0,
            borderBottomWidth: 0,
            borderRightWidth: 0,
            borderColor:
              item.idNum == this.state.selectedIDPackage
                ? "#f39c12"
                : "#0097bc",
          }}
        >
          <View
            style={{
              justifyContent: "center",
              height: 30,
              alignSelf: "center",
              marginTop: 10,
            }}
          >
            <Text
              style={{
                fontSize: metrics.text_normal,
                color: colors.black,
                textAlign: "center",
              }}
            >
              {modifyCost == "NaN" ? "0.00" : modifyCost}
            </Text>
          </View>
          <View style={{ height: 45 }}>
            <Text
              style={{
                fontSize: metrics.text_normal,
                color: colors.black,
                textAlign: "center",
                marginBottom: 2,
              }}
            >
              {item?.name}{" "}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  renderShipping = (item) => {
    let modifyCost = parseFloat(item.cost.slice(1)).toFixed(2);
    return (
      <TouchableOpacity
        onPress={() => this.handleSelectionShipping(item, item.idNum)}
      >
        <View
          style={{
            flexDirection: "column",
            width: 150,
            // borderRadius: 10,
            backgroundColor:
              item.idNum == this.state.selectedIDShipping
                ? "#ffe4b9"
                : "#5cd6f326",
            marginLeft: 10,
            borderWidth: 1,
            borderTopLeftRadius: 5,
            borderBottomLeftRadius: 5,
            borderLeftWidth: 5,
            borderTopWidth: 0,
            borderBottomWidth: 0,
            borderRightWidth: 0,
            borderColor:
              item.idNum == this.state.selectedIDShipping
                ? "#f39c12"
                : "#0097bc",
          }}
        >
          <View
            style={{
              justifyContent: "center",
              height: 20,
              alignSelf: "center",
              marginTop: 10,
            }}
          >
            <Text
              style={{
                fontSize: metrics.text_normal,
                color: colors.black,
                textAlign: "center",
              }}
            >
              {modifyCost == "NaN" ? "Free" : modifyCost}
            </Text>
          </View>
          <View style={{ height: 45 }}>
            <Text
              style={{
                fontSize: metrics.text_normal,
                color: colors.black,
                textAlign: "center",
                marginBottom: 5,
              }}
            >
              {item.delivery_takes}{" "}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  handleResponse = (data) => {
    if (data.title === "success") {
      this.setState({ showModal: false, status: "Complete" });
    } else if (data.title === "cancel") {
      this.setState({ showModal: false, status: "Cancelled" });
    } else {
      return;
    }
  };
  renderPaymentOptions = (item) => {
    let modifyCost = parseFloat(item.cost).toFixed(2);
    return (
      <TouchableOpacity
        onPress={() => this.handleSelectionPayment(item, item.idNum)}
      >
        <View
          style={{
            flexDirection: "column",
            width: 150,
            // borderRadius: 10,
            padding: 10,
            backgroundColor:
              item.idNum == this.state.selectedIDPayment
                ? "#ffe4b9"
                : "#5cd6f326",
            marginLeft: 10,
            borderWidth: 1,
            borderTopLeftRadius: 5,
            borderBottomLeftRadius: 5,
            borderLeftWidth: 5,
            borderTopWidth: 0,
            borderBottomWidth: 0,
            borderRightWidth: 0,
            borderColor:
              item.idNum == this.state.selectedIDPayment
                ? "#f39c12"
                : "#0097bc",
          }}
        >
          {item.id == 7 && (
            <Image
              source={require("../../Images/cod.png")}
              style={{
                height: 40,
                width: 40,
                resizeMode: "center",
                alignSelf: "center",
              }}
            />
          )}
          {item.id == 9 && (
            <Image
              source={require("../../Images/logo.png")}
              style={{
                height: 40,
                width: 40,
                resizeMode: "center",
                alignSelf: "center",
              }}
            />
          )}
          {item.id == 2 && (
            <Image
              source={require("../../Images/credit-card.png")}
              style={{
                height: 40,
                width: 40,
                resizeMode: "center",
                alignSelf: "center",
              }}
            />
          )}
          {item.id == 3 && (
            <Image
              source={require("../../Images/credit-card.png")}
              style={{
                height: 40,
                width: 40,
                resizeMode: "center",
                alignSelf: "center",
              }}
            />
          )}
          {item.id == 4 && (
            <Image
              source={require("../../Images/credit-card.png")}
              style={{
                height: 40,
                width: 40,
                resizeMode: "center",
                alignSelf: "center",
              }}
            />
          )}
          {item.id == 5 && (
            <Image
              source={require("../../Images/credit-card.png")}
              style={{
                height: 40,
                width: 40,
                resizeMode: "center",
                alignSelf: "center",
              }}
            />
          )}
          {item.id == 1 && (
            <Image
              source={require("../../Images/paypallogo.png")}
              style={{
                height: 40,
                width: 40,
                resizeMode: "center",
                alignSelf: "center",
              }}
            />
          )}
          {item.id == 8 && (
            <Image
              source={require("../../Images/banktransfer.png")}
              style={{
                height: 40,
                width: 40,
                resizeMode: "center",
                alignSelf: "center",
              }}
            />
          )}
          <View
            style={{
              justifyContent: "center",
              height: 30,
              alignSelf: "center",
              marginTop: item.id == 9 ? -3 : 10,
            }}
          >
            <Text
              style={{
                fontSize: metrics.text_normal,
                color: colors.black,
                textAlign: "center",
              }}
            >
              {item?.name}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const { approvalUrl, approvalUrlFound } = this.state;
    return (
      <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
        {approvalUrlFound && (
          <WebView
            style={{ height: 700, width: 300 }}
            source={{ uri: approvalUrl }}
            onNavigationStateChange={this._onNavigationStateChange}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            startInLoadingState={true}
            style={{ marginTop: 20 }}
          />
        )}

        <View style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("AddAddress")}
          >
            <View
              style={{
                flexDirection: "row",
                height: metrics.dimen_50,
                justifyContent: "space-between",
                marginTop: 5,
                backgroundColor: "white",
                borderRadius: 10,
                alignSelf: "center",
                width: "95%",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: 10,
                }}
              >
                <Image
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: "contain",
                    alignSelf: "center",
                  }}
                  source={require("../../Images/location.png")}
                />
                <Text
                  style={{
                    alignSelf: "center",
                    fontSize: metrics.text_normal,
                    color: colors.theme_caribpay,
                    fontWeight: "bold",
                    marginLeft: 10,
                  }}
                >
                  {" "}
                  Add Address
                </Text>
              </View>

              <Image
                style={{
                  height: 20,
                  width: 20,
                  resizeMode: "contain",
                  alignSelf: "center",
                  tintColor: colors.black,
                  marginRight: 15,
                }}
                source={require("../../Images/plus.png")}
              />
            </View>
          </TouchableOpacity>

          <View
            style={{
              alignSelf: "center",
              borderRadius: 10,
              width: "95%",
              backgroundColor: colors.white,
              marginTop: 10,
            }}
          >
            {this.state.addressData.length > 0 && (
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    color: colors.theme_caribpay,
                    fontWeight: "bold",
                    margin: 10,
                  }}
                >
                  {"Select Delievery Address"}
                </Text>

                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("UserAddress", {
                      token: this.state.api_token,
                    })
                  }
                >
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      color: colors.theme_orange_color_caribpay,
                      fontWeight: "bold",
                      margin: 10,
                      textDecorationLine: "underline",
                    }}
                  >
                    {"Edit"}
                  </Text>
                </TouchableOpacity>
              </View>
            )}
            <FlatList
              style={{
                marginTop: metrics.dimen_10,
                marginBottom: metrics.dimen_10,
              }}
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              data={this.state.addressData}
              extraData={this.state.selectedID}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <View
                  style={{
                    flexDirection: "column",
                    // borderRadius: 10,
                    backgroundColor:
                      item.idNum == this.state.selectedID
                        ? "#ffe4b9"
                        : "#5cd6f326",
                    marginLeft: 10,
                    borderWidth: 1,
                    borderTopLeftRadius: 5,
                    borderBottomLeftRadius: 5,
                    borderLeftWidth: 5,
                    borderTopWidth: 0,
                    borderBottomWidth: 0,
                    borderRightWidth: 0,
                    borderColor:
                      item.idNum == this.state.selectedID
                        ? "#f39c12"
                        : "#0097bc",
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.handleSelection(item, item.idNum)}
                  >
                    <View style={{ margin: 5 }}>
                      <Text
                        style={{
                          fontSize: metrics.text_header,
                          color: colors.app_gray,
                          color: colors.black,
                          fontWeight: "bold",
                        }}
                      >
                        {item.address_type}
                      </Text>
                      <Text
                        style={{
                          fontSize: metrics.text_description,
                          color: colors.app_gray,
                          color: colors.black,
                        }}
                      >
                        {item.address_title}
                      </Text>
                      <Text
                        style={{
                          fontSize: metrics.text_description,
                          color: colors.app_gray,
                          color: colors.black,
                        }}
                      >
                        {item.address_line_1 + "," + item.address_line_2}
                      </Text>
                      <Text
                        style={{
                          fontSize: metrics.text_description,
                          color: colors.app_gray,
                          color: colors.black,
                        }}
                      >
                        {item.city + " - "}
                        {this.renderStatesAndCountry(item)}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>

          <View
            style={{
              flexDirection: "column",
              backgroundColor: "white",
              marginTop: 15,
              width: "95%",
              alignSelf: "center",
              borderRadius: 10,
            }}
          >
            <View style={{ flexDirection: "row", marginTop: 10 }}>
              <View style={{ flexDirection: "column" }}>
                <Text
                  style={{
                    alignSelf: "center",
                    fontSize: metrics.text_normal,
                    color: colors.theme_caribpay,
                    fontWeight: "bold",
                    marginLeft: 12,
                    marginBottom: 10,
                  }}
                >
                  {this.state.allCartData[0].shop?.name}
                </Text>
              </View>
            </View>

            <FlatList
              style={{ marginBottom: 10 }}
              showsVerticalScrollIndicator={false}
              data={this.state.cartItems}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <View style={{ flexDirection: "row", margin: 15 }}>
                  <Image
                    style={{
                      height: 90,
                      width: 90,
                      borderRadius: 5,
                      alignSelf: "center",
                    }}
                    source={{ uri: item.image }}
                  />
                  <View
                    style={{
                      flexDirection: "column",
                      marginLeft: 15,
                      flex: 1,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: metrics.text_normal,
                        color: colors.black,
                      }}
                    >
                      {item.title}
                    </Text>
                    <Text
                      style={{
                        fontSize: metrics.text_normal,
                        color: colors.app_red,
                        marginTop: 5,
                      }}
                    >
                      {item.unit_price}
                      <Text
                        style={{
                          fontSize: metrics.text_normal,
                          color: colors.app_gray,
                        }}
                      >
                        {" x " + item.quantity}
                      </Text>
                    </Text>
                  </View>
                </View>
              )}
            />
          </View>

          <View
            style={{
              backgroundColor: colors.white,
              borderRadius: 10,
              margin: 15,
              width: "95%",
              alignSelf: "center",
            }}
          >
            <Text
              style={{
                fontSize: metrics.text_normal,
                color: colors.theme_caribpay,
                fontWeight: "bold",
                margin: 10,
              }}
            >
              Apply Coupon
            </Text>
            <View style={{ flexDirection: "row" }}>
              <Input
                placeholder={"Enter Coupon code"}
                placeholderTextColor={colors.place_holder_color}
                value={this.state.coupon_data}
                onChangeText={(text) => this.setState({ coupon_data: text })}
                containerStyle={{
                  width: "70%",
                  height: 50,
                  backgroundColor: colors.light_grey_backgroud,
                  borderRadius: 10,
                  marginLeft: 15,
                }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={{ fontSize: 14 }}
              />
              <TouchableOpacity onPress={() => this.onCouponsApply()}>
                <View
                  style={{
                    marginLeft: 10,
                    height: 50,
                    width: 80,
                    justifyContent: "center",
                    backgroundColor: colors.app_blue,
                    borderRadius: 10,
                  }}
                >
                  <Text style={{ textAlign: "center", color: colors.white }}>
                    Apply
                  </Text>
                </View>
              </TouchableOpacity>
            </View>

            <Text
              style={{
                fontSize: metrics.text_normal,
                color: colors.theme_caribpay,
                fontWeight: "bold",
                margin: 10,
              }}
            >
              Leave a message for seller
            </Text>
            <Input
              placeholder={"Write a message here"}
              placeholderTextColor={colors.place_holder_color}
              containerStyle={{
                height: metrics.dimen_100,
                borderColor: colors.app_gray,
                borderWidth: 0.5,
                borderRadius: metrics.dimen_7,
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.light_grey_backgroud,
                marginBottom: 20,
              }}
              value={this.state.description}
              multiline={true}
              numberOfLines={10}
              inputContainerStyle={{
                borderBottomWidth: 0,
                justifyContent: "center",
                alignItems: this.multiline ? "flex-start" : "center",
              }}
              inputStyle={{ fontSize: 14, textAlign: "center" }}
              onChangeText={(text) => this.setState({ description: text })}
            />
          </View>

          <View
            style={{
              flexDirection: "column",
              backgroundColor: "white",
              width: "95%",
              alignSelf: "center",
              borderRadius: 10,
            }}
          >
            <View style={{ flexDirection: "row", marginTop: 10 }}>
              <View style={{ flexDirection: "column" }}>
                <Text
                  style={{
                    alignSelf: "center",
                    fontSize: metrics.text_normal,
                    color: colors.theme_caribpay,
                    fontWeight: "bold",
                    marginLeft: 12,
                  }}
                >
                  {"Shipping Options"}
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: "row", margin: 10 }}>
              <FlatList
                style={{
                  marginTop: metrics.dimen_10,
                  marginBottom: metrics.dimen_10,
                }}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                data={this.state.shippingOptionsData}
                extraData={this.state.selectedIDShipping}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => this.renderShipping(item)}
              />
            </View>

            <View style={{ flexDirection: "row", marginTop: 10 }}>
              <View style={{ flexDirection: "column" }}>
                <Text
                  style={{
                    alignSelf: "center",
                    fontSize: metrics.text_normal,
                    color: colors.theme_caribpay,
                    fontWeight: "bold",
                    marginLeft: 12,
                  }}
                >
                  {"Packaging Options"}
                </Text>
              </View>
            </View>

            <View style={{ flexDirection: "row", margin: 10 }}>
              <FlatList
                style={{
                  marginTop: metrics.dimen_10,
                  marginBottom: metrics.dimen_10,
                }}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                data={this.state.packgingData}
                extraData={this.state.selectedIDPackage}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => this.renderPackaging(item)}
              />
            </View>

            <View style={{ flexDirection: "row", marginTop: 10 }}>
              <View style={{ flexDirection: "column" }}>
                <Text
                  style={{
                    alignSelf: "center",
                    fontSize: metrics.text_normal,
                    color: colors.theme_caribpay,
                    fontWeight: "bold",
                    marginLeft: 12,
                  }}>
                  {"Payment Options"}
                </Text>
              </View>
            </View>

            <View style={{ flexDirection: "row", margin: 10 }}>
              <FlatList
                style={{
                  marginTop: metrics.dimen_10,
                  marginBottom: metrics.dimen_10,
                }}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                data={this.state.paymentOptionsData}
                extraData={this.state.selectedIDPayment}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => this.renderPaymentOptions(item)}
              />
            </View>

            {this.state.payment_id == 9 && this.state.activeWallet && (
              <View
                style={{
                  height: 150,
                  backgroundColor: colors.light_grey_backgroud,
                  borderRadius: 5,
                  margin: 10,
                }}
              >
                <Text style={{ margin: 5 }}>* Pay via Carib Pay Wallet</Text>
                <Text style={{ margin: 5 }}>
                  * Your Active wallet has{" "}
                  {this.state.defaultCurrencyId == 1 ? "USD ($)" : "XCD (EC$)"}
                </Text>
                <Text style={{ margin: 5 }}>
                  * All you need to do is exchange your money.
                </Text>
                <View
                  style={{
                    width: 150,
                    height: 50,
                    backgroundColor: colors.app_green,
                    borderRadius: 5,
                    justifyContent: "center",
                    marginLeft: 10,
                    marginBottom: 10,
                  }}
                >
                  <Text
                    style={{
                      alignSelf: "center",
                      textAlign: "center",
                      color: colors.white,
                      fontWeight: "bold",
                    }}
                  >
                    Exchange Money
                  </Text>
                </View>
              </View>
            )}
          </View>

          <View style={{ flexDirection: "column", margin: 7 }}>
            <View
              style={{
                backgroundColor: colors.white,
                borderRadius: 15,
                margin: 7,
              }}
            >
              <Text
                style={{
                  fontSize: metrics.text_normal,
                  color: colors.theme_caribpay,
                  fontWeight: "bold",
                  margin: 10,
                }}
              >
                Order Summary
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: 5,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    color: colors.app_gray,
                    fontWeight: "bold",
                    marginLeft: 15,
                  }}
                >
                  Product Price
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_medium,
                    color: colors.app_black_text,
                    fontWeight: "bold",
                    marginRight: 15,
                  }}
                >
                  {this.state.itemProductPrice}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: 5,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    color: colors.app_gray,
                    fontWeight: "bold",
                    marginLeft: 15,
                  }}
                >
                  Discount
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_medium,
                    color: colors.app_black_text,
                    fontWeight: "bold",
                    marginRight: 15,
                  }}
                >
                  {this.state.itemDiscountPrice}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: 5,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    color: colors.app_gray,
                    fontWeight: "bold",
                    marginLeft: 15,
                  }}
                >
                  Packaging
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_medium,
                    color: colors.app_black_text,
                    fontWeight: "bold",
                    marginRight: 15,
                  }}
                >
                  {this.state.itemPackagingPrice}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: 5,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    color: colors.app_gray,
                    fontWeight: "bold",
                    marginLeft: 15,
                  }}
                >
                  Shipping
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_medium,
                    color: colors.app_black_text,
                    fontWeight: "bold",
                    marginRight: 15,
                  }}
                >
                  {this.state.itemShippingPrice}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: 5,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    color: colors.app_gray,
                    fontWeight: "bold",
                    marginLeft: 15,
                  }}
                >
                  Handling
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_medium,
                    color: colors.app_black_text,
                    fontWeight: "bold",
                    marginRight: 15,
                  }}
                >
                  {this.state.itemhandlingPrice}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: 5,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    color: colors.app_gray,
                    fontWeight: "bold",
                    marginLeft: 15,
                  }}
                >
                  {"VAT (" + this.state.itemTaxesPrice + ")"}
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_medium,
                    color: colors.black,
                    fontWeight: "bold",
                    marginRight: 15,
                  }}
                >
                  {this.state.allCartData[0].taxes}
                </Text>
              </View>

              <View
                style={{
                  borderBottomWidth: 1,
                  borderColor: colors.light_grey_backgroud,
                }}
              />

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: 5,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    color: colors.black,
                    fontWeight: "bold",
                    marginLeft: 15,
                  }}
                >
                  Total
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    color: colors.app_black_text,
                    fontWeight: "bold",
                    marginRight: 15,
                  }}
                >
                  {this.state.allCartData[0].grand_total.charAt(0) +
                    " " +
                    this.state.grandTotalAmountToPay}
                </Text>
              </View>
            </View>
          </View>
        </View>

        <TouchableOpacity onPress={() => this.onPlaceOrders()}>
          <View style={styles.shadowEffect}>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: metrics.text_header,
                color: colors.white,
                textAlign: "center",
                alignSelf: "center",
              }}
            >
              Place Order
            </Text>
          </View>
        </TouchableOpacity>

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisible}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.isOrderPlaced}
        >
          <View
            style={{
              width: "80%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: !this.state.spinOrderlacedvisible ? 250 : 300,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinOrderlacedvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
            {!this.state.spinOrderlacedvisible && (
              <View style={{ margin: 10, marginBottom: 10 }}>
                <Text
                  style={{
                    alignSelf: "center",
                    textAlign: "center",
                    marginTop: 15,
                    fontSize: 16,
                    color: colors.black,
                    fontFamily: metrics.quicksand_bold,
                  }}
                >
                  {"Order Confirmation!"}
                </Text>

                <Image
                  source={require("../../Images/orderplaced.png")}
                  style={{
                    height: metrics.dimen_80,
                    width: metrics.dimen_80,
                    resizeMode: "center",
                    alignSelf: "center",
                  }}
                />
                <Text
                  style={{
                    alignSelf: "center",
                    textAlign: "center",
                    marginTop: 15,
                    color: colors.app_gray,
                    fontSize: metrics.text_normal,
                    fontFamily: metrics.quicksand_bold,
                  }}
                >
                  {"Your order " +
                    this.state.order_number +
                    " has been placed!"}
                </Text>

                <TouchableOpacity
                  onPress={() =>
                    this.setState({ isOrderPlaced: false }, () =>
                      this.props.navigation.navigate("CaribMallHome")
                    )
                  }
                >
                  <View
                    style={{
                      width: "80%",
                      backgroundColor: colors.theme_caribpay,
                      height: 40,
                      borderRadius: 10,
                      alignSelf: "center",
                      justifyContent: "center",
                      marginTop: 15,
                    }}
                  >
                    <Text
                      style={{
                        alignSelf: "center",
                        textAlign: "center",
                        color: colors.white,
                        fontFamily: metrics.quicksand_bold,
                      }}
                    >
                      {"Continue Shopping"}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </Modal>

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.invalid}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                margin: metrics.dimen_20,
                flexDirection: "column",
                height: 250,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <View>
                  <TouchableOpacity onPress={()=>this.setState({invalid:false})}>
                    <Image
                      style={{
                        height: metrics.dimen_16,
                        width: metrics.dimen_16,
                        resizeMode: "contain",
                        margin: 15,
                      }}
                      source={require("../../Images/cross-sign.png")}
                    />
                  </TouchableOpacity>

                  <Image
                    style={{
                      height: metrics.dimen_80,
                      width: metrics.dimen_100,
                      resizeMode: "contain",
                      alignSelf: "center",
                      marginTop: 20,
                    }}
                    source={require("../../Images/addfund.png")}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_heading,
                      color: colors.black,
                      fontFamily: metrics.quicksand_bold,
                      textAlign: "center",
                      margin: 15,
                    }}
                  >
                    {"You have Insufficient balance to make this order."}
                  </Text>
                </View>
              </View>
            </View>
          </Modal>
        </View>

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.isCoupounInvalid}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                // margin: metrics.dimen_10,
                flexDirection: "column",
                height: 200,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isCoupounInvalid: false,
                    })
                  }
                >
                  <Image
                    style={{
                      height: metrics.dimen_16,
                      width: metrics.dimen_16,
                      resizeMode: "contain",
                      margin: 15,
                    }}
                    source={require("../../Images/cross-sign.png")}
                  />
                </TouchableOpacity>
                <Image
                  style={{
                    height: 150,
                    width: 150,
                    resizeMode: "cover",
                    alignSelf: "center",
                    marginTop: -30,
                  }}
                  source={require("../../Images/sorrypic.jpg")}
                />
                <Text
                  style={{
                    fontSize: metrics.text_heading,
                    color: colors.black,
                    fontFamily: metrics.quicksand_bold,
                    textAlign: "center",
                    marginTop: -20,
                  }}
                >
                  {"Invalid Coupon Applied!"}
                </Text>
              </View>
            </View>
          </Modal>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  shadowEffect: {
    justifyContent: "center",
    alignSelf: "center",
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    height: 50,
    borderRadius: 25,
    bottom: 10,
  },
  bottomview2: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_5,
  },
});
