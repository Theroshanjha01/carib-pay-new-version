import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, FlatList, Image, TouchableOpacity, BackHandler, ImageBackground } from 'react-native';
import metrics from '../../Themes/Metrics.js';
import colors from '../../Themes/Colors.js';
var Spinner = require('react-native-spinkit');
const axios = require('axios');
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';


export default class Coupons extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            starCount: 3.5,
            couponsData: []
        }
    }

    componentDidMount() {
        this.getCoupons();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }


    getCoupons = async () => {
        this.setState({
            spinvisible: true
        })
        let caribmall_data = await AsyncStorage.getItem("carib_mall_data")
        let data = JSON.parse(caribmall_data)
        console.log("token", data.api_token)
        axios({
            method: 'get',
            url: 'https://caribmall.com/api/coupons?api_token=' + data.api_token,
        })
            .then(response => {
                console.log("response", response.data.data)
                this.setState({
                    couponsData: response.data.data,
                    spinvisible: false
                })
                // console.log(this.state.couponsData)
            }).catch((error) => {
                console.log("response coupons errors", error)
            })
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    renderCoupons = (item) => {
        // console.log("items", item.shop['name'])
        return (
            <View style={{ backgroundColor: 'white', flexDirection: 'column', margin: 7, borderRadius: 10 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View>
                        <Image
                            source={require("../../Images/coupon.png")}
                            style={{ height: 110, width: 180 }}>
                        </Image>
                        <Text style={{ position: 'absolute', alignSelf: 'center', textAlignVertical: 'center', top: 35, fontSize: metrics.text_21, color: colors.white, fontWeight: 'bold', left: 32 }}>{item.amount + " OFF"}</Text>
                    </View>
                    <View style={{ flexDirection: 'column', backgroundColor: colors.white, margin: 5, marginLeft: 10 }}>
                        <Text style={{ fontSize: metrics.text_21, fontWeight: 'bold', color: colors.app_blueColor }}>{item.code}</Text>
                        {this.renderShopName(item)}
                        <Text style={{ fontSize: metrics.text_medium, color: colors.app_gray, width: 200 }}>{"USE BEFORE : " + item.validity}</Text>
                    </View>
                </View>

            </View>
        )
    }


    renderShopName = (item) => {
        let shopName
        console.log(item.shop == null ? shopName = "---" : shopName = item.shop['name'])
        return (
            <Text style={{ fontSize: metrics.text_normal, color: colors.black, textAlignVertical: 'center', fontWeight: 'bold', marginTop: 20 }}>{shopName}</Text>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }} >
                <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                    <View style={{ flexDirection: 'row', margin: 10 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white' }} source={require('../../Images/leftarrow.png')}></Image>
                            </View>
                        </TouchableOpacity>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold', marginTop: 2, marginLeft: 10 }}>Coupons</Text>
                    </View>
                </View>
                <View style={styles.curveview}>
                    {this.state.couponsData.length > 0 && this.state.spinvisible == false &&
                        <View style={{ flexDirection: 'row', margin: 7 }}>
                            <FlatList
                                style={{ flex: 1, marginTop: 10, marginBottom: 40 }}
                                showsVerticalScrollIndicator={false}
                                data={this.state.couponsData}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item }) => this.renderCoupons(item)} />
                        </View>}

                    {this.state.couponsData.length == 0 && this.state.spinvisible == false && <View>
                        <Image source={require('../../Images/nocou.png')}
                            style={{ height: 150, width: 150, resizeMode: 'contain', alignSelf: 'center', marginTop: 200 }}>
                        </Image>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', marginTop: 2, textAlign: 'center' }}>No Promocode Available!</Text>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, marginTop: 2, textAlign: 'center' }}>We will notify you once something arrives</Text>
                    </View>}


                </View>

                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.spinvisible}>
                    <View style={{ width: "50%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 100 }}>
                        <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinvisible} size={70} type={"ThreeBounce"} color={colors.black} />
                    </View>
                </Modal>

            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({
    horizontalLine: { borderBottomColor: '#f2f2f2', borderBottomWidth: 8, width: '100%', alignSelf: 'center' },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    curveview: { height: metrics.newView.curveview + 160, position: "absolute", top: metrics.newView.curvetop - 100, width: "100%", backgroundColor: colors.light_grey_backgroud, borderRadius: 40 }
})