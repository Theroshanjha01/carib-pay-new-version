import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  FlatList,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
const axios = require("axios");
import Modal from "react-native-modal";
var Spinner = require("react-native-spinkit");
import Toast, { DURATION } from "react-native-easy-toast";

export default class DisputeDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      disputeData: "",
      token: this.props.navigation.state.params.token,
      dispute_id: this.props.navigation.state.params.dispute_id,
    };
  }

  componentDidMount() {
    this.getDisputesDetails();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  getDisputesDetails = () => {
    this.setState({
      spinvisible: true,
    });
    axios
      .get(
        "https://caribmall.com/api/dispute/" +
          this.state.dispute_id +
          "?api_token=" +
          this.state.token
      )
      .then((response) => {
        // console.log("response dd :", response.data.data.shop["name"]);
        this.setState({
          disputeData: response.data.data,
          shopname: response.data.data.shop["name"],
          order_number: response.data.data.order_details.order_number,
          itemsData: response.data.data.order_details.items,
          spinvisible: false,
        });

        console.log("response disputeData :", this.state.itemsData);
      })
      .catch((error) => {
        if (error.response) {
          console.log(" error while get disputes ---> ", error.response.data);
          this.setState({
            spinvisible: false,
          });
        }
      });
  };

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }}
      >
        <View
          style={{
            backgroundColor: colors.theme_caribpay,
            height: metrics.newView.upperview,
          }}
        >
          <View style={{ flexDirection: "row", margin: 10 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
            >
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                  }}
                  source={require("../../Images/leftarrow.png")}
                />
              </View>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: metrics.text_heading,
                color: colors.white,
                fontWeight: "bold",
                marginTop: 2,
                marginLeft: 10,
              }}
            >
              Dispute Details
            </Text>
          </View>
        </View>
        <View style={styles.curveview}>
          <View style={{ margin: 10 }}>
            <View style={{ flexDirection: "row", margin: 5 }}>
              <Text style={styles.order_header}>Reason : </Text>
              <Text style={styles.order_info}>
                {this.state.disputeData.reason}
              </Text>
            </View>

            <View style={{ flexDirection: "row", margin: 5 }}>
              <Text style={styles.order_header}>Description : </Text>
              <Text style={styles.order_info}>
                {this.state.disputeData.description}
              </Text>
            </View>
            <View style={{ flexDirection: "row", margin: 5 }}>
              <Text style={styles.order_header}>Goods Recieved :</Text>
              <Text style={styles.order_info}>
                {this.state.spinvisible == false &&
                this.state.disputeData.goods_received == false
                  ? "No"
                  : "Yes"}
              </Text>
            </View>
            <View style={{ flexDirection: "row", margin: 5 }}>
              <Text style={styles.order_header}>Shop :</Text>
              <Text style={styles.order_info}>{this.state.shopname}</Text>
            </View>
            <View style={{ flexDirection: "row", margin: 5 }}>
              <Text style={styles.order_header}>Order ID :</Text>
              <Text style={styles.order_info}>{this.state.order_number}</Text>
            </View>

            <View style={{ margin: 5 }}>
              <FlatList
                style={{ marginTop: metrics.dimen_10 , backgroundColor:colors.white ,borderRadius:5 }}
                showsVerticalScrollIndicator={false}
                ListHeaderComponent={() => (
                  <Text style={styles.order_header}>Items</Text>
                )}
                data={this.state.itemsData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <View style={{ flexDirection: "row", margin: 10}}>
                    <Image
                      style={{
                        height: metrics.dimen_60,
                        width: metrics.dimen_60,
                        alignSelf: "center",
                      }}
                      source={{ uri: item.image }}
                    />
                    <Text
                      style={{
                        fontSize: metrics.text_normalss,
                        color: colors.app_black_text,
                        fontFamily: metrics.quicksand_semibold,
                        width:250,marginLeft:30
                      }}
                    >
                      {item.title}
                    </Text>
                  </View>
                )}
              />
            </View>
          </View>
        </View>

        {/* {this.state.data.length == 0 && this.state.spinvisible == false && (
            <View>
              <Image
                source={require("../../Images/error.png")}
                style={{
                  height: 150,
                  width: 200,
                  resizeMode: "contain",
                  alignSelf: "center",
                  marginTop: 200,
                }}
              />
              <Text
                style={{
                  textAlign: "center",
                  fontSize: metrics.text_heading,
                  color: colors.black,
                  fontWeight: "bold",
                  marginTop: 2,
                  marginLeft: 10,
                }}
              >
                No Disputes Yet!
              </Text>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: metrics.text_header,
                  color: colors.app_gray,
                  fontWeight: "bold",
                  marginTop: 2,
                  marginLeft: 10,
                }}
              >
                This section will contains your order disputes
              </Text>
            </View>
          )}
        </View>
        {this.state.data.length == 0 && this.state.spinvisible == false && (
          <TouchableOpacity
            style={{
              position: "absolute",
              bottom: 10,
              height: 40,
              backgroundColor: colors.theme_caribpay,
              borderRadius: 20,
              width: "90%",
              alignSelf: "center",
              justifyContent: "center",
            }}
            onPress={() => this.props.navigation.navigate("CaribMallHome")}
          >
            <View>
              <Text
                style={{
                  fontSize: metrics.text_heading,
                  color: colors.white,
                  fontWeight: "bold",
                  textAlignVertical: "center",
                  textAlign: "center",
                }}
              >
                Start Shopping
              </Text>
            </View>
          </TouchableOpacity>
        )} */}

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisible}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  horizontalLine: {
    borderBottomColor: "#f2f2f2",
    borderBottomWidth: 2,
    width: "100%",
    alignSelf: "center",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
  },
  curveview: {
    height: metrics.newView.curveview + 160,
    position: "absolute",
    top: metrics.newView.curvetop - 110,
    width: "100%",
    backgroundColor: colors.light_grey_backgroud,
    borderRadius: 40,
  },
  order_header: {
    fontSize: metrics.text_normal,
    color: colors.app_gray,
    marginLeft: 15,
    flex: 1 / 2,
    fontFamily: metrics.quicksand_bold,
  },
  order_info: {
    fontSize: metrics.text_normalss,
    color: colors.app_black_text,
    fontFamily: metrics.quicksand_semibold,
    flex: 1 / 2,
  },
});
