import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
import { Input } from "react-native-elements";
const axios = require("axios");
import Modal from "react-native-modal";
var Spinner = require("react-native-spinkit");
import Toast, { DURATION } from "react-native-easy-toast";

export default class DisputeMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dispute_id: this.props.navigation.state.params.dispute_id,
      token: this.props.navigation.state.params.token,
      type_message: "",
      replies: [],
    };
  }

  componentDidMount() {
    console.log("info coming ", this.state.dispute_id, this.state.token);
    this.getDisputeReposeData();
    this.getDisputeData();

    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  getDisputeReposeData = async () => {
    this.setState({
      spinvisible: true,
    });
    axios({
      method: "get",
      url:
        "https://caribmall.com/api/dispute/" +
        this.state.dispute_id +
        "/response?api_token=" +
        this.state.token,
    })
      .then((response) => {
        console.log("response --> ", response.data);
        this.setState({
          data: response.data.dispute,
          spinvisible: false,
        });
      })
      .catch((error) => {
        console.log("response  errors ", error);
      });
  };

  getDisputeData = async () => {
    this.setState({
      spinvisible: true,
    });
    axios({
      method: "get",
      url:
        "https://caribmall.com/api/dispute/" +
        this.state.dispute_id +
        "?api_token=" +
        this.state.token,
    })
      .then((response) => {
        console.log("response --> ", response.data);
        this.setState({
          replies: response.data.data.replies,
          spinvisible: false,
        });
      })
      .catch((error) => {
        console.log("response  errors ", error);
      });
  };

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  onSendMessage = () => {
    if (this.state.type_message == "") {
      this.refs.toast.show("Type a message first!");
    } else {
      this.setState({
        spinvisible: true,
      });
      let postData = {
        user_id: this.state.data.shop_id,
        reply: this.state.type_message,
      };
      axios({
        method: "post",
        url:
          "https://caribmall.com/api/dispute/" +
          this.state.dispute_id +
          "/response?api_token=" +
          this.state.token,
        data: postData,
      })
        .then((response) => {
          console.log("response --> ", response.data);
          this.setState({
            type_message: "",
            spinvisible: false,
            replies: response.data.data.replies,
          });
          console.log("replies ---> ", this.state.replies);
          console.log("replies LENGTBH---> ", this.state.replies.length);

          this.getDisputeData();
        })
        .catch((error) => {
          console.log("response  errors ", error);
        });
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.state.replies.length > 0 && this.state.spinvisible == false && (
          <FlatList
            style={{ marginTop: metrics.dimen_10, marginBottom: 60 }}
            showsVerticalScrollIndicator={false}
            data={this.state.replies}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <View
                style={{
                  flexDirection: "row",
                  backgroundColor: colors.light_grey_backgroud,
                  borderRadius: 5,
                  width: "95%",
                  alignSelf: "center",
                  borderRadius: 20,
                  margin: 10,
                  height: 40,
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_40,
                    width: metrics.dimen_40,
                    resizeMode: "contain",
                  }}
                  source={{ uri: item.user.avatar }}
                />

                <View style={{ marginLeft: 10 }}>
                  <Text
                    style={{
                      fontFamily: metrics.quicksand_bold,
                      fontSize: metrics.text_normal,
                    }}
                  >
                    {item.customer.name}
                  </Text>
                  <Text
                    style={{
                      fontFamily: metrics.quicksand_regular,
                      fontSize: metrics.text_normal,
                    }}
                  >
                    {item.reply}
                  </Text>
                </View>
              </View>
            )}
          />
        )}

        {this.state.replies.length == 0 && this.state.spinvisible == false && (
          <Text
            style={{
              textAlign: "center",
              alignSelf: "center",
              fontFamily: metrics.quicksand_bold,
              marginTop:40
            }}
          >
            No Data Found
          </Text>
        )}

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisible}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>
        <View
          style={{
            position: "absolute",
            bottom: 2,
            flexDirection: "row",
            flex: 1,
          }}
        >
          <View style={{ flex: 0.15, alignSelf: "center" }}>
            <Image
              style={{
                height: 24,
                width: 24,
                resizeMode: "contain",
                alignSelf: "center",
              }}
              source={require("../../Images/attach.png")}
            />
          </View>

          <View style={{ flex: 0.79, flexDirection: "row" }}>
            <Input
              placeholder={"Type Message"}
              placeholderTextColor={colors.place_holder_color}
              value={this.state.type_message}
              onChangeText={(text) => this.setState({ type_message: text })}
              containerStyle={{
                width: "100%",
                height: 40,
                backgroundColor: colors.light_grey_backgroud,
                borderRadius: 25,
              }}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              inputStyle={{ fontSize: 14 }}
            />

            <View>
              <TouchableOpacity
                style={{
                  flex: 0.15,
                  alignSelf: "center",
                  justifyContent: "center",
                }}
                onPress={() => this.onSendMessage()}
              >
                <Image
                  style={{
                    height: 24,
                    width: 24,
                    resizeMode: "contain",
                    alignSelf: "center",
                    marginLeft: 10,
                    marginTop: 5,
                  }}
                  source={require("../../Images/sendding.png")}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <Toast
          ref="toast"
          style={{ backgroundColor: "black" }}
          position="center"
          positionValue={200}
          fadeInDuration={200}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: "white" }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  horizontalLine: {
    borderBottomColor: "#f2f2f2",
    borderBottomWidth: 8,
    width: "100%",
    alignSelf: "center",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
  },
  curveview: {
    height: metrics.newView.curveview,
    position: "absolute",
    top: metrics.newView.curvetop - 110,
    width: "100%",
    backgroundColor: colors.white,
    borderRadius: 40,
  },
});
