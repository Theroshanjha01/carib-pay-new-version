import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
const axios = require("axios");
import Modal from "react-native-modal";
var Spinner = require("react-native-spinkit");

export default class Disputes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.navigation.state.params.token,
      data: [],
    };
  }

  componentDidMount() {
    this.getDisputes();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  getDisputes = async () => {
    this.setState({
      spinvisible: true,
    });
    axios({
      method: "get",
      url: "https://caribmall.com/api/disputes?api_token=" + this.state.token,
    })
      .then((response) => {
        this.setState({
          data: response.data.data.reverse(),
          spinvisible: false,
        });
      })
      .catch((error) => {
        console.log("response coupons errors ", error);
      });
  };

  onMarkSolved = (item) => {
    this.setState({
      spinvisible: true,
    });
    axios({
      method: "put",
      url:
        "https://caribmall.com/api/dispute/" +
        item.id +
        "/solved?api_token=" +
        this.state.token,
    })
      .then((response) => {
        console.log("response updated --> ", response.data);
        this.setState({
          spinvisible: false,
        });
        this.getDisputes();
      })
      .catch((error) => {
        console.log("response coupons errors ", error);
      });
  };

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }}
      >
        <View
          style={{
            backgroundColor: colors.theme_caribpay,
            height: metrics.newView.upperview,
          }}
        >
          <View style={{ flexDirection: "row", margin: 10 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
            >
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                  }}
                  source={require("../../Images/leftarrow.png")}
                />
              </View>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: metrics.text_heading,
                color: colors.white,
                fontWeight: "bold",
                marginTop: 2,
                marginLeft: 10,
              }}
            >
              Disputes
            </Text>
          </View>
        </View>
        <View style={styles.curveview}>
          {this.state.spinvisible == false && this.state.data.length > 0 && (
            <FlatList
              style={{ marginTop: metrics.dimen_15,marginBottom:metrics.dimen_20 }}
              showsVerticalScrollIndicator={false}
              data={this.state.data}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("DisputeDetails",{dispute_id:item.id,token:this.state.token})}>
                <View
                  style={{
                    backgroundColor: colors.white,
                    borderRadius: 10,
                    margin: 7,
                  }}
                >
                  <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text style={styles.order_header}>Status : </Text>
                    <Text
                      style={{
                        fontSize: metrics.text_header,
                        color:
                          item.closed == false
                            ? colors.app_red
                            : colors.app_green,
                        fontFamily: metrics.quicksand_semibold,
                        flex: 1 / 2,
                      }}
                    >
                      {item.closed == false ? "Open" : "Closed"}
                    </Text>
                  </View>

                  <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text style={styles.order_header}>Store : </Text>
                    <Text style={styles.order_info}>{item.shop.name}</Text>
                  </View>
                  <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text style={styles.order_header}>Refund Amount :</Text>
                    <Text style={styles.order_info}>
                      {item.order_details.grand_total}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text style={styles.order_header}>Order ID :</Text>
                    <Text style={styles.order_info}>
                      {item.order_details.order_number}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text style={styles.order_header}>Return Good :</Text>
                    <Text style={styles.order_info}>
                      {item.return_goods == null ? "No" : "Yes"}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text style={styles.order_header}>Order Recieved :</Text>
                    <Text style={styles.order_info}>
                      {item.goods_received == false ? "No" : "Yes"}
                    </Text>
                  </View>

                  <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text style={styles.order_header}>Reason :</Text>
                    <Text style={styles.order_info}>{item.reason}</Text>
                  </View>

                 {item.closed == false && <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      marginBottom: 10,
                      marginTop: 10,
                    }}
                  >
                    <TouchableOpacity
                      style={{
                        backgroundColor: colors.theme_orange_color_caribpay,
                        height: 40,
                        borderRadius: 5,
                        justifyContent: "center",
                        width: "40%",
                      }}
                      onPress={() =>
                        this.props.navigation.navigate("DisputeMessage", {
                          token: this.state.token,
                          dispute_id: item.id,
                        })
                      }
                    >
                      <View>
                        <Text style={{ alignSelf: "center" }}>Response</Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => this.onMarkSolved(item)}
                      style={{
                        backgroundColor: colors.theme_orange_color_caribpay,
                        height: 40,
                        borderRadius: 5,
                        justifyContent: "center",
                        width: "40%",
                        marginLeft: 10,
                      }}
                    >
                      <View>
                        <Text style={{ alignSelf: "center" }}>
                          Mark as Solved
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>}
                </View>
                </TouchableOpacity>
              )}
            />
          )}

          {this.state.data.length == 0 && this.state.spinvisible == false && (
            <View>
              <Image
                source={require("../../Images/error.png")}
                style={{
                  height: 150,
                  width: 200,
                  resizeMode: "contain",
                  alignSelf: "center",
                  marginTop: 200,
                }}
              />
              <Text
                style={{
                  textAlign: "center",
                  fontSize: metrics.text_heading,
                  color: colors.black,
                  fontWeight: "bold",
                  marginTop: 2,
                  marginLeft: 10,
                }}
              >
                No Disputes Yet!
              </Text>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: metrics.text_header,
                  color: colors.app_gray,
                  fontWeight: "bold",
                  marginTop: 2,
                  marginLeft: 10,
                }}
              >
                This section will contains your order disputes
              </Text>
            </View>
          )}
        </View>
        {this.state.data.length == 0 && this.state.spinvisible == false && (
          <TouchableOpacity
            style={{
              position: "absolute",
              bottom: 10,
              height: 40,
              backgroundColor: colors.theme_caribpay,
              borderRadius: 20,
              width: "90%",
              alignSelf: "center",
              justifyContent: "center",
            }}
            onPress={() => this.props.navigation.navigate("CaribMallHome")}
          >
            <View>
              <Text
                style={{
                  fontSize: metrics.text_heading,
                  color: colors.white,
                  fontWeight: "bold",
                  textAlignVertical: "center",
                  textAlign: "center",
                }}
              >
                Start Shopping
              </Text>
            </View>
          </TouchableOpacity>
        )}

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisible}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  horizontalLine: {
    borderBottomColor: "#f2f2f2",
    borderBottomWidth: 8,
    width: "100%",
    alignSelf: "center",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
  },
  curveview: {
    height: metrics.newView.curveview + 160,
    position: "absolute",
    top: metrics.newView.curvetop - 110,
    width: "100%",
    backgroundColor: colors.light_grey_backgroud,
    borderRadius: 40,
  },
  order_header: {
    fontSize: metrics.text_normal,
    color: colors.app_gray,
    marginLeft: 15,
    flex: 1 / 2,
    fontFamily: metrics.quicksand_bold,
  },
  order_info: {
    fontSize: metrics.text_normalss,
    color: colors.app_black_text,
    fontFamily: metrics.quicksand_semibold,
    flex: 1 / 2,
  },
});
