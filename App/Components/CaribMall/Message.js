import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, FlatList, Image, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../../Themes/Metrics.js';
import colors from '../../Themes/Colors.js';
import AsyncStorage from '@react-native-community/async-storage';
const axios = require('axios');
import Modal from 'react-native-modal';
var Spinner = require('react-native-spinkit');
import { Input } from 'react-native-elements';


export default class Message extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            starCount: 3.5,
            conversationData: [],
            customer_id: this.props.navigation.state.params.customer_id,
            order_id: this.props.navigation.state.params.order_id,
            type_message: '',
            data: this.props.navigation.state.params.data,

        }
    }

    componentDidMount() {
        console.log("data --->", this.state.data, this.state.order_id)
        this.getConversationData()
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    getConversationData = async () => {
        this.setState({
            spinvisible: true
        })
        let caribmall_data = await AsyncStorage.getItem("carib_mall_data")
        let data = JSON.parse(caribmall_data)
        this.setState({
            apiToken: data.api_token
        })

        let postData = {
            shop_id: this.state.data.id,
            customer_id: this.state.customer_id,
            message: "hello"
        }
        console.log("postdata-->", postData)
        axios({
            method: 'get',
            url: 'https://caribmall.com/api/order/' + this.state.order_id + '/conversation?api_token=' + this.state.apiToken,
            post: postData
        }).then(response => {
            console.log("response conversation --> ", response.data.data)
            console.log("response conversation --> ", response.data.data.replies)

            this.setState({
                spinvisible: false,
                conversationData: response.data.data,
                replies: response.data.data.replies
            })

            // console.log(this.state.conversationData.avatar)

        }).catch((error) => {
            console.log("response conversation errors ", error)
            this.setState({
                spinvisible: false
            })
        })
    }


    onSendMessage = async () => {
        this.setState({
            spinvisible: true
        })
        let postData = {
            shop_id: this.state.data.shop.id,
            customer_id: this.state.customer_id,
            message: this.state.type_message
        }
        axios({
            method: 'post',
            url: 'https://caribmall.com/api/order/' + this.state.order_id + '/conversation?api_token=' + this.state.apiToken,
            post: postData
        }).then(response => {
            console.log("response conversation send message --> ", response.data)
            this.setState({
                spinvisible: false,
                conversationData: response.data.data,
                replies: response.data.data.replies,
                type_message:''
            })
           
            // this.getConversationData()

        }).catch((error) => {
            console.log("response coupons errors ", error)
            this.setState({
                spinvisible: false
            })
        })
    }



render() {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }} >
            <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                <View style={{ flexDirection: 'row', margin: 10 }}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                        <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center' }}>
                            <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white' }} source={require('../../Images/leftarrow.png')}></Image>
                        </View>
                    </TouchableOpacity>
                    <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold', marginTop: 2, marginLeft: 10 }}>Messages</Text>
                </View>
            </View>
            <View style={styles.curveview}>
                <View style={{ backgroundColor: 'white', flexDirection: 'column', margin: 15, borderRadius: 10 }}>
                    <View style={{ margin: 7, flexDirection: 'row' }}>
                        <Image style={{ height: metrics.dimen_45, width: metrics.dimen_45 }} source={require("../../Images/user.png")}></Image>
                        <View>
                            <Text style={{ fontSize: metrics.text_header, color: colors.app_gray, marginLeft: 5, textAlignVertical: 'center', fontFamily:metrics.quicksand_bold }}>{this.state.conversationData.subject}</Text>
                            <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, marginTop: 2, marginLeft: 5, textAlignVertical: 'center', width: 300 , fontFamily:metrics.quicksand_semibold}}>{this.state.conversationData.message}</Text>
                        </View>
                    </View>
                </View>

                <FlatList
                    style={{ marginTop: 5, marginBottom: metrics.dimen_70 }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.replies}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                        <View style={{ backgroundColor: 'white', flexDirection: 'column', margin: 5, borderRadius: 10 }}>
                            <View style={{ alignSelf: 'flex-end', margin: 10, flexDirection: 'row' }}>
                                <View style={{ alignItems: 'flex-end' }}>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.black, marginTop: 2, marginLeft: 5, textAlignVertical: 'center', paddingRight: 20 }}>{item.reply}</Text>
                                    <Text style={{ fontSize: metrics.text_medium, color: colors.app_gray, marginTop: 2, marginLeft: 5, textAlignVertical: 'center', paddingRight: 20 }}>{item.updated_at}</Text>
                                </View>
                                <Image style={{ height: metrics.dimen_45, width: metrics.dimen_45 }} source={{ uri: item.customer.avatar }}></Image>
                            </View>
                        </View>
                    )} />



            </View>

            <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.spinvisible}>
                <View style={{ width: "50%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 100 }}>
                    <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinvisible} size={70} type={"ThreeBounce"} color={colors.black} />
                </View>
            </Modal>




            <View style={{ position: 'absolute', bottom: 2, flexDirection: 'row', flex: 1 }}>

                <View style={{ flex: 0.15, alignSelf: 'center' }}>
                    <Image style={{ height: 24, width: 24, resizeMode: 'contain', alignSelf: 'center' }}
                        source={require('../../Images/attach.png')}></Image></View>

                <View style={{ flex: 0.79, flexDirection: 'row' }}>
                    <Input
                        placeholder={'Type Message'}
                        placeholderTextColor={colors.place_holder_color}
                        value={this.state.type_message}
                        onChangeText={(text) => this.setState({ type_message: text })}
                        containerStyle={{ width: "100%", height: 40, backgroundColor: colors.white, borderRadius: 25 }}
                        inputContainerStyle={{ borderBottomWidth: 0 }}
                        inputStyle={{ fontSize: 14 }}>
                    </Input>

                    <View>
                        <TouchableOpacity style={{ flex: 0.15, alignSelf: 'center', justifyContent: 'center' }}
                            onPress={() => this.onSendMessage()}>
                            <Image style={{ height: 24, width: 24, resizeMode: 'contain', alignSelf: 'center', marginLeft: 10, marginTop: 5 }}
                                source={require('../../Images/sendding.png')}></Image>
                        </TouchableOpacity>
                    </View>

                </View>


            </View>

        </SafeAreaView >
    )
}
}

const styles = StyleSheet.create({
    horizontalLine: { borderBottomColor: '#f2f2f2', borderBottomWidth: 8, width: '100%', alignSelf: 'center' },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    curveview: { height: metrics.newView.curveview, position: "absolute", top: metrics.newView.curvetop - 110, width: "100%", backgroundColor: colors.light_grey_backgroud, borderRadius: 40 }
})