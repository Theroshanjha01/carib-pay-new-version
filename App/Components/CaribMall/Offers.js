import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, FlatList, Image, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../../Themes/Metrics.js';
import colors from '../../Themes/Colors.js';
import InputSpinner from "react-native-input-spinner";



export default class Offers extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: "",
            netTotal: '',
            item: this.props.navigation.state.params.item
        }
    }

    componentDidMount() {
        console.log("items-------->", this.props.navigation.state.params.item)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }} >
                <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                    <View style={{ flexDirection: 'row', margin: 10 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white' }} source={require('../../Images/leftarrow.png')}></Image>
                            </View>
                        </TouchableOpacity>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold', marginTop: 2, marginLeft: 10 }}>Offers</Text>
                    </View>
                </View>
                <View style={styles.curveview}>
                    <View style={{ width: '93%', borderRadius: 10, backgroundColor: colors.white, marginTop: 15, alignSelf: 'center' }}>
                        <Image style={{ height: 80, width: 80, alignSelf: 'center', margin: 10 }}
                            source={{ uri: this.props.navigation.state.params.item.image }}></Image>
                        {this.state.item.name != null && <Text style={{ fontSize: metrics.text_normal, color: colors.black, marginTop: 10, marginLeft: 10,marginBottom:2 }}>{this.state.item.name}</Text>}
                        {this.state.item.brand != null && <Text style={{ fontSize: metrics.text_normal, color: colors.app_blue, fontWeight: 'bold', marginTop: 2, marginLeft: 10 }}>{this.state.item.brand == null ? "" : this.state.item.brand}</Text>}
                        {this.state.item.gtin_type != null && <Text style={{ fontSize: metrics.text_normal, color: colors.black, marginTop: 2, marginLeft: 10, marginBottom: 10 }}>{this.state.item.gtin_type == null ? " " : this.state.item.gtin_type + " : "}
                            <Text style={{ fontSize: metrics.text_normal, color: colors.black, marginTop: 2, marginLeft: 10, marginBottom: 10 }}>{this.state.item.gtin == null ? " " : this.state.item.gtin}</Text>
                        </Text>
                        }
                    </View>



                    <FlatList
                        style={{ marginTop: metrics.dimen_10, marginBottom: metrics.dimen_70 }}
                        showsVerticalScrollIndicator={false}
                        data={this.props.navigation.state.params.item.listings}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => (
                            <View style={{ flexDirection: 'row', margin: 15, backgroundColor: colors.white, borderRadius: 10 }}>
                                <Image style={{ height: 40, width: 40, resizeMode: 'contain', alignSelf: 'center', margin: 5 }}
                                    source={{ uri: item.image }}></Image>

                                <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 15, flex: 1, margin: 15 }}>
                                    <Text style={{ fontSize: metrics.text_description, color: colors.white, backgroundColor: colors.app_blue, textAlign: "center", borderRadius: 20, width: 80 }}>{item.condition}</Text>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.black }}>{item.title}</Text>
                                    <Text style={{ fontSize: metrics.text_normal, color: colors.app_red, marginTop: 5 }}>{item.price}
                                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>{" x 1"}</Text>
                                    </Text>
                                </View>


                                <View style={{ height: 30, width: 30, borderRadius: 15, backgroundColor: colors.light_grey_backgroud, justifyContent: 'center', alignSelf: "center", marginRight: 20 }}>
                                    <Image style={{ height: 18, width: 18, resizeMode: 'contain', alignSelf: 'center' }}
                                        source={require('../../Images/supermarket.png')}></Image></View>
                            </View>
                        )} />

                </View>
            </SafeAreaView >
        )
    }
}




const styles = StyleSheet.create({
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    curveview: { height: metrics.newView.curveview + 110, position: "absolute", top: metrics.newView.curvetop - 110, width: "100%", backgroundColor: colors.light_grey_backgroud, borderRadius: 40 },
    shadowEffect: {
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: colors.white,
        width: '90%',
        height: 30,
        borderRadius: 1,
        position: 'absolute',
        bottom: 0,
        borderWidth: 1,
        borderRadius: 7,
        borderColor: '#ddd',
        borderBottomWidth: 1,
        shadowColor: 'black',
        shadowOffset: { width: 1, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
    }
})