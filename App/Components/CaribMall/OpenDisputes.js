import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
import { Input } from "react-native-elements";
import RBSheet from "react-native-raw-bottom-sheet";
const axios = require("axios");
import Modal from "react-native-modal";
import { FlatList } from "react-native-gesture-handler";
var Spinner = require("react-native-spinkit");
import Toast, { DURATION } from "react-native-easy-toast";

export default class OpenDisputes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      order_id: this.props.navigation.state.params.order_id,
      token: this.props.navigation.state.params.token,
      disputeData: [],
      data: this.props.navigation.state.params.data,
      description: "",
      reason_name: "",
    };
  }

  componentDidMount() {
    this.getDisputesData();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  getDisputesData = () => {
    this.setState({
      spinvisible: true,
    });
    axios
      .get(
        "https://caribmall.com/api/order/" +
          this.state.order_id +
          "/dispute?api_token=" +
          this.state.token
      )
      .then((response) => {
        let obj = response.data.data.dispute_type;
        let item_obj = response.data.data.items;

        let item_object_data = Object.keys(item_obj);
        console.log(item_object_data);
        this.setState({
          product_id: parseInt(item_object_data[0]),
        });

        let i = 0;
        let newData = Object.keys(obj);
        console.log("length --> ", newData.length);

        let array = [];

        for (i = 0; i < newData.length; i++) {
          console.log("value ---> ", newData[i]);
          const data_dispute = {
            dispute_id: newData[i],
            name: obj[newData[i]],
          };
          array.push(data_dispute);
        }
        this.setState({
          disputeData: response.data.data,
          spinvisible: false,
          disputeIdAray: array,
          //   disputeProductId:itemArray
        });

        // console.log("items --> ",this.state.disputeIdAray)
      })
      .catch((error) => {
        if (error.response) {
          console.log(" error while get disputes ---> ", error.response.data);
          this.setState({
            spinvisible: false,
          });
        }
      });
  };

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  renderHowtoDisputes = () => {
    return (
      <View
        style={{
          position: "absolute",
          bottom: 0,
          width: "95%",
          margin: 10,
          borderRadius: 15,
          backgroundColor: colors.white,
          marginBottom: 2,
          alignSelf: "center",
        }}
      >
        <Text
          style={{
            fontSize: metrics.text_description,
            color: colors.black,
            fontWeight: "bold",
            marginTop: 10,
            marginLeft: 10,
          }}
        >
          How to Open Dispute ?
        </Text>
        <Text
          style={{
            fontSize: metrics.text_description,
            color: colors.black,
            fontWeight: "bold",
            marginLeft: 10,
          }}
        >
          First Step :{" "}
        </Text>
        <Text
          style={{
            fontSize: metrics.text_description,
            color: colors.app_gray,
            fontWeight: "bold",
            marginLeft: 10,
          }}
        >
          Before opening a dispute we strongly recommened you to contact the
          seller, to discuss about the issue.
        </Text>
        <Text
          style={{
            fontSize: metrics.text_description,
            color: colors.black,
            fontWeight: "bold",
            marginLeft: 10,
          }}
        >
          Second Step : You can choose between two options{" "}
        </Text>
        <Text
          style={{
            fontSize: metrics.text_description,
            color: colors.app_gray,
            fontWeight: "bold",
            marginLeft: 10,
          }}
        >
          Return Only - this means that you want return the item and apply for a
          full refund.
        </Text>
        <Text
          style={{
            fontSize: metrics.text_description,
            color: colors.app_gray,
            fontWeight: "bold",
            marginLeft: 10,
            marginBottom: 10,
          }}
        >
          Refund Only - this means that either you didn't recieve the item and
          you are apply for a full refund or you did recieve the item but apply
          for a partial refund.
        </Text>
      </View>
    );
  };

  createDisputes = () => {
    if (this.state.reason_name == "") {
      this.refs.toast.show("Please Give Appropriate Reason First!");
    } else {
      this.setState({
        spinvisible: true,
      });

      let postData = {
        order_id: this.state.order_id,
        shop_id: this.state.data.shop.id,
        customer_id: this.state.data.customer_id,
        dispute_type_id: this.state.dispute_id,
        order_received: this.state.disputeData.order_received == null ? 0 : 1,
        description: this.state.description,
        product_id: this.state.product_id,
        refund_amount: this.state.disputeData.grand_total.slice(1),
      };

      console.log("params -->, ", postData);

      axios({
        method: "post",
        url:
          "https://caribmall.com/api/order/" +
          this.state.order_id +
          "/dispute" +
          "?api_token=" +
          this.state.token,
        data: postData,
      })
        .then((response) => {
          console.log("create dispute response --> ", response.data.data);
          this.refs.toast.show("Dispute Created Successfully!");
          this.props.navigation.navigate('Orders')
          this.setState({
            spinvisible: false,
          });
          // }
        })
        .catch((error) => {
          console.log("create dispute response -->  errors ", error);
          this.setState({
            spinvisible: false,
          });
        });
    }
  };

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }}
      >
        <View
          style={{
            backgroundColor: colors.theme_caribpay,
            height: metrics.newView.upperview,
          }}
        >
          <View style={{ flexDirection: "row", margin: 10 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
            >
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                  }}
                  source={require("../../Images/leftarrow.png")}
                />
              </View>
            </TouchableOpacity>

            <Text
              style={{
                fontSize: metrics.text_heading,
                color: colors.white,
                fontWeight: "bold",
                marginTop: 2,
                marginLeft: 10,
                flex: 1,
              }}
            >
              Open Dispute
            </Text>

            <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{ height: 30, width: 30 }}
                  source={require("../../Images/info.png")}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.curveview}>
          <View
            style={{
              flexDirection: "column",
              marginTop: 10,
              marginLeft: 10,
              marginRight: 10,
            }}
          >
            <View style={{ backgroundColor: colors.white, borderRadius: 15 }}>
              <Text
                style={{
                  fontSize: metrics.text_normal,
                  color: colors.theme_caribpay,
                  fontFamily: metrics.quicksand_bold,
                  textAlignVertical: "center",
                  marginLeft: 20,
                  marginTop: 10,
                }}
              >
                Order Details
              </Text>
              <View style={{ flexDirection: "row", margin: 5 }}>
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    color: colors.app_gray,
                    fontFamily: metrics.quicksand_semibold,
                    marginLeft: 15,
                  }}
                >
                  Order Number
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_medium,
                    color: colors.app_black_text,
                    fontFamily: metrics.quicksand_semibold,
                    marginLeft: 30,
                    marginTop: 2,
                  }}
                >
                  {this.state.disputeData.order_number}
                </Text>
              </View>
              <View style={{ flexDirection: "row", margin: 5 }}>
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    color: colors.app_gray,
                    fontFamily: metrics.quicksand_semibold,
                    marginLeft: 15,
                  }}
                >
                  Order Status
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_medium,
                    color: colors.app_black_text,
                    fontFamily: metrics.quicksand_semibold,
                    marginLeft: 40,
                    marginTop: 2,
                  }}
                >
                  {this.state.disputeData.order_status}
                </Text>
              </View>
              <View
                style={{ flexDirection: "row", margin: 5, marginBottom: 10 }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    color: colors.app_gray,
                    fontFamily: metrics.quicksand_semibold,
                    marginLeft: 15,
                  }}
                >
                  Amount Paid
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_medium,
                    color: colors.app_black_text,
                    fontFamily: metrics.quicksand_semibold,
                    marginLeft: 38,
                    marginTop: 2,
                  }}
                >
                  {this.state.disputeData.grand_total}
                </Text>
              </View>
            </View>
          </View>

          <View
            style={{
              backgroundColor: colors.white,
              borderRadius: 15,
              margin: 7,
            }}
          >
            <Text
              style={{
                fontSize: metrics.text_normal,
                marginLeft: 20,
                marginTop: 20,
                fontFamily: metrics.quicksand_bold,
              }}
            >
              
              
            </Text>

            <TouchableOpacity
              onPress={() =>
                this.setState({
                  disputeIdVisible: true,
                })
              }
            >
              <View
                style={{
                  flexDirection: "row",
                  width: "90%",
                  alignSelf: "center",
                  marginTop: 10,
                  justifyContent: "center",
                  backgroundColor: colors.light_grey_backgroud,
                  height: metrics.dimen_40,
                  borderRadius: 10,
                  marginBottom: 10,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    textAlignVertical: "center",
                    marginLeft: 10,
                    flex: 1,
                  }}
                >
                  Reason
                </Text>

                <View
                  style={{ justifyContent: "center", flexDirection: "row" }}
                >
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      textAlignVertical: "center",
                      marginLeft: 10,
                      color: colors.app_gray,
                    }}
                  >
                    {this.state.reason_name}
                  </Text>
                  <Image
                    style={{
                      height: metrics.dimen_20,
                      width: metrics.dimen_20,
                      tintColor: colors.app_gray,
                      alignSelf: "center",
                      marginRight: 10,
                    }}
                    source={require("../../Images/dropdown.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.setState({ isGoodRecvd: true })}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: "90%",
                  alignSelf: "center",
                  marginTop: 1,
                  justifyContent: "center",
                  backgroundColor: colors.light_grey_backgroud,
                  height: metrics.dimen_40,
                  borderRadius: 10,
                  marginBottom: 10,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    textAlignVertical: "center",
                    marginLeft: 10,
                    flex: 1,
                  }}
                >
                  Good Received ?
                </Text>

                <View
                  style={{ justifyContent: "center", flexDirection: "row" }}
                >
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      textAlignVertical: "center",
                      marginLeft: 10,
                      color: colors.app_gray,
                    }}
                  >
                    {this.state.good_recdv_info}
                  </Text>
                  <Image
                    style={{
                      height: metrics.dimen_20,
                      width: metrics.dimen_20,
                      tintColor: colors.app_gray,
                      alignSelf: "center",
                      marginRight: 10,
                    }}
                    source={require("../../Images/dropdown.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>

            <Input
              placeholder={"Refund Amount"}
              placeholderTextColor={colors.black}
              value={this.state.username}
              onChangeText={(text) => this.setState({ username: text })}
              containerStyle={{
                width: "90%",
                height: 40,
                backgroundColor: colors.light_grey_backgroud,
                borderRadius: metrics.dimen_7,
                alignSelf: "center",
                marginBottom: 10,
              }}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              inputStyle={{ fontSize: 14, color: colors.black }}
            />

            <Input
              placeholder={"Description"}
              placeholderTextColor={colors.black}
              value={this.state.description}
              onChangeText={(text) => this.setState({ description: text })}
              containerStyle={{
                width: "90%",
                height: 80,
                backgroundColor: colors.light_grey_backgroud,
                borderRadius: metrics.dimen_7,
                alignSelf: "center",
                marginBottom: 10,
              }}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              inputStyle={{ fontSize: 14, color: colors.black }}
            />

            <View
              style={{
                alignItems: "flex-end",
                marginRight: 15,
                flexDirection: "row",
                justifyContent: "flex-end",
                marginTop: 5,
                marginBottom: 10,
              }}
            >
              <TouchableOpacity onPress={() => this.createDisputes()}>
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    backgroundColor: colors.theme_caribpay,
                    color: colors.white,
                    fontWeight: "bold",
                    padding: 10,
                    borderRadius: 20,
                  }}
                >
                  {"Open Dispute"}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <RBSheet
          ref={(ref) => {
            this.RBSheetAlert = ref;
          }}
          height={200}
          duration={0}
        >
          {this.renderHowtoDisputes()}
        </RBSheet>

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisible}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.isGoodRecvd}
        >
          <View
            style={{
              width: "70%",
              alignSelf: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: 15,
              height: 200,
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  isGoodRecvd: false,
                })
              }
            >
              <Image
                style={{
                  height: metrics.dimen_16,
                  width: metrics.dimen_16,
                  resizeMode: "contain",
                  margin: 15,
                }}
                source={require("../../Images/cross-sign.png")}
              />
            </TouchableOpacity>

            <Text
              style={{
                alignSelf: "center",
                fontFamily: metrics.quicksand_bold,
                color: colors.black,
                fontSize: metrics.text_16,
                textAlign: "center",
                textAlignVertical: "center",
                marginBottom: 10,
              }}
            >
              Goods Received ?
            </Text>

            <TouchableOpacity
              onPress={() =>
                this.setState({
                  good_recdv_info: "Yes",
                  isGoodRecvd: false,
                })
              }
            >
              <View
                style={{
                  backgroundColor: colors.app_gray,
                  borderRadius: 5,
                  height: 40,
                  width: "90%",
                  alignSelf: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    alignSelf: "center",
                    fontFamily: metrics.quicksand_bold,
                    color: colors.black,
                    fontSize: metrics.text_16,
                    textAlign: "center",
                    textAlignVertical: "center",
                  }}
                >
                  YES
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.setState({
                  good_recdv_info: "No",
                  isGoodRecvd: false,
                })
              }
            >
              <View
                style={{
                  backgroundColor: colors.app_gray,
                  borderRadius: 5,
                  height: 40,
                  width: "90%",
                  alignSelf: "center",
                  marginTop: 10,
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    alignSelf: "center",
                    fontFamily: metrics.quicksand_bold,
                    color: colors.black,
                    fontSize: metrics.text_16,
                    textAlign: "center",
                    textAlignVertical: "center",
                  }}
                >
                  No
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </Modal>

        <Modal
          style={{ borderRadius: metrics.dimen_10, margin: 10 }}
          isVisible={this.state.disputeIdVisible}
        >
          <View
            style={{
              width: "80%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  disputeIdVisible: false,
                })
              }
            >
              <Image
                style={{
                  height: metrics.dimen_16,
                  width: metrics.dimen_16,
                  resizeMode: "contain",
                  margin: 15,
                }}
                source={require("../../Images/cross-sign.png")}
              />
            </TouchableOpacity>

            <Text
              style={{
                fontFamily: metrics.quicksand_bold,
                fontSize: metrics.text_large,
                color: colors.black,
                alignSelf: "center",
                marginBottom: 20,
              }}
            >
              {"Select Reason"}
            </Text>

            <FlatList
              style={{ marginBottom: metrics.dimen_10 }}
              data={this.state.disputeIdAray}
              showsVerticalScrollIndicator={false}
              ItemSeparatorComponent={() => (
                <View style={styles.horizontalLine} />
              )}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      reason_name: item.name,
                      disputeIdVisible: false,
                      dispute_id: parseInt(item.dispute_id),
                    })
                  }
                >
                  <Text
                    style={{
                      fontFamily: metrics.quicksand_semibold,
                      fontSize: metrics.text_normal,
                      color: colors.black,
                      margin: 5,
                      alignSelf: "center",
                    }}
                  >
                    {item.name}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
        </Modal>

        <Toast
          ref="toast"
          style={{ backgroundColor: "black" }}
          position="center"
          positionValue={200}
          fadeInDuration={200}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: "white" }}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  horizontalLine: {
    borderBottomColor: "#f2f2f2",
    borderBottomWidth: 2,
    width: "100%",
    alignSelf: "center",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
  },
  curveview: {
    height: metrics.newView.curveview + 160,
    position: "absolute",
    top: metrics.newView.curvetop - 110,
    width: "100%",
    backgroundColor: colors.light_grey_backgroud,
    borderRadius: 40,
  },
});
