import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  BackHandler,
  FlatList,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
import StarRating from "react-native-star-rating";
import AsyncStorage from "@react-native-community/async-storage";
const axios = require("axios");
import Modal from "react-native-modal";
var Spinner = require("react-native-spinkit");

export default class OrderDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      order_id: this.props.navigation.state.params.order_id,
      apiToken: "",
      data: [],
      shop_image: "",
      order_items: [],
    };
  }

  componentDidMount() {
    this.getOrderDetails();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  getOrderDetails = async () => {
    this.setState({
      spinvisible: true,
    });
    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    this.setState({
      apiToken: data.api_token,
    });
    axios({
      method: "get",
      url:
        "https://caribmall.com/api/order/" +
        this.state.order_id +
        "?api_token=" +
        data.api_token,
    })
      .then((response) => {
        console.log("response ordder details --", response.data.data);
        this.setState({
          data: response.data.data,
          spinvisible: false,
          shop_image: response.data.data.shop.image,
          shop_name: response.data.data.shop.name,
          shop_rating: response.data.data.shop.rating,
          order_items: response.data.data.items,
        });
      })
      .catch((error) => {
        console.log("response coupons errors ", error);
      });
  };

  trackProduct = () => {
    this.setState({
      trackvisible: true,
      spintrackvisible: true,
    });
    axios({
      method: "get",
      url:
        "https://caribmall.com/api/order/" +
        this.state.order_id +
        "/track?api_token=" +
        this.state.apiToken,
    })
      .then((response) => {
        console.log("response track detail --", response.data);
        this.setState({
          spintrackvisible: false,
          tracking_status: response.data.tracking_url,
          trackvisible: true,
        });
      })
      .catch((error) => {
        console.log("response track detail  errors ", error);
        this.setState({
          // trackvisible: false,
          spintrackvisible: false,
        });
      });
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
        <View
          style={{
            backgroundColor: colors.theme_caribpay,
            height: metrics.newView.upperview,
          }}
        >
          <View style={{ flexDirection: "row", margin: 10 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
            >
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                  }}
                  source={require("../../Images/leftarrow.png")}
                />
              </View>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: metrics.text_heading,
                color: colors.white,
                fontWeight: "bold",
                marginTop: 2,
                marginLeft: 10,
                flex: 1,
              }}
            >
              Order Details
            </Text>
          </View>
        </View>
        <View style={styles.curveview}>
          <ScrollView
            style={{ marginTop: 10, marginBottom: 20 }}
            showsVerticalScrollIndicator={false}
          >
            <View style={{ flexDirection: "column", margin: 7 }}>
              <View
                style={{
                  backgroundColor: colors.white,
                  borderRadius: 15,
                  margin: 7,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    color: colors.theme_caribpay,
                    fontFamily: metrics.quicksand_bold,
                    marginLeft: 20,
                    marginTop: 10,
                  }}
                >
                  Order Details
                </Text>

                <View style={{ flexDirection: "row", margin: 5 }}>
                  <Text style={styles.order_header}>Order No. </Text>
                  <Text style={styles.order_info}>
                    {this.state.data.order_number}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", margin: 5 }}>
                  <Text style={styles.order_header}>Date</Text>
                  <Text style={styles.order_info}>
                    {this.state.data.order_date}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", margin: 5 }}>
                  <Text style={styles.order_header}>Order Status</Text>
                  <Text style={styles.order_info}>
                    {this.state.data.order_status == "CANCELED" ? "CANCELLED": this.state.data.order_status}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", margin: 5 }}>
                  <Text style={styles.order_header}>Shipping Address</Text>
                  <Text style={styles.order_info}>
                    {this.state.data.shipping_address}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", margin: 5 }}>
                  <Text style={styles.order_header}>Billing Address</Text>
                  <Text style={styles.order_info}>
                    {this.state.data.billing_address}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", margin: 5 }}>
                  <Text style={styles.order_header}>Shipping Cost</Text>
                  <Text style={styles.order_info}>
                    {this.state.data.shipping}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", margin: 5 }}>
                  <Text style={styles.order_header}>Packaging Cost</Text>
                  <Text style={styles.order_info}>{"$0.00"}</Text>
                </View>
                <View style={{ flexDirection: "row", margin: 5 }}>
                  <Text style={styles.order_header}>Shipping Weight</Text>
                  <Text style={styles.order_info}>
                    {this.state.data.shipping_weight}
                  </Text>
                </View>

                <TouchableOpacity onPress={() => this.trackProduct()}>
                  <View
                    style={{ marginBottom: 5, marginLeft: 15, marginTop: 20 }}
                  >
                    <Text
                      style={{
                        fontSize: metrics.text_header,
                        color: colors.app_blue,
                        fontWeight: "bold",
                        padding: 5,
                        textDecorationLine: "underline",
                      }}
                    >
                      {"Track Your Product"}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: "column",
                  backgroundColor: "white",
                  marginTop: 5,
                  width: "95%",
                  alignSelf: "center",
                  borderRadius: 10,
                }}
              >
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    color: colors.theme_caribpay,
                    fontFamily: metrics.quicksand_bold,
                    marginLeft: 20,
                    marginTop: 10,
                  }}
                >
                  Product Details
                </Text>

                <View style={{ flexDirection: "row", marginTop: 20 }}>
                  <Image
                    style={{
                      height: 30,
                      width: 30,
                      resizeMode: "contain",
                      marginLeft: 5,
                    }}
                    source={{ uri: this.state.shop_image }}
                  />
                  <View
                    style={{
                      flexDirection: "column",
                      marginLeft: 10,
                      marginTop: -3,
                    }}
                  >
                    <Text
                      style={{
                        alignSelf: "center",
                        fontSize: metrics.text_normal,
                        color: colors.theme_caribpay,
                        fontFamily: metrics.quicksand_bold,
                      }}
                    >
                      {this.state.shop_name}
                    </Text>
                    <View style={{ width: 70, marginRight: 10 }}>
                      <StarRating
                        disabled={false}
                        maxStars={5}
                        starSize={12}
                        fullStarColor={colors.app_yellow_color}
                        rating={
                          this.state.shop_rating != null
                            ? this.state.shop_rating
                            : 0
                        }
                      />
                    </View>
                  </View>
                </View>

                <View style={{ flexDirection: "row", margin: 20 }}>
                  <FlatList
                    style={{ marginBottom: metrics.dimen_10 }}
                    data={this.state.order_items}
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={() => (
                      <View style={styles.horizontalLine} />
                    )}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                      <View style={{ flexDirection: "row", margin: 15 }}>
                        <Image
                          style={{
                            height: 40,
                            width: 40,
                            resizeMode: "contain",
                          }}
                          source={{ uri: item.image }}
                        />
                        <View
                          style={{
                            flexDirection: "column",
                            justifyContent: "center",
                            marginLeft: 15,
                            flex: 1,
                          }}
                        >
                          <Text
                            style={{
                              fontSize: metrics.text_description,
                              color: colors.black,
                              fontFamily: metrics.quicksand_semibold,
                            }}
                          >
                            {item.description}
                          </Text>
                          <Text
                            style={{
                              fontSize: metrics.text_normal,
                              color: colors.app_red,
                              marginTop: 5,
                            }}
                          >
                            {item.unit_price}
                            <Text
                              style={{
                                fontSize: metrics.text_normal,
                                color: colors.app_gray,
                              }}
                            >
                              {" x " + item.quantity}
                            </Text>
                          </Text>
                        </View>
                      </View>
                    )}
                  />

                  {/* <Image style={{ height: 40, width: 40, resizeMode: 'contain', alignSelf: 'center' }}
                                        source={require('../../Images/guitar.png')}></Image>

                                    <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 15, flex: 1 }}>
                                        <Text style={{ fontSize: metrics.text_description, color: colors.black }}>Kadence Acoustica Series Semi Acoustic Guitar (36-inch)</Text>
                                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_red, marginTop: 5 }}>{"$445.38"}
                                            <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>{" x 1"}</Text>
                                        </Text>
                                    </View>
                                </View> */}
                  {/* <View style={styles.horizontalLine}></View> */}
                  {/* <View style={{ flexDirection: 'row', margin: 20 }}>
                                    <Image style={{ height: 40, width: 40, resizeMode: 'contain', alignSelf: 'center' }}
                                        source={require('../../Images/headphones.png')}></Image>
                                    <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 15, flex: 1 }}>
                                        <Text style={{ fontSize: metrics.text_description, color: colors.black }}>Sennheiser HD 205 II (Black)</Text>
                                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_red, marginTop: 5 }}>{"$445.38"}
                                            <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray }}>{" x 1"}</Text>
                                        </Text>
                                    </View>
                                </View> */}
                  {/* <TouchableOpacity onPress={() => this.props.navigation.navigate("OrderEvaluations")}>
                                    <View style={{ alignItems: 'flex-end', padding: 10 }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: metrics.text_medium, color: colors.app_gray, textDecorationLine: 'underline' }}>{"Order Evaluations"}</Text>
                                    </View>
                                </TouchableOpacity> */}
                </View>
              </View>

              <View
                style={{
                  flexDirection: "column",
                  margin: 7,
                  marginBottom: 20,
                  width: "99%",
                  alignSelf: "center",
                }}
              >
                <View
                  style={{
                    backgroundColor: colors.white,
                    borderRadius: 10,
                    margin: 7,
                  }}
                >
                  <Text
                    style={{
                      fontSize: metrics.text_header,
                      color: colors.theme_caribpay,
                      fontFamily: metrics.quicksand_bold,
                      marginLeft: 20,
                      marginTop: 10,
                    }}
                  >
                    Payment Details
                  </Text>
                  <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text style={styles.order_header}>Product Price </Text>
                    <Text style={styles.order_info}>
                      {this.state.data.total}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text style={styles.order_header}>Taxes</Text>
                    <Text style={styles.order_info}>
                      {this.state.data.taxes == null
                        ? "$0.00"
                        : this.state.data.taxes}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text style={styles.order_header}>Shipping </Text>
                    <Text style={styles.order_info}>
                      {this.state.data.shipping}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", margin: 5 }}>
                    <Text style={styles.order_header}>NET TOTAL </Text>
                    <Text
                      style={{
                        fontSize: metrics.text_header,
                        color: colors.app_black_text,
                        fontFamily: metrics.quicksand_bold,
                        flex: 1 / 2,
                      }}
                    >
                      {this.state.data.grand_total}
                    </Text>
                  </View>
                </View>
                <View style={{ height: 50 }} />
              </View>
            </View>

            <TouchableOpacity
              style={{
                position: "absolute",
                bottom: 10,
                backgroundColor: colors.theme_caribpay,
                height: 40,
                borderRadius: 20,
                width: "90%",
                marginLeft: 2,
                alignSelf: "center",
                justifyContent: "center",
              }}
              onPress={() =>
                this.state.data.dispute_id == null ?
                this.props.navigation.navigate("OpenDisputes", {
                  order_id: this.state.order_id,
                  token: this.state.apiToken,
                  data: this.state.data,
                })
                :
                this.props.navigation.navigate("DisputeDetails", {
                  token: this.state.apiToken,
                  dispute_id: this.state.data.dispute_id,
                })
              }
            >
              <View>
                <Text
                  style={{
                    alignSelf: "center",
                    fontFamily: metrics.quicksand_bold,
                    color: colors.white,
                  }}
                >
                  Open Dispute
                </Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
        </View>

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisible}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.trackvisible}
        >
          <View
            style={{
              width: "80%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 170,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spintrackvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
            {!this.state.spintrackvisible && (
              <View style={{ margin: 10 }}>
                <Image
                  source={require("../../Images/pointer.png")}
                  style={{
                    height: metrics.dimen_60,
                    width: metrics.dimen_60,
                    resizeMode: "center",
                    alignSelf: "center",
                  }}
                />
                <Text
                  style={{
                    alignSelf: "center",
                    textAlign: "center",
                    marginTop: 10,
                  }}
                >
                  {this.state.tracking_status == null
                    ? "Relax,we will update you once we track your product!"
                    : this.state.tracking_status}
                </Text>
                <TouchableOpacity
                  onPress={() => this.setState({ trackvisible: false })}
                >
                  <View
                    style={{
                      width: 100,
                      backgroundColor: colors.theme_caribpay,
                      height: 30,
                      borderRadius: 5,
                      alignSelf: "center",
                      justifyContent: "center",
                      marginTop: 10,
                      marginBottom: 5,
                    }}
                  >
                    <Text
                      style={{
                        alignSelf: "center",
                        textAlign: "center",
                        color: colors.white,
                      }}
                    >
                      {"OK"}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  horizontalLine: {
    borderBottomColor: "#f2f2f2",
    borderBottomWidth: 1,
    width: "100%",
    alignSelf: "center",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
  },
  curveview: {
    height: metrics.newView.curveview + 160,
    position: "absolute",
    top: metrics.newView.curvetop - 110,
    width: "100%",
    backgroundColor: colors.light_grey_backgroud,
    borderRadius: 40,
  },
  order_header: {
    fontSize: metrics.text_normal,
    color: colors.app_gray,
    marginLeft: 15,
    flex: 1 / 2,
    fontFamily: metrics.quicksand_bold,
  },
  order_info: {
    fontSize: metrics.text_medium,
    color: colors.app_black_text,
    fontFamily: metrics.quicksand_semibold,
    flex: 1 / 2,
  },
});
