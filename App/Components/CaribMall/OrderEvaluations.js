import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  BackHandler,
  Dimensions,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
import StarRating from "react-native-star-rating";
import { Input } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import AsyncStorage from "@react-native-community/async-storage";
import Modal from "react-native-modal";
var Spinner = require("react-native-spinkit");
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
const axios = require("axios");
import DropdownAlert from "react-native-dropdownalert";

export default class OrderEvaluations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      starCountSeller: 0,
      starCountProduct: 0,
      data: this.props.navigation.state.params.data,
      commentSeller: "",
      commentProduct: "",
      apiToken: "",
      isRated: false,
      isproductRated: false,
    };
  }

  async componentDidMount() {
    console.log("datais-->", this.state.data);
    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    this.setState({
      apiToken: data.api_token,
    });
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  onStarRatingPress(rating) {
    this.setState({
      starCountProduct: rating,
    });
  }

  onStarRatingPressSeller(rating) {
    this.setState({
      starCountSeller: rating,
    });
  }

  giveFeedback = (item) => {
    if (this.state.starCountProduct == 0 || this.state.commentProduct == "") {
      this.dropDownAlertRef.alertWithType(
        "error",
        this.state.starCountProduct == 0
          ? "Please rate your product"
          : "Please give your feedback"
      );
    } else {
      this.setState({
        isRated: true,
        spinRatedvisible: true,
      });

      // let postData = {
      //   rating: this.state.starCountProduct,
      //   comment: this.state.commentProduct,
      // };

      let postData = {
        items: [
          {
            id: item.id,
            rating: this.state.starCountProduct,
            comment: this.state.commentProduct,
          },
        ],
      };

      axios({
        method: "post",
        url:
          "https://caribmall.com/api/order/" +
          this.state.data.id +
          "/feedback?api_token=" +
          this.state.apiToken,
        data: postData,
      })
        .then((response) => {
          console.log("response feedback ---> ", response.data);
          this.setState({
            spinRatedvisible: false,
            isproductRated: true,
          });
        })
        .catch((error) => {
          console.log("response feedback error ---> ", error);
          this.setState({
            spinRatedvisible: false,
            isRated: false,
          });
        });
    }
  };

  giveFeedbackSeller = () => {
    if (this.state.starCountSeller == 0 || this.state.commentSeller == "") {
      this.dropDownAlertRef.alertWithType(
        "error",
        this.state.starCountSeller == 0
          ? "Please rate your Seller"
          : "Please give your Seller Feedback"
      );
    } else {
      this.setState({
        isRated: true,
        spinRatedvisible: true,
      });

      let postData = {
        customer_id: this.state.data.customer_id,
        rating: this.state.starCountSeller,
        comment: this.state.commentSeller,
      };

      axios({
        method: "post",
        url:
          "https://caribmall.com/api/shop/" +
          this.state.data.id +
          "/feedback?api_token=" +
          this.state.apiToken,
        data: postData,
      })
        .then((response) => {
          console.log("response feedback ---> ", response.data);
          this.setState({
            spinRatedvisible: false,
          });
        })
        .catch((error) => {
          if (error.response) {
            // console.log("response feedback error ---> ", error.response.data.comment);
            this.setState({
              spinRatedvisible: false,
              isRated: false,
              // isComingSoon:true,
              // facilittyName:error.response.data.message
            });
          }
        });
    }
  };

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }}
      >
        <KeyboardAwareScrollView
          contentContainerStyle={{ flex: 1 }}
          showsVerticalScrollIndicator={false}
        >
          <View
            style={{
              backgroundColor: colors.theme_caribpay,
              height: metrics.newView.upperview,
            }}
          >
            <View style={{ flexDirection: "row", margin: 10 }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.goBack(null)}
              >
                <View
                  style={{
                    height: metrics.dimen_40,
                    width: metrics.dimen_40,
                    justifyContent: "center",
                  }}
                >
                  <Image
                    style={{
                      height: metrics.dimen_25,
                      width: metrics.dimen_25,
                      tintColor: "white",
                    }}
                    source={require("../../Images/leftarrow.png")}
                  />
                </View>
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: metrics.text_heading,
                  color: colors.white,
                  fontWeight: "bold",
                  marginTop: 5,
                }}
              >
                Order Rating
              </Text>
              {/* <TouchableOpacity onPress={() => this.giveFeedback()}
                                style={{ fontSize: metrics.text_normal, color: colors.white, fontWeight: 'bold', textAlign: 'right', alignSelf: 'center', marginLeft: 150, marginTop: -4, borderColor: 'white', borderWidth: 0.5, padding: 7, borderRadius: metrics.dimen_7 }}>
                                <Text style={{ color: colors.white }}>Submit</Text>
                            </TouchableOpacity> */}
            </View>
          </View>
          <View style={styles.curveview}>
            <ScrollView style={{ marginTop: 5 }}>
              <View
                style={{
                  flexDirection: "column",
                  margin: 15,
                  backgroundColor: "white",
                  borderRadius: 10,
                }}
              >
                <View style={{ margin: 10, flex: 1 }}>
                  <Text
                    style={{
                      fontSize: metrics.text_header,
                      color: colors.theme_caribpay,
                      fontWeight: "bold",
                    }}
                  >
                    Rate Seller
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: 30,
                      marginLeft: 5,
                    }}
                  >
                    <Image
                      style={{ height: 40, width: 40, alignSelf: "center" }}
                      source={{ uri: this.state.data.shop.image }}
                    />
                    <Text
                      style={{
                        fontSize: metrics.text_normal,
                        color: colors.theme_caribpay,
                        fontWeight: "bold",
                        marginLeft: 7,
                        marginTop: -3,
                      }}
                    >
                      {this.state.data.shop.name}
                    </Text>
                  </View>
                  <View
                    style={{
                      width: "100%",
                      alignSelf: "center",
                      height: 40,
                      backgroundColor: colors.light_grey_backgroud,
                      justifyContent: "center",
                      marginTop: 20,
                      borderRadius: metrics.dimen_7,
                    }}
                  >
                    <View
                      style={{
                        width: 200,
                        alignSelf: "center",
                        marginLeft: 10,
                      }}
                    >
                      <StarRating
                        disabled={false}
                        maxStars={5}
                        starSize={20}
                        fullStarColor={colors.app_yellow_color}
                        rating={this.state.starCountSeller}
                        selectedStar={(rating) =>
                          this.onStarRatingPressSeller(rating)
                        }
                      />
                    </View>
                  </View>
                  <Input
                    placeholder={"Write your Feedback"}
                    placeholderTextColor={colors.app_gray}
                    containerStyle={{
                      height: metrics.dimen_100,
                      borderRadius: metrics.dimen_7,
                      marginTop: metrics.dimen_10,
                      backgroundColor: colors.light_grey_backgroud,
                      marginBottom: 10,
                    }}
                    value={this.state.commentSeller}
                    multiline={true}
                    numberOfLines={10}
                    inputContainerStyle={{
                      borderBottomWidth: 0,
                      justifyContent: "center",
                      alignItems: this.multiline ? "flex-start" : "center",
                    }}
                    inputStyle={{
                      fontSize: 14,
                      color: colors.app_gray,
                      fontWeight: "bold",
                      textAlign: "center",
                    }}
                    onChangeText={(text) =>
                      this.setState({ commentSeller: text })
                    }
                  />

                  <TouchableOpacity onPress={() => this.giveFeedbackSeller()}>
                    <View style={styles.bottomview2}>
                      <Text
                        style={{
                          fontFamily: metrics.quicksand_bold,
                          color: colors.white,
                          fontSize: metrics.text_16,
                          textAlign: "center",
                        }}
                      >
                        Submit
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  flexDirection: "column",
                  margin: 15,
                  backgroundColor: "white",
                  borderRadius: 10,
                  marginTop: -5,
                  marginBottom: 10,
                  width: "93%",
                }}
              >
                <FlatList
                  style={{ marginBottom: metrics.dimen_10 }}
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                  data={this.state.data.items}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item }) => (
                    <View
                      style={{
                        margin: 10,
                        width: Dimensions.get("screen").width - 50,
                      }}
                    >
                      <Text
                        style={{
                          fontSize: metrics.text_header,
                          color: colors.theme_caribpay,
                          fontWeight: "bold",
                        }}
                      >
                        Rate Product
                      </Text>
                      <View style={{ flexDirection: "row", marginTop: 30 }}>
                        <Image
                          style={{
                            height: 40,
                            width: 40,
                            resizeMode: "contain",
                            alignSelf: "center",
                          }}
                          source={{ uri: item.image }}
                        />
                        <View
                          style={{
                            flexDirection: "column",
                            justifyContent: "center",
                            marginLeft: 20,
                            flex: 1,
                          }}
                        >
                          <Text
                            style={{
                              fontWeight: "bold",
                              fontSize: metrics.text_normal,
                              color: colors.black,
                            }}
                          >
                            {item.title}
                          </Text>
                        </View>
                      </View>
                      <View
                        style={{
                          width: "100%",
                          alignSelf: "center",
                          height: 40,
                          backgroundColor: colors.light_grey_backgroud,
                          justifyContent: "center",
                          marginTop: 20,
                          borderRadius: 7,
                        }}
                      >
                        <View
                          style={{
                            width: 200,
                            alignSelf: "center",
                            marginLeft: 10,
                          }}
                        >
                          <StarRating
                            disabled={false}
                            maxStars={5}
                            starSize={20}
                            fullStarColor={colors.app_yellow_color}
                            rating={this.state.starCountProduct}
                            selectedStar={(rating) =>
                              this.onStarRatingPress(rating)
                            }
                          />
                        </View>
                      </View>
                      <Input
                        placeholder={"Write your Feedback"}
                        placeholderTextColor={colors.app_gray}
                        containerStyle={{
                          height: metrics.dimen_100,
                          borderRadius: metrics.dimen_7,
                          marginTop: metrics.dimen_10,
                          backgroundColor: colors.light_grey_backgroud,
                          width: "100%",
                        }}
                        value={this.state.commentProduct}
                        multiline={true}
                        numberOfLines={10}
                        inputContainerStyle={{
                          borderBottomWidth: 0,
                          justifyContent: "center",
                          alignItems: this.multiline ? "flex-start" : "center",
                        }}
                        inputStyle={{
                          fontSize: 14,
                          color: colors.app_gray,
                          fontWeight: "bold",
                          textAlign: "center",
                        }}
                        onChangeText={(text) =>
                          this.setState({ commentProduct: text })
                        }
                      />
                      <TouchableOpacity onPress={() => this.giveFeedback(item)}>
                        <View style={styles.bottomview2}>
                          <Text
                            style={{
                              fontFamily: metrics.quicksand_bold,
                              color: colors.white,
                              fontSize: metrics.text_16,
                              textAlign: "center",
                            }}
                          >
                            Submit
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  )}
                />
              </View>
              <View style={{ height: 300 }} />
            </ScrollView>
          </View>

          <DropdownAlert ref={(ref) => (this.dropDownAlertRef = ref)} />

          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.isRated}
          >
            <View
              style={{
                width: "80%",
                alignSelf: "center",
                justifyContent: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_5,
                margin: metrics.dimen_20,
                height: this.state.isproductRated ? 280 : 200,
              }}
            >
              <Spinner
                style={{ alignSelf: "center" }}
                isVisible={this.state.spinRatedvisible}
                size={70}
                type={"ThreeBounce"}
                color={colors.black}
              />
              {!this.state.spinRatedvisible && (
                <View style={{ margin: 10 }}>
                  {this.state.isproductRated == false && <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        isRated: false,
                      })
                    }
                  >
                    <Image
                      source={require("../../Images/cross-sign.png")}
                      style={{
                        height: metrics.dimen_16,
                        width: metrics.dimen_16,
                        resizeMode: "center",
                      }}
                    />
                  </TouchableOpacity>
}
                  <Image
                    source={require("../../Images/ratingus.png")}
                    style={{
                      height: metrics.dimen_100,
                      width: metrics.dimen_100,
                      resizeMode: "center",
                      alignSelf: "center",
                    }}
                  />
                  <Text
                    style={{
                      alignSelf: "center",
                      textAlign: "center",
                      marginTop: 15,
                      fontFamily: metrics.quicksand_bold,
                    }}
                  >
                    {"Thanks for Rating Us!"}
                  </Text>

                  {this.state.isproductRated && (
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({ isRated: false }, () =>
                          this.props.navigation.navigate("CaribMallHome")
                        )
                      }
                    >
                      <View
                        style={{
                          width: "90%",
                          backgroundColor: colors.theme_caribpay,
                          height: 40,
                          borderRadius: 20,
                          alignSelf: "center",
                          justifyContent: "center",
                          marginTop: 15,
                        }}
                      >
                        <Text
                          style={{
                            alignSelf: "center",
                            textAlign: "center",
                            color: colors.white,
                          }}
                        >
                          {"Back to Home"}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )}
                </View>
              )}
            </View>
          </Modal>


          <View>
                  <Modal
                    style={{ borderRadius: metrics.dimen_10 }}
                    isVisible={this.state.isComingSoon}
                  >
                    <View
                      style={{
                        width: "90%",
                        alignSelf: "center",
                        backgroundColor: colors.white,
                        borderRadius: metrics.dimen_10,
                        // margin: metrics.dimen_10,
                        flexDirection: "column",
                        height: 200,
                        justifyContent: "center",
                      }}
                    >
                      <View style={{ flex: 1 }}>
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({
                              isComingSoon: false,
                            })
                          }
                        >
                          <Image
                            style={{
                              height: metrics.dimen_16,
                              width: metrics.dimen_16,
                              resizeMode: "contain",
                              margin: 15,
                            }}
                            source={require("../../Images/cross-sign.png")}
                          />
                        </TouchableOpacity>
                        <Image
                          style={{
                            height: 80,
                            width: 80,
                            resizeMode: "cover",
                            alignSelf: "center",
                            marginTop: -20,
                          }}
                          source={require("../../Images/danger.png")}
                        />
                        <Text
                          style={{
                            fontSize: metrics.text_heading,
                            color: colors.black,
                            fontFamily: metrics.quicksand_bold,
                            textAlign: "center",
                            marginTop: 20,
                          }}
                        >
                          {this.state.facilittyName}
                        </Text>
                      </View>
                    </View>
                  </Modal>
                </View>




        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  horizontalLine: {
    borderBottomColor: "#f2f2f2",
    borderBottomWidth: 8,
    width: "100%",
    alignSelf: "center",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
  },
  curveview: {
    height: metrics.newView.curveview + 160,
    position: "absolute",
    top: metrics.newView.curvetop - 100,
    width: "100%",
    backgroundColor: colors.light_grey_backgroud,
    borderRadius: 40,
  },
  textInputContainer: {
    flexDirection: "row",
    paddingLeft: 8,
    paddingRight: 8,
  },
  textInput: {
    paddingLeft: 10,
    fontSize: 17,
    flex: 1,
    backgroundColor: "white",
    borderWidth: 0,
    borderRadius: 4,
  },
  bottomview2: {
    height: 40,
    backgroundColor: colors.theme_caribpay,
    width: "97%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 20,
    marginTop: 15,
  },
});
