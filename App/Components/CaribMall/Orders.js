import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
import StarRating from "react-native-star-rating";
import AsyncStorage from "@react-native-community/async-storage";
const axios = require("axios");
import Modal from "react-native-modal";
var Spinner = require("react-native-spinkit");

export default class Orders extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      data: [],
    };
  }

  componentDidMount() {
    this.getOrderList();
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getOrderList();
      }
    );
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  getOrderList = async () => {
    this.setState({
      spinvisible: true,
    });
    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    this.setState({
      apiToken: data.api_token,
    });
    axios({
      method: "get",
      url: "https://caribmall.com/api/orders?api_token=" + data.api_token,
    })
      .then((response) => {
        console.log("response orders data --> ", response.data.data);
        console.log(
          "response orders data items --> ",
          response.data.data[0].items
        );
        this.setState({
          data: response.data.data,
          spinvisible: false,
        });
      })
      .catch((error) => {
        console.log("response coupons errors ", error);
      });
  };

  ongoodsRecieved = (item) => {
    this.setState({
      goodsRecvd: true,
      spingoodsRecvd: true,
    });
    axios({
      method: "post",
      url:
        "https://caribmall.com/api/order/" +
        item.id +
        "/goodsReceived?api_token=" +
        this.state.apiToken,
    })
      .then((response) => {
        console.log(response.data.data);
        // if (response.data.data.order_status == "DELIVERED") {
        this.setState({
          spingoodsRecvd: false,
        });
        // }
      })
      .catch((error) => {
        console.log("response coupons errors ", error);
        this.setState({
          spingoodsRecvd: false,
        });
      });
  };

  onCancelOrder = (data) => {
    this.setState({
      spinvisible: true,
    });
    axios({
      method: "put",
      url:
        "https://caribmall.com/api/order/cancel/" +
        data.id +
        "?api_token=" +
        this.state.apiToken,
    })
      .then((response) => {
        console.log("order cancel repsonse --> ", response.data.data);
        this.setState({
          spinvisible: false,
        });

        this.getOrderList();
      })
      .catch((error) => {
        console.log("order cancel repsonse errors ", error);
        this.setState({
          spinvisible: false,
        });
      });
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
        <View
          style={{
            backgroundColor: colors.theme_caribpay,
            height: metrics.newView.upperview,
          }}
        >
          <View style={{ flexDirection: "row", margin: 10 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
            >
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                  }}
                  source={require("../../Images/leftarrow.png")}
                />
              </View>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: metrics.text_heading,
                color: colors.white,
                fontWeight: "bold",
                marginTop: 2,
                marginLeft: 10,
              }}
            >
              Orders
            </Text>
          </View>
        </View>
        <View
          style={{
            height: metrics.newView.curveview + 160,
            position: "absolute",
            top: metrics.newView.curvetop - 110,
            width: "100%",
            backgroundColor:
              this.state.data.length > 0
                ? colors.light_grey_backgroud
                : colors.white,
            borderRadius: 40,
          }}
        >
          {this.state.data.length > 0 && (
            <View style={{ flexDirection: "row", margin: 7 }}>
              <FlatList
                style={{
                  marginBottom: metrics.dimen_50,
                  marginTop: 5,
                  backgroundColor: colors.white,
                  borderTopLeftRadius: 10,
                  borderTopRightRadius: 10,
                }}
                showsVerticalScrollIndicator={false}
                data={this.state.data}
                ItemSeparatorComponent={() => (
                  <View
                    style={{
                      borderBottomWidth: 7,
                      borderColor: colors.light_grey_backgroud,
                      marginTop: 2,
                      marginBottom: 10,
                    }}
                  />
                )}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <View style={{ flexDirection: "column" }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate("OrderDetails", {
                          order_id: item.id,
                        })
                      }
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                        }}
                      >
                        <View style={{ marginLeft: 10 }}>
                          <View style={{ flexDirection: "row" }}>
                            <Text
                              style={{
                                fontFamily: metrics.quicksand_bold,
                                fontSize: metrics.text_16,
                                color: colors.black,
                              }}
                            >
                              {"Order " + item.order_number}
                            </Text>

                            <Text
                              style={{
                                fontFamily: metrics.quicksand_semibold,
                                fontSize: 10,
                                color: colors.white,
                                justifyContent: "center",
                                textAlignVertical: "center",
                                borderRadius: 5,
                                backgroundColor:
                                  item.order_status == "CANCELED"
                                    ? colors.app_red
                                    : colors.white,
                                marginLeft: 10,
                                marginTop: 5,
                                padding: 5,
                              }}
                            >
                              {item.order_status == "CANCELED"
                                ? "CANCELLED"
                                : ""}
                            </Text>
                          </View>

                          <Text
                            style={{
                              fontFamily: metrics.quicksand_regular,
                              fontSize: metrics.text_normal,
                              color: colors.app_gray,
                            }}
                          >
                            {"Placed on " + item.order_date}
                          </Text>
                        </View>

                        <Text
                          style={{
                            fontFamily: metrics.quicksand_bold,
                            fontSize: metrics.text_normal,
                            color:
                              item.payment_status == "PAID"
                                ? colors.app_green
                                : colors.app_gray,
                            marginRight: 10,
                          }}
                        >
                          {item.payment_status}
                        </Text>
                      </View>

                      <FlatList
                        style={{ marginBottom: metrics.dimen_10, marginTop: 5 }}
                        showsVerticalScrollIndicator={false}
                        data={item.items}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => (
                          <View style={{ flexDirection: "row", margin: 10 }}>
                            <Image
                              style={{
                                height: metrics.dimen_60,
                                width: metrics.dimen_60,
                                alignSelf: "center",
                              }}
                              source={{ uri: item.image }}
                            />
                            <View>
                              <Text
                                style={{
                                  fontFamily: metrics.quicksand_bold,
                                  fontSize: metrics.text_normal,
                                  color: colors.app_gray,
                                  marginLeft: 10,
                                  marginTop: -5,
                                  width: 280,
                                }}
                              >
                                {item.title}
                              </Text>

                              <Text
                                style={{
                                  fontFamily: metrics.quicksand_bold,
                                  fontSize: metrics.text_normal,
                                  color: colors.theme_orange_color_caribpay,
                                  marginLeft: 10,
                                }}
                              >
                                {item.unit_price}
                              </Text>

                              <Text
                                style={{
                                  fontFamily: metrics.quicksand_bold,
                                  fontSize: metrics.text_normal,
                                  color: colors.app_gray,
                                  marginLeft: 10,
                                }}
                              >
                                {" X " + item.quantity}
                              </Text>
                            </View>
                          </View>
                        )}
                      />

                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "flex-end",
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: metrics.quicksand_bold,
                            fontSize: metrics.text_16,
                            color: colors.black,
                            marginRight: 10,
                          }}
                        >
                          {"Total : " + item.grand_total}
                        </Text>
                      </View>

                      <View style={{ flexDirection: "row", margin: 10 }}>
                        <TouchableOpacity
                          style={{
                            flex: 1 / 4,
                            height: metrics.dimen_40,
                            borderRadius: 5,
                            marginLeft: 5,
                            backgroundColor:
                              item.order_status == "CANCELED"
                                ? colors.app_red
                                : colors.theme_orange_color_caribpay,
                            justifyContent: "center",
                          }}
                          onPress={() =>
                            this.setState({
                              isCancelOrder:
                                item.order_status == "CANCELED" ? false : true,
                              cancelOrderItem: item,
                            })
                          }
                        >
                          <View>
                            <Text
                              style={{
                                alignSelf: "center",
                                color: colors.white,
                                fontFamily: metrics.quicksand_bold,
                                textAlign: "center",
                              }}
                            >
                              {item.order_status == "CANCELED"
                                ? "Cancelled"
                                : "Cancel Order"}
                            </Text>
                          </View>
                        </TouchableOpacity>

                        {item.order_status != "DELIVERED" && (
                          <TouchableOpacity
                            style={{
                              flex: 1 / 4,
                              height: metrics.dimen_40,
                              borderRadius: 5,
                              marginLeft: 5,
                              backgroundColor:
                                colors.theme_orange_color_caribpay,
                              justifyContent: "center",
                            }}
                            onPress={() => this.ongoodsRecieved(item)}
                          >
                            <View>
                              <Text
                                style={{
                                  alignSelf: "center",
                                  color: colors.white,
                                  fontFamily: metrics.quicksand_bold,
                                  textAlign: "center",
                                }}
                              >
                                Confirm Recieved
                              </Text>
                            </View>
                          </TouchableOpacity>
                        )}

                        {item.can_evaluate == true && (
                          <TouchableOpacity
                            style={{
                              flex: 1 / 4,
                              height: metrics.dimen_40,
                              borderRadius: 5,
                              marginLeft: 5,
                              backgroundColor:
                                colors.theme_orange_color_caribpay,
                              justifyContent: "center",
                            }}
                            onPress={() =>
                              this.props.navigation.navigate("Message", {
                                customer_id: item.customer_id,
                                order_id: item.id,
                                data: item.shop,
                              })
                            }
                          >
                            <View>
                              <Text
                                style={{
                                  alignSelf: "center",
                                  color: colors.white,
                                  fontFamily: metrics.quicksand_bold,
                                  textAlign: "center",
                                }}
                              >
                                Contact Seller
                              </Text>
                            </View>
                          </TouchableOpacity>
                        )}

                        <TouchableOpacity
                          style={{
                            flex: 1 / 4,
                            borderRadius: 5,
                            marginLeft: 5,
                            backgroundColor: colors.theme_orange_color_caribpay,
                            justifyContent: "center",
                          }}
                          onPress={() =>
                            this.props.navigation.navigate("OrderEvaluations", {
                              data: item,
                            })
                          }
                        >
                          <View>
                            <Text
                              style={{
                                alignSelf: "center",
                                color: colors.white,
                                fontFamily: metrics.quicksand_bold,
                                textAlign: "center",
                              }}
                            >
                              Order Ratings
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    </TouchableOpacity>
                  </View>
                )}
              />
            </View>
          )}

          {this.state.spinvisible == false && this.state.data.length == 0 && (
            <View>
              <Image
                source={require("../../Images/noOrders.png")}
                style={{
                  height: 200,
                  width: 250,
                  resizeMode: "contain",
                  alignSelf: "center",
                  marginTop: 200,
                }}
              />
              <Text
                style={{
                  textAlign: "center",
                  fontSize: metrics.text_heading,
                  color: colors.black,
                  fontWeight: "bold",
                  marginTop: 2,
                  marginLeft: 10,
                }}
              >
                No Orders Yet!
              </Text>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: metrics.text_header,
                  color: colors.app_gray,
                  fontWeight: "bold",
                  marginTop: 2,
                  marginLeft: 10,
                }}
              >
                Looks Like you, haven't made your order yet.
              </Text>
            </View>
          )}
        </View>

        {this.state.data.length == 0 && this.state.spinvisible == false && (
          <TouchableOpacity
            style={{
              position: "absolute",
              bottom: 10,
              height: 40,
              backgroundColor: colors.theme_caribpay,
              borderRadius: 7,
              width: "90%",
              alignSelf: "center",
              justifyContent: "center",
              marginTop: 20,
            }}
            onPress={() => this.props.navigation.navigate("CaribMallHome")}
          >
            <View>
              <Text
                style={{
                  fontSize: metrics.text_heading,
                  color: colors.white,
                  fontWeight: "bold",
                  textAlignVertical: "center",
                  textAlign: "center",
                }}
              >
                Start Shopping
              </Text>
            </View>
          </TouchableOpacity>
        )}

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisible}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.goodsRecvd}
        >
          <View
            style={{
              width: "80%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 170,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spingoodsRecvd}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
            {!this.state.spingoodsRecvd && (
              <View style={{ margin: 10 }}>
                <Image
                  source={require("../../Images/delivered.png")}
                  style={{
                    height: metrics.dimen_60,
                    width: metrics.dimen_60,
                    resizeMode: "center",
                    alignSelf: "center",
                  }}
                />
                <Text
                  style={{
                    alignSelf: "center",
                    textAlign: "center",
                    marginTop: 10,
                  }}
                >
                  {"Thank you for the order."}
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({ goodsRecvd: false }, () =>
                      this.getOrderList()
                    )
                  }
                >
                  <View
                    style={{
                      width: 100,
                      backgroundColor: colors.theme_caribpay,
                      height: 30,
                      borderRadius: 5,
                      alignSelf: "center",
                      justifyContent: "center",
                      marginTop: 15,
                      marginBottom: 5,
                    }}
                  >
                    <Text
                      style={{
                        alignSelf: "center",
                        textAlign: "center",
                        color: colors.white,
                      }}
                    >
                      {"Close"}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </Modal>

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.isCancelOrder}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                margin: metrics.dimen_20,
                flexDirection: "column",
                height: 180,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <View>
                  <Text
                    style={{
                      fontSize: metrics.text_heading,
                      color: colors.black,
                      fontWeight: "bold",
                      textAlign: "center",
                      margin: 10,
                    }}
                  >
                    Confirmation!
                  </Text>
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      color: colors.app_gray,
                      margin: 10,
                      fontWeight: "bold",
                      textAlign: "center",
                      width: 200,
                      alignSelf: "center",
                    }}
                  >
                    {"Are you sure ?\n" + "You can't undo this action"}
                  </Text>
                </View>
                <View style={styles.bottomview22}>
                  <View
                    style={{
                      width: "45%",
                      borderRadius: 10,
                      justifyContent: "center",
                      borderColor: colors.light_grey_backgroud,
                      borderWidth: 1,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => this.setState({ isCancelOrder: false })}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          color: colors.app_gray,
                          fontWeight: "bold",
                          textAlign: "center",
                          alignSelf: "center",
                        }}
                      >
                        {"Cancel "}
                      </Text>
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      width: "45%",
                      marginLeft: 10,
                      backgroundColor: colors.app_green,
                      borderRadius: 10,
                      justifyContent: "center",
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({ isCancelOrder: false }, () =>
                          this.onCancelOrder(this.state.cancelOrderItem)
                        )
                      }
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          color: colors.white,
                          fontWeight: "bold",
                          textAlign: "center",
                          alignSelf: "center",
                          marginLeft: 10,
                        }}
                      >
                        {"Proceed "}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  horizontalLine: {
    borderBottomColor: "#f2f2f2",
    borderBottomWidth: 8,
    width: "100%",
    alignSelf: "center",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
  },
  bottomview22: {
    bottom: 20,
    position: "absolute",
    color: colors.theme_caribpay,
    height: 50,
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
    flexDirection: "row",
  },
  curveview: {
    height: metrics.newView.curveview + 140,
    position: "absolute",
    top: metrics.newView.curvetop - 100,
    width: "100%",
    backgroundColor: colors.white,
    borderRadius: 40,
  },
  shodowEffect: {
    backgroundColor: colors.white,
    margin: metrics.dimen_5,
    justifyContent: "center",
    width: 70,
    height: 70,
    borderRadius: 10,
    borderWidth: 2.5,
    borderRadius: 5,
    borderColor: "#ddd",
    borderBottomWidth: 0.5,
    shadowColor: "#000000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.9,
    shadowRadius: 7,
    elevation: 6,
  },
});
