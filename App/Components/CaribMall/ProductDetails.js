import React from "react";
import {
  Share,
  Dimensions,
  SafeAreaView,
  Text,
  View,
  Image,
  BackHandler,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ImageBackground,
} from "react-native";
import metrics from "../../Themes/Metrics";
import colors from "../../Themes/Colors";
import Toast, { DURATION } from "react-native-easy-toast";
var Spinner = require("react-native-spinkit");
const axios = require("axios");
import Modal from "react-native-modal";
import { SliderBox } from "react-native-image-slider-box";
import StarRating from "react-native-star-rating";
import InputSpinner from "react-native-input-spinner";
import RBSheet from "react-native-raw-bottom-sheet";
import RadioButton from "react-native-radio-button";
import AsyncStorage from "@react-native-community/async-storage";
import DropdownAlert from "react-native-dropdownalert";
import HTML from "react-native-render-html";

export default class ProductDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      isFeatures: true,
      isDescrpn: false,
      isSeller: false,
      slug: this.props.navigation.state.params.slug,
      data: [],
      product_images: [],
      shop_image_url: "",
      shop_name: "",
      shop_rating: 0.0,
      shop_slug: "",
      value: 1,
      data_cartItems: [],
      linkedeItems: [],
      productTechnicalDetails: [],
      manufacturerName: "",
      keyFeaturesData: "",
      shippingOptions: "",
      frequentlyproduct: false,
      offer_slug: "",
      offersData: [],
      cartData: [],
      countriesData: [],
      number: 1,
      customer_id: "",
    };
    console.disableYellowBox = true;
  }

  async componentDidMount() {
    console.log("pro slug --- > ", this.state.slug);
    this.setState({
      spinvisible: true,
    });
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    this.getProductDetails();

    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getcartItems();
      }
    );

    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    console.log("mall user data -- > ", data);
    this.setState({
      customer_id: data.id,
    });
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  getOffersDetails = () => {
    axios
      .get("https://caribmall.com/api/offers/" + this.state.offer_slug)
      .then((response) => {
        this.setState({
          offersData: response.data.data,
          offersLength: response.data.data.listings.length,
        });
        console.log(
          "offers data --->",
          this.state.offersData,
          this.state.offersLength
        );
      })
      .catch((err) => {});
  };

  getProductDetails = () => {
   
    axios
      .get("https://caribmall.com/api/listing/" + this.state.slug)
      .then((response) => {
        console.log("response product --- >", response.data.data);
        // console.log("response product shipping options --- >", response.data.url);
        
        let images = this.state.product_images;
        if (response.data.data.images.length == 0) {
          console.log("image else ");
          images.push(response.data.data.product.image);
          console.log(images);
        } else {
          console.log("image if ");
          for (i = 0; i < response.data.data.images.length; i++) {
            let path = response.data.data.images[i].path;
            images.push(path);
          }
        }

        if (response.data.data.key_features.length > 0) {
          console.log("not empty");
          let keyFeatures = [];
          for (i = 0; i < response.data.data.key_features.length; i++) {
            const obj = {
              idNum: i,
              feature: response.data.data.key_features[i],
            };
            keyFeatures.push(obj);
          }
          let arr_key_features = keyFeatures.map((item, index) => {
            item.id = ++index;
            return { ...item };
          });
          this.setState({
            keyFeaturesData: arr_key_features,
          });
        } else {
          console.log(" empty");
          this.setState({
            keyFeaturesData: [],
          });
        }

        let proOrigin =
          response.data.countries[response.data.data.product.origin];
        console.log("proOrigin--->", proOrigin);

        // let title = this.state.headerTitle
        // let newtitle = title.substr(0, 10)
        // console.log("new title ---> ", newtitle)

        this.setState({
          product_images: images,
          data: response.data.data,
          countriesData: response.data.countries,
          shop_image_url: response.data.data.shop.image,
          shop_name: response.data.data.shop.name,
          shop_rating: parseFloat(response.data.data.shop.rating),
          shop_verified: response.data.data.shop.verified,
          shop_slug: response.data.data.shop.slug,
          product_shop_id: response.data.data.shop.id,
          linkedeItems: response.data.data.linked_items,
          productTechnicalDetails: response.data.data.product,
          productOriginName: proOrigin == null ? "Not Available" : proOrigin,
          manufacturerName: response.data.data.product.manufacturer.name,
          offer_slug: response.data.data.product.slug,
          share_url:response.data.url,
          shippingOptions:
            response.data.shipping_options == null
              ? []
              : response.data.shipping_options,
          shippingCost:
            response.data.shipping_options[0].cost == null
              ? ""
              : response.data.shipping_options[0].cost,
          shippingCarrierName:
            response.data.shipping_options[0].carrier_name == null
              ? ""
              : response.data.shipping_options[0].carrier_name,
          shippingEstimatedTime:
            response.data.shipping_options[0].delivery_takes == null
              ? ""
              : response.data.shipping_options[0].carrier_name,
          country: response.data.countries[response.data.shipping_country_id],
          country_id: response.data.shipping_country_id,
          shiping_id: response.data.shipping_options[0].id,
          shiping_zone_id: response.data.shipping_options[0].shipping_zone_id,
          frequentlyproduct: false,
          spinvisible:false,
        });

        console.log("product details --> ", response.data.data.stock_quantity);
        this.getOffersDetails();
      })
      .catch((error) => {
        if (error.response) {
          console.log(
            "product detail response error --> ",
            error.response.data
          );
          this.setState({
            spinvisible: false,
          });
        }
      });
  };

  referPeoples = async () => {
    if (Platform.OS === "ios") {
      try {
        const result = await Share.share({
          message:
            "Hey, Checkout this cool " +
            this.state.data.title +
            " Product on Carib Mall ,To See Follow this link \n "+this.state.share_url,
          title: "Carib Mall",
        });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
          } else {
          }
        } else if (result.action === Share.dismissedAction) {
        }
      } catch (error) {
        alert(error.message);
      }
    } else {
      try {
        const result = await Share.share({
          message:
          "Hey, Checkout this cool " +
          this.state.data.title +
          " Product on Carib Mall ,To See Follow this link \n "+this.state.share_url,
          title: "Carib Mall",
        });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
          } else {
          }
        } else if (result.action === Share.dismissedAction) {
        }
      } catch (error) {
        alert(error.message);
      }
    }
  };

  addtoCart = async () => {
    if (
      this.state.data_cartItems.length > 0 &&
      this.state.product_shop_id != this.state.cart_shop_id
    ) {
      this.setState({
        isDiffrentShop: true,
      });
    } else {
      let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
      let data = JSON.parse(caribmall_data);
      console.log("token", data.api_token, this.state.slug);

      let postData = {
        quantity: this.state.number,
        country_id: this.state.country_id,
        shipping_option_id: this.state.shiping_id,
        shipping_zone_id: this.state.shiping_zone_id,
        packaging_id: 6,
      };
      console.log("add to cart params--->", postData);
      axios({
        method: "post",
        url:
          "https://caribmall.com/api/addToCart/" +
          this.state.slug +
          "?api_token=" +
          data.api_token,
        data: postData,
      })
        .then((response) => {
          console.log("add to cart response --- > ", response.data);
          this.setState({
            fromCart: true,
            fromWislist: false,
          });
          this.dropDownAlertRef.alertWithType("success", response.data.message);
          this.getcartItems();
        })
        .catch((error) => {
          if (error.response) {
            console.log(
              "response add to cart error block ",
              error.response.data
            );
            this.dropDownAlertRef.alertWithType(
              "error",
              error.response.data.message
            );
          }
        });
    }
  };

  addtoWishlist = async () => {
    this.setState({
      spinvisibleWishlist: true,
    });
    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    console.log("token", data.api_token);
    axios({
      method: "get",
      url:
        "https://caribmall.com/api/wishlist/" +
        this.state.slug +
        "/add?api_token=" +
        data.api_token,
    })
      .then((response) => {
        console.log("response address data", response.data);
        this.setState({
          spinvisibleWishlist: false,
        });
        this.setState({
          fromCart: false,
          fromWislist: true,
        });
        this.dropDownAlertRef.alertWithType("success", response.data.message);
      })
      .catch((error) => {
        if (error.response) {
          console.log("response", error.response.data);
          this.setState({
            spinvisibleWishlist: false,
          });
          this.dropDownAlertRef.alertWithType(
            "error",
            error.response.data.message
          );
        }
      });
  };

  getcartItems = async () => {
    this.setState({
      spinvisible: true,
    });
    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    console.log("token", data.api_token);

    axios
      .get("https://caribmall.com/api/carts?api_token=" + data.api_token)
      .then((response) => {
        if (response.data.data.length > 0) {
          this.setState({
            cart_shop_id: response.data.data[0].shop.id,
            cart_item_id: response.data.data[0].id,
            data_cartItems: response.data.data[0].items,
            cart_length: response.data.data[0].items.length,
            spinvisible: false,
          });
        } else {
          this.setState({
            cartData: [],
            data_cartItems: [],
            cart_length: 0,
            spinvisible: false,
          });
        }
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          spinvisible: false,
        });
      });
  };

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  handleSelection = (item, id) => {
    var selectedid = this.state.selectedID;
    if (selectedid === id) {
      this.setState({
        selectedID: null,
        amount: "",
      });
    } else {
      this.setState({
        selectedID: id,
        shippingCost: item.cost,
        shippingCarrierName: item.carrier_name,
        shippingEstimatedTime: item.delivery_takes,
      });
    }
  };

  renderShipping = () => {
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
            backgroundColor: colors.theme_caribpay,
            height: 50,
          }}
        >
          <TouchableOpacity onPress={() => this.RBSheetShippingOptions.close()}>
            <View
              style={{
                height: metrics.dimen_40,
                width: metrics.dimen_40,
                justifyContent: "center",
                alignSelf: "center",
              }}
            >
              <Image
                style={{
                  height: metrics.dimen_25,
                  width: metrics.dimen_25,
                  tintColor: "white",
                  alignSelf: "center",
                  marginTop: 5,
                }}
                source={require("../../Images/leftarrow.png")}
              />
            </View>
          </TouchableOpacity>
          <Text
            style={{
              fontSize: metrics.text_heading,
              color: colors.white,
              fontWeight: "bold",
              marginTop: 7,
              marginLeft: 10,
            }}
          >
            Shipping Address
          </Text>
        </View>

        <Text
          style={{
            fontSize: metrics.text_normal,
            marginLeft: 20,
            marginTop: 20,
            fontWeight: "bold",
          }}
        >
          Ship To
        </Text>
        <View
          style={{
            flexDirection: "row",
            width: "90%",
            alignSelf: "center",
            marginTop: 10,
            justifyContent: "center",
            backgroundColor: colors.light_grey_backgroud,
            height: metrics.dimen_40,
            borderRadius: 10,
          }}
        >
          <Text
            style={{
              fontSize: metrics.text_normal,
              textAlignVertical: "center",
              marginLeft: 10,
              flex: 1,
            }}
          >
            India
          </Text>
          <Image
            style={{
              height: metrics.dimen_25,
              width: metrics.dimen_25,
              tintColor: colors.black,
              alignSelf: "center",
              marginRight: 10,
            }}
            source={require("../../Images/dropdown.png")}
          />
        </View>

        <Text
          style={{
            fontSize: metrics.text_normal,
            marginLeft: 20,
            marginTop: 15,
            fontWeight: "bold",
          }}
        >
          Shipping Method
        </Text>

        <FlatList
          style={{ marginBottom: metrics.dimen_10 }}
          showsVerticalScrollIndicator={false}
          data={this.state.shippingOptions}
          extraData={this.state.selectedID}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <View
              style={{
                margin: 7,
              }}
            >
              <TouchableOpacity
                onPress={() => this.handleSelection(item, item.id)}
              >
                <View style={{ flexDirection: "row", margin: 16 }}>
                  <View
                    style={{
                      justifyContent: "center",
                      alignSelf: "center",
                      flex: 0.3,
                    }}
                  >
                    <Text
                      style={{
                        color: colors.app_red,
                        alignSelf: "center",
                        marginRight: 30,
                      }}
                    >
                      {item.cost}
                    </Text>
                  </View>
                  <View
                    style={{
                      justifyContent: "center",
                      alignSelf: "center",
                      flex: 0.7,
                    }}
                  >
                    <Text
                      style={{
                        color: colors.app_black_text,
                        fontWeight: "bold",
                      }}
                    >
                      {item.name}
                    </Text>
                    <Text style={{ color: colors.app_gray }}>
                      {"Via " + item.carrier_name}
                    </Text>
                    <Text style={{ color: colors.app_gray }}>
                      {item.delivery_takes}
                    </Text>
                  </View>
                  <View
                    style={{
                      justifyContent: "center",
                      alignSelf: "center",
                      flex: 0.3,
                    }}
                  >
                    <RadioButton
                      size={metrics.dimen_12}
                      animation={"bounceIn"}
                      isSelected={
                        item.id == this.state.selectedID ? true : false
                      }
                      innerColor={colors.theme_caribpay}
                      outerColor={colors.theme_caribpay}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
    );
  };

  onProceedOtherCart = async () => {
    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    let postData = {
      cart: this.state.cart_item_id,
    };
    console.log(postData);
    this.setState({ isDiffrentShop: false });
    axios({
      method: "delete",
      url:
        "https://caribmall.com/api/cart/removeItem?api_token=" + data.api_token,
      data: postData,
    })
      .then((response) => {
        console.log("response allcart remove --- > ", response.data);
        this.dropDownAlertRef.alertWithType("error", response.data.message);
        this.getcartItems();
        this.addtoCart();
      })
      .catch((error) => {
        if (error.response) {
          console.log("response allcart remove error ", error.response.data);
          this.dropDownAlertRef.alertWithType(
            "error",
            error.response.data.message
          );
        }
      });
  };

  render() {
    return (
      <SafeAreaView>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.header}>
            <View style={{ flexDirection: "row", margin: 10 }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.goBack(null)}
              >
                <View
                  style={{
                    height: metrics.dimen_40,
                    width: metrics.dimen_40,
                    justifyContent: "center",
                  }}
                >
                  <Image
                    style={{
                      height: metrics.dimen_25,
                      width: metrics.dimen_25,
                      tintColor: "white",
                    }}
                    source={require("../../Images/leftarrow.png")}
                  />
                </View>
              </TouchableOpacity>
              {this.state.data.title != undefined && (
                <Text
                  style={{
                    fontSize: metrics.text_16,
                    color: colors.white,
                    fontWeight: "bold",
                    textAlignVertical: "center",
                    marginLeft: 10,
                  }}
                >
                  {this.state.data.title.substr(0, 38)}
                </Text>
              )}
            </View>
          </View>

          {this.state.spinvisible ? (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignSelf: "center",
                height: Dimensions.get("screen").height - 100,
              }}
            >
              <View
                style={{
                  width: 120,
                  borderRadius: metrics.dimen_7,
                  height: 100,
                  justifyContent: "center",
                }}
              >
                <Spinner
                  style={{ alignSelf: "center" }}
                  isVisible={this.state.spinvisible}
                  size={70}
                  type={"ThreeBounce"}
                  color={colors.black}
                />
              </View>
            </View>
          ) : (
            <View>
              <SliderBox
                inactiveDotColor={colors.app_gray}
                dotColor={colors.theme_caribpay}
                imageLoadingColor={colors.transparent}
                images={this.state.product_images}
                sliderBoxHeight={400}
                parentWidth={this.state.width}
                ImageComponentStyle={{ height: 400, resizeMode: "contain" }}
              />

              <View style={{ flexDirection: "column", margin: 10 }}>
                <View style={{ flexDirection: "row" }}>
                  {this.state.data.has_offer && (
                    <Text
                      style={{
                        fontSize: metrics.text_25,
                        color: colors.app_red,
                        fontWeight: "bold",
                      }}
                    >
                      {this.state.data.offer_price}
                    </Text>
                  )}
                  <Text
                    style={{
                      fontSize: this.state.data.has_offer
                        ? metrics.text_header
                        : metrics.text_25,
                      color: this.state.data.has_offer
                        ? colors.app_gray
                        : colors.app_red,
                      fontWeight: "bold",
                      textDecorationLine: this.state.data.has_offer
                        ? "line-through"
                        : null,
                      alignSelf: "center",
                      marginLeft: this.state.data.has_offer ? 20 : 0,
                      flex: 1,
                    }}
                  >
                    {this.state.data.price}
                  </Text>
                  <TouchableOpacity
                    style={{
                      height: 20,
                      width: 20,
                      resizeMode: "contain",
                      tintColor: colors.app_gray,
                      alignSelf: "center",
                    }}
                    onPress={() => this.referPeoples()}
                  >
                    <Image
                      style={{
                        height: 20,
                        width: 20,
                        resizeMode: "contain",
                        tintColor: colors.app_gray,
                        alignSelf: "center",
                      }}
                      source={require("../../Images/share.png")}
                    />
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      height: 20,
                      width: 20,
                      resizeMode: "contain",
                      tintColor: colors.app_gray,
                      alignSelf: "center",
                      marginLeft: 20,
                    }}
                    onPress={() => this.addtoWishlist()}
                  >
                    <Image
                      style={{
                        height: 20,
                        width: 20,
                        resizeMode: "contain",
                        tintColor: colors.app_gray,
                        alignSelf: "center",
                      }}
                      source={require("../../Images/heart.png")}
                    />
                  </TouchableOpacity>
                </View>
                <Text
                  style={{
                    fontSize: metrics.text_16,
                    color: colors.black,
                    width: 300,
                  }}
                >
                  {this.state.data.title}
                </Text>
                {this.state.data.rating != null && (
                  <View style={{ width: 150, marginTop: 7 }}>
                    <StarRating
                      disabled={false}
                      maxStars={5}
                      starSize={20}
                      halfStarEnabled={true}
                      fullStarColor={colors.app_yellow_color}
                      rating={parseFloat(this.state.data.rating)}
                    />
                  </View>
                )}
              </View>
              <View style={styles.horizontalLine} />

              <View style={{ flexDirection: "row", margin: 15 }}>
                <View style={{ flexDirection: "column", flex: 1 }}>
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      fontWeight: "bold",
                      color: colors.black,
                      flex: 1,
                    }}
                  >
                    Quantity
                  </Text>
                  <Text style={{ fontSize: metrics.text_normal, flex: 1 }}>
                    {"( " + this.state.data.stock_quantity + " in Stock" + " )"}
                  </Text>
                </View>
                <InputSpinner
                  max={10}
                  min={1}
                  step={1}
                  height={40}
                  width={120}
                  rounded={false}
                  editable={false}
                  value={this.state.number}
                  onChange={(num) => {
                    this.setState({
                      number: num,
                    });
                  }}
                />
              </View>
              {this.state.shippingOptions.length > 0 && (
                <View style={styles.horizontalLine} />
              )}
              {this.state.shippingOptions.length > 0 && (
                <TouchableOpacity
                  onPress={() => this.RBSheetShippingOptions.open()}
                >
                  <View style={{ flexDirection: "row", margin: 15 }}>
                    <Image
                      style={{
                        height: 30,
                        width: 30,
                        resizeMode: "contain",
                        tintColor: colors.app_gray,
                        alignSelf: "center",
                      }}
                      source={require("../../Images/location.png")}
                    />
                    <View
                      style={{
                        flexDirection: "column",
                        justifyContent: "center",
                        marginLeft: 20,
                        flex: 1,
                      }}
                    >
                      <Text
                        style={{
                          fontSize: metrics.text_description,
                          color: colors.black,
                        }}
                      >
                        {"Shipping : " + this.state.shippingCost}
                      </Text>
                      <Text
                        style={{
                          fontSize: metrics.text_description,
                          color: colors.heading_blue,
                        }}
                      >
                        {"To " +
                          this.state.country +
                          " via " +
                          this.state.shippingCarrierName}
                      </Text>
                      <Text
                        style={{
                          fontSize: metrics.text_description,
                          color: colors.app_gray,
                        }}
                      >
                        {this.state.shippingEstimatedTime}
                      </Text>
                    </View>
                    <Image
                      style={{
                        height: 20,
                        width: 20,
                        resizeMode: "contain",
                        tintColor: colors.app_gray,
                        alignSelf: "center",
                      }}
                      source={require("../../Images/rightarrow.png")}
                    />
                  </View>
                </TouchableOpacity>
              )}
              <View style={styles.horizontalLine} />

              <View
                style={{
                  flexDirection: "row",
                  backgroundColor: colors.light_grey_backgroud,
                  height: 55,
                  paddingTop: 5,
                  marginTop: 10,
                }}
              >
                <View style={{ flexDirection: "column", flex: 1 / 2 }}>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        isFeatures: true,
                        isDescrpn: false,
                      })
                    }
                    style={{
                      flexDirection: "column",
                      backgroundColor: this.state.isFeatures
                        ? "white"
                        : colors.transparent,
                      margin: 5,
                      borderRadius: 5,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: metrics.text_description,
                        color: colors.black,
                        alignSelf: "center",
                      }}
                    >
                      Product
                    </Text>
                    <Text
                      style={{
                        fontSize: metrics.text_description,
                        color: colors.app_gray,
                        alignSelf: "center",
                      }}
                    >
                      Description
                    </Text>
                  </TouchableOpacity>
                </View>

                <View style={{ flexDirection: "column", flex: 1 / 2 }}>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        isFeatures: false,
                        isDescrpn: true,
                      })
                    }
                    style={{
                      flexDirection: "column",
                      backgroundColor: this.state.isDescrpn
                        ? "white"
                        : colors.transparent,
                      margin: 5,
                      borderRadius: 5,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: metrics.text_description,
                        color: colors.black,
                        alignSelf: "center",
                      }}
                    >
                      Shipping
                    </Text>
                    <Text
                      style={{
                        fontSize: metrics.text_description,
                        color: colors.app_gray,
                        alignSelf: "center",
                      }}
                    >
                      Info
                    </Text>
                  </TouchableOpacity>
                </View>

                {/* <View style={{ flexDirection: 'column', flex: 1 / 3 }}>
                                <TouchableOpacity onPress={() => this.setState({
                                    isFeatures: false, isDescrpn: false, isSeller: true
                                })} style={{ flexDirection: 'column', backgroundColor: this.state.isSeller ? 'white' : colors.transparent, margin: 5, borderRadius: 5 }}>
                                    <Text style={{ fontSize: metrics.text_description, color: colors.black, alignSelf: 'center' }}>Seller</Text>
                                    <Text style={{ fontSize: metrics.text_description, color: colors.app_gray, alignSelf: 'center' }}>Specification</Text>
                                </TouchableOpacity>
                            </View> */}
              </View>

              {this.state.isFeatures && (
                <View
                  style={{
                    margin: 15,
                    backgroundColor: "white",
                    borderRadius: 5,
                    justifyContent: "center",
                  }}
                >
                  <FlatList
                    style={{ marginBottom: metrics.dimen_10 }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.keyFeaturesData}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                      <View
                        style={{
                          margin: 7,
                          flexDirection: "row",
                        }}
                      >
                        <View
                          style={{
                            height: 10,
                            width: 10,
                            borderRadius: 5,
                            backgroundColor: colors.theme_caribpay,
                          }}
                        />
                        <Text
                          style={{
                            fontSize: metrics.text_normal,
                            color: colors.heading_black_text,
                            marginLeft: 10,
                            marginTop: -5,
                          }}
                        >
                          {item.feature}
                        </Text>
                      </View>
                    )}
                  />
                  {/* <Text style={{ fontSize: metrics.text_description, color: colors.black, fontWeight: 'bold', marginTop: 10 }}>{"Technical Details"}</Text> */}

                  <View style={{ flexDirection: "row", marginTop: 10 }}>
                    <Text style={styles.order_header}>Brand</Text>
                    <Text style={styles.order_info}>
                      {this.state.productTechnicalDetails.brand == null
                        ? "Not Available"
                        : this.state.productTechnicalDetails.brand}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <Text style={styles.order_header}>Model Number</Text>
                    <Text style={styles.order_info}>
                      {this.state.productTechnicalDetails.model_number == null
                        ? "Not Available"
                        : this.state.productTechnicalDetails.model_number}
                    </Text>
                  </View>

                  {this.state.productTechnicalDetails.gtin_type != null && (
                    <View style={{ flexDirection: "row", marginTop: 5 }}>
                      <Text style={styles.order_header}>
                        {this.state.productTechnicalDetails.gtin_type}
                      </Text>
                      <Text style={styles.order_info}>
                        {this.state.productTechnicalDetails.gtin}
                      </Text>
                    </View>
                  )}

                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <Text style={styles.order_header}>Manufacturer</Text>
                    <Text style={styles.order_info}>
                      {this.state.manufacturerName == null ? "Not Available" : this.state.manufacturerName}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <Text style={styles.order_header}>Origin</Text>
                    <Text style={styles.order_info}>
                      {this.state.productOriginName}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <Text style={styles.order_header}>
                      Minimum Order Quantity
                    </Text>
                    <Text style={styles.order_info}>
                      {this.state.data.min_order_quantity}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <Text style={styles.order_header}>Shipping Weight</Text>
                    <Text style={styles.order_info}>
                      {this.state.data.shipping_weight== null ? "Not Available" : this.state.data.shipping_weight}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <Text style={styles.order_header}>Added on</Text>
                    <Text style={styles.order_info}>
                      {this.state.productTechnicalDetails.available_from}
                    </Text>
                  </View>
                </View>
              )}
              {this.state.isDescrpn && (
                <View
                  style={{
                    margin: 15,
                    backgroundColor: "white",
                    borderRadius: 5,
                    justifyContent: "center",
                  }}
                >
                  {this.state.data.description == null ? (
                    <Text
                      style={{
                        fontSize: metrics.text_description,
                        color: colors.app_gray,
                      }}
                    >
                      {"Product Description is not available!"}
                    </Text>
                  ) : (
                    <ScrollView style={{ flex: 1, margin: 10 }}>
                      <HTML
                        html={this.state.data.description}
                        imagesMaxWidth={Dimensions.get("window").width}
                      />
                    </ScrollView>
                  )}
                </View>
              )}

              <Modal
                style={{ alignSelf: "center", width: "50%" }}
                isVisible={this.state.frequentlyproduct}
              >
                <View
                  style={{
                    backgroundColor: "white",
                    borderRadius: metrics.dimen_10,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "column",
                      margin: metrics.dimen_20,
                    }}
                  >
                    <Spinner
                      style={{ alignSelf: "center" }}
                      isVisible={this.state.frequentlyproduct}
                      size={70}
                      type={"ThreeBounce"}
                      color={colors.black}
                    />
                    <Text
                      style={{
                        fontSize: metrics.text_header,
                        color: colors.black,
                        textAlign: "center",
                        marginTop: 20,
                        fontWeight: "bold",
                      }}
                    >
                      Fetching Details...
                    </Text>
                  </View>
                </View>
              </Modal>

              {this.state.data.feedbacks_count > 0 && (
                <View style={{ flexDirection: "column" }}>

                  <View style={styles.horizontalLine} />

                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      margin: 10,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: metrics.text_normal,
                        color: colors.theme_caribpay,
                        alignSelf: "center",
                        marginLeft: 10,
                      }}
                    >
                      {"Reviews " +
                        "( " +
                        this.state.data.feedbacks_count +
                        " )"}
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate("Reviews", {
                          item: this.state.data.feedbacks,
                        })
                      }
                    >
                      <Text
                        style={{
                          fontSize: metrics.text_description,
                          color: colors.app_gray,
                          alignSelf: "center",
                          marginRight: 10,
                          borderColor: colors.theme_caribpay,
                          borderWidth: 0.5,
                          borderRadius: 10,
                          padding: 5,
                        }}
                      >
                        View all
                      </Text>
                    </TouchableOpacity>
                  </View>

                  <FlatList
                    style={{ marginBottom: metrics.dimen_10 }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.data.feedbacks.slice(0)}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                      <View
                        style={{
                          backgroundColor: "white",
                          flexDirection: "column",
                          margin: 10,
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: "white",
                            flexDirection: "row",
                            margin: 10,
                          }}
                        >
                          <Image
                            source={{ uri: item.customer.avatar }}
                            style={{
                              height: 30,
                              width: 30,
                              borderRadius: 15,
                              alignSelf: "center",
                            }}
                          />
                          <View
                            style={{
                              flexDirection: "row",
                              justifyContent: "space-between",
                              flex: 1,
                            }}
                          >
                            <View
                              style={{ flexDirection: "column", marginLeft: 7 }}
                            >
                              <Text
                                style={{
                                  fontSize: metrics.text_medium,
                                  fontFamily: metrics.quicksand_regular,
                                  color: colors.heading_black_text,
                                  textAlign: "center",
                                  marginTop: 2,
                                  fontWeight: "bold",
                                }}
                              >
                                {item.customer.name}
                              </Text>
                              <Text
                                style={{
                                  fontSize: metrics.text_medium,
                                  color: colors.app_gray,
                                  textAlign: "center",
                                  marginTop: 2,
                                  marginLeft: 10,
                                }}
                              >
                                {item.updated_at}
                              </Text>
                            </View>
                            <View>
                              <Text
                                style={{
                                  fontSize: metrics.text_medium,
                                  fontFamily: metrics.quicksand_regular,
                                  color: colors.app_green,
                                  textAlign: "center",
                                  marginTop: 2,
                                  fontWeight: "bold",
                                  marginBottom: 5,
                                }}
                              >
                                {item.labels}
                              </Text>
                              <View style={{ width: 100, marginRight: 10 }}>
                                <StarRating
                                  disabled={false}
                                  maxStars={5}
                                  starSize={12}
                                  fullStarColor={colors.app_yellow_color}
                                  rating={parseFloat(item.rating)}
                                />
                              </View>
                            </View>
                          </View>
                        </View>

                        <Text
                          style={{
                            fontSize: metrics.text_medium,
                            color: colors.app_gray,
                            marginTop: 2,
                            marginLeft: 5,
                          }}
                        >
                          {item.comment}
                        </Text>
                      </View>
                    )}
                  />
                </View>
              )}

              {/* <View style={styles.horizontalLine}></View> */}

              {this.state.offersLength > 1 && (
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("Offers", {
                      item: this.state.offersData,
                    })
                  }
                >
                  <View style={{ margin: 15 }}>
                    <Text
                      style={{
                        color: colors.app_blue,
                        fontSize: metrics.text_normal,
                      }}
                    >
                      {"More (" +
                        this.state.offersLength +
                        ") " +
                        "offers from other sellers >"}
                    </Text>
                  </View>
                </TouchableOpacity>
              )}

              <View style={styles.horizontalLine} />

              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("VisitStore", {
                    shopName: this.state.shop_name,
                    slug: this.state.shop_slug,
                  })
                }
              >
                <View style={{ flexDirection: "row", margin: 15 }}>
                  <Image
                    style={{
                      height: 30,
                      width: 30,
                      resizeMode: "contain",
                      alignSelf: "center",
                    }}
                    source={{ uri: this.state.shop_image_url }}
                  />
                  <View
                    style={{
                      flexDirection: "column",
                      justifyContent: "center",
                      marginLeft: 20,
                      flex: 1,
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          fontWeight: "bold",
                          fontSize: metrics.text_description,
                          color: colors.black,
                        }}
                      >
                        {this.state.shop_name}
                      </Text>
                      {this.state.shop_verified && (
                        <Image
                          style={{
                            height: 20,
                            width: 20,
                            resizeMode: "contain",
                            alignSelf: "center",
                            marginLeft: 10,
                          }}
                          source={require("../../Images/check.png")}
                        />
                      )}
                    </View>
                    <View
                      style={{
                        width: 300,
                        marginRight: 10,
                        flexDirection: "row",
                        marginTop: 2,
                      }}
                    >
                      <StarRating
                        disabled={false}
                        maxStars={5}
                        starSize={12}
                        fullStarColor={colors.app_yellow_color}
                        rating={this.state.shop_rating}
                      />
                      {this.state.shop_rating != null ||
                        (NaN && (
                          <Text
                            style={{
                              fontSize: metrics.text_small,
                              color: colors.app_blue,
                              alignSelf: "center",
                              marginLeft: 5,
                            }}
                          >
                            {"( " + this.state.shop_rating + " )"}
                          </Text>
                        ))}
                    </View>
                  </View>
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      color: colors.app_blue,
                      fontFamily:metrics.quicksand_bold,
                      alignSelf: "center",
                    }}
                  >
                    Visit Stores
                  </Text>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      resizeMode: "contain",
                      tintColor: colors.app_blue,
                      alignSelf: "center",
                    }}
                    source={require("../../Images/rightarrow.png")}
                  />
                </View>
              </TouchableOpacity>

              <View
                style={{
                  backgroundColor: colors.light_grey_backgroud,
                  width: "100%",
                  alignSelf: "center",
                  height: 7,
                  marginBottom: this.state.linkedeItems.length == 0 ? 50 : 0,
                }}
              />

              {this.state.linkedeItems.length > 0 && (
                <View style={{ margin: 15 }}>
                  <Text style={{ fontWeight: "bold" }}>
                    Frequently Bought Together
                  </Text>
                  <FlatList
                    style={{
                      marginTop: metrics.dimen_10,
                      marginBottom: metrics.dimen_70,
                    }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.linkedeItems}
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                      <View
                        style={{
                          margin: 7,
                          flex: 1 / 2,
                          backgroundColor: colors.lightWhite,
                          width: 200,
                          height: 220,
                          borderRadius: 10,
                          justifyContent: "center",
                        }}
                      >
                        <TouchableOpacity
                          onPress={() =>
                            this.setState(
                              {
                                slug: item.slug,
                                product_images: [],
                                frequentlyproduct: true,
                              },
                              () => this.getProductDetails()
                            )
                          }
                        >
                          <Image
                            source={{ uri: item.image }}
                            style={{
                              height: metrics.dimen_140,
                              width: metrics.dimen_140,
                              alignSelf: "center",
                              margin: 5,
                            }}
                          />
                          <Text
                            numberOfLines={2}
                            style={{
                              fontSize: metrics.text_normal,
                              fontFamily: metrics.quicksand_regular,
                              color: colors.heading_black_text,
                              marginTop: 2,
                              marginLeft: 2,
                            }}
                          >
                            {item.title}
                          </Text>
                          <View
                            style={{
                              flexDirection: "row",
                              marginTop: 5,
                              marginBottom: 2,
                              justifyContent: "space-between",
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row",
                                justifyContent: "space-between",
                                marginLeft: 0.5,
                              }}
                            >
                              {item.offer_price != null && (
                                <Text
                                  style={{
                                    fontSize: metrics.text_normal,
                                    fontFamily: metrics.quicksand_regular,
                                    color: colors.heading_black_text,
                                    marginTop: 2,
                                    fontWeight: "bold",
                                    marginLeft: 2,
                                  }}
                                >
                                  {item.offer_price}
                                </Text>
                              )}
                              <Text
                                style={{
                                  fontSize:
                                    item.offer_price != null
                                      ? metrics.text_medium
                                      : metrics.text_normal,
                                  color:
                                    item.offer_price != null
                                      ? colors.app_gray
                                      : colors.heading_black_text,
                                  marginTop: 1.5,
                                  fontWeight: "bold",
                                  marginLeft: 2,
                                  textDecorationLine:
                                    item.offer_price != null
                                      ? "line-through"
                                      : "none",
                                  alignSelf: "center",
                                  textAlignVertical: "center",
                                }}
                              >
                                {item.price}
                              </Text>
                            </View>
                            <Image
                              source={require("../../Images/supermarket.png")}
                              style={{
                                height: metrics.dimen_15,
                                width: metrics.dimen_15,
                                alignSelf: "center",
                                marginRight: 10,
                              }}
                            />
                          </View>
                        </TouchableOpacity>
                      </View>
                    )}
                  />
                </View>
              )}
            </View>
          )}
        </ScrollView>
        {this.state.spinvisible == false && (
          <View style={styles.shadowEffect}>
            <View style={{ flexDirection: "row", flex: 1 }}>
              <TouchableOpacity
                style={{
                  justifyContent: "center",
                  alignSelf: "center",
                  flex: 0.25,
                  backgroundColor: colors.white,
                  height: 45,
                  borderTopLeftRadius: 7,
                  borderBottomLeftRadius: 7,
                }}
                onPress={() =>
                  this.props.navigation.navigate("Cart", {
                    item: this.state.data_cartItems,
                    shopSlug: this.state.shop_slug,
                  })
                }
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignSelf: "center",
                    flex: 0.25,
                    backgroundColor: colors.white,
                    height: 45,
                    borderTopLeftRadius: 7,
                    borderBottomLeftRadius: 7,
                  }}
                >
                  <Image
                    style={{
                      height: 22,
                      width: 22,
                      resizeMode: "center",
                      alignSelf: "center",
                    }}
                    source={require("../../Images/supermarket.png")}
                  />
                  {this.state.data_cartItems.length > 0 && (
                    <View
                      style={{
                        width: 18,
                        height: 18,
                        borderRadius: 18 / 2,
                        backgroundColor: colors.app_red,
                        position: "absolute",
                        top: -8,
                        marginLeft: 18,
                      }}
                    >
                      <Text
                        style={{
                          fontSize: metrics.text_description,
                          alignSelf: "center",
                          fontWeight: "bold",
                          color: colors.white,
                        }}
                      >
                        {this.state.data_cartItems.length}
                      </Text>
                    </View>
                  )}
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  justifyContent: "center",
                  alignSelf: "center",
                  flex: 0.25,
                  backgroundColor: colors.theme_caribpay,
                  height: 45,
                  backgroundColor: colors.app_blue,
                }}
                onPress={() =>
                  this.props.navigation.navigate("Chat", {
                    customer_id: this.state.customer_id,
                    shop_id: this.state.product_shop_id,
                    shopName: this.state.shop_name,
                    product_name:this.state.data.title
                  })
                }
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignSelf: "center",
                    flex: 0.25,
                    backgroundColor: colors.theme_caribpay,
                    height: 45,
                    backgroundColor: colors.app_blue,
                  }}
                >
                  <Image
                    style={{
                      height: 22,
                      width: 22,
                      resizeMode: "center",
                      alignSelf: "center",
                      tintColor: "white",
                    }}
                    source={require("../../Images/chat.png")}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  justifyContent: "center",
                  alignSelf: "center",
                  flex: 0.5,
                  flexDirection: "row",
                  backgroundColor: colors.theme_caribpay,
                  height: 45,
                  borderTopRightRadius: 7,
                  borderBottomRightRadius: 7,
                }}
                onPress={() => this.addtoCart()}
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignSelf: "center",
                    flex: 0.5,
                    flexDirection: "row",
                    backgroundColor: colors.theme_caribpay,
                    height: 45,
                    borderTopRightRadius: 7,
                    borderBottomRightRadius: 7,
                  }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      fontSize: metrics.text_description,
                      color: colors.white,
                      textAlign: "center",
                      alignSelf: "center",
                    }}
                  >
                    Add to Cart
                  </Text>
                  <Image
                    style={{
                      marginLeft: 10,
                      height: 14,
                      width: 14,
                      resizeMode: "contain",
                      tintColor: colors.white,
                      alignSelf: "center",
                    }}
                    source={require("../../Images/rightarrow.png")}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        )}

        <Toast
          ref="toast"
          style={{ backgroundColor: "black" }}
          position="center"
          positionValue={200}
          fadeInDuration={200}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: "white" }}
        />

        <RBSheet
          ref={(ref) => {
            this.RBSheetShippingOptions = ref;
          }}
          height={Dimensions.get("screen").height}
          duration={0}
        >
          {this.renderShipping()}
        </RBSheet>

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisibleWishlist}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisibleWishlist}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>

        <DropdownAlert
          ref={(ref) => (this.dropDownAlertRef = ref)}
          successImageSrc={
            this.state.fromCart
              ? require("../../Images/addedCart.png")
              : require("../../Images/wishlistheart.png")
          }
        />

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.isDiffrentShop}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                margin: metrics.dimen_20,
                flexDirection: "column",
                height: 180,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <View>
                  <Text
                    style={{
                      fontSize: metrics.text_heading,
                      color: colors.black,
                      fontWeight: "bold",
                      textAlign: "center",
                      margin: 10,
                    }}
                  >
                    Proceed to Start New Cart?
                  </Text>
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      color: colors.app_gray,
                      margin: 10,
                      fontWeight: "bold",
                      textAlign: "center",
                      width: 200,
                      alignSelf: "center",
                    }}
                  >
                    Adding this item will clear your current Cart Proceed?
                  </Text>
                </View>
                <View style={styles.bottomview}>
                  <View
                    style={{
                      width: "45%",
                      borderRadius: 10,
                      justifyContent: "center",
                      borderColor: colors.light_grey_backgroud,
                      borderWidth: 1,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => this.setState({ isDiffrentShop: false })}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          color: colors.app_gray,
                          fontWeight: "bold",
                          textAlign: "center",
                          alignSelf: "center",
                        }}
                      >
                        {"Cancel "}
                      </Text>
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      width: "45%",
                      marginLeft: 10,
                      backgroundColor: colors.app_green,
                      borderRadius: 10,
                      justifyContent: "center",
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({ isDiffrentShop: false }, () =>
                          this.onProceedOtherCart()
                        )
                      }
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          color: colors.white,
                          fontWeight: "bold",
                          textAlign: "center",
                          alignSelf: "center",
                          marginLeft: 10,
                        }}
                      >
                        {"Proceed "}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  horizontalLine: {
    backgroundColor: colors.light_grey_backgroud,
    width: "100%",
    alignSelf: "center",
    height: 7,
  },
  shadowEffect: {
    justifyContent: "center",
    alignSelf: "center",
    backgroundColor: colors.white,
    width: "90%",
    height: 30,
    borderRadius: 1,
    position: "absolute",
    bottom: 10,
    borderWidth: 1,
    borderRadius: 7,
    borderColor: "#ddd",
    borderBottomWidth: 1,
    shadowColor: "black",
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.9,
    shadowRadius: 3,
    elevation: 5,
  },
  bottomview2: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_5,
  },
  order_header: {
    fontSize: metrics.text_normal,
    color: colors.app_gray,
    flex: 1 / 2,
  },
  order_info: {
    fontSize: metrics.text_medium,
    color: colors.app_black_text,
    fontWeight: "bold",
    flex: 1 / 2,
  },
  bottomview: {
    bottom: 20,
    position: "absolute",
    color: colors.theme_caribpay,
    height: 50,
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
    flexDirection: "row",
  },
  header: {
    height: metrics.dimen_60,
    backgroundColor: colors.carib_pay_blue,
    paddingHorizontal: metrics.dimen_10,
    flexDirection: "row",
  },
});
