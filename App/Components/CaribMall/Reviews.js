import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, FlatList, Image, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../../Themes/Metrics.js';
import colors from '../../Themes/Colors.js';
import StarRating from 'react-native-star-rating';


export default class Reviews extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            starCount: 3.5,
            data:this.props.navigation.state.params.item
        }
    }

    componentDidMount() {
        console.log(this.state.data)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }








    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }} >
                <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                    <View style={{ flexDirection: 'row', margin: 10 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white' }} source={require('../../Images/leftarrow.png')}></Image>
                            </View>
                        </TouchableOpacity>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold', marginTop: 2, marginLeft: 10 }}>{"Reviews "+"( " +this.state.data.length +" ) "}</Text>
                    </View>
                </View>
                <View style={styles.curveview}>
                    <View style={{ flexDirection: 'row', margin: 7 }}>
                        <FlatList
                            style={{ marginBottom: metrics.dimen_10 }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.data}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item }) => (
                                <View style={{ backgroundColor: 'white', flexDirection: 'column', margin: 10 }}>
                                    <View style={{ backgroundColor: 'white', flexDirection: 'row', margin: 10 }}>
                                        <Image source={require("../../Images/user.png")} style={{ height: metrics.dimen_30, width: metrics.dimen_30, alignSelf: 'center' }}></Image>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1 }}>
                                            <View style={{ flexDirection: 'column', marginLeft:5}}>
                                                <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, textAlign: 'center', marginTop: 2, fontWeight: 'bold' }}>{item.customer.name}</Text>
                                                <Text style={{ fontSize: metrics.text_medium, color: colors.app_gray, textAlign: 'center', marginTop: 2, marginLeft: 10}}>{item.updated_at}</Text>
                                            </View>
                                            <View>
                                                <Text style={{ fontSize: metrics.text_medium, fontFamily: metrics.quicksand_regular, color: colors.app_green, textAlign: 'center', marginTop: 2, fontWeight: 'bold', marginBottom: 5 }}>{item.labels}</Text>
                                                <View style={{ width: 100, marginRight: 10 }}>
                                                    <StarRating
                                                        disabled={false}
                                                        maxStars={5}
                                                        starSize={12}
                                                        fullStarColor={colors.app_yellow_color}
                                                        rating={item.rating} />
                                                </View>
                                            </View>
                                        </View>
                                    </View>

                                    <Text style={{ fontSize: metrics.text_medium, color: colors.app_gray, marginTop: 2, marginLeft: 5 }}>{item.comment}</Text>

                                </View>
                            )} />
                    </View>
                </View>
            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({
    horizontalLine: { borderBottomColor: '#f2f2f2', borderBottomWidth: 8, width: '100%', alignSelf: 'center' },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    curveview: { height: metrics.newView.curveview, position: "absolute", top: metrics.newView.curvetop - 110, width: "100%", backgroundColor: colors.white, borderRadius: 40 }
})