import React from 'react';
import { SafeAreaView, Text, View, Image, Dimensions, StyleSheet, TouchableOpacity, ScrollView, FlatList, ImageBackground, BackHandler } from 'react-native';
import metrics from '../../Themes/Metrics'
import colors from '../../Themes/Colors';
import Slideshow from "../../Slideshow.js"
var Spinner = require('react-native-spinkit');
const axios = require('axios');

import { Input } from 'react-native-elements'
import RBSheet from 'react-native-raw-bottom-sheet';

const filter_condition_const = {
    new : 'new',
    used: 'Used',
    refurbished: 'refurbished'
}

export default class CaribMallHome extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            spinvisible: false,
            search_text: '',
            products: [],
            free_shipping: false,
            new_arrivals: false,
            filters_condition: '',
            filters_offers: 'no',
            filters_price_mininum : '',
            filters_price_maximum : ''
        }
        
    }
    
    componentDidMount() {
        console.log(this.props);
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }


    doProductSearch = () => {
        const text = this.state.search_text;
        this.setState({
            onChangeFrom: true,
            spinvisible: true,

        })
        let price_min = this.state.filters_price_mininum  ? this.state.filters_price_mininum : '' ;
        let price_max = price_min && this.state.filters_price_maximum  ? '&price='+ price_min+'-' +this.state.filters_price_maximum : '';

        let new_arrivals = this.state.new_arrivals ? '&new_arrivals=' :'';
        let free_shipping = this.state.free_shipping ? '&free_shipping=' :'';
        let has_offers = this.state.filters_offers == 'no' ? '' : '&has_offers=';
        let condition = this.state.filters_condition ? '&condition='+ this.state.filters_condition : ''; 
        let url = 'https://caribmall.com/api/search/'+text+'?'+ has_offers+new_arrivals+free_shipping+condition+price_max;

        console.log(url);
        axios.get(url).then((response) => {
            this.setState({
                products: response.data.data,
                spinvisible: false
            })
            console.log("products data ", response.data.data)
        }).catch((err) => {
            this.setState({
                spinvisible: false
            })
            console.log(err)
        })

        if (text == "") {
            this.setState({
                products: [],
                onChangeFrom: false, spinvisible: false
            })
        }
    }
    setFilters = () => {

        this.RBSheetAlert.close();
        this.doProductSearch();
    } 

    renderFilters = () => {
        return (
          <View style={{ flex: 1 }}>
            <View
              style={{
                flexDirection: "row",
                backgroundColor: colors.theme_caribpay,
                height: 50,
              }}
            >
              <TouchableOpacity
                style={{
                  height: metrics.dimen_25,
                  width: metrics.dimen_25,
                  tintColor: "white",
                  alignSelf: "center",
                  marginLeft: 5,
                }}
                onPress={() => this.RBSheetAlert.close()}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                    alignSelf: "center",
                    marginLeft: 5,
                  }}
                  source={require("../../Images/leftarrow.png")}
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: 18,
                  color: colors.white,
                  fontWeight: "bold",
                  alignSelf: "center",
                  marginHorizontal: 10,
                }}
              >
                Apply Filters
              </Text>
            </View>
            <View style={{ margin: 10 }}>
              <Text
                style={{ fontSize: 18, color: colors.app_gray, marginLeft: 10 }}
              >
                Condition
              </Text>
            </View>
            <View style={{ flexDirection: "row", marginLeft: 20 }}>
              <TouchableOpacity
                style={{
                  width: 100,
                  borderRadius: 5,
                  height: 30,
                  backgroundColor: this.state.filters_condition === filter_condition_const.new ? colors.show_all_text : colors.light_grey_backgroud,
                  justifyContent: "center",
                }}
                onPress={() => this.setState({filters_condition : filter_condition_const.new})}
              >
                <Text
                  style={{
                    fontSize: 14,
                    alignSelf: "center",
                    marginHorizontal: 10,
                    color: colors.app_gray,
                  }}
                >
                  New
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: 100,
                  borderRadius: 5,
                  height: 30,
                  backgroundColor: this.state.filters_condition === filter_condition_const.used ? colors.show_all_text : colors.light_grey_backgroud,
                  justifyContent: "center",
                  marginLeft: 10,
                }}
                onPress={() => this.setState({filters_condition :filter_condition_const.used})}
              >
                <Text
                  style={{
                    fontSize: 14,
                    alignSelf: "center",
                    marginHorizontal: 10,
                    color: colors.app_gray,
                  }}
                >
                  Used
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: 100,
                  borderRadius: 5,
                  height: 30,
                  backgroundColor: this.state.filters_condition === filter_condition_const.refurbished ? colors.show_all_text : colors.light_grey_backgroud,
                  justifyContent: "center",
                  marginLeft: 10,
                }}
                onPress={() => this.setState({filters_condition : filter_condition_const.refurbished})}
              >
                <Text
                  style={{
                    fontSize: 14,
                    alignSelf: "center",
                    marginHorizontal: 10,
                    color: colors.app_gray,
                  }}
                >
                  Refurbished
                </Text>
              </TouchableOpacity>
            </View>
    
            <View style={{ margin: 10 }}>
              <Text
                style={{ fontSize: 18, color: colors.app_gray, marginLeft: 10 }}
              >
                Offers
              </Text>
            </View>
            <View style={{ flexDirection: "row", marginLeft: 20 }}>
              <TouchableOpacity
                style={{
                  width: 100,
                  borderRadius: 5,
                  height: 30,
                  backgroundColor: this.state.filters_offers === 'yes' ? colors.show_all_text : colors.light_grey_backgroud,
                  justifyContent: "center",
                }}
                onPress={() => this.setState({filters_offers : 'yes'})}
              >
                <Text
                  style={{
                    fontSize: 14,
                    alignSelf: "center",
                    marginHorizontal: 10,
                    color: colors.app_gray,
                  }}
                >
                  Yes
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: 100,
                  borderRadius: 5,
                  height: 30,
                  backgroundColor: this.state.filters_offers === 'no' ? colors.show_all_text : colors.light_grey_backgroud,
                  justifyContent: "center",
                  marginLeft: 10,
                }}
                onPress={() => this.setState({filters_offers : 'no'})}
              >
                <Text
                  style={{
                    fontSize: 14,
                    alignSelf: "center",
                    marginHorizontal: 10,
                    color: colors.app_gray,
                  }}
                >
                  No
                </Text>
              </TouchableOpacity>
            </View>
    
            <View style={{ margin: 10 }}>
              <Text
                style={{ fontSize: 18, color: colors.app_gray, marginLeft: 10 }}
              >
                Price
              </Text>
            </View>
            <View style={{ flexDirection: "row", marginLeft: 50 }}>
              <Input
                placeholder={"Min"}
                keyboardType="number-pad"
                placeholderTextColor={colors.app_gray}
                value={this.state.filters_price_mininum}
                onChangeText={(text) => this.setState({filters_price_mininum : text })}
                containerStyle={{
                  width: "40%",
                  height: 50,
                  borderColor: colors.light_grey_backgroud,
                  borderWidth: 1,
                }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={{ fontSize: 14 }}
              />
    
              <Input
                placeholder={"Max"}
                placeholderTextColor={colors.app_gray}
                value={this.state.filters_price_maximum}
                keyboardType="number-pad"
                maxLength={5}
                onChangeText={(text) => this.setState({filters_price_maximum : text})}
                containerStyle={{
                  width: "40%",
                  height: 50,
                  borderColor: colors.light_grey_backgroud,
                  borderWidth: 1,
                  marginLeft: 10,
                }}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={{ fontSize: 14 }}
              />
            </View>
    
            <View
              style={{
                flexDirection: "row",
                height: 50,
                position: "absolute",
                bottom: 80,
                width: "100%",
              }}
            >
              <TouchableOpacity
                style={{
                  flex: 1 / 2,
                  height: 50,
                  backgroundColor: colors.light_grey_backgroud,
                  borderRadius: 5,
                  justifyContent: "center",
                  marginLeft: 10,
                }}
                onPress={() => this.setFilters()}
              >
                <Text
                  style={{
                    color: colors.app_gray,
                    textAlign: "center",
                    alignSelf: "center",
                  }}
                >
                  Reset
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 1 / 2,
                  height: 50,
                  backgroundColor: colors.app_orange_color,
                  borderRadius: 5,
                  marginLeft: 10,
                  justifyContent: "center",
                  marginRight: 10,
                }}
                onPress={() => this.setFilters()}
              >
                <Text
                  style={{
                    color: colors.white,
                    textAlign: "center",
                    alignSelf: "center",
                  }}
                >
                  Done
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        );
      };

      
    render() {
        return (
            // <View>
            <View style={{ backgroundColor: colors.light_grey_backgroud, flex: 1 }}>
                <View style={styles.header}>
                    <TouchableOpacity style={{ width: 30, height: 30, resizeMode: 'contain', alignSelf: 'center' }} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={{ width: 24, height: 24, resizeMode: 'contain', alignSelf: 'center', tintColor: colors.white ,marginTop:4}}
                            source={require('../../Images/back-button.png')}></Image>
                    </TouchableOpacity>
                    <View style={{ marginTop: metrics.dimen_5, width: "90%", alignSelf: 'center', marginBottom: 5, marginHorizontal: 15 }}>
                        <Input
                            containerStyle={{ alignSelf: 'center', backgroundColor: colors.white, borderRadius: 35 / 2, height: 35, marginLeft: -15 }}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            placeholder="I'm Shopping for..."
                            placeholderTextColor="#9D9D9F"
                            inputStyle={{ color: colors.black, fontSize: metrics.text_normal, marginTop: -10 }}
                            onChangeText={text => this.setState({search_text: text}, () => setTimeout(this.doProductSearch, 500))}
                            leftIcon={<Image style={{ marginTop: -10, width: metrics.dimen_25, height: metrics.dimen_25, resizeMode: 'contain' }}
                                source={require('../../Images/search.png')}></Image>} />
                    </View>
                </View>
                
{/* filter menu  */}
            <RBSheet
                ref={(ref) => {
                    this.RBSheetAlert = ref;
                }}
                height={Dimensions.get("screen").height}
                duration={0}
                onClose={() => {
                    console.log('hi');
                  }}
                >
                {this.renderFilters()}
            </RBSheet>
            
            {/* {this.state.spinvisible == false && this.state.products.length > 0 && */}
            <View
                style={{
                  flexDirection: "row",
                  justifyContent: 'space-around',
                  height: 50,
                  backgroundColor: colors.light_grey_backgroud,
                  marginRight: 5,
                }}
              >
                <TouchableOpacity
                  style={{
                    height: 30,
                    width: 30,
                    tintColor: "white",
                    alignSelf: "center",
                  }}
                  onPress={() => this.setState({
                    free_shipping: !this.state.free_shipping
                  },() => setTimeout(this.doProductSearch, 10))}
                    >
                
                <Image
                  style={{ height: 30, width: 30, alignSelf: "center" }}
                  source={require("../../Images/newArr.png")}
                  />
                </TouchableOpacity>

                <TouchableOpacity
                  style={{
                    height: 30,
                    width: 30,
                    tintColor: "white",
                    alignSelf: "center",
                  }}
                  onPress={() => this.setState({new_arrivals: !this.state.new_arrivals},() => setTimeout(this.doProductSearch, 10))}
                >
                <Image
                  style={{
                    height: 30,
                    width: 30,
                    alignSelf: "center",
                    marginLeft: 30,
                  }}
                  source={require("../../Images/free.png")}
                />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    height: 30,
                    width: 30,
                    tintColor: "white",
                    alignSelf: "center",
                  }}
                  onPress={() => this.RBSheetAlert.open()}
                >
                  <Image
                    style={{
                      height: 22,
                      width: 22,
                      alignSelf: "center",
                      marginLeft: 30,
                      marginTop: 4,
                    }}
                    source={require("../../Images/filter.png")}
                  />
                </TouchableOpacity>
            </View>
            {/* } */}

            <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinvisible} size={70} type={"ThreeBounce"} color={colors.black} />
            
                {this.state.spinvisible == false && this.state.products.length > 0 &&
                    <View style={{ alignSelf: 'center', borderRadius: 10, width: '90%' }}>

                        <FlatList
                            style={{ marginTop: metrics.dimen_10, marginBottom: metrics.dimen_70 }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.products}
                            numColumns={2}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item }) => (
                                <View style={{
                                    margin: 7, flex: 1 / 2, backgroundColor: colors.white, width: 210, height: 220, borderRadius: 10, justifyContent: 'center'
                                }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate("ProductDetails", { slug: item.slug })}>
                                        <Image source={{ uri: item.image }} style={{
                                            height: metrics.dimen_140, width: metrics.dimen_140, alignSelf: 'center', margin: 5
                                        }}></Image>
                                        <Text numberOfLines={2} style={{ fontSize: metrics.text_normal, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, marginTop: 2, marginLeft: 5 }}>{item.title}</Text>
                                        <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 2, justifyContent: 'space-between' }}>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 0.5 }}>
                                                {item.offer_price != null && <Text style={{ fontSize: metrics.text_normal, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, marginTop: 2, fontWeight: 'bold', marginLeft: 2 }}>{item.offer_price}</Text>}
                                                <Text style={{ fontSize: item.offer_price != null ? metrics.text_medium : metrics.text_normal, color: item.offer_price != null ? colors.app_gray : colors.heading_black_text, marginTop: 1.5, fontWeight: 'bold', marginLeft: 2, textDecorationLine: item.offer_price != null ? 'line-through' : 'none', alignSelf: 'center', textAlignVertical: 'center' }}>{item.price}</Text>
                                            </View>
                                            <Image source={require('../../Images/supermarket.png')} style={{
                                                height: metrics.dimen_15, width: metrics.dimen_15, alignSelf: 'center', marginRight: 10
                                            }}></Image>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            )} />
                    </View>}

                {this.state.spinvisible == false && this.state.products.length == 0 && <View style={{ flex: 1, justifyContent: 'center', backgroundColor: colors.lightWhite }}>
                    <Image style={{ height: this.state.onChangeFrom ? metrics.dimen_200 : metrics.dimen_100, width: this.state.onChangeFrom ? metrics.dimen_200 : metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center' }} source={this.state.onChangeFrom ? require("../../Images/nosrch.png") : require("../../Images/seo.png")}></Image>
                    {this.state.onChangeFrom && <Text style={{ alignSelf: 'center', fontSize: metrics.text_21, marginTop: 20 }}>{this.state.onChangeFrom ? "Oops! No Result Found" : "Search Anything you want"}</Text>}
                    <Text style={{ alignSelf: 'center', fontSize: this.state.onChangeFrom ? metrics.text_normal : metrics.text_large, textAlign: 'center', marginTop: this.state.onChangeFrom ? 0 : 10 }}>{this.state.onChangeFrom ? "We could not find what you are looking for.\nPlease Try Again" : "We hope you find what you are looking for."}</Text>
                </View>} 


            </View>
        )
    }


}

const styles = StyleSheet.create({
    header: {
        height: metrics.dimen_60,
        backgroundColor: colors.carib_pay_blue,
        paddingHorizontal: metrics.dimen_10,
        flexDirection: 'row',
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        alignSelf: 'center',
        marginHorizontal: 100
    },

    absouluteview: { position: 'absolute', top: 51, borderTopRightRadius: metrics.dimen_20, borderTopLeftRadius: metrics.dimen_20, width: '100%', height: Dimensions.get('screen').height, backgroundColor: colors.white },
    container: { flexDirection: 'row', margin: metrics.dimen_15 },
    image_style: { height: metrics.dimen_25, width: metrics.dimen_25, resizeMode: 'contain' },
    headingg: { fontSize: metrics.text_header, color: colors.black, textAlignVertical: 'center' },
    descriiption: { fontSize: metrics.text_description, color: colors.app_gray, fontWeight: '300' },
    arrowstyle: { height: metrics.dimen_15, width: metrics.dimen_15, resizeMode: 'contain' },
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' }
})

