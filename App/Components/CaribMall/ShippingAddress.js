import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, Image, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../../Themes/Metrics.js';
import colors from '../../Themes/Colors.js';
import RadioButton from 'react-native-radio-button';


// import AsyncStorage from '@react-native-community/async-storage';
// import RadioForm from 'react-native-simple-radio-button';
// const axios = require('axios');
// import ImgToBase64 from 'react-native-image-base64';
// import ImagePicker from 'react-native-image-crop-picker';
// import Modal from 'react-native-modal';
// import RBSheet from "react-native-raw-bottom-sheet";
// import { Input } from 'react-native-elements';
// var Spinner = require('react-native-spinkit');


export default class ShippingAddress extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }} >
                <View style={{ backgroundColor: colors.theme_caribpay, height: metrics.newView.upperview }}>
                    <View style={{ flexDirection: 'row', margin: 10 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                            <View style={{ height: metrics.dimen_40, width: metrics.dimen_40, justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: 'white' }} source={require('../../Images/leftarrow.png')}></Image>
                            </View>
                        </TouchableOpacity>
                        <Text style={{ fontSize: metrics.text_heading, color: colors.white, fontWeight: 'bold', marginTop: 2, marginLeft: 10 }}>Shipping Address</Text>
                    </View>
                </View>

                <View style={styles.curveview}>

                    <Text style={{ fontSize: metrics.text_normal, marginLeft: 20, marginTop: 20, fontWeight: 'bold' }}>Ship To</Text>
                    <View style={{ flexDirection: 'row', width: '90%', alignSelf: 'center', marginTop: 10, justifyContent: 'center', backgroundColor: colors.light_grey_backgroud, height: metrics.dimen_40, borderRadius: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, textAlignVertical: 'center', marginLeft: 10, flex: 1 }}>India</Text>
                        <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, tintColor: colors.black, alignSelf: 'center', marginRight: 10 }}
                            source={require("../../Images/dropdown.png")}></Image>
                    </View>

                    <Text style={{ fontSize: metrics.text_normal, marginLeft: 20, marginTop: 15, fontWeight: 'bold' }}>Shipping Method</Text>

                    <View style={{ flexDirection: 'row', margin:16 }}>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.3 }}>
                            <Text style={{ color: colors.app_red, alignSelf: 'center' }}>$4.00</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.7 }}>
                            <Text style={{ color: colors.app_black_text, fontWeight: 'bold' }}>Evenite</Text>
                            <Text style={{ color: colors.app_gray }}>Via USP</Text>
                            <Text style={{ color: colors.app_gray }}>Estimated Delivery time : 12-19 Days</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.3 }}>
                            <RadioButton
                                size={metrics.dimen_12}
                                animation={'bounceIn'}
                                isSelected={this.state.isSM1}
                                innerColor={colors.theme_caribpay}
                                outerColor={colors.theme_caribpay}
                                onPress={() => this.setState({
                                    isSM5: false,
                                    isSM4: false,
                                    isSM3: false,
                                    isSM2: false,
                                    isSM1: true                                })}
                            />
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', margin: 16}}>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.3 }}>
                            <Text style={{ color: colors.app_red, alignSelf: 'center' }}>$5.00</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.7 }}>
                            <Text style={{ color: colors.app_black_text, fontWeight: 'bold' }}>Quas</Text>
                            <Text style={{ color: colors.app_gray }}>Via USPS</Text>
                            <Text style={{ color: colors.app_gray }}>Estimated Delivery time : 12-13 Days</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.3 }}>
                            <RadioButton
                                size={metrics.dimen_12}
                                animation={'bounceIn'}
                                isSelected={this.state.isSM2}
                                innerColor={colors.theme_caribpay}
                                outerColor={colors.theme_caribpay}
                                onPress={() => this.setState({
                                    isSM5: false,
                                    isSM4: false,
                                    isSM3: false,
                                    isSM2: true,
                                    isSM1: false
                                })}
                            />
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', margin: 16 }}>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.3 }}>
                            <Text style={{ color: colors.app_red, alignSelf: 'center' }}>$10.00</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.7 }}>
                            <Text style={{ color: colors.app_black_text, fontWeight: 'bold' }}>AUT</Text>
                            <Text style={{ color: colors.app_gray }}>Via YRC</Text>
                            <Text style={{ color: colors.app_gray }}>Estimated Delivery time : 15-16 Days</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.3 }}>
                            <RadioButton
                                size={metrics.dimen_12}
                                animation={'bounceIn'}
                                isSelected={this.state.isSM3}
                                innerColor={colors.theme_caribpay}
                                outerColor={colors.theme_caribpay}
                                onPress={() => this.setState({
                                    isSM5: false,
                                    isSM4: false,
                                    isSM3: true,
                                    isSM2: false,
                                    isSM1: false
                                })}
                            />
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', margin: 16}}>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.3 }}>
                            <Text style={{ color: colors.app_red, alignSelf: 'center' }}>$12.00</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.7 }}>
                            <Text style={{ color: colors.app_black_text, fontWeight: 'bold' }}>QUI</Text>
                            <Text style={{ color: colors.app_gray }}>Via DTDC</Text>
                            <Text style={{ color: colors.app_gray }}>Estimated Delivery time : 4-12 Days</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.3 }}>
                            <RadioButton
                                size={metrics.dimen_12}
                                animation={'bounceIn'}
                                isSelected={this.state.isSM4}
                                innerColor={colors.theme_caribpay}
                                outerColor={colors.theme_caribpay}
                                onPress={() => this.setState({
                                    isSM4: true,
                                    isSM3: false,
                                    isSM2: false,
                                    isSM1: false,
                                    isSM5:false
                                })}
                            />
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', margin: 16 }}>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.3 }}>
                            <Text style={{ color: colors.app_red, alignSelf: 'center' }}>$14.00</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.7 }}>
                            <Text style={{ color: colors.app_black_text, fontWeight: 'bold' }}>Voluptatem</Text>
                            <Text style={{ color: colors.app_gray }}>Via DTDC</Text>
                            <Text style={{ color: colors.app_gray }}>Estimated Delivery time : 4-11 Days</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 0.3 }}>
                            <RadioButton
                                size={metrics.dimen_12}
                                animation={'bounceIn'}
                                isSelected={this.state.isSM5}
                                innerColor={colors.theme_caribpay}
                                outerColor={colors.theme_caribpay}
                                onPress={() => this.setState({
                                    isSM5: true,
                                    isSM4: false,
                                    isSM3: false,
                                    isSM2: false,
                                    isSM1: false
                                })}
                            />
                        </View>
                    </View>
                </View>
            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({
    horizontalLine: { borderBottomColor: '#D3D3D3', borderBottomWidth: 0.4, width: '90%', alignSelf: 'center' },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    curveview: { height: metrics.newView.curveview + 110, position: "absolute", top: metrics.newView.curvetop - 110, width: "100%", backgroundColor: colors.white, borderRadius: 40 },
    shadowEffect: {
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: colors.white,
        width: '90%',
        height: 30,
        borderRadius: 1,
        position: 'absolute',
        bottom: 0,
        borderWidth: 1,
        borderRadius: 7,
        borderColor: '#ddd',
        borderBottomWidth: 1,
        shadowColor: 'black',
        shadowOffset: { width: 1, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
    }
})