import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, FlatList, Image, TouchableOpacity, BackHandler } from 'react-native';
import metrics from '../../Themes/Metrics.js';
import colors from '../../Themes/Colors.js';
import { Input } from 'react-native-elements';

export default class ShopMore extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.navigation.state.params.data,
            arrayholder: [],
        }
    }

    componentDidMount() {
        this.setState({
            arrayholder: this.props.navigation.state.params.data
        })
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }



    SearchFilterFunction(text) {
        const newData = this.state.arrayholder.filter(item => {
            const itemData = `${item.name.toUpperCase()}`;
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({ data: newData, text: text });
    }


    renderAllcategories = item => {
        return (
            <View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("CategoryProduct", { titleName: item.name, id: item.id, slug: item.slug })}>
                    <View style={{ flexDirection: 'row', margin: metrics.dimen_15 }}>
                    {item.id == 3 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/kids.png")}
                              />
                            )}
                            {item.id == 4 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/cloth.png")}
                              />
                            )}
                            {/* {item.id == 5 && <Icon
                                                            name={'leaf'}
                                                            type='font-awesome'
                                                            size={35}
                                                            color={colors.app_blue}
                                                        />} */}
                            {item.id == 6 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/sports.png")}
                              />
                            )}
                            {item.id == 7 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/jewels.png")}
                              />
                            )}
                            {item.id == 8 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/dog.png")}
                              />
                            )}
                            {item.id == 9 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/hobbies.png")}
                              />
                            )}
                            {item.id == 10 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/mens.png")}
                              />
                            )}
                            {/* {item.id == 11 &&
                                                            <Image source={require('../../Images/baby.png')}
                                                                style={{ height: 35, width: 35, resizeMode: 'center', alignSelf: 'center', tintColor: colors.app_blue }}></Image>
                                                        }
                                                        {item.id == 12 &&
                                                            <Image source={require('../../Images/household.png')}
                                                                style={{ height: 30, width: 30, resizeMode: 'center', alignSelf: 'center', tintColor: colors.app_blue }}></Image>
                                                        } */}
                            {item.id == 13 && (
                              <Image
                                source={require("../../Images/women-dress.png")}
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "center",
                                  alignSelf: "center",
                                  tintColor: colors.app_blue,
                                }}
                              />
                            )}
                            {item.id == 14 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/hk.png")}
                              />
                            )}
                            {item.id == 15 && (
                              <Image
                                source={require("../../Images/mechanic-tools.png")}
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "center",
                                  alignSelf: "center",
                                  tintColor: colors.app_blue,
                                }}
                              />
                            )}
                            {item.id == 16 && (
                              <Image
                                source={require("../../Images/smart-home.png")}
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "center",
                                  alignSelf: "center",
                                  tintColor: colors.app_blue,
                                }}
                              />
                            )}
                            {item.id == 17 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/antivirus.png")}
                              />
                            )}
                            {item.id == 18 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/electronics.png")}
                              />
                            )}
                            {item.id == 20 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/ed.png")}
                              />
                            )}
                            {item.id == 21 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/automobiles.png")}
                              />
                            )}
                            {item.id == 24 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/ea.png")}
                              />
                            )}
                            {item.id == 25 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/women.png")}
                              />
                            )}
                            {item.id == 26 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/beauty.png")}
                              />
                            )}
                            {item.id == 28 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/giftss.png")}
                              />
                            )}
                            {item.id == 29 && (
                              <Image
                                style={{
                                  height: 45,
                                  width: 45,
                                  resizeMode: "contain",
                                }}
                                source={require("../../Images/ca.png")}
                              />
                            )}
                        <Text
                            style={{
                                fontFamily:metrics.quicksand_bold,
                                fontSize: metrics.text_normal,
                                paddingHorizontal: 10,
                                alignSelf:"center"
                            }}>
                            {item.name}
                        </Text>
                    </View>
                    <View
                        style={{
                            borderBottomColor: colors.light_grey_backgroud,
                            borderBottomWidth: 1,
                        }}
                    />
                </TouchableOpacity>
            </View>
        );
    };


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }} >
                <View>

                    <View
                        style={{
                            marginTop: metrics.dimen_5,
                            width: '95%',
                            alignSelf: 'center',
                        }}>
                        <Input
                            containerStyle={{
                                alignSelf: 'center',
                                borderColor:colors.light_grey_backgroud,
                                backgroundColor:colors.white,
                                borderRadius: 20,
                                height: 40,
                                borderWidth:1
                            }}
                            inputContainerStyle={{ borderBottomWidth: 0,marginTop:-5 }}
                            placeholder="Search"
                            placeholderTextColor="#9D9D9F"
                            inputStyle={{ color: colors.black, fontSize: metrics.text_16 }}
                            value={this.state.text}
                            onChangeText={text => this.SearchFilterFunction(text)}
                            leftIcon={
                                <Image
                                    style={{
                                        width: metrics.dimen_25,
                                        height: metrics.dimen_25,
                                        resizeMode: 'contain',
                                        tintColor:colors.app_gray
                                    }}
                                    source={require('../../Images/find.png')}
                                />
                            }
                        />
                    </View>

                    <FlatList
                        style={{ marginTop: metrics.dimen_7,marginBottom:40 }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.data}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => this.renderAllcategories(item)}
                    />
                </View>
            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({
    horizontalLine: { borderBottomColor: '#f2f2f2', borderBottomWidth: 8, width: '100%', alignSelf: 'center' },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_10 },
    curveview: { height: metrics.newView.curveview, position: "absolute", top: metrics.newView.curvetop - 110, width: "100%", backgroundColor: colors.white, borderRadius: 40 }
})