import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
var Spinner = require("react-native-spinkit");
import Toast, { DURATION } from "react-native-easy-toast";
import Stripe from "react-native-stripe-api";
import Modal from "react-native-modal";
const axios = require("axios");
import { CreditCardInput } from "react-native-credit-card-input";

export default class AddMoneyStripe extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: this.props.navigation.state.params.amount,
      token: this.props.navigation.state.params.token,
      cvv: "",
      expiryMonth: "",
      expiryYear: "",
      visible: false,
      cardNumber: "",
      stripeKey: "",
      updated_balance: "",
      isVisible: false,
      isVisiblePayment: false,
      card_details: "",
      isAmexCard: false,

      shipping_option_id: this.props.navigation.state.params.shipping_option_id,
      shipping_address: this.props.navigation.state.params.shipping_address,
      payment_method_id: this.props.navigation.state.params.payment_method_id,
      address_id: this.props.navigation.state.params.address_id,
      buyer_note: this.props.navigation.state.params.buyer_note,
      cart_id: this.props.navigation.state.params.cart_id,
      phone_number: this.props.navigation.state.params.phone_number,
    };
  }

  componentDidMount() {
    let postData = { method_id: "2", currency_id: "1" };
    axios({
      method: "post",
      url: "https://sandbox.caribpayintl.com/api/deposit/get-stripe-info",
      data: postData,
      headers: { Authorization: this.state.token },
    })
      .then((response) => {
        if (response.data.success.status == 200) {
          this.setState({
            stripeKey: response.data.success.stripe_keys.secret_key,
          });
          console.log(this.state.stripeKey);
        }
      })
      .catch((err) => {
        console.log("error ", err);
      });
  }

  onSendMessage = () => {
    console.log("called on send messge");
    axios({
      method: "post",
      url:
        "https://www.experttexting.com/ExptRestApi/sms/json/Message/Send?username=caribpay&api_key=6ljxqccz53x23ns&api_secret=2bukt1ihaefc34w&from=DEFAULT&to=" +
        this.state.phone_number +
        "&text=" +
        "Your Order has been placed Successfully!" +
        "&type=text",
    })
      .then((response) => {
        console.log(" response message ---> ", response.data);
        this.setState({
          isSMSVisible: false,
          message: "",
        });
      })
      .catch((error) => {
        console.log("response message  errors ", error);
      });
  };

  OnProccede = async () => {
    this.setState({
      isOrderPlaced: true,
      spinOrderlacedvisible: true,
    });

    let month = this.state.card_details.expiry.substr(0, 2);
    let year = this.state.card_details.expiry.substr(3, 5);
    this.setState({ visible: true });
    const apiKey = this.state.stripeKey;
    const client = new Stripe(apiKey);
    const token = await client
      .createToken({
        number: this.state.card_details.number,
        exp_month: month,
        exp_year: year,
        cvc: this.state.card_details.cvc,
        address_zip: "12345",
      })
      .then((response) => {
        if (response) {
          console.log("response", response);
            let postData1 = {
              shipping_option_id: this.state.shipping_option_id,
              shipping_address: this.state.address_id,
              payment_method_id: this.state.payment_id,
              address_id: this.state.address_id,
              buyer_note: this.state.description,
            };

            console.log("posdata placed orders  ---> ", postData1);

            axios({
              method: "post",
              url:
                "https://caribmall.com/api/cart/" +
                this.state.cart_id +
                "/checkout?api_token=" +
                this.state.token,
              data: postData1,
            })
              .then((response) => {
                console.log("response order placed ---> ", response.data.data);
                console.log("order number", response.data.data.order_number);
                this.setState({
                  spinOrderlacedvisible: false,
                  order_number: response.data.data.order_number,
                });
                this.onSendMessage();
              })

          .catch((error) => {
            console.log("response order placed errors ", error);
            this.setState({
              spinvisible: false,
            });
          });
          
        } else {
          this.setState({ visible: false });
        }
      })
      .catch((err) => {
        console.log(err);
        this.setState({ visible: false });
      });
  };

  _onChange = (form) => {
    let card_details = form.values;

    if (
      card_details.number.startsWith("34") ||
      card_details.number.startsWith("37")
    ) {
      this.setState({
        isAmexCard: true,
      });
    }
    if (
      card_details.number != "" &&
      card_details.expiry !== "" &&
      card_details.cvc.length == 3
    ) {
      this.setState({
        creditInputDone: true,
        card_details: card_details,
      });
    }
    console.log(card_details);
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.header}>
          <TouchableOpacity
            style={styles.headerleftImage}
            onPress={() => this.props.navigation.goBack(null)}
          >
            <Image
              style={styles.headerleftImage}
              source={require("../../Images/leftarrow.png")}
            />
          </TouchableOpacity>
          <Text style={styles.headertextStyle}>Stripe Payment</Text>
        </View>
        <View style={styles.absouluteview}>
          {/* <LinearGradient colors={['#b4e6f1', '#d7f3f7', '#f9fdff']} style={{ flex: 1 }}> */}
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              backgroundColor: colors.light_grey_backgroud,
              height: metrics.dimen_35,
            }}
          >
            <Text
              style={{
                color: colors.black,
                fontSize: metrics.text_normal,
                fontFamily: metrics.quicksand_semibold,
                marginLeft: metrics.dimen_10,
                textAlignVertical: "center",
              }}
            >
              Amount to Pay
            </Text>
            <Text
              style={{
                color: colors.black,
                fontSize: metrics.text_normal,
                fontFamily: metrics.quicksand_semibold,
                marginRight: metrics.dimen_10,
                textAlignVertical: "center",
              }}
            >
              {"$ " + this.state.amount}
            </Text>
          </View>
          <View style={{ flex: 1, margin: metrics.dimen_15 }}>
            <View style={{ margin: 20 }}>
              <CreditCardInput
                onChange={this._onChange}
                placeholderColor={colors.app_light_gray}
              />
            </View>

            <Modal
              style={{ alignSelf: "center", width: "80%" }}
              isVisible={this.state.visible}
            >
              <View
                style={{
                  backgroundColor: "white",
                  borderRadius: metrics.dimen_10,
                }}
              >
                <View
                  style={{ flexDirection: "column", margin: metrics.dimen_20 }}
                >
                  <Spinner
                    style={{ alignSelf: "center" }}
                    isVisible={this.state.visible}
                    size={70}
                    type={"ThreeBounce"}
                    color={colors.black}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_header,
                      color: colors.black,
                      textAlign: "center",
                      marginTop: 20,
                    }}
                  >
                    We are processing now, Please wait a while!{" "}
                  </Text>
                </View>
              </View>
            </Modal>

            <Toast
              ref="toast"
              style={{ backgroundColor: "black" }}
              position="center"
              positionValue={200}
              fadeInDuration={200}
              fadeOutDuration={1000}
              opacity={0.8}
              textStyle={{ color: "white" }}
            />
          </View>
        </View>

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.isAmexCard}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                margin: metrics.dimen_20,
                flexDirection: "column",
                height: 250,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <View>
                  <Image
                    style={{
                      height: metrics.dimen_120,
                      width: metrics.dimen_80,
                      resizeMode: "contain",
                      alignSelf: "center",
                      marginTop: 20,
                    }}
                    source={require("../../Images/danger.png")}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      color: colors.black,
                      fontWeight: "bold",
                      textAlign: "center",
                    }}
                  >
                    {
                      "Sorry! AMEX CARD is not Valid/Unsupported,Please try other card!"
                    }
                  </Text>
                </View>
                <View style={styles.bottomview2}>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        isAmexCard: false,
                      })
                    }
                  >
                    <Text
                      style={{
                        fontSize: 15,
                        color: "white",
                        fontWeight: "bold",
                        textAlign: "center",
                        alignSelf: "center",
                        marginLeft: 10,
                      }}
                    >
                      Try Again
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </View>

        {this.state.creditInputDone && (
          <View
            style={{
              ...styles.bottomview,
              backgroundColor: colors.carib_pay_blue,
            }}
          >
            <TouchableOpacity onPress={() => this.OnProccede()}>
              <Text
                style={{
                  fontSize: 15,
                  color: "white",
                  fontFamily: metrics.quicksand_semibold,
                  textAlign: "center",
                  alignSelf: "center",
                  marginLeft: 10,
                }}
              >
                {"Placed Order "}
              </Text>
            </TouchableOpacity>
          </View>
        )}

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.isOrderPlaced}
        >
          <View
            style={{
              width: "80%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: !this.state.spinOrderlacedvisible ? 250 : 300,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinOrderlacedvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
            {!this.state.spinOrderlacedvisible && (
              <View style={{ margin: 10, marginBottom: 10 }}>
                <Text
                  style={{
                    alignSelf: "center",
                    textAlign: "center",
                    marginTop: 15,
                    fontSize: 16,
                    color: colors.black,
                    fontFamily: metrics.quicksand_bold,
                  }}
                >
                  {"Order Confirmation!"}
                </Text>

                <Image
                  source={require("../../Images/orderplaced.png")}
                  style={{
                    height: metrics.dimen_80,
                    width: metrics.dimen_80,
                    resizeMode: "center",
                    alignSelf: "center",
                  }}
                />
                <Text
                  style={{
                    alignSelf: "center",
                    textAlign: "center",
                    marginTop: 15,
                    color: colors.app_gray,
                    fontSize: metrics.text_normal,
                    fontFamily: metrics.quicksand_bold,
                  }}
                >
                  {"Your order " +
                    this.state.order_number +
                    " has been placed!"}
                </Text>

                <TouchableOpacity
                  onPress={() =>
                    this.setState({ isOrderPlaced: false }, () =>
                      this.props.navigation.navigate("CaribMallHome")
                    )
                  }
                >
                  <View
                    style={{
                      width: "80%",
                      backgroundColor: colors.theme_caribpay,
                      height: 40,
                      borderRadius: 10,
                      alignSelf: "center",
                      justifyContent: "center",
                      marginTop: 15,
                    }}
                  >
                    <Text
                      style={{
                        alignSelf: "center",
                        textAlign: "center",
                        color: colors.white,
                        fontFamily: metrics.quicksand_bold,
                      }}
                    >
                      {"Continue Shopping"}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: metrics.dimen_70,
    backgroundColor: colors.carib_pay_blue,
    flexDirection: "row",
    paddingHorizontal: metrics.dimen_20,
  },
  headerleftImage: {
    height: metrics.dimen_24,
    width: metrics.dimen_24,
    alignSelf: "center",
    resizeMode: "contain",
    tintColor: colors.whitesmoke,
    marginBottom: metrics.dimen_15,
  },
  headertextStyle: {
    fontSize: metrics.text_17,
    fontFamily: metrics.quicksand_semibold,
    color: colors.white,
    marginBottom: metrics.dimen_15,
    textAlignVertical: "center",
    paddingHorizontal: metrics.dimen_20,
  },
  absouluteview: {
    position: "absolute",
    top: 51,
    borderTopRightRadius: metrics.dimen_20,
    borderTopLeftRadius: metrics.dimen_20,
    width: "100%",
    height: Dimensions.get("screen").height,
    backgroundColor: colors.white,
  },
  container: { flexDirection: "row", margin: metrics.dimen_20 },
  image_style: {
    height: metrics.dimen_30,
    width: metrics.dimen_30,
    resizeMode: "contain",
  },
  headingg: { fontSize: metrics.text_header, color: colors.black },
  descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
  arrowstyle: {
    height: metrics.dimen_15,
    width: metrics.dimen_15,
    resizeMode: "contain",
  },
  horizontalLine: {
    borderBottomColor: "#D3D3D3",
    borderBottomWidth: 0.4,
    width: "90%",
    alignSelf: "center",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 20,
  },
  bottomview2: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_5,
  },
});
