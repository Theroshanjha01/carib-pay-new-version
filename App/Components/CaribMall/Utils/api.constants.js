export const environment = {
    // currentEndPoint: 'https://private-c533ef-cardx.apiary-mock.com/',
    currentEndPoint: 'https://sandbox.caribpayintl.com/api/',
    // currentEndPoint: 'http://373a97127e45.ngrok.io/api/v1/',
  };
  
  export const returnUrl = 'https://www.paypal.com/in/webapps/mpp/merchant';
  export const cancelUrl = 'https://www.paypal.com/in/webapps/mpp/merchant';

  export const maxValue = 5000012112120000; 
  export const paypal_basictoken = 'Basic QWRFM0ZyRWdmQTktbVZoM1RiMTktNkhCRWN5SUU2cGp4X0wyMjB3SThnekk0SmhibGk0b3NZZDZvUkJRY1dFcml6eDlmOEtYLVlqWXZ1NEw6RUFxVWdFQmpFNmtFc0FHQ05RSldKMlNBSUtvalpseEhZMXE3RDRmMnVsR2pyS0FwSV9jSnByX2lHanhQak9nSlY5VmNzb0JVQjdJd1FfOWQ=';
  export const paypal_gettoken = 'https://api.sandbox.paypal.com/v1/oauth2/token';
  export const paypal_gettoken_live = 'https://api.paypal.com/v1/oauth2/token';
  export const paypal_payment = 'https://api.sandbox.paypal.com/v1/payments/payment';
  export const paypal_payment_live = 'https://api.paypal.com/v1/payments/payment';

  export const touchAPi = environment.currentEndPoint +'enable-touch';
  export const getDeviceInfoApi = environment.currentEndPoint +'get-device-info';
  export const logWithTouchApi = environment.currentEndPoint +'login-with-touch';

  export const expressmerchantwalletsreview =
  environment.currentEndPoint +
  "perform-express-merchant-payment-qr-code-merchant-currency-user-wallets-review";

  export const expressmerchantamountreview =
  environment.currentEndPoint +
  "perform-express-merchant-payment-qr-code-merchant-amount-review";

  export const expressmerchantcodesubmit =
  environment.currentEndPoint + "perform-express-merchant-payment-qr-code-submit";
