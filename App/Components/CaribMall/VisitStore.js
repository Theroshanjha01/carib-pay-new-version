import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, FlatList, Image, TouchableOpacity, BackHandler, Dimensions, ScrollView, ActivityIndicator } from 'react-native';
import metrics from '../../Themes/Metrics.js';
import colors from '../../Themes/Colors.js';
var Spinner = require('react-native-spinkit');
const axios = require('axios');
import StarRating from 'react-native-star-rating';
import RBSheet from "react-native-raw-bottom-sheet";
import HTML from "react-native-render-html";


export default class VisitStore extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            starCount: 3.5,
            spinvisible: false,
            slug: this.props.navigation.state.params.slug,
            shopName: this.props.navigation.state.params.shopName,
            shopProductListing: [],
            data: [],
            fetching_from_server: false,
            page: 1,
            pagination_url_limit: false
        }
    }

    componentDidMount() {
        this.setState({
            spinvisible: true
        })
        console.log("slug--->", this.state.slug)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.getShopDetails()
    }


    getShopDetails = () => {
        axios.get('https://caribmall.com/api/shop/' + this.state.slug).then((response) => {
            // console.log("slug response -->", response.data.data)
            this.setState({
                data: response.data.data
            })
        }).catch((err) => {
            console.log(err)
        })
        this.getShopProductist()

    }


    getShopProductist = () => {
        axios.get('https://caribmall.com/api/shop/' + this.state.slug + '/listings?page=' + this.state.page).then((response) => {
            console.log("slug response -->", response.data.data)
            this.setState({
                shopProductListing: [...this.state.shopProductListing, ...response.data.data.listings],
                pagination_url_limit: response.data.data.listings.length > 0 ? false : true,
                spinvisible: false
            })
        }).catch((err) => {
            console.log(err)
        })

    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }

    renderShop = () => {
        return (
            <View style={{ backgroundColor: colors.light_grey_backgroud }}>
                <View style={{ height: metrics.dimen_60, backgroundColor: colors.theme_caribpay, flexDirection: 'row' }}>
                    <TouchableOpacity style={{ height: metrics.dimen_25, width: metrics.dimen_25, marginStart: metrics.dimen_10, alignSelf: 'center' }}
                        onPress={() => this.RBSheetAlert.close()}>
                        <Image style={{ height: metrics.dimen_25, width: metrics.dimen_25, alignSelf: 'center', paddingHorizontal: 10 }}
                            source={require('../../Images/close.png')}></Image>
                    </TouchableOpacity>
                    <Text style={{ fontSize: metrics.text_16, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>{this.state.data.name}</Text>
                </View>
                <View style={{ flexDirection: 'column', backgroundColor: colors.light_grey_backgroud }}>
                    <Image style={{ height: 200, width: Dimensions.get('window').width }}
                        source={{ uri: this.state.data.banner_image }}>
                    </Image>

                    <View style={{ flexDirection: 'row', backgroundColor: colors.white, padding: 10 }}>
                        <Image style={{ height: 30, width: 30, resizeMode: 'contain', alignSelf: 'center' }}
                            source={{ uri: this.state.data.image }}></Image>
                        <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 20, flex: 1 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: metrics.text_description, color: colors.black }}>{this.state.data.name}</Text>
                                {this.state.data.verified &&
                                    <Image style={{ height: 25, width: 25, resizeMode: 'contain', alignSelf: 'center', marginLeft: 10 }}
                                        source={require('../../Images/check.png')}></Image>
                                }
                            </View>
                            <Text style={{ fontWeight: 'bold', fontSize: metrics.text_description, color: colors.app_gray }}>{"Member Since : " + this.state.data.member_since}</Text>
                            <View style={{ width: 300, marginRight: 10, flexDirection: 'row', marginTop: 2 }}>
                                <StarRating
                                    disabled={false}
                                    maxStars={5}
                                    starSize={12}
                                    fullStarColor={colors.app_yellow_color}
                                    rating={this.state.data.rating} />
                                {this.state.data.rating != null && <Text style={{ fontSize: metrics.text_small, color: colors.app_blue, alignSelf: 'center', marginLeft: 5 }}>{"( " + this.state.data.rating + " )"}</Text>}
                            </View>
                        </View>
                        <Image style={{ height: 20, width: 20, resizeMode: 'contain', tintColor: colors.app_blue, alignSelf: 'center' }}
                            source={require('../../Images/rightarrow.png')}></Image>
                    </View>

                    <View style={{ flexDirection: 'row', margin: 10, backgroundColor: colors.white }}>
                        <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 5, flex: 1 / 3, height: 100, borderRadius: 7 }}>
                            <Text style={{ fontSize: metrics.text_23, color: colors.app_blue, textAlign: 'center' }}>{this.state.data.active_listings_count}</Text>
                            <Text style={{ fontSize: metrics.text_normal, color: colors.black, textAlign: 'center' }}>{"Active Listing"}
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 5, flex: 1 / 3, height: 100, borderRadius: 7 }}>
                            <Text style={{ fontSize: metrics.text_23, color: colors.app_blue, textAlign: 'center' }}>{this.state.data.rating}</Text>
                            <Text style={{ fontSize: metrics.text_normal, color: colors.black, textAlign: 'center' }}>{"Rating"}
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 5, flex: 1 / 3, height: 100, borderRadius: 7 }}>
                            <Text style={{ fontSize: metrics.text_23, color: colors.app_blue, textAlign: 'center' }}>{this.state.data.sold_item_count}</Text>
                            <Text style={{ fontSize: metrics.text_normal, color: colors.black, textAlign: 'center' }}>{"Items Sold"}
                            </Text>
                        </View>
                    </View>
                </View>


                <ScrollView style={{ margin: 10, backgroundColor: 'white' }}>
                    <HTML
                        containerStyle={{ padding: 10 }}
                        html={this.state.data.description}
                        imagesMaxWidth={Dimensions.get("window").width} />
                </ScrollView>
            </View>
        )
    }


    renderFooter() {
        return (
            this.state.pagination_url_limit == false ?
                <View style={styles.footer}>
                    <TouchableOpacity
                        activeOpacity={0.9}
                        onPress={this.loadMoreData}
                        //On Click of button calling loadMoreData function to load more data
                        style={styles.loadMoreBtn}>
                        <Text style={styles.btnText}>Load More</Text>
                        {this.state.fetching_from_server ? (
                            <ActivityIndicator color="white" style={{ marginLeft: 8 }} />
                        ) : null}
                    </TouchableOpacity>
                </View>
                :
                null
        );
    }

    loadMoreData = () => {
        this.setState({
            page: this.state.page + 1
        })
        this.setState({ fetching_from_server: true }, () => {
            axios.get('https://caribmall.com/api/shop/' + this.state.slug + '/listings?page=' + this.state.page).then((response) => {
                this.setState({
                    shopProductListing: [...this.state.shopProductListing, ...response.data.data.listings],
                    pagination_url_limit: response.data.data.listings.length > 0 ? false : true,
                    fetching_from_server: false
                })
                console.log("Next Page --->", this.state.pagination_url)
            })
                .catch(error => {
                    console.error(error);
                });
        });
    };


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }}>
                {this.state.spinvisible ?
                    <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                        <View style={{ width: 120, backgroundColor: colors.white, borderRadius: metrics.dimen_7, height: 100, justifyContent: 'center' }}>
                            <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinvisible} size={70} type={"ThreeBounce"} color={colors.black} />
                        </View>
                    </View>
                    :
                    <ScrollView>
                        <View style={styles.header}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                                <View style={{ width: 36, height: 36, marginTop: 10 }}>
                                    <Image style={{ height: 24, width: 20, alignSelf: 'center', resizeMode: "contain", marginTop: 6 }}
                                        source={require('../../Images/leftarrow.png')}></Image>
                                </View>
                            </TouchableOpacity>
                            <Text style={styles.headertextStyle}>{this.state.shopName}</Text>
                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <Image style={{ height: 200, width: Dimensions.get('window').width }}
                                source={{ uri: this.state.data.banner_image }}>
                            </Image>

                            <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
                                <View style={{ flexDirection: 'row', backgroundColor: colors.white, padding: 10 }}>
                                    <Image style={{ height: 30, width: 30, resizeMode: 'contain', alignSelf: 'center' }}
                                        source={{ uri: this.state.data.image }}></Image>
                                    <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 20, flex: 1 }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={{ fontWeight: 'bold', fontSize: metrics.text_description, color: colors.black }}>{this.state.data.name}</Text>
                                            {this.state.data.verified &&
                                                <Image style={{ height: 25, width: 25, resizeMode: 'contain', alignSelf: 'center', marginLeft: 10 }}
                                                    source={require('../../Images/check.png')}></Image>
                                            }
                                        </View>
                                        <Text style={{ fontWeight: 'bold', fontSize: metrics.text_description, color: colors.app_gray }}>{"Member Since : " + this.state.data.member_since}</Text>
                                        <View style={{ width: 300, marginRight: 10, flexDirection: 'row', marginTop: 2 }}>
                                            <StarRating
                                                disabled={false}
                                                maxStars={5}
                                                starSize={12}
                                                fullStarColor={colors.app_yellow_color}
                                                rating={this.state.data.rating} />
                                            {this.state.data.rating != null && <Text style={{ fontSize: metrics.text_small, color: colors.app_blue, alignSelf: 'center', marginLeft: 5 }}>{"( " + this.state.data.rating + " )"}</Text>}
                                        </View>
                                    </View>
                                    <Image style={{ height: 20, width: 20, resizeMode: 'contain', tintColor: colors.app_blue, alignSelf: 'center' }}
                                        source={require('../../Images/rightarrow.png')}></Image>
                                </View>
                            </TouchableOpacity>


                            <View style={{ alignSelf: 'center', borderRadius: 10, width: '90%' }}>
                                <FlatList
                                    style={{ marginTop: metrics.dimen_10, marginBottom: metrics.dimen_30 }}
                                    showsVerticalScrollIndicator={false}
                                    data={this.state.shopProductListing}
                                    ListFooterComponent={this.renderFooter.bind(this)}
                                    numColumns={2}
                                    keyExtractor={(item, index) => index.toString()}
                                    renderItem={({ item }) => (
                                        <View
                                          style={{
                                            flex: 1 / 2,
                                            backgroundColor: colors.white,
                                            borderRadius: 10,
                                            margin: 5,
                                            flexDirection: "column",
                                            borderColor: colors.light_grey_backgroud,
                                            borderWidth: 1,
                                          }}
                                        >
                                           <TouchableOpacity onPress={() => this.props.navigation.navigate("StoreProductDetails", { slug: item.slug, name: 'roshan' })}
                                          >
                                            <View
                                              style={{
                                                borderTopLeftRadius: 10,
                                                borderTopRightRadius: 10,
                                              }}
                                            >
                                              <Image
                                                source={{ uri: item.image }}
                                                style={{
                                                  height: metrics.dimen_200,
                                                  width: "100%",
                                                  borderTopLeftRadius: 10,
                                                  borderTopRightRadius: 10,
                                                }}
                                              />
                                              <View
                                                style={{
                                                  position: "absolute",
                                                  top: -7,
                                                  alignSelf: "flex-end",
                                                }}
                                              >
                                                <Image
                                                  source={require("../../Images/tag.png")}
                                                  style={{ height: 30, width: 35 }}
                                                />
                                                <Text
                                                  style={{
                                                    position: "absolute",
                                                    top: 1,
                                                    fontSize: 10,
                                                    color: colors.white,
                                                    alignSelf: "center",
                                                    marginLeft: 10,
                                                    marginTop: 7,
                                                  }}
                                                >
                                                  {item.discount == null
                                                    ? "0%"
                                                    : item.discount.substring(0, 4)}
                                                </Text>
                                              </View>
                                            </View>
                                            <View
                                              style={{
                                                marginLeft: 5,
                                                marginBottom: 5,
                                                marginTop: 10,
                                              }}
                                            >
                                              <Text
                                                style={{ color: colors.app_gray }}
                                                numberOfLines={1}
                                              >
                                                {item.title}
                                              </Text>
                                              <View style={{ flexDirection: "row" }}>
                                                {item.offer_price != null && (
                                                 
                                                    <Text
                                                      style={{
                                                        alignSelf: "center",
                                                        fontSize: 15,
                                                        color: colors.app_orange_color,
                                                        marginTop: 2,
                                                        fontWeight: "bold",
                                                      }}
                                                    >
                                                      {item.offer_price}
                                                  </Text>
                                                )}
                                                <Text
                                                  style={{
                                                    alignSelf: "center",
                                                    color:
                                                      item.offer_price == null
                                                        ? colors.app_orange_color
                                                        : colors.app_gray,
                                                    fontSize: 12,
                                                    textDecorationLine:
                                                      item.offer_price == null
                                                        ? null
                                                        : "line-through",
                                                    marginLeft: item.offer_price == null ? 0 : 10,
                                                  }}
                                                >
                                                  <Text
                                                    style={{
                                                      alignSelf: "center",
                                                      fontSize:
                                                        item.offer_price == null ? 15 : 12,
                                                      color:
                                                        item.offer_price == null
                                                          ? colors.app_orange_color
                                                          : colors.app_gray,
                                                      marginTop: 2,
                                                      fontWeight: "bold",
                                                      textDecorationLine:
                                                        item.offer_price == null
                                                          ? null
                                                          : "line-through",
                                                      marginLeft:
                                                        item.offer_price == null ? 0 : 10,
                                                    }}
                                                  >
                                                    {item.price}
                                                  </Text>
                                                </Text>
                                              </View>
                                              <View style={{ width: 40, marginTop: 3 }}>
                                                <StarRating
                                                  disabled={false}
                                                  maxStars={5}
                                                  starSize={10}
                                                  buttonStyle={{ margin: 1 }}
                                                  halfStarEnabled={true}
                                                  fullStarColor={"#FEC750"}
                                                  rating={item.rating == null ? 0 : item.rating}
                                                />
                                              </View>
                  
                                              {/* <Text style={{ color: colors.app_gray }}>{"St. Lucia"}</Text> */}
                                            </View>
                                          </TouchableOpacity>
                                        </View>
                                      )}
                                    // renderItem={({ item }) => (
                                    //     <View style={{
                                    //         margin: 7, flex: 1 / 2, backgroundColor: colors.lightWhite, width: 200, height: 220, borderRadius: 10, justifyContent: 'center'
                                    //     }}>
                                    //         <TouchableOpacity onPress={() => this.props.navigation.navigate("StoreProductDetails", { slug: item.slug, name: 'roshan' })}>
                                    //             <Image source={{ uri: item.image }} style={{
                                    //                 height: metrics.dimen_140, width: metrics.dimen_140, alignSelf: 'center', margin: 5
                                    //             }}></Image>
                                    //             <Text numberOfLines={2} style={{ fontSize: metrics.text_normal, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, marginTop: 2, marginLeft: 5 }}>{item.title}</Text>
                                    //             <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 2, justifyContent: 'space-between' }}>
                                    //                 <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 0.5 }}>
                                    //                     {item.offer_price != null && <Text style={{ fontSize: metrics.text_normal, fontFamily: metrics.quicksand_regular, color: colors.heading_black_text, marginTop: 2, fontWeight: 'bold', marginLeft: 5 }}>{item.offer_price}</Text>}
                                    //                     <Text style={{ fontSize: item.offer_price != null ? metrics.text_medium : metrics.text_normal, color: item.offer_price != null ? colors.app_gray : colors.heading_black_text, marginTop: 1.5, fontWeight: 'bold', marginLeft: 5, textDecorationLine: item.offer_price != null ? 'line-through' : 'none', alignSelf: 'center', textAlignVertical: 'center' }}>{item.price}</Text>
                                    //                 </View>
                                    //                 <Image source={require('../../Images/supermarket.png')} style={{
                                    //                     height: metrics.dimen_15, width: metrics.dimen_15, alignSelf: 'center', marginRight: 10
                                    //                 }}></Image>
                                    //             </View>
                                    //         </TouchableOpacity>
                                    //     </View>
                                    // )
                                 />
                            </View>

                        </View>
                        <RBSheet
                            ref={ref => {
                                this.RBSheetAlert = ref;
                            }}
                            height={Dimensions.get('screen').height}
                            duration={0}>
                            {this.renderShop()}
                        </RBSheet>
                    </ScrollView>}
            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({
    horizontalLine: { borderBottomColor: '#f2f2f2', borderBottomWidth: 8, width: '100%', alignSelf: 'center' },
    header: {
        height: metrics.dimen_60,
        backgroundColor: colors.carib_pay_blue,
        paddingHorizontal: metrics.dimen_10,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        fontWeight: 'bold',
        marginBottom: metrics.dimen_15,
        paddingHorizontal: metrics.dimen_5,
        marginTop: 12,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center'
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    loadMoreBtn: {
        padding: 10,
        backgroundColor: colors.theme_caribpay,
        borderRadius: 20,
        height:40,
        width:120,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnText: {
        color: 'white',
        fontSize: 15,
        textAlign: 'center',
    },
})