import Axios from "axios";
import React, { useEffect, useState } from "react";
import { Modal } from "react-native-modal";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  BackHandler,
} from "react-native";
import WebView from "react-native-webview";
import colors from "../../Themes/Colors.js";
import metrics from "../../Themes/Metrics.js";
import Spinner from "react-native-spinkit";
import AsyncStorage from "@react-native-community/async-storage";
import { cos } from "react-native-reanimated";
const axios = require("axios");
const WebPage = ({ navigation }) => {
  const {
    approvalUrl,
    accessToken,
    url,
    data,
    paymentId,
    token,
    requestData,
    from,
    amount,
    note,
  } = navigation.state.params;

  const _onNavigationStateChange = (webViewState) => {
    console.log("webview statys", webViewState);
    if (webViewState.canGoBack) {
      console.log("go back called");
    }
    if (webViewState.url.includes("https://www.paypal.com/")) {
      const PayerID1 = webViewState.url.substring(
        webViewState.url.indexOf("PayerID=")
      );
      const PayerID = PayerID1.substring(PayerID1.indexOf("=") + 1);

      // https://api.sandbox.paypal.com/v1/payments/payment/${paymentId}/execute
      axios
        .post(
          `https://api.paypal.com/v1/payments/payment/${paymentId}/execute`,
          { payer_id: PayerID },
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${accessToken}`,
            },
          }
        )
        .then((response) => {
          console.log(response);
          Axios({
            method: "post",
            url: url,
            data: data,
            headers: { Authorization: token },
          })
            .then(async (response) => {
              console.log("response order placed ---> ", response);
              if (from === "Addfund") {
                let user_id = await AsyncStorage.getItem("id");
                let createdOn = response.data.success.transaction.created_at;
                let postData = { user_id: user_id };
                axios
                  .get(
                    "https://sandbox.caribpayintl.com/api/get-default-wallet-balance",
                    {
                      params: postData,
                      headers: { Authorization: token },
                    }
                  )
                  .then((response) => {
                    if (response.data.success.status == 200) {
                      let balance = response.data.success.defaultWalletBalance;
                      let available_bal = parseFloat(balance).toFixed(2);
                      navigation.navigate("SuccessFull", {
                        amount: amount,
                        remarks: note,
                        updated_balance: available_bal,
                        createdOn: createdOn,
                        payment_mode: "PayPal",
                        user_id: user_id,
                        token: token,
                      });
                    }
                  })
                  .catch((error) => {
                    console.log("error updated bal---", error);
                  });
              } else if (token) {
                navigation.navigate("SuccessFullMobile", requestData);
              } else {
                navigation.navigate("successfull", {
                  orderNO: response.data.data.order_number,
                });
              }
            })
            .catch((error) => {
              console.log("response order placed errors ", error);
            });
        })
        .catch((err) => {
          console.log({ ...err });
        });
    }
  };

  const renderMainView = () => {
    return (
      <WebView
        source={{ uri: approvalUrl }}
        style={[styles.container]}
        onNavigationStateChange={_onNavigationStateChange}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        scalesPageToFit={false}
        onMessage={(event) => {
          console.log("event:", event);
        }}
      />
    );
  };

  return renderMainView();
};

const styles = StyleSheet.create({
  navigationButton: {
    padding: 10,
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: '#e5e5e5',
  },
  activityStyle: {
    marginVertical: 12,
  },
});

export default WebPage;
