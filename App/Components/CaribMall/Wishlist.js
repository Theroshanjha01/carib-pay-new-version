import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import metrics from "../../Themes/Metrics.js";
import colors from "../../Themes/Colors.js";
import AsyncStorage from "@react-native-community/async-storage";
const axios = require("axios");
import Modal from "react-native-modal";
var Spinner = require("react-native-spinkit");
import DropdownAlert from "react-native-dropdownalert";

export default class Wishlist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      apiToken: "",
    };
  }

  componentDidMount() {
    this.getWishlists();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  addtoCart = (item) => {
    this.setState({
      spinvisible: true,
    });

    let postData = {
      quantity: "1",
    };

    console.log("add to cart params--->", postData);
    axios({
      method: "post",
      url:
        "https://caribmall.com/api/addToCart/" +
        item.slug +
        "?api_token=" +
        this.state.apiToken,
      data: postData,
    })
      .then((response) => {
        console.log("add to cart response --- > ", response.data);
        this.setState({ spinvisible: false });
        this.dropDownAlertRef.alertWithType("success", response.data.message);
      })
      .catch((error) => {
        if (error.response) {
          console.log("response add to cart error block ", error.response.data);
          this.dropDownAlertRef.alertWithType(
            "error",
            error.response.data.message
          );

          this.setState({ spinvisible: false });
        }
      });
  };

  getWishlists = async () => {
    this.setState({
      spinvisible: true,
    });
    let caribmall_data = await AsyncStorage.getItem("carib_mall_data");
    let data = JSON.parse(caribmall_data);
    this.setState({
      apiToken: data.api_token,
    });
    axios({
      method: "get",
      url: "https://caribmall.com/api/wishlist?api_token=" + data.api_token,
    })
      .then((response) => {
        console.log("response coupons", response.data);
        this.setState({
          data: response.data.data,
          spinvisible: false,
        });
      })
      .catch((error) => {
        console.log("response coupons errors ", error);
      });
  };

  OnDeleteItems = (item) => {
    this.setState({
      spinvisible: true,
    });
    axios({
      method: "delete",
      url:
        "https://caribmall.com/api/wishlist/" +
        item.id +
        "/remove?api_token=" +
        this.state.apiToken,
    })
      .then((response) => {
        console.log("response wishlist removed data", response.data);
        this.setState({
          spinvisible: false,
        });
        this.dropDownAlertRef.alertWithType("success", response.data.message);
        this.getWishlists();
      })
      .catch((error) => {
        if (error.response) {
          console.log("response", error.response.data);
          this.setState({
            spinvisible: false,
          });
          this.dropDownAlertRef.alertWithType(
            "error",
            error.response.data.message
          );
        }
      });
  };

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }}
      >
        <View
          style={{
            backgroundColor: colors.theme_caribpay,
            height: metrics.newView.upperview,
          }}
        >
          <View style={{ flexDirection: "row", margin: 10 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
            >
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                  }}
                  source={require("../../Images/leftarrow.png")}
                />
              </View>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: metrics.text_heading,
                color: colors.white,
                fontWeight: "bold",
                marginTop: 2,
                marginLeft: 10,
              }}
            >
              Wishlist
            </Text>
          </View>
        </View>
        <View style={styles.curveview}>
          {this.state.spinvisible == false && this.state.data.length > 0 && (
            <FlatList
              style={{ marginTop: metrics.dimen_10 }}
              showsVerticalScrollIndicator={false}
              data={this.state.data}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <View
                  style={{
                    flexDirection: "row",
                    margin: 10,
                    backgroundColor: colors.white,
                    borderRadius: 10,
                  }}
                >
                  <Image
                    style={{
                      height: 60,
                      width: 60,
                      alignSelf: "center",
                      marginLeft: 10,
                    }}
                    source={{ uri: item.image }}
                  />

                  <View
                    style={{
                      flexDirection: "column",
                      justifyContent: "center",
                      marginLeft: 10,
                      flex: 1,
                      margin: 10,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: metrics.text_description,
                        color: colors.white,
                        backgroundColor: colors.app_blue,
                        textAlign: "center",
                        borderRadius: 20,
                        width: 80,
                        height: 30,
                        textAlignVertical: "center",
                      }}
                    >
                      {item.condition}
                    </Text>
                    <Text
                      style={{
                        fontSize: metrics.text_normal,
                        color: colors.black,
                        marginTop: 7,
                      }}
                    >
                      {item.title}
                    </Text>
                    <Text
                      style={{
                        fontSize: metrics.text_normal,
                        color: colors.app_red,
                        marginTop: 5,
                      }}
                    >
                      {item.price}
                      <Text
                        style={{
                          fontSize: metrics.text_normal,
                          color: colors.app_gray,
                        }}
                      >
                        {" x 1"}
                      </Text>
                    </Text>
                  </View>

                  <View
                    style={{
                      justifyContent: "center",
                      alignSelf: "center",
                      marginRight: 20,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({ fromaddcart: true }, () =>
                          this.addtoCart(item)
                        )
                      }
                    >
                      <Image
                        style={{
                          height: 20,
                          width: 20,
                          resizeMode: "contain",
                          alignSelf: "center",
                        }}
                        source={require("../../Images/supermarket.png")}
                      />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.OnDeleteItems(item)}>
                      <Image
                        style={{
                          height: 20,
                          width: 20,
                          resizeMode: "contain",
                          alignSelf: "center",
                          marginTop: 30,
                        }}
                        source={require("../../Images/delete.png")}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              )}
            />
          )}

          {this.state.data.length == 0 && this.state.spinvisible == false && (
            <View>
              <Image
                source={require("../../Images/no_wish.png")}
                style={{
                  height: 200,
                  width: 300,
                  resizeMode: "contain",
                  alignSelf: "center",
                  marginTop: 180,
                }}
              />
              <Text
                style={{
                  fontSize: metrics.text_heading,
                  color: colors.black,
                  fontWeight: "bold",
                  marginTop: 2,
                  textAlign: "center",
                }}
              >
                Your Wishlist is Empty!
              </Text>
              <Text
                style={{
                  fontSize: metrics.text_normal,
                  color: colors.app_gray,
                  marginTop: 2,
                  textAlign: "center",
                }}
              >
                Explore More and shortlist more items!
              </Text>
            </View>
          )}
        </View>
        {this.state.data.length == 0 && this.state.spinvisible == false && (
          <TouchableOpacity
            style={{
              position: "absolute",
              bottom: 10,
              height: 40,
              backgroundColor: colors.theme_caribpay,
              borderRadius: 20,
              width: "90%",
              alignSelf: "center",
              justifyContent: "center",
            }}
            onPress={() => this.props.navigation.navigate("CaribMallHome")}
          >
            <View>
              <Text
                style={{
                  fontSize: metrics.text_heading,
                  color: colors.white,
                  fontWeight: "bold",
                  textAlignVertical: "center",
                  textAlign: "center",
                }}
              >
                Start Shopping
              </Text>
            </View>
          </TouchableOpacity>
        )}

        <Modal
          style={{ borderRadius: metrics.dimen_10 }}
          isVisible={this.state.spinvisible}
        >
          <View
            style={{
              width: "50%",
              alignSelf: "center",
              justifyContent: "center",
              backgroundColor: colors.white,
              borderRadius: metrics.dimen_5,
              margin: metrics.dimen_20,
              height: 100,
            }}
          >
            <Spinner
              style={{ alignSelf: "center" }}
              isVisible={this.state.spinvisible}
              size={70}
              type={"ThreeBounce"}
              color={colors.black}
            />
          </View>
        </Modal>

        <DropdownAlert ref={(ref) => (this.dropDownAlertRef = ref)} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  horizontalLine: {
    borderBottomColor: "#f2f2f2",
    borderBottomWidth: 8,
    width: "100%",
    alignSelf: "center",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.app_light_yellow_color,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
  },
  curveview: {
    height: metrics.newView.curveview + 100,
    position: "absolute",
    top: metrics.newView.curvetop - 110,
    width: "100%",
    backgroundColor: colors.light_grey_backgroud,
    borderRadius: 40,
  },
});
