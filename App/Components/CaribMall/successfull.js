import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import { Modal } from 'react-native-modal';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import WebView from 'react-native-webview';
import colors from '../../Themes/Colors.js';
import metrics from '../../Themes/Metrics.js';
import Spinner from 'react-native-spinkit';

const successfull = ({navigation}) => {
    console.log(navigation);
  const {orderNO} = navigation.state.params;

    return (
          <View style={{ width: "80%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 170 }}>
              {/* <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinOrderlacedvisible} size={70} type={"ThreeBounce"} color={colors.black} /> */}
              {/* {!this.state.spinOrderlacedvisible && */}
                  <View style={{ margin: 10 }}>
                      <Image source={require("../../Images/orderplaced.png")}
                          style={{ height: metrics.dimen_60, width: metrics.dimen_60, resizeMode: 'center', alignSelf: 'center' }}></Image>
                      <Text style={{
                          alignSelf: 'center', textAlign: 'center', marginTop: 15
                      }}>{"Your order " + orderNO + " has been placed!"}</Text>
                      <TouchableOpacity onPress={() => navigation.navigate("CaribMallHome")}>
                          <View style={{ width: 200, backgroundColor: colors.theme_caribpay, height: 40, borderRadius: 10, alignSelf: 'center', justifyContent: 'center', marginTop: 15 }}>
                              <Text style={{ alignSelf: 'center', textAlign: 'center', color: colors.white }}>{"Continue Shopping"}</Text>
                          </View>
                      </TouchableOpacity>
                  </View>
                  {/* } */}
          </View>
      );
}

export default successfull;