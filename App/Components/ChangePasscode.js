import React, { Component } from 'react';
import {
    SafeAreaView, StyleSheet, Text, View, BackHandler, Image, TouchableOpacity, Dimensions
} from 'react-native';
import metrics from '../Themes/Metrics';
import colors from '../Themes/Colors';
import AsyncStorage from '@react-native-community/async-storage';
import { Input } from 'react-native-elements';
import Toast, { DURATION } from 'react-native-easy-toast';
import ProgressDialog from '../ProgressDialog.js';
import Modal from 'react-native-modal';
const axios = require('axios');
import DeviceInfo from 'react-native-device-info';
var Spinner = require('react-native-spinkit');
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';





export default class TermsNConditions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            passcode: '',
            newPasscode: '',
            oldPasscode: '',
            confirmPasscode: '',
            modalvisible: false,
            oldPasscodenotVerify: false,
            passwordmismatches: false,
            deviceID: '',
            visible: false,
            enabled: false,
            token: ''

        }
    };

    async componentDidMount() {
        let uniqueId = DeviceInfo.getUniqueId();
        this.setState({
            deviceID: uniqueId
        });

        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let ispasscode = await AsyncStorage.getItem('passcode')
        ispasscode = JSON.parse(ispasscode)
        console.log("ispascode --- > ", ispasscode)
        this.setState({
            passcode: ispasscode
        })

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }


    OnChangePasscode = async () => {
        if (this.state.oldPasscode == "") {
            this.refs.toast.show("Please enter your current Passcode");
        }
        else if (this.state.newPasscode == "") {
            this.refs.toast.show("Please enter your New Passcode");
        }
        else if (this.state.confirmPasscode == "") {
            this.refs.toast.show("Please confirm your New Passcode");
        }
        else if (this.state.newPasscode != this.state.confirmPasscode) {
            this.setState({
                passwordmismatches: true
            })
        }
        else if (this.state.oldPasscode != this.state.passcode.passcode) {
            this.setState({
                oldPasscodenotVerify: true
            })
        }
        else {
            let user_id = await AsyncStorage.getItem('id');
            let user_token = await AsyncStorage.getItem('token');
            this.setState({ visible: true, token: user_token })
            let data = {
                device_id: this.state.deviceID,
                passcode: this.state.newPasscode,
                user_id: user_id
            }
            console.log('test: ', data, user_token)
            axios({
                method: 'post',
                url: 'https://sandbox.caribpayintl.com/api/set-passcode',
                headers: { 'Authorization': user_token },
                data: data,
            }).then((response) => {
                if (response.data.success.status == 200) {
                    AsyncStorage.setItem("DeviceId", this.state.deviceID)
                    AsyncStorage.setItem("passcode", JSON.stringify({ user_id: user_id, passcode: this.state.newPasscode }))
                    this.setState({
                        visible: false,
                        enabled: true
                    })
                }
            }).catch((err) => {
                this.setState({ visible: false })
                alert(err)
            });
        }
    }

    render() {

        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    <Text style={styles.headertextStyle}>{"Change Passcode"}</Text>
                </View>

                <View style={{ flex: 1 }}>
                    <ProgressDialog visible={this.state.visible} />
                    <View style={{ margin: metrics.dimen_20 }}>
                    <Text style={{fontFamily:metrics.quicksand_bold, fontSize:metrics.text_normal,marginLeft:15}}>Old Passcode</Text>
                    <View style={{marginLeft:10,marginTop:10,marginBottom:10,alignSelf:'center'}}>
                            <SmoothPinCodeInput
                                placeholder=""
                                cellSize={55}
                                codeLength={4}
                                cellSpacing={4}
                                keyboardType="phone-pad"
                                returnKeyLabel="Done"
                                returnKeyType="done"
                                cellStyle={{
                                    borderWidth: 2,
                                    borderRadius: 7,
                                    borderColor: colors.theme_caribpay,
                                    backgroundColor: colors.light_grey_backgroud,
                                }}
                                cellStyleFocused={{
                                    borderColor: 'lightseagreen',
                                    backgroundColor: 'lightcyan',
                                }}
                                textStyle={{
                                    fontSize: 24,
                                    color: colors.black,
                                    fontWeight: 'bold',
                                }}
                                textStyleFocused={{
                                    color: 'crimson',
                                }}
                                value={this.state.oldPasscode}
                                // onFulfill={() => setTimeout(() => { this.onSetPasscodeClick() }, 500)}
                                onTextChange={code => this.setState({ oldPasscode })}
                            />
                        </View>


                        <Text style={{fontFamily:metrics.quicksand_bold, fontSize:metrics.text_normal}}>New Passcode</Text>
                        <View style={{marginLeft:10,marginTop:10,marginBottom:10,alignSelf:'center'}}>
                            <SmoothPinCodeInput
                                placeholder=""
                                cellSize={55}
                                codeLength={4}
                                cellSpacing={4}
                                keyboardType="phone-pad"
                                returnKeyLabel="Done"
                                returnKeyType="done"
                                cellStyle={{
                                    borderWidth: 2,
                                    borderRadius: 7,
                                    borderColor: colors.theme_caribpay,
                                    backgroundColor: colors.light_grey_backgroud,
                                }}
                                cellStyleFocused={{
                                    borderColor: 'lightseagreen',
                                    backgroundColor: 'lightcyan',
                                }}
                                textStyle={{
                                    fontSize: 24,
                                    color: colors.black,
                                    fontWeight: 'bold',
                                }}
                                textStyleFocused={{
                                    color: 'crimson',
                                }}
                                value={this.state.oldPasscode}
                                // onFulfill={() => setTimeout(() => { this.onSetPasscodeClick() }, 500)}
                                onTextChange={code => this.setState({ oldPasscode })}
                            />
                        </View>


                        <Text style={{fontFamily:metrics.quicksand_bold, fontSize:metrics.text_normal}}>Confirm Passcode</Text>
                    <View style={{marginLeft:10,marginTop:10,marginBottom:10, alignSelf:'center'}}>
                            <SmoothPinCodeInput
                                placeholder=""
                                cellSize={55}
                                codeLength={4}
                                cellSpacing={4}
                                keyboardType="phone-pad"
                                returnKeyLabel="Done"
                                returnKeyType="done"
                                cellStyle={{
                                    borderWidth: 2,
                                    borderRadius: 7,
                                    borderColor: colors.theme_caribpay,
                                    backgroundColor: colors.light_grey_backgroud,
                                }}
                                cellStyleFocused={{
                                    borderColor: 'lightseagreen',
                                    backgroundColor: 'lightcyan',
                                }}
                                textStyle={{
                                    fontSize: 24,
                                    color: colors.black,
                                    fontWeight: 'bold',
                                }}
                                textStyleFocused={{
                                    color: 'crimson',
                                }}
                                value={this.state.confirmPasscode}
                                // onFulfill={() => setTimeout(() => { this.onSetPasscodeClick() }, 500)}
                                onTextChange={code => this.setState({ confirmPasscode })}
                            />
                        </View>


                        {/* <Input
                            containerStyle={{ width: '100%', alignSelf: 'center', height: 50, backgroundColor: colors.sub_cat_menu, marginTop: 5 }}
                            keyboardType='number-pad'
                            placeholder='New Passcode'
                            maxLength={4}
                            placeholderTextColor={colors.place_holder_color}
                            inputContainerStyle={{ borderBottomWidth: 0.2, marginBottom: 0 }}
                            inputStyle={{ color: colors.white, fontSize: metrics.text_header, fontWeight: 'bold' }}
                            value={this.state.newPasscode}
                            onChangeText={(text) => this.setState({ newPasscode: text })} /> */}


                        {/* <Text style={{ fontSize: metrics.text_header, color: colors.black, marginTop: 15 }}>Confirm Passcode</Text> */}


                        {/* <Input
                            containerStyle={{ width: '100%', alignSelf: 'center', height: 50, backgroundColor: colors.sub_cat_menu, marginTop: 5 }}
                            keyboardType='number-pad'
                            placeholder='Confirm Passcode'
                            maxLength={4}
                            placeholderTextColor={colors.place_holder_color}
                            inputContainerStyle={{ borderBottomWidth: 0.2 }}
                            inputStyle={{ color: colors.white, fontSize: metrics.text_header, fontWeight: 'bold' }}
                            value={this.state.confirmPasscode}
                            onChangeText={(text) => this.setState({ confirmPasscode: text })} /> */}

                    </View>

                    <View style={{ ...styles.bottomview, backgroundColor: colors.theme_caribpay }}>
                        <TouchableOpacity onPress={() => this.OnChangePasscode()}>
                            <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>{"Change Passcode "}</Text>
                        </TouchableOpacity>
                    </View>

                </View>

                <Toast
                    ref="toast"
                    style={{ backgroundColor: 'black' }}
                    position='center'
                    positionValue={200}
                    fadeInDuration={200}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: 'white' }} />



                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.oldPasscodenotVerify} >
                        <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                            <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                <Text style={{ color: colors.white, fontSize: metrics.text_header, textAlign: 'center', textAlignVertical: 'center' }}>{"CaribPay"}</Text>
                            </View>
                            <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"Old Passcode is Wrong , Try Again!"}</Text>
                                <TouchableOpacity onPress={() => this.setState({
                                    oldPasscodenotVerify: false
                                })}>
                                    <View style={{ height: metrics.dimen_30, width: 60, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                        <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"OK"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>

                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.passwordmismatches} >
                        <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                            <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                <Text style={{ color: colors.white, fontSize: metrics.text_header, textAlign: 'center', textAlignVertical: 'center' }}>{"CaribPay"}</Text>
                            </View>
                            <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"New Passcode Mismatchs , Please Try Again!"}</Text>
                                <TouchableOpacity onPress={() => this.setState({
                                    passwordmismatches: false
                                })}>
                                    <View style={{ height: metrics.dimen_30, width: 60, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                        <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"OK"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>




                <View>
                    <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.enabled}>
                        <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 220, justifyContent: 'center' }}>
                            <View style={{ flex: 1 }}>
                                <View>
                                    <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                                        source={require("../Images/successfully.png")}></Image>
                                    <Text style={{ fontSize: metrics.text_heading, color: colors.black, fontWeight: 'bold', textAlign: 'center', margin: 15 }}> {'Passcode Changed Successfully!'}</Text>
                                </View>
                                <View style={styles.bottomview2}>
                                    <TouchableOpacity onPress={() => this.setState({ enabled: false }, () => this.props.navigation.navigate('Settings', { token: this.state.token }))}>
                                        <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>{"OK "}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>



            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    bottomview: { bottom: 10, position: 'absolute', height: 50, backgroundColor: colors.app_light_yellow_color, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: 25 },
    header: {
        height: metrics.dimen_60,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_15,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        // marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        // marginBottom: metrics.dimen_15,
        paddingHorizontal: metrics.dimen_20,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', textAlign: 'center'
    },
    boxContainer: { flexDirection: 'row', justifyContent: 'space-between', backgroundColor: "#f2f2f2", borderRadius: metrics.dimen_5, height: metrics.dimen_50, alignSelf: 'center', width: '90%' },
    bottomview2: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },
});
