import * as React from "react";
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  BackHandler,
  Dimensions,
} from "react-native";
import colors from "../Themes/Colors";
import metrics from "../Themes/Metrics";
import Modal from "react-native-modal";
import RBSheet from "react-native-raw-bottom-sheet";
import RadioButton from "react-native-radio-button";
import StepIndicator from "react-native-step-indicator";
import ScratchView from "react-native-scratch";

const labels = ["", "", ""];
const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: "#fe7013",
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: "#fe7013",
  stepStrokeUnFinishedColor: "#aaaaaa",
  separatorFinishedColor: "#fe7013",
  separatorUnFinishedColor: "#aaaaaa",
  stepIndicatorFinishedColor: "#fe7013",
  stepIndicatorUnFinishedColor: "#ffffff",
  stepIndicatorCurrentColor: "#ffffff",
  stepIndicatorLabelFontSize: 14,
  currentStepIndicatorLabelFontSize: 14,
  stepIndicatorLabelCurrentColor: "#fe7013",
  stepIndicatorLabelFinishedColor: "#ffffff",
  stepIndicatorLabelUnFinishedColor: "#aaaaaa",
  labelColor: colors.app_gray,
  labelSize: 14,
  currentStepLabelColor: colors.theme_orange_color_caribpay,
};

export default class CouponsDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.navigation.state.params.data,
      brandName: this.props.navigation.state.params.brandName,
      currentPosition: 3,
    };
  }

  componentDidMount() {
    console.log(this.state.data);
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  onImageLoadFinished = ({ id, success }) => {
    // Do something
  };

  onScratchProgressChanged = ({ value, id }) => {
    // Do domething like showing the progress to the user
  };

  onScratchDone = ({ isScratchDone, id }) => {
    // Do something
  };

  onScratchTouchStateChanged = ({ id, touchState }) => {
    // Example: change a state value to stop a containing
    // FlatList from scrolling while scratching
    this.setState({ scrollEnabled: !touchState });
  };

  renderTncs = () => {
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            margin: 10,
          }}
        >
          <Text
            style={{
              fontFamily: metrics.quicksand_bold,
              fontSize: metrics.text_16,
            }}
          >
            {"Terms & Conditions"}
          </Text>

          <TouchableOpacity onPress={() => this.RBSheetAlertTnc.close()}>
            <Text
              style={{
                fontFamily: metrics.quicksand_bold,
                color: colors.app_orange,
              }}
            >
              {"Cancel"}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ margin: 5 }}>
          <Text style={styles.textStyle}>
            1. User get 5% Cashback upto $200 on shopping from CaribPay Mall
          </Text>
          <Text style={styles.textStyle}>
            2. Minimum transaction value is $50
          </Text>
          <Text style={styles.textStyle}>
            3. Voucher is valid for one time usage per user
          </Text>
          <Text style={styles.textStyle}>
            4. Cashback will be credited within 24 hours
          </Text>
          <Text style={styles.textStyle}>
            5. Voucher is valid for selected products
          </Text>
          <Text style={styles.textStyle}>6. Voucher is valid for 30 Days</Text>
          <Text style={styles.textStyle}>
            7. Voucher is applicable on only CaribPay App
          </Text>
          <Text style={styles.textStyle}>
            8. Cancelled order will not be eligible for Cashback
          </Text>
        </View>
      </View>
    );
  };

  renderPaymentOptions = () => {
    return (
      <View style={{ margin: 10 }}>
        <Text style={{ margin: 10, fontFamily: metrics.quicksand_bold }}>
          Select Payment Method
        </Text>
        <View style={{ flexDirection: "row", margin: 20 }}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            <RadioButton
              size={metrics.dimen_12}
              animation={"bounceIn"}
              isSelected={this.state.fromCaribPay}
              innerColor={colors.theme_caribpay}
              outerColor={colors.theme_caribpay}
              onPress={() =>
                this.setState({
                  fromCaribPay: true,
                  fromCards: false,
                })
              }
            />
            <Text
              style={{
                fontSize: metrics.text_14,
                fontFamily: metrics.quicksand_semibold,
                color: colors.black,
                marginLeft: metrics.dimen_10,
              }}
            >
              CaribPay Wallet
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: "row", marginLeft: 20 }}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            <RadioButton
              size={metrics.dimen_12}
              animation={"bounceIn"}
              isSelected={this.state.fromCards}
              innerColor={colors.theme_caribpay}
              outerColor={colors.theme_caribpay}
              onPress={() =>
                this.setState({
                  fromCaribPay: false,
                  fromCards: true,
                })
              }
            />
            <Text
              style={{
                fontSize: metrics.text_14,
                fontFamily: metrics.quicksand_semibold,
                color: colors.black,
                marginLeft: metrics.dimen_10,
              }}
            >
              Credit / Debit cards
            </Text>
          </View>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.scene}>
        <ScrollView>
          <View
            style={{
              width: "100%",
              alignSelf: "center",
            }}
          >
            <Image
              style={{
                width: "100%",
                height: metrics.dimen_200,
              }}
              source={{ uri: this.state.data.image }}
            />

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                position: "absolute",
                top: 10,
              }}
            >
              <TouchableOpacity
                onPress={() => this.props.navigation.goBack(null)}
              >
                <Image
                  style={{
                    height: 24,
                    width: 24,
                    resizeMode: "center",
                    alignSelf: "center",
                    marginLeft: 10,
                    tintColor: colors.app_gray,
                  }}
                  source={require("../Images/leftarrow.png")}
                />
              </TouchableOpacity>

              <Image
                style={{
                  height: 22,
                  width: 22,
                  resizeMode: "center",
                  alignSelf: "center",
                  marginLeft: Dimensions.get("screen").width - 70,
                  tintColor: colors.app_gray,
                }}
                source={require("../Images/share.png")}
              />
            </View>
          </View>
          <Image
            style={{
              width: 60,
              height: 60,
              borderRadius: 10,
              borderColor: colors.app_gray,
              borderWidth: 1.5,
              position: "absolute",
              top: 150,
              left: 15,
            }}
            source={{ uri: this.state.data.image }}
          />
          <View style={{ marginTop: 20 }}>
            <Text
              style={{
                marginTop: 25,
                marginLeft: 10,
                fontSize: 20,
                fontFamily: metrics.quicksand_bold,
              }}
            >
              {this.state.brandName + " Fashion"}
            </Text>

            <Text
              style={{
                marginLeft: 10,
                fontSize: metrics.text_17,
                fontFamily: metrics.quicksand_bold,
                color: colors.app_gray,
                marginTop: 7,
              }}
            >
              {this.state.brandName + " Fashion Voucher"}
            </Text>

            <Text
              style={{
                marginLeft: 10,
                fontSize: 20,
                marginTop: 5,
                fontFamily: metrics.quicksand_bold,
                // color: colors.app_gray,
              }}
            >
              {this.state.data.vouceherwrth}
            </Text>

            <Text
              style={{
                marginLeft: 10,
                fontSize: metrics.text_normal,
                marginTop: 2,
                fontFamily: metrics.quicksand_bold,
                // color: "#696969",
              }}
            >
              {"Inclusive of all taxes"}
            </Text>

            <Text
              style={{
                marginLeft: 10,
                fontSize: metrics.text_normal,
                marginTop: 15,
                fontFamily: metrics.quicksand_bold,
                color: colors.app_gray,
              }}
            >
              {"NOTE : Non-returnable / non - replaceable"}
            </Text>

            <View
              style={{
                borderBottomColor: colors.light_grey_backgroud,
                borderBottomWidth: 1.5,
                width: "95%",
                alignSelf: "center",
                marginTop: 20,
              }}
            />

            <Text
              style={{
                marginLeft: 10,
                fontSize: metrics.text_17,
                marginTop: 15,
                fontFamily: metrics.quicksand_bold,
              }}
            >
              {"Available Offers"}
            </Text>

            <Text
              style={{
                marginLeft: 10,
                fontSize: metrics.text_medium,
                marginTop: 2,
                fontFamily: metrics.quicksand_bold,
                color: colors.app_gray,
              }}
            >
              {"Offer Applied!"}
            </Text>

            <View
              style={{
                flexDirection: "row",
                marginTop: 10,
                marginLeft: 10,
              }}
            >
              <View
                style={{
                  backgroundColor: colors.app_blue,
                  height: 20,
                  width: 20,
                  borderRadius: 10,
                  marginTop: 4,
                }}
              />
              <View>
                <Text
                  style={{
                    marginLeft: 10,
                    fontSize: metrics.text_normal,
                    marginTop: -2,
                    fontFamily: metrics.quicksand_bold,
                    color: colors.theme_orange_color_caribpay,
                  }}
                >
                  {"Get Code to Scratch this"}
                </Text>

                <View style={{ marginTop: 10 }}>
                  <View
                    style={{
                      width: 200,
                      height: 40,
                      borderRadius: 10,
                      marginLeft: 10,
                      justifyContent:'center'
                    }}
                  >
                    <Text style={{alignSelf:'center'}}>CODE : 198HROSHS </Text>
                    <ScratchView
                      id={1} // ScratchView id (Optional)
                      brushSize={10} // Default is 10% of the smallest dimension (width/height)
                      threshold={70} // Report full scratch after 70 percentage, change as you see fit. Default is 50
                      fadeOut={false} // Disable the fade out animation when scratch is done. Default is true
                      placeholderColor="#AAAAAA" // Scratch color while image is loading (or while image not present)
                      imageUrl="http://yourUrlToImage.jpg" // A url to your image (Optional)
                      resourceName="your_image" // An image resource name (without the extension like '.png/jpg etc') in the native bundle of the app (drawble for Android, Images.xcassets in iOS) (Optional)
                      resizeMode="cover|contain|stretch" // Resize the image to fit or fill the scratch view. Default is stretch
                      onImageLoadFinished={this.onImageLoadFinished} // Event to indicate that the image has done loading
                      onTouchStateChanged={this.onTouchStateChangedMethod} // Touch event (to stop a containing FlatList for example)
                      onScratchProgressChanged={this.onScratchProgressChanged} // Scratch progress event while scratching
                      onScratchDone={this.onScratchDone} // Scratch is done event
                    />
                  </View>
                </View>

                {/* <Text
                  style={{
                    marginLeft: 10,
                    fontSize: metrics.text_normal,
                    marginTop: -2,
                    fontFamily: metrics.quicksand_bold,
                    color: colors.app_black_text,
                    backgroundColor: colors.light_grey_backgroud,
                    padding: 5,
                    borderRadius: 5,
                    marginTop: 5,
                    width: 220,
                  }}
                >
                  {"Promocode : DEALOFFERS606"}
                </Text> */}

                <Text
                  style={{
                    marginLeft: 10,
                    fontSize: metrics.text_normal,
                    marginTop: 2,
                    fontFamily: metrics.quicksand_bold,
                    color: colors.app_gray,
                  }}
                >
                  {
                    "Use above promocode to get Cashback of $80 and Mall Voucher of $70"
                  }
                </Text>

                <View
                  style={{
                    flexDirection: "row",
                    marginLeft: 10,
                    marginTop: 10,
                  }}
                >
                  <Image
                    style={{
                      height: metrics.dimen_30,
                      width: metrics.dimen_30,
                      resizeMode: "contain",
                      alignSelf: "center",
                    }}
                    source={require("../Images/send1.png")}
                  />

                  <View style={{ marginLeft: 10 }}>
                    <Text
                      style={{
                        marginLeft: 10,
                        fontSize: metrics.text_normal,
                        marginTop: 2,
                        fontFamily: metrics.quicksand_bold,
                        //   color: colors.app_gray,
                      }}
                    >
                      {"CaribPay Cash"}
                    </Text>
                    <Text
                      style={{
                        marginLeft: 10,
                        fontSize: metrics.text_normal,
                        marginTop: 2,
                        fontFamily: metrics.quicksand_bold,
                        color: colors.app_gray,
                      }}
                    >
                      {"worth $80"}
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: "row",
                    marginLeft: 10,
                    marginTop: 10,
                  }}
                >
                  <Image
                    style={{
                      height: metrics.dimen_30,
                      width: metrics.dimen_30,
                      resizeMode: "contain",
                      alignSelf: "center",
                    }}
                    source={require("../Images/shopping.png")}
                  />

                  <View style={{ marginLeft: 10 }}>
                    <Text
                      style={{
                        marginLeft: 10,
                        fontSize: metrics.text_normal,
                        marginTop: 2,
                        fontFamily: metrics.quicksand_bold,
                        //   color: colors.app_gray,
                      }}
                    >
                      {"CaribPay Mall"}
                    </Text>
                    <Text
                      style={{
                        marginLeft: 10,
                        fontSize: metrics.text_normal,
                        marginTop: 2,
                        fontFamily: metrics.quicksand_bold,
                        color: colors.app_gray,
                      }}
                    >
                      {"worth $70"}
                    </Text>
                  </View>
                </View>
                <TouchableOpacity onPress={() => this.RBSheetAlertTnc.open()}>
                  <Text
                    style={{
                      marginLeft: 10,
                      fontSize: metrics.text_normal,
                      marginTop: 2,
                      fontFamily: metrics.quicksand_bold,
                      color: colors.app_gray,
                      marginTop: 10,
                      textDecorationLine: "underline",
                    }}
                  >
                    {"Terms & Conditions"}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View
            style={{
              borderBottomColor: colors.light_grey_backgroud,
              borderBottomWidth: 4,
              width: "100%",
              alignSelf: "center",
              marginTop: 20,
            }}
          />

          <View style={{ margin: 10 }}>
            <Text
              style={{
                fontSize: metrics.text_17,
                fontFamily: metrics.quicksand_bold,
                marginBottom: 5,
              }}
            >
              Things to note
            </Text>
            <Text style={styles.textStyle}>
              1. Partial redemption of voucher is not allowed.
            </Text>
            <Text style={styles.textStyle}>2. Valid for 1 year.</Text>
          </View>

          <View
            style={{
              borderBottomColor: colors.light_grey_backgroud,
              borderBottomWidth: 4,
              width: "100%",
              alignSelf: "center",
              marginTop: 20,
            }}
          />

          <View style={{ margin: 10, marginBottom: 80 }}>
            <Text
              style={{
                fontSize: metrics.text_17,
                fontFamily: metrics.quicksand_bold,
                marginBottom: 5,
              }}
            >
              How to Redeem
            </Text>

            <View style={{ height: 200, flexDirection: "row" }}>
              <StepIndicator
                customStyles={customStyles}
                direction={"vertical"}
                stepCount={3}
                currentPosition={this.state.currentPosition}
                labels={labels}
                style={{ marginLeft: -15 }}
              />
              <View
                style={{
                  flexDirection: "column",
                  marginLeft: 30,
                  marginTop: 15,
                }}
              >
                <Text style={styles.textStyle}>
                  Visit the store to redeem the voucher.
                </Text>
                <Text style={{ ...styles.textStyle, marginTop: 30 }}>
                  Open the voucher from MY Coupons section of the CaribPay App.
                </Text>
                <Text style={{ ...styles.textStyle, marginTop: 30 }}>
                  Share the voucher at the cashier at the time of payment.
                </Text>
              </View>
            </View>
          </View>
        </ScrollView>

        <View
          style={{
            position: "absolute",
            bottom: 10,
            left: 20,
            right: 20,
            height: 40,
            backgroundColor: colors.theme_caribpay,
            borderRadius: 20,
            justifyContent: "center",
          }}
        >
          <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
            <Text
              style={{
                fontSize: metrics.text_16,
                fontFamily: metrics.quicksand_bold,
                color: colors.white,
                alignSelf: "center",
              }}
            >
              {"Buy for " + this.state.data.vouceherwrth}
            </Text>
          </TouchableOpacity>
        </View>

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.isComingSoon}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                // margin: metrics.dimen_10,
                flexDirection: "column",
                height: 200,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isComingSoon: false,
                    })
                  }
                >
                  <Image
                    style={{
                      height: metrics.dimen_16,
                      width: metrics.dimen_16,
                      resizeMode: "contain",
                      margin: 15,
                    }}
                    source={require("../Images/cross-sign.png")}
                  />
                </TouchableOpacity>
                <Image
                  style={{
                    height: 150,
                    width: 150,
                    resizeMode: "cover",
                    alignSelf: "center",
                    marginTop: -30,
                  }}
                  source={require("../Images/soon.png")}
                />
                <Text
                  style={{
                    fontSize: metrics.text_heading,
                    color: colors.black,
                    fontFamily: metrics.quicksand_bold,
                    textAlign: "center",
                    marginTop: -20,
                  }}
                >
                  {"Buy Vouchers"}
                </Text>
              </View>
            </View>
          </Modal>
        </View>

        <RBSheet
          ref={(ref) => {
            this.RBSheetAlert = ref;
          }}
          height={200}
          closeOnDragDown={true}
          closeOnPressMask={false}
          customStyles={{
            wrapper: {
              backgroundColor: "transparent",
            },
            draggableIcon: {
              backgroundColor: "#000",
            },
            container: {
              borderTopLeftRadius: 30,
              borderTopRightRadius: 30,
            },
          }}
        >
          {this.renderPaymentOptions()}
        </RBSheet>

        <RBSheet
          ref={(ref) => {
            this.RBSheetAlertTnc = ref;
          }}
          height={400}
          closeOnDragDown={true}
          closeOnPressMask={false}
          customStyles={{
            wrapper: {
              backgroundColor: "transparent",
            },
            draggableIcon: {
              backgroundColor: "#000",
            },
            container: {
              borderTopLeftRadius: 30,
              borderTopRightRadius: 30,
            },
          }}
        >
          {this.renderTncs()}
        </RBSheet>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
    backgroundColor: colors.white,
  },
  textStyle: {
    fontFamily: metrics.quicksand_semibold,
    fontSize: metrics.text_normal,
    margin: 5,
    color: colors.app_gray,
  },
});
