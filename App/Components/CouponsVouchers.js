import * as React from "react";
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  BackHandler,
} from "react-native";
import colors from "../Themes/Colors";
import metrics from "../Themes/Metrics";
import { SliderBox } from "react-native-image-slider-box";

let dataFirstBanner = [
  {
    title: "",
    caption: "",
    url:
      "https://images.pexels.com/photos/1639729/pexels-photo-1639729.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  },
];

let dataCategory = [
  {
    title: "Grocery, Home Needs & Furniture",
    caption: "Big Bazar , Croma & more",
    image: require("../Images/supermarket.png"),
  },
  {
    title: "Fashion",
    caption: "Shoppers Stop , Pantaloons & more",
    image: require("../Images/women.png"),
  },
  {
    title: "Jewellery & Fashion Accessories",
    caption: "Kalyan, PCJ & more",
    image: require("../Images/jewels.png"),
  },
  {
    title: "Footwear",
    caption: "Red tape, Red Cheif & more",
    image: require("../Images/shoe.png"),
  },
  {
    title: "Grocery, Home Needs & Furniture",
    caption: "Big Bazar , Croma & more",
    image: require("../Images/supermarket.png"),
  },
  {
    title: "Fashion",
    caption: "Shoppers Stop , Pantaloons & more",
    image: require("../Images/women.png"),
  },
  {
    title: "Jewellery & Fashion Accessories",
    caption: "Kalyan, PCJ & more",
    image: require("../Images/jewels.png"),
  },
  {
    title: "Footwear",
    caption: "Red tape, Red Cheif & more",
    image: require("../Images/shoe.png"),
  },
  {
    title: "Fashion",
    caption: "Shoppers Stop , Pantaloons & more",
    image: require("../Images/women.png"),
  },
];

let dataCategoryUpto40K = [
  {
    title: "Grocery",
    caption: "$120  Cashback",
    image: require("../Images/supermarket.png"),
  },
  {
    title: "Fashion",
    caption: "$200 Cashback",
    image: require("../Images/women.png"),
  },
  {
    title: "Jewellery",
    caption: "20% Cashback",
    image: require("../Images/jewels.png"),
  },
  {
    title: "Footwear",
    caption: "5% Cashback",
    image: require("../Images/shoe.png"),
  },
  {
    title: "Grocery",
    caption: "40% Cashback",
    image: require("../Images/supermarket.png"),
  },
  {
    title: "Fashion",
    caption: "30% Cashback",
    image: require("../Images/women.png"),
  },
  {
    title: "Jewellery",
    caption: "$125 Cashback",
    image: require("../Images/jewels.png"),
  },
  {
    title: "Footwear",
    caption: "20% Cashback",
    image: require("../Images/shoe.png"),
  },
  {
    title: "Fashion",
    caption: "80% Cashback",
    image: require("../Images/women.png"),
  },
];

let fashionSubData = [
  {
    title: "Life Style",
    caption: "$120  Cashback",
    image:
      "https://upload.wikimedia.org/wikipedia/en/1/1c/This_is_the_logo_of_the_Lifestyle_Stores.jpg",
  },
  {
    title: "Pantaloons",
    caption: "$200 Cashback",
    image:
      "https://content.pantaloons.com/uploads/2019/12/11135526/17-06-2019-pantaloons-logo.jpg",
  },
  {
    title: "Shoppers Stop",
    caption: "20% Cashback",
    image:
      "https://i0.wp.com/www.indiaretailing.com/wp-content/uploads/2017/04/shoppers-stop-new-1.jpg?fit=681%2C400&ssl=1",
  },
  {
    title: "Unlimited",
    caption: "5% Cashback",
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSoEm3pEcd46DUvWNspKxhdhSSyyeNA44LN2g&usqp=CAU",
  },
  {
    title: "Max Fashions",
    caption: "40% Cashback",
    image:
      "https://www.centralparkjakarta.com/wp-content/uploads/2018/08/MAX-Fashions-3.png",
  },
  {
    title: "Levis",
    caption: "30% Cashback",
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTFkDeyb7yHWMdWd_5pQnCJ2CptGyAzl6DESA&usqp=CAU",
  },
  {
    title: "UCB",
    caption: "$125 Cashback",
    image:
      "https://www.benetton.com/on/demandware.static/Sites-UCB_world-Site/-/default/dwbf622814/favicons/favicon-512x512.png",
  },
  {
    title: "Gucci",
    caption: "20% Cashback",
    image:
      "https://i.pinimg.com/736x/90/ab/92/90ab92f1549a67fb0f3769d90229ff76.jpg",
  },
  {
    title: "Armani",
    caption: "80% Cashback",
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQKm5iWOB690xfB0yt-gahwDr5Jap9iyTmnww&usqp=CAU",
  },
];

let bannerData = [
  "https://static.vecteezy.com/system/resources/previews/000/258/463/non_2x/vector-cash-back-colorful-sale-banner-design.jpg",
  "https://asset20.ckassets.com/resources/image/slider_images/icicicashback_Mobile/mobile-slide2-1603433076.png",
  "https://st3.depositphotos.com/15678184/37749/v/950/depositphotos_377499958-stock-illustration-vector-abstract-banner-concept-online.jpg",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTaqrXaSajeiTRTGJiBx8rWQEsakDcfgUPyMg&usqp=CAU",
];

let justArrviedData = [
  {
    image_url:
      "https://upload.wikimedia.org/wikipedia/en/1/1c/This_is_the_logo_of_the_Lifestyle_Stores.jpg",
    cashback: "Upto to $200\n  Cashback",
  },
  {
    image_url:
      "https://www.centralparkjakarta.com/wp-content/uploads/2018/08/MAX-Fashions-3.png",
    cashback: "Upto to $20\n  Cashback",
  },
  {
    image_url:
      "https://i.pinimg.com/736x/90/ab/92/90ab92f1549a67fb0f3769d90229ff76.jpg",
    cashback: "Upto to $100\n Cashback",
  },
  {
    image_url:
      "https://www.benetton.com/on/demandware.static/Sites-UCB_world-Site/-/default/dwbf622814/favicons/favicon-512x512.png",
    cashback: "Upto to $200\n  Cashback",
  },
  {
    image_url:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTFkDeyb7yHWMdWd_5pQnCJ2CptGyAzl6DESA&usqp=CAU",
    cashback: "Upto to $20\n  Cashback",
  },
  {
    image_url:
      "https://content.pantaloons.com/uploads/2019/12/11135526/17-06-2019-pantaloons-logo.jpg",
    cashback: "Upto to $100\n  Cashback",
  },
];

export default class CouponsVouchers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isCategory: false,
      isCategorySub: false,
      isItemClicked: false,
      categoryData: [],
    };
  }

  componentDidMount() {
    let data = dataCategory.map((item, index) => {
      item.idNum = ++index;
      return { ...item };
    });

    this.setState({
      categoryData: data,
    });

    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  handleSelection = (item, idNum) => {
    var selectedid = this.state.selectedID;
    if (selectedid === idNum) {
      this.setState({
        selectedID: null,
      });
    } else {
      this.setState({
        selectedID: idNum,
      });
    }
  };

  render() {
    return (
      <View style={styles.scene}>
        <ScrollView>
          <View
            style={{
              height: metrics.dimen_55,
              backgroundColor: colors.theme_caribpay,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity
                style={{
                  marginLeft: 10,
                  height: metrics.dimen_24,
                  width: metrics.dimen_24,
                  resizeMode: "contain",
                  alignSelf: "center",
                }}
                onPress={() => this.props.navigation.goBack(null)}
              >
                <Image
                  style={{
                    height: metrics.dimen_24,
                    width: metrics.dimen_24,
                    resizeMode: "contain",
                    alignSelf: "center",
                  }}
                  source={require("../Images/leftarrow.png")}
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: metrics.text_17,
                  fontFamily: metrics.quicksand_semibold,
                  color: colors.white,
                  marginLeft: 10,
                  alignSelf: "center",
                  marginTop: -5,
                }}
              >
                {"eCoupons & eVouchers"}
              </Text>
            </View>

            <View style={{ flexDirection: "row", marginRight: 15 }}>
              <Image
                style={{
                  marginLeft: 10,
                  height: metrics.dimen_28,
                  width: metrics.dimen_28,
                  resizeMode: "contain",
                  alignSelf: "center",
                  tintColor: colors.white,
                }}
                source={require("../Images/find.png")}
              />
              <Image
                style={{
                  marginLeft: 15,
                  height: metrics.dimen_22,
                  width: metrics.dimen_22,
                  resizeMode: "contain",
                  alignSelf: "center",
                  tintColor: colors.white,
                }}
                source={require("../Images/heart.png")}
              />
              <Image
                style={{
                  marginLeft: 15,
                  height: metrics.dimen_24,
                  width: metrics.dimen_24,
                  resizeMode: "contain",
                  alignSelf: "center",
                  tintColor: colors.white,
                }}
                source={require("../Images/supermarket.png")}
              />
            </View>
          </View>

          <View
            style={{
              width: "95%",
              alignSelf: "center",
              borderRadius: 10,
              marginTop: 10,
            }}
          >
            <FlatList
              data={dataFirstBanner}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <Image
                  style={{
                    width: "100%",
                    height: metrics.dimen_140,
                    borderRadius: 10,
                  }}
                  source={{ uri: item.url }}
                />
              )}
            />

            {/* <View style={{marginLeft}}> */}
            <SliderBox
              images={bannerData}
              sliderBoxHeight={160}
              onCurrentImagePressed={(index) =>
                console.warn(`image ${index} pressed`)
              }
              inactiveDotColor="#90A4AE"
              paginationBoxVerticalPadding={20}
              autoplay
              circleLoop
              resizeMethod={"resize"}
              resizeMode={"cover"}
              dotStyle={{
                width: 10,
                height: 10,
                borderRadius: 5,
                marginHorizontal: 0,
                padding: 0,
                margin: 0,
                backgroundColor: "rgba(128, 128, 128, 0.92)",
              }}
              ImageComponentStyle={{
                borderRadius: 10,
                marginTop: 10,
                width: "95%",
                marginLeft: -20,
              }}
              imageLoadingColor={colors.light_grey_backgroud}
            />
          </View>
          {/* </View> */}

          <View
            style={{
              flexDirection: "row",
              backgroundColor: colors.app_lightBlue,
              marginTop: 10,
              justifyContent: "space-between",
              height: 50,
            }}
          >
            <View style={{ flexDirection: "row", margin: 10 }}>
              <Image
                style={{
                  height: metrics.dimen_22,
                  width: metrics.dimen_22,
                  resizeMode: "contain",
                  alignSelf: "center",
                  // marginLeft:7
                }}
                source={require("../Images/ham.png")}
              />
              <Text
                style={{
                  fontSize: metrics.text_17,
                  fontFamily: metrics.quicksand_bold,
                  color: colors.app_black_text,
                  marginLeft: 10,
                  alignSelf: "center",
                  marginTop: -5,
                }}
              >
                Shop By Category
              </Text>
            </View>

            <View style={{ margin: 10 }}>
              <TouchableOpacity
                onPress={() =>
                  this.setState({ isCategory: !this.state.isCategory })
                }
              >
                <Image
                  style={{
                    height: metrics.dimen_16,
                    width: metrics.dimen_16,
                    marginTop: 5,
                    resizeMode: "contain",
                    alignSelf: "center",
                    tintColor: colors.app_gray,
                    marginRight: 10,
                  }}
                  source={
                    !this.state.isCategory
                      ? require("../Images/downbtn.png")
                      : require("../Images/upbtn.png")
                  }
                />
              </TouchableOpacity>
            </View>
          </View>

          {this.state.isCategory && (
            <View style={{ marginBottom: 20 }}>
              <FlatList
                data={this.state.categoryData}
                keyExtractor={(item, index) => index.toString()}
                extraData={this.state.selectedID}
                ItemSeparatorComponent={() => (
                  <View
                    style={{
                      borderBottomWidth: 0.5,
                      borderBottomColor: colors.app_gray,
                      marginTop: 15,
                      marginBottom: 15,
                    }}
                  />
                )}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    onPress={() => this.handleSelection(item, item.idNum)}
                  >
                    <View>
                      <View
                        style={{
                          flexDirection: "row",
                          margin: 10,
                          justifyContent: "space-between",
                        }}
                      >
                        <View style={{ flexDirection: "row" }}>
                          <Image
                            style={{
                              width: metrics.dimen_40,
                              height: metrics.dimen_40,
                              margin: 10,
                              //   tintColor: colors.theme_caribpay,
                            }}
                            source={item.image}
                          />

                          <View style={{ marginLeft: 10 }}>
                            <Text
                              style={{
                                fontSize: metrics.text_normal,
                                fontFamily: metrics.quicksand_bold,
                                marginTop: 5,
                              }}
                            >
                              {item.title}
                            </Text>

                            <Text
                              style={{
                                fontSize: metrics.text_normal,
                                fontFamily: metrics.quicksand_medium,
                                color: colors.app_gray,
                              }}
                            >
                              {item.caption}
                            </Text>
                          </View>
                        </View>

                        <View style={{ margin: 10 }}>
                          <TouchableOpacity
                            onPress={() =>
                              this.handleSelection(item, item.idNum)
                            }
                          >
                            <Image
                              style={{
                                height: metrics.dimen_15,
                                width: metrics.dimen_15,
                                resizeMode: "contain",
                                alignSelf: "center",
                                tintColor: colors.app_gray,
                                // marginLeft:20
                              }}
                              source={
                                item.idNum == this.state.selectedID
                                  ? require("../Images/upbtn.png")
                                  : require("../Images/downbtn.png")
                              }
                            />
                          </TouchableOpacity>
                        </View>
                      </View>

                      {item.idNum == this.state.selectedID && (
                        <FlatList
                          data={fashionSubData}
                          keyExtractor={(item, index) => index.toString()}
                          numColumns={3}
                          renderItem={({ item }) => (
                            <TouchableOpacity
                              style={{ margin: 10, flex: 1 / 3 }}
                              onPress={() =>
                                this.props.navigation.navigate("ListCoupons", {
                                  name: item.title,
                                  image: item.image,
                                })
                              }
                            >
                              <View style={{ margin: 10, flex: 1 / 3 }}>
                                <Image
                                  style={{
                                    height: 60,
                                    width: 60,
                                    alignSelf: "center",
                                    //   borderRadius: 30,
                                    resizeMode: "contain",
                                  }}
                                  source={{ uri: item.image }}
                                />
                                <Text
                                  style={{
                                    fontSize: metrics.text_medium,
                                    fontFamily: metrics.quicksand_bold,
                                    color: colors.app_gray,
                                    alignSelf: "center",
                                  }}
                                >
                                  {item.title}
                                </Text>
                                <Text
                                  style={{
                                    fontSize: metrics.text_medium,
                                    fontFamily: metrics.quicksand_bold,
                                    color: colors.theme_orange_color_caribpay,
                                    alignSelf: "center",
                                  }}
                                >
                                  {item.caption}
                                </Text>
                              </View>
                            </TouchableOpacity>
                          )}
                        />
                      )}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          )}

          <View>
            <View>
             {this.state.isCategory && <View
                style={{
                  borderBottomColor: colors.light_grey_backgroud,
                  borderBottomWidth: 4,
                  width: "100%",
                  alignSelf: "center",
                  marginTop: 20,
                }}
              />}

              <Text
                style={{
                  margin: 10,
                  fontSize: metrics.text_17,
                  fontFamily: metrics.quicksand_bold,
                }}
              >
                {"Just Arrived Brands >> "}
              </Text>
            </View>

            <FlatList
              data={justArrviedData}
              keyExtractor={(item, index) => index.toString()}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item }) => (
                <View style={{ margin: 10 }}>
                  <Image
                    style={{
                      height: 150,
                      width: 150,
                      alignSelf: "center",
                      borderRadius: 7,
                      borderColor:colors.light_grey_backgroud,
                      borderWidth:1
                    }}
                    source={{ uri: item.image_url }}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      fontFamily: metrics.quicksand_bold,
                      color: colors.app_gray,
                      alignSelf: "center",
                    }}
                  >
                    {item.cashback}
                  </Text>
                </View>
              )}
            />
          </View>

          <View
            style={{
              borderBottomColor: colors.light_grey_backgroud,
              borderBottomWidth: 4,
              width: "100%",
              alignSelf: "center",
              marginTop: 20,
            }}
          />

          <View style={{ marginBottom: 20 }}>
            <Text
              style={{
                margin: 10,
                fontSize: metrics.text_17,
                fontFamily: metrics.quicksand_bold,
              }}
            >
              {"Upto $4,000 Cashback"}
            </Text>

            <FlatList
              data={fashionSubData}
              keyExtractor={(item, index) => index.toString()}
              numColumns={3}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={{ margin: 10, flex: 1 / 3 }}
                  onPress={() =>
                    this.props.navigation.navigate("ListCoupons", {
                      name: item.title,
                      image: item.image,
                    })
                  }
                >
                  <View style={{ margin: 10, flex: 1 / 3 }}>
                    <Image
                      style={{
                        height: 60,
                        width: 60,
                        alignSelf: "center",
                        //   borderRadius: 30,
                        resizeMode: "contain",
                      }}
                      source={{ uri: item.image }}
                    />
                    <Text
                      style={{
                        fontSize: metrics.text_medium,
                        fontFamily: metrics.quicksand_bold,
                        color: colors.app_gray,
                        alignSelf: "center",
                      }}
                    >
                      {item.title}
                    </Text>
                    <Text
                      style={{
                        fontSize: metrics.text_medium,
                        fontFamily: metrics.quicksand_bold,
                        color: colors.theme_orange_color_caribpay,
                        alignSelf: "center",
                      }}
                    >
                      {item.caption}
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
