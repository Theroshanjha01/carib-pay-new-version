import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
  BackHandler,
  Dimensions,
  ScrollView,
  Platform,
  Linking,
  Share,
} from "react-native";
import metrics from "../Themes/Metrics.js";
import colors from "../Themes/Colors.js";
import { SliderBox } from "react-native-image-slider-box";
import AsyncStorage from "@react-native-community/async-storage";
import Modal from "react-native-modal";
const axios = require("axios");
import RadioForm from "react-native-simple-radio-button";
var Spinner = require("react-native-spinkit");
import VersionNumber from "react-native-version-number";

let Facilities_data = [
  {
    id: 1,
    image: require("../Images/addfundnew.png"),
    name: "Add Funds",
  },
  {
    id: 2,
    image: require("../Images/topupnew.png"),
    name: "Mobile Topup",
  },
  {
    id: 6,
    image: require("../Images/caribmallnew.png"),
    name: "Carib Mall",
  },
  {
    id: 5,
    image: require("../Images/vouchernew.png"),
    name: "eCoupon",
  },
  {
    id: 3,
    image: require("../Images/billnew.png"),
    name: "Pay Bills",
  },
  {
    id: 4,
    image: require("../Images/cardnew.png"),
    name: "Card",
  },
];

let PayBillsData = [
  {
    id: 1,
    image: require("../Images/electnew.png"),
    name: "Electricity",
  },
  {
    id: 2,
    image: require("../Images/waternew.png"),
    name: "Water",
  },
  {
    id: 3,
    image: require("../Images/wifinew.png"),
    name: "Internet",
  },
  {
    id: 4,
    image: require("../Images/insunew.png"),
    name: "Insurance",
  },
  {
    id: 5,
    image: require("../Images/televisionnew.png"),
    name: "Cable TV",
  },
  {
    id: 6,
    image: require("../Images/telephonenew.png"),
    name: "Telephone",
  },
];

let dataBannerSource = [
  require("../Images/shoppingtimes.png"),
  require("../Images/reloadmobile.jpg"),
  require("../Images/qr.png"),
];

let dataBannerSourcePromotions = [
  require("../Images/download.png"),
  require("../Images/slider_coupon.jpg"),
  require("../Images/fest.png"),
];

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      available_Balance: "",
      currency_symbol: "nosymbol",
      position: 0,
      // promotion_posn: 0,
      data: "",
      isVisible: false,
      isComingSoon: false,
      subtotalIncome: "",
      subtotalOutcome: "",
      enablePasscode: false,
      multipleCurrency: [],
      multipleEnable: false,
      multiWalletData: [],
      selectedCurrency: "",
      facilittyName: "",
      user_picture: "",
      visible: false,
      appUpdates: false,
      notification_count: 0,
      bannerDataSource: [],
      bannerData2Source: [],
      isPayBills: false,
    };

    console.disableYellowBox = true;
  }

  async componentDidMount() {
    // this.checkAppUpdates();

    this.setState({
      visible: true,
    });

    let selected_Currency = await AsyncStorage.getItem("selectedCurrency");
    let balance = await AsyncStorage.getItem("available_balance");
    let currency = await AsyncStorage.getItem("symbol");
    let fname = await AsyncStorage.getItem("first_name");
    let lname = await AsyncStorage.getItem("last_name");
    let user_id = await AsyncStorage.getItem("id");
    let token = await AsyncStorage.getItem("token");
    let passcode = await AsyncStorage.getItem("passcode");
    let data_wallet = await AsyncStorage.getItem("walletData");
    console.log("data Wallets --> ", data_wallet, passcode);

    const user_login_data = {
      token: token,
      fname: this.camelCase(fname),
      lname: this.camelCase(lname),
      user_id: user_id,
    };

    this.setState({
      data: user_login_data,
      available_Balance: balance,
      currency_symbol: currency,
      selectedCurrency:
        selected_Currency == null || undefined ? "" : selected_Currency,
    });
    console.log("tokrnnnnnn ", this.state.data.token);
    // this.KnowAllTxns();
    this.getUserProfilePicture();
    this.getNotification();
    this.getBanners();

    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.reloadBalance();
        this.getUserProfilePicture();
        this.getNotification();
      }
    );

    if (passcode == 0 || passcode == null || passcode == undefined) {
      this.setState({
        enablePasscode: true,
      });
    }

    this.setState({
      multiWalletData: JSON.parse(data_wallet),
    });

    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  referPeoples = async () => {
    if (Platform.OS === "ios") {
      try {
        const result = await Share.share({
          message:
            "Hey! Download CaribPay for Fast & Easy Payment  https://share.caribpayintl.com",
          title: "CaribPay",
        });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
          } else {
          }
        } else if (result.action === Share.dismissedAction) {
        }
      } catch (error) {
        alert(error.message);
      }
    } else {
      try {
        const result = await Share.share({
          message:
            "Hey! Download CaribPay for Fast & Easy Payment  https://share.caribpayintl.com",
          title: "CaribPay",
        });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
          } else {
          }
        } else if (result.action === Share.dismissedAction) {
        }
      } catch (error) {
        alert(error.message);
      }
    }
  };

  reloadBalance = () => {
    let postData = { user_id: this.state.data.user_id };
    axios
      .get("https://sandbox.caribpayintl.com/api/available-balance", {
        params: postData,
        headers: { Authorization: this.state.data.token },
      })
      .then((response) => {
        let datas = response.data.wallets;
        this.setWalletdata(datas);
      });
  };


  setWalletdata = async (data) => {
    await AsyncStorage.setItem("walletData", JSON.stringify(data));
    let data_wallet = await AsyncStorage.getItem("walletData");
    this.setState({
      multiWalletData: JSON.parse(data_wallet),
    });

  };

  // setWalletdata = async (data) => {
  //   await AsyncStorage.setItem("walletData", JSON.stringify(data));
  //   let data_wallet = await AsyncStorage.getItem("walletData");
  //   let array = JSON.parse(data_wallet);
  //   this.setState({
  //     multiWalletData: array,
  //   });
  //   let primarywallet = this.state.multiWalletData.filter(
  //     (x) => x.is_default == "Yes"
  //   );
  //   let secondarywallet = this.state.multiWalletData.filter(
  //     (x) => x.is_default == "No"
  //   );
  //   console.log("Primary wallet ---> ", primarywallet[0]);
  //   console.log("Secondary wallet ---> ", secondarywallet[0]);
  //   let WalletDatawithTxnInfo = [];

  //   let postIncomeData = {
  //     type: "allTransactions",
  //     user_id: this.state.data.user_id,
  //     currency_id: primarywallet[0].curr_code == "USD" ? "1" : "5",
  //     summary_type: "outcome",
  //   };
  //   axios
  //     .get("https://sandbox.caribpayintl.com/api/activitysummary", {
  //       params: postIncomeData,
  //       headers: { Authorization: this.state.data.token },
  //     })
  //     .then((response) => {
  //       var spentValue = parseFloat(response.data.transactions).toFixed(2);
  //       console.log("spent value ares ---> ", response.data.transactions);
  //       if (spentValue.charAt(0) == "-") {
  //         var spentValue = spentValue.slice(1);
  //       }
  //       Object.assign(primarywallet[0], { spent: spentValue });
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  //   let postIncomeDataa = {
  //     type: "allTransactions",
  //     user_id: this.state.data.user_id,
  //     currency_id: primarywallet[0].curr_code == "USD" ? "1" : "5",
  //     summary_type: "income",
  //   };
  //   axios
  //     .get("https://sandbox.caribpayintl.com/api/activitysummary", {
  //       params: postIncomeDataa,
  //       headers: { Authorization: this.state.data.token },
  //     })
  //     .then((response) => {
  //       var recieveValue = parseFloat(response.data.transactions).toFixed(2);
  //       if (recieveValue.charAt(0) == "-") {
  //         var recieveValue = recieveValue.slice(1);
  //       }
  //       Object.assign(primarywallet[0], { recieve: recieveValue });
  //       console.log("Primary Wallet New ----> ", primarywallet[0]);
  //       WalletDatawithTxnInfo.push(primarywallet[0]);
  //       console.log("Primary Wallet New ----> ", WalletDatawithTxnInfo);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });

  //   let postIncomeDataSec = {
  //     type: "allTransactions",
  //     user_id: this.state.data.user_id,
  //     currency_id: secondarywallet[0].curr_code == "USD" ? "1" : "5",
  //     summary_type: "outcome",
  //   };
  //   axios
  //     .get("https://sandbox.caribpayintl.com/api/activitysummary", {
  //       params: postIncomeDataSec,
  //       headers: { Authorization: this.state.data.token },
  //     })
  //     .then((response) => {
  //       var spentValue = parseFloat(response.data.transactions).toFixed(2);
  //       if (spentValue.charAt(0) == "-") {
  //         var spentValue = spentValue.slice(1);
  //       }
  //       Object.assign(secondarywallet[0], { spent: spentValue });
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });

  //   let postIncomeDataSecs = {
  //     type: "allTransactions",
  //     user_id: this.state.data.user_id,
  //     currency_id: secondarywallet[0].curr_code == "USD" ? "1" : "5",
  //     summary_type: "income",
  //   };
  //   axios
  //     .get("https://sandbox.caribpayintl.com/api/activitysummary", {
  //       params: postIncomeDataSecs,
  //       headers: { Authorization: this.state.data.token },
  //     })
  //     .then((response) => {
  //       var recieveValue = parseFloat(response.data.transactions).toFixed(2);
  //       if (recieveValue.charAt(0) == "-") {
  //         var recieveValue = recieveValue.slice(1);
  //       }
  //       Object.assign(secondarywallet[0], { recieve: recieveValue });
  //       console.log(
  //         "Secondary Wallet New ----> ",
  //         secondarywallet[0],
  //         WalletDatawithTxnInfo
  //       );
  //       WalletDatawithTxnInfo.push(secondarywallet[0]);
  //       console.log("Entire Data mulitwallet ", WalletDatawithTxnInfo);
  //       this.setState({
  //         multiWalletData: WalletDatawithTxnInfo,
  //         visible: false,
  //       });
  //       console.log("mulitwallet setData ", this.state.multiWalletData);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // };

  // getSetBalanceWallet = async (data) => {
  //   await AsyncStorage.setItem(
  //     "available_balance",
  //     this.state.available_Balance
  //   );
  //   await AsyncStorage.setItem("symbol", this.state.currency_symbol);
  //   {
  //     data.length > 1 && this.setWalletdata(data);
  //   }
  // };

  // getTxnInfo = () => {
  //   console.log("called first tym ");
  //   // this.setState({
  //   //   visible: true
  //   // })
  //   console.log("my wallet data ---> ", this.state.multiWalletData);
  //   let primarywallet = this.state.multiWalletData.filter(
  //     (x) => x.is_default == "Yes"
  //   );
  //   let secondarywallet = this.state.multiWalletData.filter(
  //     (x) => x.is_default == "No"
  //   );
  //   console.log("Primary wallet ---> ", primarywallet[0]);
  //   console.log("Secondary wallet ---> ", secondarywallet[0]);
  //   let WalletDatawithTxnInfo = [];

  //   let postIncomeData = {
  //     type: "allTransactions",
  //     user_id: this.state.data.user_id,
  //     currency_id: primarywallet[0].curr_code == "USD" ? "1" : "5",
  //     summary_type: "outcome",
  //   };
  //   axios
  //     .get("https://sandbox.caribpayintl.com/api/activitysummary", {
  //       params: postIncomeData,
  //       headers: { Authorization: this.state.data.token },
  //     })
  //     .then((response) => {
  //       var spentValue = parseFloat(response.data.transactions).toFixed(2);
  //       console.log("spent value ares ---> ", response.data.transactions);
  //       if (spentValue.charAt(0) == "-") {
  //         var spentValue = spentValue.slice(1);
  //       }
  //       Object.assign(primarywallet[0], { spent: spentValue });
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  //   let postIncomeDataa = {
  //     type: "allTransactions",
  //     user_id: this.state.data.user_id,
  //     currency_id: primarywallet[0].curr_code == "USD" ? "1" : "5",
  //     summary_type: "income",
  //   };
  //   axios
  //     .get("https://sandbox.caribpayintl.com/api/activitysummary", {
  //       params: postIncomeDataa,
  //       headers: { Authorization: this.state.data.token },
  //     })
  //     .then((response) => {
  //       var recieveValue = parseFloat(response.data.transactions).toFixed(2);
  //       if (recieveValue.charAt(0) == "-") {
  //         var recieveValue = recieveValue.slice(1);
  //       }
  //       Object.assign(primarywallet[0], { recieve: recieveValue });
  //       console.log("Primary Wallet New ----> ", primarywallet[0]);
  //       WalletDatawithTxnInfo.push(primarywallet[0]);
  //       console.log("Primary Wallet New ----> ", WalletDatawithTxnInfo);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });

  //   let postIncomeDataSec = {
  //     type: "allTransactions",
  //     user_id: this.state.data.user_id,
  //     currency_id: secondarywallet[0].curr_code == "USD" ? "1" : "5",
  //     summary_type: "outcome",
  //   };
  //   axios
  //     .get("https://sandbox.caribpayintl.com/api/activitysummary", {
  //       params: postIncomeDataSec,
  //       headers: { Authorization: this.state.data.token },
  //     })
  //     .then((response) => {
  //       var spentValue = parseFloat(response.data.transactions).toFixed(2);
  //       if (spentValue.charAt(0) == "-") {
  //         var spentValue = spentValue.slice(1);
  //       }
  //       Object.assign(secondarywallet[0], { spent: spentValue });
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });

  //   let postIncomeDataSecs = {
  //     type: "allTransactions",
  //     user_id: this.state.data.user_id,
  //     currency_id: secondarywallet[0].curr_code == "USD" ? "1" : "5",
  //     summary_type: "income",
  //   };
  //   axios
  //     .get("https://sandbox.caribpayintl.com/api/activitysummary", {
  //       params: postIncomeDataSecs,
  //       headers: { Authorization: this.state.data.token },
  //     })
  //     .then((response) => {
  //       var recieveValue = parseFloat(response.data.transactions).toFixed(2);
  //       if (recieveValue.charAt(0) == "-") {
  //         var recieveValue = recieveValue.slice(1);
  //       }
  //       Object.assign(secondarywallet[0], { recieve: recieveValue });
  //       console.log(
  //         "Secondary Wallet New ----> ",
  //         secondarywallet[0],
  //         WalletDatawithTxnInfo
  //       );
  //       WalletDatawithTxnInfo.push(secondarywallet[0]);
  //       console.log("Entire Data mulitwallet ", WalletDatawithTxnInfo);
  //       this.setState({
  //         multiWalletData: WalletDatawithTxnInfo,
  //         // visible: false
  //       });
  //       console.log("mulitwallet setData ", this.state.multiWalletData);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // };

  getBanners = () => {
    // this.setState({
    //   visible:true
    // })
    let postData = {
      position: "top",
    };
    let postData2 = {
      position: "bottom",
    };
    axios({
      method: "post",
      url: "https://sandbox.caribpayintl.com/api/banner",
      data: postData,
    })
      .then((response) => {
        console.log(
          "homepage banner response 1 ---> ",
          response.data.success.data
        );
        let i = 0;
        let bannerData = [];
        let responseData = response.data.success.data;
        for (i = 0; i < responseData.length; i++) {
          const imagePath = responseData[i].banner_image;
          bannerData.push(imagePath);
        }
        axios({
          method: "post",
          url: "https://sandbox.caribpayintl.com/api/banner",
          data: postData2,
        })
          .then((response) => {
            console.log(
              "homepage banner response ---> ",
              response.data.success.data
            );
            let i = 0;
            let bannerData2 = [];
            let responseData = response.data.success.data;
            for (i = 0; i < responseData.length; i++) {
              const imagePath = responseData[i].banner_image;
              bannerData2.push(imagePath);
            }
            this.setState({
              bannerDataSource: bannerData,
              bannerData2Source: bannerData2,
              visible: false,
            });

            console.log(this.state.bannerData2Source)
          })
          .catch((err) => {
            console.log("homepage banner response  error   --- > ", err);
          });
      })
      .catch((err) => {
        console.log("homepage banner response  error   --- > ", err);
      });
  };

  getNotification = () => {
    let postData = {
      user_id: this.state.data.user_id,
    };
    axios({
      method: "post",
      url: "https://sandbox.caribpayintl.com/api/noticeboard",
      headers: { Authorization: this.state.data.token },
      data: postData,
    })
      .then((response) => {
        console.log(
          "notification response ---> ",
          response.data.success.data,
          response.data.success.data.length
        );
        let unseenNorifications = response.data.success.data.filter(
          (roshan) => roshan.read_status == 0
        );
        console.log("unreead notifi --- > ", unseenNorifications);
        this.setState({
          // spinvisible: false,
          notification_count:
            unseenNorifications.length > 0 ? unseenNorifications.length : 0,
        });
      })
      .catch((err) => {
        console.log("notification response  error   --- > ", err);
        this.setState({
          spinvisible: false,
        });
      });
  };

  getUserProfilePicture = () => {
    console.log("called dp");
    let postData = { user_id: this.state.data.user_id };
    axios
      .get("https://sandbox.caribpayintl.com/api/get-user-profile", {
        params: postData,
        headers: { Authorization: this.state.data.token },
      })
      .then((response) => {
        console.log("picture ---> ", response.data.success.user.picture);
        this.setState({
          user_picture: response.data.success.user.picture,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // reloadBalance = () => {
  //   console.log("sv---", this.state.selectedCurrency);
  //   let postData = { user_id: this.state.data.user_id };
  //   axios
  //     .get("https://sandbox.caribpayintl.com/api/available-balance", {
  //       params: postData,
  //       headers: { Authorization: this.state.data.token },
  //     })
  //     .then((response) => {
  //       let data = response.data.wallets;
  //       let filterData;
  //       if (this.state.selectedCurrency == "") {
  //         filterData = data.filter((roshan) => roshan.is_default == "Yes");
  //       } else {
  //         filterData = data.filter(
  //           (roshan) => roshan.curr_code == this.state.selectedCurrency
  //         );
  //       }
  //       let balance = filterData[0].balance;
  //       let currency_symbol = filterData[0].curr_code;
  //       this.setState({
  //         available_Balance: parseFloat(balance).toFixed(2),
  //         currency_symbol: currency_symbol,
  //       });
  //       AsyncStorage.setItem("available_balance", this.state.available_Balance);
  //       AsyncStorage.setItem("symbol", this.state.currency_symbol);
  //     });
  //   this.KnowAllTxns();
  // };

  camelCase(str) {
    console.log("string received = ", str);
    let wordsArray = str.split(" ");
    return wordsArray
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
      .join(" ");
  }

  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position:
            this.state.position === this.state.bannerDataSource.length
              ? 0
              : this.state.position + 1,
        });
      }, 3000),
    });
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
    clearInterval(this.state.interval);
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  checkAppUpdates = () => {
    console.log("app update response -- > ");
    let data = {
      platform: Platform.OS === "ios" ? "ios" : "android",
      app_ver: VersionNumber.appVersion,
      app_build_ver: VersionNumber.buildVersion,
      api_ver: "1",
      device: "",
    };

    axios({
      method: "post",
      url: "https://sandbox.caribpayintl.com/api/check-app-update",
      data: data,
    })
      .then((response) => {
        console.log("app update response -- > ", data, response);
        this.setState({
          appUpdates: true,
        });
      })
      .catch((err) => {
        // this.setState({ visible: false });
        console.log(err, data, "rodhan");
      });
  };

  camelCase(str) {
    console.log("string received = ", str);
    let wordsArray = str.split(" ");
    return wordsArray
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
      .join(" ");
  }

  knowKycStatus = () => {
    let postData = { user_id: this.state.data.user_id };
    axios({
      method: "get",
      url: "https://sandbox.caribpayintl.com/api/get-kyc-status",
      headers: { Authorization: this.state.data.token },
      data: postData,
    })
      .then((response) => {
        if (response.data.status == 200) {
          console.log("doctstatus --> ", response.data);
          let kyc_status = response.data.success.status;
          if (kyc_status == "Not Uploaded") {
            this.setState({
              isVisible: true,
            });
          } else {
            this.setState({
              isVisible: false,
            });
          }
        }
      })
      .catch((err) => {
        alert(err);
      });
  };

  // KnowAllTxns = () => {
  //   let postData = {
  //     type: "allTransactions",
  //     user_id: this.state.data.user_id,
  //     currency_id: this.state.multiWalletData.length > 1 ? "1" : "1",
  //   };
  //   axios
  //     .get("https://sandbox.caribpayintl.com/api/activityall", {
  //       params: postData,
  //       headers: { Authorization: this.state.data.token },
  //     })
  //     .then((response) => {
  //       let datas = response.data.transactions;
  //       // console.log('Data', datas);
  //       const txnId1 = datas.filter(
  //         (roshan) =>
  //           roshan.transaction_type_id == 1 &&
  //           roshan.curr_code == this.state.currency_symbol
  //       );
  //       let sum_subtotal_txnid1 = 0.0;
  //       if (txnId1.length > 0) {
  //         let i = 0;
  //         for (i = 0; i < txnId1.length; i++) {
  //           sum_subtotal_txnid1 =
  //             parseFloat(sum_subtotal_txnid1) +
  //             parseFloat(txnId1[i].subsubtotal);
  //         }
  //         console.log("subtotal txn 1", sum_subtotal_txnid1);
  //       } else {
  //         console.log("subtotal txn 1", sum_subtotal_txnid1);
  //       }

  //       const txnId2 = datas.filter(
  //         (roshan) =>
  //           roshan.transaction_type_id == 4 &&
  //           roshan.curr_code == this.state.currency_symbol
  //       );
  //       let sum_subtotal_txnid4 = 0.0;
  //       if (txnId2.length > 0) {
  //         let i = 0;
  //         for (i = 0; i < txnId2.length; i++) {
  //           sum_subtotal_txnid4 =
  //             parseFloat(sum_subtotal_txnid4) +
  //             parseFloat(txnId2[i].subsubtotal);
  //         }
  //         console.log("subtotal txn 4", sum_subtotal_txnid4);
  //       } else {
  //         console.log("subtotal txn 4", sum_subtotal_txnid4);
  //       }
  //       const txnId9 = datas.filter(
  //         (roshan) =>
  //           roshan.transaction_type_id == 9 &&
  //           roshan.curr_code == this.state.currency_symbol
  //       );
  //       let sum_subtotal_txnid9 = 0.0;
  //       if (txnId9.length > 0) {
  //         let i = 0;
  //         for (i = 0; i < txnId9.length; i++) {
  //           sum_subtotal_txnid9 =
  //             parseFloat(sum_subtotal_txnid9) +
  //             parseFloat(txnId9[i].subsubtotal);
  //         }
  //         console.log("subtotal txn 9", sum_subtotal_txnid9);
  //       } else {
  //         console.log("subtotal txn 9", sum_subtotal_txnid9);
  //       }

  //       const txnId12 = datas.filter(
  //         (roshan) =>
  //           roshan.transaction_type_id == 12 &&
  //           roshan.curr_code == this.state.currency_symbol
  //       );
  //       let sum_subtotal_txnid12 = 0.0;
  //       if (txnId12.length > 0) {
  //         let i = 0;
  //         for (i = 0; i < txnId12.length; i++) {
  //           sum_subtotal_txnid12 =
  //             parseFloat(sum_subtotal_txnid12) +
  //             parseFloat(txnId12[i].subsubtotal);
  //         }
  //         console.log("subtotal txn 12", sum_subtotal_txnid12);
  //       } else {
  //         console.log("subtotal txn 12", sum_subtotal_txnid12);
  //       }

  //       let subtotal_income =
  //         sum_subtotal_txnid1 +
  //         sum_subtotal_txnid4 +
  //         sum_subtotal_txnid9 +
  //         sum_subtotal_txnid12;

  //       if (parseFloat(subtotal_income) > 0.0) {
  //         this.setState({
  //           subtotalIncome:
  //             this.state.currency_symbol === "nosymbol"
  //               ? "---"
  //               : this.state.currency_symbol == "USD"
  //               ? "$ " + parseFloat(subtotal_income).toFixed(2)
  //               : "EC$ " + parseFloat(subtotal_income).toFixed(2),
  //         });
  //       } else {
  //         this.setState({
  //           subtotalIncome:
  //             this.state.currency_symbol === "nosymbol"
  //               ? " --- "
  //               : this.state.currency_symbol == "USD"
  //               ? "$ 0.00"
  //               : "EC$ 0.00",
  //         });
  //       }

  //       //outcome txns:---
  //       const outcometxnId2 = datas.filter(
  //         (roshan) =>
  //           roshan.transaction_type_id == 2 &&
  //           roshan.curr_code == this.state.currency_symbol
  //       );
  //       let sum_outcome_subtotal_txnid2 = 0.0;
  //       if (outcometxnId2.length > 0) {
  //         let i = 0;
  //         for (i = 0; i < outcometxnId2.length; i++) {
  //           sum_outcome_subtotal_txnid2 =
  //             parseFloat(sum_outcome_subtotal_txnid2) +
  //             parseFloat(outcometxnId2[i].subtotal);
  //         }
  //         console.log(
  //           "sum_outcome_subtotal_txnid2",
  //           sum_outcome_subtotal_txnid2
  //         );
  //       } else {
  //         console.log(
  //           "sum_outcome_subtotal_txnid2",
  //           sum_outcome_subtotal_txnid2
  //         );
  //       }

  //       const outcometxnId3 = datas.filter(
  //         (roshan) =>
  //           roshan.transaction_type_id == 3 &&
  //           roshan.curr_code == this.state.currency_symbol
  //       );
  //       let sum_outcome_subtotal_txnid3 = 0.0;
  //       if (outcometxnId3.length > 0) {
  //         let i = 0;
  //         for (i = 0; i < outcometxnId3.length; i++) {
  //           sum_outcome_subtotal_txnid3 =
  //             parseFloat(sum_outcome_subtotal_txnid3) +
  //             parseFloat(outcometxnId3[i].subtotal);
  //           console.log(parseFloat(outcometxnId3[i].subtotal));
  //         }
  //         console.log(
  //           "sum_outcome_subtotal_txnid3",
  //           sum_outcome_subtotal_txnid3
  //         );
  //       } else {
  //         console.log(
  //           "sum_outcome_subtotal_txnid3",
  //           sum_outcome_subtotal_txnid3
  //         );
  //       }

  //       const outcometxnId10 = datas.filter(
  //         (roshan) =>
  //           roshan.transaction_type_id == 10 &&
  //           roshan.curr_code == this.state.currency_symbol
  //       );
  //       let sum_outcome_subtotal_txnid10 = 0.0;
  //       if (outcometxnId10.length > 0) {
  //         let i = 0;
  //         for (i = 0; i < outcometxnId10.length; i++) {
  //           sum_outcome_subtotal_txnid10 =
  //             parseFloat(sum_outcome_subtotal_txnid10) +
  //             parseFloat(outcometxnId10[i].subtotal);
  //         }
  //         console.log(
  //           "sum_outcome_subtotal_txnid10",
  //           sum_outcome_subtotal_txnid10
  //         );
  //       } else {
  //         console.log(
  //           "sum_outcome_subtotal_txnid10",
  //           sum_outcome_subtotal_txnid10
  //         );
  //       }

  //       const outcometxnId11 = datas.filter(
  //         (roshan) =>
  //           roshan.transaction_type_id == 11 &&
  //           roshan.curr_code == this.state.currency_symbol
  //       );
  //       let sum_outcome_subtotal_txnid11 = 0.0;
  //       if (outcometxnId11.length > 0) {
  //         let i = 0;
  //         for (i = 0; i < outcometxnId11.length; i++) {
  //           sum_outcome_subtotal_txnid11 =
  //             parseFloat(sum_outcome_subtotal_txnid11) +
  //             parseFloat(outcometxnId11[i].subtotal);
  //         }
  //         console.log(
  //           "sum_outcome_subtotal_txnid11",
  //           sum_outcome_subtotal_txnid11
  //         );
  //       } else {
  //         console.log(
  //           "sum_outcome_subtotal_txnid11",
  //           sum_outcome_subtotal_txnid11
  //         );
  //       }

  //       const outcometxnId15 = datas.filter(
  //         (roshan) =>
  //           roshan.transaction_type_id == 15 &&
  //           roshan.curr_code == this.state.currency_symbol
  //       );
  //       let sum_outcome_subtotal_txnid15 = 0.0;
  //       if (outcometxnId15.length > 0) {
  //         let i = 0;
  //         for (i = 0; i < outcometxnId15.length; i++) {
  //           sum_outcome_subtotal_txnid15 =
  //             parseFloat(sum_outcome_subtotal_txnid15) +
  //             parseFloat(outcometxnId15[i].subtotal);
  //         }
  //         console.log(
  //           "sum_outcome_subtotal_txnid11",
  //           sum_outcome_subtotal_txnid15
  //         );
  //       } else {
  //         console.log(
  //           "sum_outcome_subtotal_txnid11",
  //           sum_outcome_subtotal_txnid15
  //         );
  //       }

  //       var subtotal_outcome =
  //         sum_outcome_subtotal_txnid2 +
  //         sum_outcome_subtotal_txnid3 +
  //         sum_outcome_subtotal_txnid10 +
  //         sum_outcome_subtotal_txnid11 +
  //         sum_outcome_subtotal_txnid15;
  //       console.log(subtotal_outcome);
  //       if (parseFloat(subtotal_outcome) == 0.0) {
  //         console.log("this comes");
  //         this.setState({
  //           subtotalOutcome:
  //             this.state.currency_symbol == "nosymbol"
  //               ? "---"
  //               : this.state.currency_symbol == "USD"
  //               ? "$ 0.00"
  //               : "EC$ 0.00",
  //           visible: false,
  //         });
  //       } else {
  //         var newsubtotaloutcome = subtotal_outcome.toString();
  //         console.log("new subtotal outcome", newsubtotaloutcome);
  //         var c = newsubtotaloutcome.substr(1);
  //         console.log("new subtotal outcome substr ---> ");

  //         var convertedIntofloat = parseFloat(c).toFixed(2);
  //         this.setState({
  //           subtotalOutcome:
  //             this.state.currency_symbol == "nosymbol"
  //               ? "---"
  //               : this.state.currency_symbol == "USD"
  //               ? "$ " + convertedIntofloat
  //               : "EC$ " + convertedIntofloat,
  //           visible: false,
  //         });
  //       }
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // };

  handleBackPress = () => {
    BackHandler.exitApp();
    return true;
  };

  goPayBills = (item) => {
    this.setState({
      isPayBills: false,
    });
    this.setState({
      facilittyName: item.name + " Services",
      isComingSoon: true,
    });
    // this.props.navigation.navigate("BillsPayInfo", {
    //   billName: item.name,
    // });
  };
  onPressItems = (item) => {
    console.log(item.id);
    switch (item.id) {
      case 1:
        this.props.navigation.navigate("AddMoney", {
          available_Balance: this.state.available_Balance,
          token: this.state.data.token,
          amount: "",
          symbol: this.state.currency_symbol == "USD" ? "USD" : "XCD",
        });
        break;
      case 2:
        this.props.navigation.navigate("MobileReload", {
          user_id: this.state.data.user_id,
          token: this.state.data.token,
        });
        break;
      case 3:
        // this.props.navigation.navigate("BillsPayment");
        this.setState({ isPayBills: true });
        break;
      case 4:
        this.setState({ facilittyName: "Card", isComingSoon: true });
        break;
      case 5:
        this.props.navigation.navigate("CouponsVouchers");
        // this.setState({ facilittyName: 'Remittance', isComingSoon: true });
        break;
      case 6:
        this.props.navigation.navigate("CaribMallHome");
        // this.setState({ facilittyName: 'Carib Mall', isComingSoon: true });
        break;
      default:
        console.log("this is called");
    }
  };

  onchangeCurrencyType = async () => {
    let data = this.state.multiWalletData;
    const filterData = data.filter(
      (roshan) => roshan.curr_code == this.state.selectedCurrency
    );
    console.log("filtered result on cureency change", filterData);
    let currency_symbol = filterData[0].curr_code;
    let balance = filterData[0].balance;
    let available_bal = parseFloat(balance).toFixed(2);
    this.setState({
      available_Balance: available_bal,
      currency_symbol: currency_symbol,
    });
    await AsyncStorage.setItem("available_balance", filterData[0].balance);
    await AsyncStorage.setItem("symbol", this.state.currency_symbol);

    if (this.state.selectedCurrency != "") {
      this.setState({
        radioPosition: filterData[0].is_default == "Yes" ? 1 : 0,
      });
    }

    // this.KnowAllTxns();
  };

  onLayout = (e) => {
    this.setState({
      width: e.nativeEvent.layout.width,
    });
  };

  // renderTxnByCategory = (item) => {
  //   return (
  //     <View
  //       style={{
  //         flexDirection: "row",
  //         justifyContent: "space-between",
  //         marginLeft: -10,
  //         marginTop: 10,
  //       }}
  //     >
  //       <View style={{ flexDirection: "row", width: "30%" }}>
  //         <View
  //           style={{
  //             flexDirection: "column",
  //             justifyContent: "center",
  //             marginLeft: metrics.dimen_10,
  //           }}
  //         >
  //           <Text
  //             style={{
  //               fontSize: metrics.text_normal,
  //               color: colors.heading_black_text,
  //               marginTop: 2,
  //             }}
  //           >
  //             Received
  //           </Text>
  //           <Text
  //             style={{
  //               fontSize: metrics.text_normal,
  //               color: colors.heading_black_text,
  //               marginTop: 2,
  //             }}
  //           >
  //             {this.state.subtotalIncome}
  //           </Text>
  //         </View>
  //       </View>

  //       <View
  //         style={{
  //           borderLeftWidth: 0.5,
  //           borderLeftColor: colors.app_gray,
  //         }}
  //       />

  //       <View
  //         style={{
  //           flexDirection: "row",
  //           justifyContent: "center",
  //           width: "30%",
  //         }}
  //       >
  //         <View
  //           style={{
  //             flexDirection: "column",
  //             justifyContent: "center",
  //             marginLeft: metrics.dimen_10,
  //           }}
  //         >
  //           <Text
  //             style={{
  //               ...styles.user_text,
  //               fontSize: metrics.text_normal,
  //               color: colors.heading_black_text,
  //             }}
  //           >
  //             Spent
  //           </Text>
  //           <Text style={{ ...styles.user_text, color: colors.black }}>
  //             {this.state.subtotalOutcome}
  //           </Text>
  //         </View>
  //       </View>

  //       <View
  //         style={{
  //           borderLeftWidth: 0.5,
  //           borderLeftColor: colors.app_gray,
  //           marginLeft: 10,
  //         }}
  //       />

  //       <View
  //         style={{
  //           marginLeft: metrics.dimen_15,
  //           justifyContent: "center",
  //           backgroundColor: colors.light_grey_backgroud,
  //           borderRadius: metrics.dimen_40,
  //           width: 100,
  //         }}
  //       >
  //         <TouchableOpacity
  //           onPress={() =>
  //             this.props.navigation.navigate("TransactionsList", {
  //               user_id: this.state.data.user_id,
  //               token: this.state.data.token,
  //               available_Balance: this.state.available_Balance,
  //               symbol: this.state.currency_symbol,
  //             })
  //           }
  //         >
  //           <Text
  //             style={{
  //               ...styles.user_text,
  //               color: colors.theme_caribpay,
  //               fontSize: metrics.text_normal,
  //               textAlign: "center",
  //             }}
  //           >
  //             {"All Activity"}
  //           </Text>
  //         </TouchableOpacity>
  //       </View>
  //     </View>
  //   );
  // };


  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.visible ? (
          <Spinner
            style={{ flex: 1, justifyContent: "center", alignSelf: "center" }}
            isVisible={this.state.visible}
            size={70}
            type={"ThreeBounce"}
            color={colors.black}
          />
        ) : (
          <ScrollView>
            <View style={{ flex: 0.38 }}>
              <View style={{ flex: 0.5 }}>
                <View
                  style={{
                    backgroundColor: colors.theme_caribpay,
                    paddingBottom: 100,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      margin: metrics.dimen_20,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate("EditProfile", {
                          item: this.state.data,
                        })
                      }
                    >
                      <View style={{ flexDirection: "row" }}>
                        {this.state.user_picture == "" && (
                          <Image
                            source={require("../Images/user.png")}
                            style={styles.image_user_style}
                          />
                        )}
                        {this.state.user_picture != "" && (
                          <Image
                            source={{ uri: this.state.user_picture }}
                            style={{ height: 44, width: 44, borderRadius: 22 }}
                          />
                        )}
                        <View
                          style={{
                            flexDirection: "column",
                            justifyContent: "center",
                            marginLeft: metrics.dimen_10,
                          }}
                        >
                          <Text
                            style={{
                              ...styles.user_text,
                              fontSize: metrics.text_normal,
                              color: colors.white,
                            }}
                          >
                            Welcome Back
                          </Text>
                          {this.state.data.fname != undefined && (
                            <Text
                              style={{
                                fontSize: metrics.text_16,
                                color: colors.white,
                                fontWeight: "bold",
                              }}
                            >
                              {this.state.data.fname +
                                " " +
                                this.state.data.lname}
                            </Text>
                          )}
                        </View>
                      </View>
                    </TouchableOpacity>

                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-evenly",
                        alignSelf: "center",
                        marginLeft: 10,
                      }}
                    >
                      <TouchableOpacity onPress={() => this.referPeoples()}>
                        <View
                          style={{
                            height: 40,
                            width: 80,
                            borderRadius: 20,
                            flexDirection: "row",
                            backgroundColor: colors.theme_orange_color_caribpay,
                            justifyContent: "center",
                          }}
                        >
                          <Text
                            style={{
                              fontFamily: metrics.quicksand_semibold,
                              fontSize: metrics.text_normal,
                              color: colors.white,
                              alignSelf: "center",
                              marginTop: -1,
                            }}
                          >
                            {"Share"}
                          </Text>
                          <Image
                            source={require("../Images/share.png")}
                            style={{
                              ...styles.scan_bell_style,
                              marginLeft: 5,
                              marginRight: 1,
                            }}
                          />
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate("UserNotification", {
                            token: this.state.data.token,
                            user_id: this.state.data.user_id,
                          })
                        }
                      >
                        <View
                          style={{
                            height: 40,
                            width: 40,
                            borderRadius: 20,
                            backgroundColor: colors.theme_orange_color_caribpay,
                            justifyContent: "center",
                            marginRight: metrics.dimen_10,
                            marginLeft: metrics.dimen_10,
                          }}
                        >
                          <Image
                            source={require("../Images/notification.png")}
                            style={styles.scan_bell_style}
                          />
                          {this.state.notification_count > 0 && (
                            <View
                              style={{
                                position: "absolute",
                                backgroundColor: "red",
                                height: 20,
                                width: 20,
                                borderRadius: 10,
                                marginLeft: 15,
                                top: -10,
                              }}
                            >
                              <Text
                                style={{
                                  color: colors.white,
                                  textAlign: "center",
                                }}
                              >
                                {this.state.notification_count}
                              </Text>
                            </View>
                          )}
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>

                  <View
                    style={{
                      height: 140,
                      width:
                        this.state.multiWalletData.length > 1 ? "96%" : "95%",
                      alignSelf: "center",
                      marginBottom:
                        this.state.multiWalletData.length > 1 ? 5 : 10,
                    }}
                  >
                    <FlatList
                      data={this.state.multiWalletData}
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}
                      keyExtractor={(item, index) => index.toString()}
                      renderItem={({ item }) => (
                        <View
                          style={{
                            margin:
                              this.state.multiWalletData.length > 1 ? 2 : 0,
                            flexDirection: "column",
                            backgroundColor: colors.light_grey_backgroud,
                            borderRadius: metrics.dimen_15,
                            height: 130,
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              width:
                                this.state.multiWalletData.length > 1
                                  ? Dimensions.get("screen").width - 20
                                  : Dimensions.get("screen").width - 20,
                            }}
                          >
                            <Text
                              style={{
                                fontSize: metrics.text_normal,
                                padding: 10,
                                color: colors.heading_black_text,
                              }}
                            >
                              <Text
                                style={{
                                  fontSize: metrics.text_normal,
                                  fontWeight: "bold",
                                  color: colors.theme_caribpay,
                                }}
                              >
                                {" " + item.curr_code + " "}
                              </Text>
                              <Text
                                style={{
                                  fontSize: metrics.text_normal,
                                  color: colors.heading_black_text,
                                }}
                              >
                                Account
                              </Text>
                            </Text>
                            <View
                              style={{
                                flexDirection: "row",
                                justifyContent: "space-between",
                              }}
                            >
                              <Text
                                style={{
                                  ...styles.available_bal,
                                  marginTop: 0,
                                  marginLeft: 10,
                                }}
                              >
                                {this.state.currency_symbol === "nosymbol"
                                  ? ""
                                  : item.curr_code == "USD"
                                  ? "$ " + item.balance
                                  : "EC$ " + item.balance}
                              </Text>
                              <TouchableOpacity
                                onPress={() =>
                                  this.props.navigation.navigate("AddMoney", {
                                    available_Balance: item.balance,
                                    token: this.state.data.token,
                                    symbol:
                                      item.curr_code == "USD" ? "USD" : "XCD",
                                  })
                                }
                              >
                                <View
                                  style={{
                                    width: 100,
                                    height: 30,
                                    borderRadius: metrics.dimen_7,
                                    backgroundColor: colors.theme_caribpay,
                                    justifyContent: "center",
                                    marginRight: 5,
                                    borderRadius: 15,
                                  }}
                                >
                                  <Text
                                    style={{
                                      color: colors.white,
                                      textAlign: "center",
                                      textAlignVertical: "center",
                                      fontFamily: metrics.quicksand_bold,
                                    }}
                                  >
                                    Add Funds
                                  </Text>
                                </View>
                              </TouchableOpacity>
                            </View>
                            <View
                              style={{
                                borderBottomColor: colors.app_gray,
                                borderBottomWidth: 0.5,
                                marginTop: 10,
                              }}
                            />

                            <View
                              style={{
                                flexDirection: "row",
                                justifyContent: "space-between",
                                marginLeft: 5,
                              }}
                            >
                              <View
                                style={{
                                  flexDirection: "row",
                                  width: "30%",
                                  alignSelf: "center",
                                }}
                              >
                                <View
                                  style={{
                                    flexDirection: "column",
                                    justifyContent: "center",
                                    marginLeft: metrics.dimen_10,
                                    alignSelf: "center",
                                  }}
                                >
                                  <Text
                                    style={{
                                      fontSize: metrics.text_normal,
                                      color: colors.heading_black_text,
                                      textAlign: "center",
                                      marginTop: 2,
                                      fontFamily: metrics.quicksand_semibold,
                                    }}
                                  >
                                    Received
                                  </Text>
                                  <Text
                                    style={{
                                      fontSize: metrics.text_normal,
                                      color: colors.heading_black_text,
                                      textAlign: "center",
                                      marginTop: 2,
                                    }}
                                  >
                                    {item.curr_code == "USD"
                                      ? "$ " + item.income
                                      : "EC$ " + item.income}
                                  </Text>
                                </View>
                              </View>

                              <View
                                style={{
                                  borderLeftWidth: 0.5,
                                  borderLeftColor: colors.app_gray,
                                }}
                              />

                              <View
                                style={{
                                  flexDirection: "row",
                                  justifyContent: "center",
                                  width: "30%",
                                }}
                              >
                                <View
                                  style={{
                                    flexDirection: "column",
                                    justifyContent: "center",
                                    marginLeft: metrics.dimen_10,
                                  }}
                                >
                                  <Text
                                    style={{
                                      fontSize: metrics.text_normal,
                                      color: colors.heading_black_text,
                                      textAlign: "center",
                                      fontFamily: metrics.quicksand_semibold,
                                    }}
                                  >
                                    Spent
                                  </Text>
                                  <Text
                                    style={{
                                      fontSize: metrics.text_normal,
                                      color: colors.heading_black_text,
                                      textAlign: "center",
                                      marginLeft: -1,
                                    }}
                                  >
                                    {item.curr_code == "USD"
                                      ? "$ " + item.outcome
                                      : "EC$ " + item.outcome}
                                  </Text>
                                </View>
                              </View>

                              <View
                                style={{
                                  borderLeftWidth: 0.5,
                                  borderLeftColor: colors.app_gray,
                                  marginLeft: 10,
                                }}
                              />

                              <View
                                style={{
                                  alignSelf: "center",
                                  justifyContent: "center",
                                  marginRight: 10,
                                }}
                              >
                                <TouchableOpacity
                                  onPress={() =>
                                    this.props.navigation.navigate(
                                      "TransactionsList",
                                      {
                                        user_id: this.state.data.user_id,
                                        token: this.state.data.token,
                                        available_Balance: this.state
                                          .available_Balance,
                                        symbol: item.curr_code,
                                      }
                                    )
                                  }
                                >
                                  <Text
                                    style={{
                                      ...styles.user_text,
                                      color: colors.theme_caribpay,
                                      fontSize: metrics.text_normal,
                                      textAlign: "center",
                                      // fontWeight: "bold",
                                    }}
                                  >
                                    {"All Activity"}
                                  </Text>
                                </TouchableOpacity>
                              </View>
                            </View>
                          </View>
                        </View>
                      )}
                    />
                  </View>

                  {this.state.multiWalletData.length > 1 && (
                    <View
                      style={{
                        flexDirection: "row",
                        alignSelf: "center",
                      }}
                    >
                      <View
                        style={{
                          height: 8,
                          width: 30,
                          borderRadius: 4,
                          backgroundColor: colors.theme_orange_color_caribpay,
                        }}
                      />

                      {/* <TouchableOpacity onPress={() => this.goIndex()}> */}
                        <View
                          style={{
                            height: 8,
                            width: 30,
                            borderRadius: 4,
                            marginLeft: 10,
                            backgroundColor: colors.white,
                          }}
                        />
                      {/* </TouchableOpacity> */}
                    </View>
                  )}

                  {/* 
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('AddMoney', {
                          available_Balance: this.state.available_Balance,
                          token: this.state.data.token,
                          symbol:
                            this.state.currency_symbol == 'USD' ? 'USD' : 'XCD',
                        })
                      }>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginTop: 2,
                          marginLeft: metrics.dimen_25,
                        }}>
                        {this.state.currency_symbol != 'nosymbol' &&
                          <View
                            style={{
                              justifyContent: 'center',
                              backgroundColor: colors.light_grey_backgroud,
                              borderRadius: metrics.dimen_10,
                              flexDirection: 'row',
                              width: 80,
                              borderRadius: 50,
                              height: 35,
                            }}>
                            <TouchableOpacity
                              onPress={() =>
                                this.state.multiWalletData.length > 1
                                  ? this.setState({ multipleEnable: true })
                                  : null
                              }
                              style={{
                                justifyContent: 'center',
                                backgroundColor: colors.light_grey_backgroud,
                                borderRadius: metrics.dimen_50,
                                flexDirection: 'row',
                                width: 80,
                                height: 35,
                              }}>
                              <View
                                style={{
                                  height: 30,
                                  width: 30,
                                  borderRadius: 15,
                                  justifyContent: 'center',
                                  alignSelf: 'center',
                                }}>
                                <Image
                                  source={require('../Images/dropdown.png')}
                                  style={{
                                    height: metrics.dimen_18,
                                    width: metrics.dimen_18,
                                    alignSelf: 'center',
                                    tintColor: colors.theme_caribpay,
                                  }}
                                />
                              </View>
                              <Text
                                style={{
                                  color: colors.theme_caribpay,
                                  fontWeight: 'bold',
                                  fontSize: metrics.text_normal,
                                  alignSelf: 'center',
                                  marginRight: 10,
                                }}>
                                {this.state.currency_symbol}
                              </Text>
                            </TouchableOpacity>
                          </View>
                        }
                        <Text style={styles.available_bal}>
                          {this.state.currency_symbol === 'nosymbol'
                            ? ''
                            : this.state.currency_symbol == 'USD'
                              ? '$ ' + this.state.available_Balance
                              : 'EC$ ' + this.state.available_Balance}
                        </Text>
                        <View
                          style={{
                            height: 30,
                            width: 30,
                            borderRadius: 15,
                            backgroundColor: '#014a57',
                            justifyContent: 'center',
                            marginLeft: metrics.dimen_10,
                            alignSelf: 'center',
                          }}>
                          <Image
                            source={require('../Images/more.png')}
                            style={{
                              height: metrics.dimen_18,
                              width: metrics.dimen_18,
                              alignSelf: 'center',
                            }}
                          />
                        </View>
                      </View>
                    </TouchableOpacity>

                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: metrics.dimen_20,
                        justifyContent: 'space-evenly',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'center',
                          width: '30%',
                        }}>
                        <Image
                          source={require('../Images/income.png')}
                          style={{
                            height: metrics.dimen_18,
                            width: metrics.dimen_18,
                            alignSelf: 'center',
                          }}
                        />
                        <View
                          style={{
                            flexDirection: 'column',
                            justifyContent: 'center',
                            marginLeft: metrics.dimen_10,
                          }}>
                          <Text
                            style={{
                              ...styles.user_text,
                              fontSize: metrics.text_medium,
                            }}>
                            Received
                        </Text>
                          <Text
                            style={{ ...styles.user_text, color: colors.white }}>
                            {this.state.subtotalIncome}
                          </Text>
                        </View>
                      </View>

                      <View
                        style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <Image
                          source={require('../Images/outcome.png')}
                          style={{
                            height: metrics.dimen_18,
                            width: metrics.dimen_18,
                            alignSelf: 'center',
                          }}
                        />
                        <View
                          style={{
                            flexDirection: 'column',
                            justifyContent: 'center',
                            marginLeft: metrics.dimen_10,
                          }}>
                          <Text
                            style={{
                              ...styles.user_text,
                              fontSize: metrics.text_medium,
                            }}>
                            Spent
                        </Text>
                          <Text
                            style={{ ...styles.user_text, color: colors.white }}>
                            {this.state.subtotalOutcome}
                          </Text>
                        </View>
                      </View>

                      <View
                        style={{
                          marginLeft: metrics.dimen_15,
                          justifyContent: 'center',
                          backgroundColor: colors.light_grey_backgroud,
                          borderRadius: metrics.dimen_40,
                          width: 100,
                        }}>
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate('TransactionsList', {
                              user_id: this.state.data.user_id,
                              token: this.state.data.token,
                              available_Balance: this.state.available_Balance,
                              symbol: this.state.currency_symbol,
                            })
                          }>
                          <Text
                            style={{
                              ...styles.user_text,
                              color: colors.theme_caribpay,
                              fontSize: metrics.text_medium,
                              textAlign: 'center',
                              fontFamily: 'bold',
                            }}>
                            {'All Activity'}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View> */}
                </View>

                <View
                  style={{
                    justifyContent: "center",
                    alignSelf: "center",
                    backgroundColor: colors.white,
                    width: "95%",
                    height: 160,
                    borderRadius: metrics.dimen_5,
                    marginTop:
                      this.state.multiWalletData.length > 1 ? -90 : -110,
                    borderRadius: 7,
                    shadowColor: "white",
                    shadowOffset: { width: 1, height: 2 },
                    shadowOpacity: 0.9,
                    shadowRadius: 3,
                    elevation: 1,
                  }}
                >
                  <FlatList
                    style={{ marginTop: 5 }}
                    showsVerticalScrollIndicator={false}
                    data={Facilities_data}
                    numColumns={3}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                      <View
                        style={{
                          backgroundColor: colors.white,
                          margin: 10,
                          flex: 1 / 3,
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => this.onPressItems(item)}
                        >
                          <Image
                            source={item.image}
                            style={{
                              height: metrics.dimen_35,
                              width: metrics.dimen_35,
                              marginLeft: item.id == 2 ? 10 : 0,
                              alignSelf: "center",
                              backgroundColor: item.id > 3 ? "white" : null,
                              opacity: item.id == 4 ? 0.2 : 2,
                            }}
                          />
                          <Text
                            style={{
                              fontSize: metrics.text_normal,
                              fontFamily: metrics.quicksand_semibold,
                              color: colors.heading_black_text,
                              textAlign: "center",
                              marginTop: 2,
                            }}
                          >
                            {item.name}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                  />
                </View>

                {/* <View
                  style={{
                    width: Dimensions.get("screen").width - 20,
                    marginTop: 10,
                    alignSelf: "center",
                    borderColor:'red',
                    borderWidth:1,
                    borderRadius: metrics.dimen_5,
                  }}
                  onLayout={this.onLayout}
                >
                  <SliderBox
                    autoplay={true}
                    circleLoop
                    imageLoadingColor={colors.transparent}
                    images={this.state.bannerDataSource}
                    sliderBoxHeight={140}
                    ImageComponentStyle={{
                      height: 120,
                      width:"95%",
                      borderRadius: metrics.dimen_5,
                    }}
                  />
                </View> */}
                <SliderBox
                  // ImageComponent={FastImage}
                  images={this.state.bannerDataSource}
                  sliderBoxHeight={140}
                  onCurrentImagePressed={(index) =>
                    console.warn(`image ${index} pressed`)
                  }
                  dotColor={colors.theme_caribpay}
                  inactiveDotColor="#90A4AE"
                  paginationBoxVerticalPadding={20}
                  autoplay
                  circleLoop
                  resizeMethod={"resize"}
                  resizeMode={"cover"}
                  paginationBoxStyle={{
                    position: "absolute",
                    bottom: 0,
                    padding: 0,
                    alignItems: "center",
                    alignSelf: "center",
                    justifyContent: "center",
                    paddingVertical: 10,
                  }}
                  dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    marginHorizontal: 0,
                    padding: 0,
                    margin: 0,
                    backgroundColor: "rgba(128, 128, 128, 0.92)",
                  }}
                  ImageComponentStyle={{
                    borderRadius: 5,
                    width: "95%",
                    marginTop: 10,
                  }}
                  imageLoadingColor="#2196F3"
                />

                <Text
                  style={{
                    fontSize: metrics.text_16,
                    color: colors.theme_caribpay,
                    fontFamily: metrics.quicksand_bold,
                    marginTop: 2,
                    marginBottom: 2,
                    marginLeft: 10,
                  }}
                >
                  Offers
                </Text>

                {/* <View
                  style={{
                    width: Dimensions.get("screen").width - 30,
                    marginTop: 10,
                    alignSelf: "center",
                    borderRadius: metrics.dimen_5,
                  }}
                  onLayout={this.onLayout}
                >
                  <SliderBox
                    autoplay={true}
                    circleLoop
                    imageLoadingColor={colors.transparent}
                    images={this.state.bannerData2Source}
                    sliderBoxHeight={100}
                    parentWidth={Dimensions.get("screen").width - 26}
                    ImageComponentStyle={{
                      height: 100,
                      borderRadius: metrics.dimen_5,
                    }}
                  />
                </View> */}

                <SliderBox
                  // ImageComponent={FastImage}
                  images={this.state.bannerData2Source}
                  sliderBoxHeight={100}
                  onCurrentImagePressed={(index) =>
                    console.warn(`image ${index} pressed`)
                  }
                  dotColor={colors.theme_caribpay}
                  inactiveDotColor="#90A4AE"
                  paginationBoxVerticalPadding={20}
                  autoplay
                  circleLoop
                  resizeMethod={"resize"}
                  resizeMode={"cover"}
                  paginationBoxStyle={{
                    position: "absolute",
                    bottom: 0,
                    padding: 0,
                    alignItems: "center",
                    alignSelf: "center",
                    justifyContent: "center",
                    paddingVertical: 10,
                  }}
                  dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    marginHorizontal: 0,
                    padding: 0,
                    margin: 0,
                    backgroundColor: "rgba(128, 128, 128, 0.92)",
                  }}
                  ImageComponentStyle={{
                    borderRadius: 5,
                    width: "95%",
                    marginTop: 5,
                  }}
                  imageLoadingColor="#2196F3"
                />

                <Text
                  style={{
                    fontSize: metrics.text_16,
                    color: colors.theme_caribpay,
                    fontFamily: metrics.quicksand_bold,
                    marginTop: 2,
                    marginBottom: 5,
                    marginLeft: 10,
                  }}
                >
                  Discover More
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    alignSelf: "center",
                    marginTop: 7,
                    width: "100%",
                    height: 40,
                    marginBottom: 44,
                  }}
                >
                  <TouchableOpacity
                    style={{
                      flex: 1 / 4,
                      alignSelf: "center",
                      justifyContent: "center",
                    }}
                    onPress={() =>
                      this.setState({
                        facilittyName: "Fuel Services",
                        isComingSoon: true,
                      })
                    }
                  >
                    <View
                      style={{
                        alignSelf: "center",
                        justifyContent: "center",
                      }}
                    >
                      <View
                        style={{
                          height: 40,
                          width: 40,
                          borderRadius: 20,
                          backgroundColor: "#B0E0E6",
                          alignSelf: "center",
                          marginTop: 7,
                          justifyContent: "center",
                        }}
                      >
                        <Image
                          style={{
                            height: 35,
                            width: 35,
                            marginTop: 7,

                            resizeMode: "contain",
                            alignSelf: "center",
                          }}
                          source={require("../Images/fuelnew.png")}
                        />
                      </View>
                      <Text
                        style={{
                          fontSize: metrics.text_normal,
                          color: colors.heading_black_text,
                          textAlign: "center",
                          marginTop: 2,
                          fontFamily: metrics.quicksand_semibold,
                        }}
                      >
                        Fuel
                      </Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      flex: 1 / 4,
                      alignSelf: "center",
                      justifyContent: "center",
                    }}
                    onPress={() =>
                      this.setState({
                        facilittyName: "Facial Services",
                        isComingSoon: true,
                      })
                    }
                  >
                    <View
                      style={{
                        alignSelf: "center",
                        justifyContent: "center",
                      }}
                    >
                      <View
                        style={{
                          height: 40,
                          width: 40,
                          borderRadius: 20,
                          backgroundColor: "#B0E0E6",
                          alignSelf: "center",
                          marginTop: 7,
                          justifyContent: "center",
                        }}
                      >
                        <Image
                          style={{
                            height: 35,
                            width: 35,
                            resizeMode: "contain",
                            alignSelf: "center",
                            marginTop: 7,
                          }}
                          source={require("../Images/facialnew.png")}
                        />
                      </View>
                      <Text
                        style={{
                          fontSize: metrics.text_normal,
                          color: colors.heading_black_text,
                          textAlign: "center",
                          marginTop: 2,
                          fontFamily: metrics.quicksand_semibold,
                        }}
                      >
                        Facial
                      </Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      flex: 1 / 4,
                      alignSelf: "center",
                      justifyContent: "center",
                    }}
                    onPress={() =>
                      this.setState({
                        facilittyName: "Hair Cut Services",
                        isComingSoon: true,
                      })
                    }
                  >
                    <View
                      style={{
                        alignSelf: "center",
                        justifyContent: "center",
                      }}
                    >
                      <View
                        style={{
                          height: 40,
                          width: 40,
                          borderRadius: 20,
                          backgroundColor: "#B0E0E6",
                          alignSelf: "center",
                          justifyContent: "center",
                          marginTop: 7,
                        }}
                      >
                        <Image
                          style={{
                            height: 35,
                            width: 35,
                            resizeMode: "contain",
                            alignSelf: "center",
                            marginStart: 2,
                            marginTop: 7,
                          }}
                          source={require("../Images/haircutnew.png")}
                        />
                      </View>
                      <Text
                        style={{
                          fontSize: metrics.text_normal,
                          color: colors.heading_black_text,
                          textAlign: "center",
                          marginTop: 2,
                          fontFamily: metrics.quicksand_semibold,
                        }}
                      >
                        Hair cut
                      </Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      flex: 1 / 4,
                      alignSelf: "center",
                      justifyContent: "center",
                    }}
                    onPress={() =>
                      this.setState({
                        facilittyName: "Cafes Services",
                        isComingSoon: true,
                      })
                    }
                  >
                    <View
                      style={{
                        alignSelf: "center",
                        justifyContent: "center",
                      }}
                    >
                      <View
                        style={{
                          height: 40,
                          width: 40,
                          borderRadius: 20,
                          backgroundColor: "#B0E0E6",
                          alignSelf: "center",
                          justifyContent: "center",
                          marginTop: 7,
                        }}
                      >
                        <Image
                          style={{
                            height: 35,
                            width: 35,
                            resizeMode: "contain",
                            alignSelf: "center",
                            marginStart: 2,
                            marginTop: 7,
                          }}
                          source={require("../Images/cafenew.png")}
                        />
                      </View>
                      <Text
                        style={{
                          fontSize: metrics.text_normal,
                          color: colors.heading_black_text,
                          textAlign: "center",
                          marginTop: 2,
                          fontFamily: metrics.quicksand_semibold,
                        }}
                      >
                        Cafes
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>

                <View>
                  <Modal
                    backdropTransitionOutTiming={0}
                    backdropTransitionInTiming={0}
                    style={{ borderRadius: metrics.dimen_10 }}
                    isVisible={this.state.isVisible}
                  >
                    <View
                      style={{
                        width: "80%",
                        alignSelf: "center",
                        backgroundColor: colors.white,
                        borderRadius: metrics.dimen_5,
                        margin: metrics.dimen_20,
                      }}
                    >
                      <View
                        style={{
                          justifyContent: "center",
                          height: metrics.dimen_60,
                          backgroundColor: colors.carib_pay_blue,
                        }}
                      >
                        <Text
                          style={{
                            color: colors.white,
                            fontWeight: "bold",
                            fontSize: metrics.text_17,
                            textAlign: "center",
                          }}
                        >
                          Confirm!
                        </Text>
                      </View>
                      <View
                        style={{
                          alignSelf: "center",
                          margin: metrics.dimen_15,
                          flexDirection: "column",
                        }}
                      >
                        <Text
                          style={{
                            color: colors.heading_black_text,
                            fontSize: metrics.text_header,
                            textAlign: "center",
                          }}
                        >
                          {"Please complete your KYC\nprocess"}
                        </Text>
                        <View
                          style={{
                            margin: metrics.dimen_15,
                            flexDirection: "row",
                            justifyContent: "center",
                          }}
                        >
                          <TouchableOpacity
                            onPress={() => this.setState({ isVisible: false })}
                          >
                            <View
                              style={{
                                width: metrics.dimen_120,
                                backgroundColor: colors.carib_pay_blue,
                                borderRadius: metrics.dimen_7,
                                height: metrics.dimen_40,
                                justifyContent: "center",
                              }}
                            >
                              <Text
                                style={{
                                  paddingHorizontal: metrics.dimen_10,
                                  fontSize: metrics.text_header,
                                  color: colors.white,
                                  textAlign: "center",
                                  fontWeight: "900",
                                }}
                              >
                                LATER
                              </Text>
                            </View>
                          </TouchableOpacity>
                          <TouchableOpacity
                            onPress={() =>
                              this.setState({ isVisible: false }, () =>
                                this.props.navigation.navigate("UpdateKYC")
                              )
                            }
                          >
                            <View
                              style={{
                                width: metrics.dimen_120,
                                backgroundColor: colors.carib_pay_blue,
                                borderRadius: metrics.dimen_7,
                                height: metrics.dimen_40,
                                marginLeft: 10,
                                justifyContent: "center",
                              }}
                            >
                              <Text
                                style={{
                                  paddingHorizontal: metrics.dimen_10,
                                  fontSize: metrics.text_header,
                                  color: colors.white,
                                  textAlign: "center",
                                  fontWeight: "900",
                                }}
                              >
                                PROCEED
                              </Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </Modal>
                </View>

                <View>
                  <Modal
                    style={{ borderRadius: metrics.dimen_10 }}
                    isVisible={this.state.isComingSoon}
                  >
                    <View
                      style={{
                        width: "90%",
                        alignSelf: "center",
                        backgroundColor: colors.white,
                        borderRadius: metrics.dimen_10,
                        // margin: metrics.dimen_10,
                        flexDirection: "column",
                        height: 200,
                        justifyContent: "center",
                      }}
                    >
                      <View style={{ flex: 1 }}>
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({
                              isComingSoon: false,
                            })
                          }
                        >
                          <Image
                            style={{
                              height: metrics.dimen_16,
                              width: metrics.dimen_16,
                              resizeMode: "contain",
                              margin: 15,
                            }}
                            source={require("../Images/cross-sign.png")}
                          />
                        </TouchableOpacity>
                        <Image
                          style={{
                            height: 150,
                            width: 150,
                            resizeMode: "cover",
                            alignSelf: "center",
                            marginTop: -30,
                          }}
                          source={require("../Images/soon.png")}
                        />
                        <Text
                          style={{
                            fontSize: metrics.text_heading,
                            color: colors.black,
                            fontFamily: metrics.quicksand_bold,
                            textAlign: "center",
                            marginTop: -20,
                          }}
                        >
                          {this.state.facilittyName}
                        </Text>
                      </View>
                    </View>
                  </Modal>
                </View>

                <View>
                  <Modal
                    style={{ borderRadius: metrics.dimen_10 }}
                    isVisible={this.state.enablePasscode}
                  >
                    <View
                      style={{
                        width: "90%",
                        alignSelf: "center",
                        backgroundColor: colors.white,
                        borderRadius: metrics.dimen_10,
                        margin: metrics.dimen_20,
                        flexDirection: "column",
                        height: 300,
                        justifyContent: "center",
                      }}
                    >
                      <View style={{ flex: 1 }}>
                        <View>
                          <TouchableOpacity
                            onPress={() =>
                              this.setState({
                                enablePasscode: false,
                              })
                            }
                          >
                            <Image
                              style={{
                                height: metrics.dimen_16,
                                width: metrics.dimen_16,
                                resizeMode: "contain",
                                margin: 15,
                              }}
                              source={require("../Images/cross-sign.png")}
                            />
                          </TouchableOpacity>
                          <Image
                            style={{
                              height: metrics.dimen_80,
                              width: metrics.dimen_100,
                              resizeMode: "contain",
                              alignSelf: "center",
                              marginTop: 20,
                            }}
                            source={require("../Images/pincode.png")}
                          />
                          <Text
                            style={{
                              fontSize: metrics.text_heading,
                              color: colors.black,
                              fontFamily: metrics.quicksand_bold,
                              // fontWeight: 'bold',
                              textAlign: "center",
                              margin: 20,
                            }}
                          >
                            Do you want to set passcode now?
                          </Text>
                        </View>

                        <View style={styles.bottomview}>
                          <TouchableOpacity
                            style={{
                              width: "90%",
                              backgroundColor: colors.theme_caribpay,
                              borderRadius: 25,
                              justifyContent: "center",
                            }}
                            onPress={() =>
                              this.setState({ enablePasscode: false }, () =>
                                this.props.navigation.navigate("SetPasscode", {
                                  user_id: this.state.data.user_id,
                                  token: this.state.data.token,
                                  fromForgetpassword: false,
                                })
                              )
                            }
                          >
                            <Text
                              style={{
                                fontSize: 15,
                                color: "white",
                                fontFamily: metrics.quicksand_semibold,
                                // fontWeight: 'bold',
                                textAlign: "center",
                                alignSelf: "center",
                              }}
                            >
                              {"Set Passcode "}
                            </Text>
                          </TouchableOpacity>

                          {/* <TouchableOpacity
                              style={{
                                width: '40%',
                                backgroundColor: colors.theme_caribpay,
                                borderRadius: 4,
                                justifyContent: 'center',
                                marginLeft: 10
                              }}
                              onPress={() =>
                                this.setState({ enablePasscode: false })
                              }>
                              <Text
                                style={{
                                  fontSize: 15,
                                  color: 'white',
                                  fontWeight: 'bold',
                                  textAlign: 'center',
                                  alignSelf: 'center',
                                }}>
                                {"LATER "}
                              </Text>
                            </TouchableOpacity> */}
                        </View>
                      </View>
                    </View>
                  </Modal>
                </View>

                <View>
                  <Modal
                    style={{ borderRadius: metrics.dimen_10 }}
                    isVisible={this.state.appUpdates}
                  >
                    <View
                      style={{
                        width: "90%",
                        alignSelf: "center",
                        backgroundColor: colors.white,
                        borderRadius: metrics.dimen_10,
                        margin: metrics.dimen_20,
                        flexDirection: "column",
                        height: 300,
                        justifyContent: "center",
                      }}
                    >
                      <View style={{ flex: 1 }}>
                        <View>
                          <Image
                            style={{
                              height: metrics.dimen_80,
                              width: metrics.dimen_100,
                              resizeMode: "contain",
                              alignSelf: "center",
                              marginTop: 20,
                            }}
                            source={require("../Images/logo.png")}
                          />
                          <Text
                            style={{
                              fontSize: metrics.text_heading,
                              color: colors.black,
                              fontWeight: "bold",
                              textAlign: "center",
                              margin: 10,
                            }}
                          >
                            New App Updates?
                          </Text>
                          <Text
                            style={{
                              fontSize: metrics.text_normal,
                              color: colors.app_gray,
                              margin: 10,
                              fontWeight: "bold",
                              textAlign: "center",
                              width: 200,
                              alignSelf: "center",
                            }}
                          >
                            {
                              "Oops, looks like you are on the old version of app.Update to the new & improved version now!"
                            }
                          </Text>
                        </View>

                        <View style={styles.bottomview}>
                          <View
                            style={{
                              width: "45%",
                              backgroundColor: colors.theme_caribpay,
                              borderRadius: 10,
                              justifyContent: "center",
                            }}
                          >
                            <TouchableOpacity
                              onPress={() =>
                                this.setState({ appUpdates: false }, () =>
                                  Linking.openURL(
                                    "market://details?id=carib.pay.com"
                                  )
                                )
                              }
                            >
                              <Text
                                style={{
                                  fontSize: 15,
                                  color: "white",
                                  fontWeight: "bold",
                                  textAlign: "center",
                                  alignSelf: "center",
                                }}
                              >
                                Update
                              </Text>
                            </TouchableOpacity>
                          </View>

                          <View
                            style={{
                              width: "45%",
                              marginLeft: 10,
                              backgroundColor: colors.theme_caribpay,
                              borderRadius: 10,
                              justifyContent: "center",
                            }}
                          >
                            <TouchableOpacity
                              onPress={() =>
                                this.setState({ appUpdates: false })
                              }
                            >
                              <Text
                                style={{
                                  fontSize: 15,
                                  color: "white",
                                  fontWeight: "bold",
                                  textAlign: "center",
                                  alignSelf: "center",
                                  marginLeft: 10,
                                }}
                              >
                                Later
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    </View>
                  </Modal>
                </View>

                <Modal
                  style={{ width: metrics.dimen_300, alignSelf: "center" }}
                  isVisible={this.state.multipleEnable}
                >
                  <View
                    style={{
                      backgroundColor: "white",
                      borderRadius: metrics.dimen_7,
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "column",
                        height: metrics.dimen_60,
                        backgroundColor: colors.carib_pay_blue,
                        justifyContent: "center",
                      }}
                    >
                      <Text
                        style={{
                          fontSize: metrics.text_heading,
                          color: colors.white,
                          padding: 20,
                          fontWeight: "bold",
                          textAlign: "center",
                        }}
                      >
                        Currency
                      </Text>
                    </View>
                    <View style={{ flexDirection: "column" }}>
                      <View
                        style={{ margin: 20, marginLeft: metrics.dimen_30 }}
                      >
                        <RadioForm
                          radio_props={this.state.multipleCurrency}
                          initial={this.state.radioPosition}
                          buttonColor={colors.carib_pay_blue}
                          buttonSize={metrics.dimen_20}
                          onPress={(value) => {
                            this.setState({ selectedCurrency: value });
                          }}
                        />
                      </View>
                      <View style={styles.horizontalLine} />
                    </View>

                    <View
                      style={{
                        margin: metrics.dimen_15,
                        flexDirection: "row",
                        justifyContent: "center",
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.setState({ multipleEnable: false })}
                      >
                        <View
                          style={{
                            width: metrics.dimen_120,
                            backgroundColor: colors.carib_pay_blue,
                            borderRadius: metrics.dimen_7,
                            height: metrics.dimen_40,
                            justifyContent: "center",
                          }}
                        >
                          <Text
                            style={{
                              paddingHorizontal: metrics.dimen_10,
                              fontSize: metrics.text_header,
                              color: colors.white,
                              textAlign: "center",
                              fontWeight: "900",
                            }}
                          >
                            CANCEL
                          </Text>
                        </View>
                      </TouchableOpacity>

                      <TouchableOpacity
                        onPress={() =>
                          this.setState({ multipleEnable: false }, () =>
                            this.onchangeCurrencyType()
                          )
                        }
                      >
                        <View
                          style={{
                            width: metrics.dimen_120,
                            backgroundColor: colors.carib_pay_blue,
                            borderRadius: metrics.dimen_7,
                            height: metrics.dimen_40,
                            marginLeft: 10,
                            justifyContent: "center",
                          }}
                        >
                          <Text
                            style={{
                              paddingHorizontal: metrics.dimen_10,
                              fontSize: metrics.text_header,
                              color: colors.white,
                              textAlign: "center",
                              fontWeight: "900",
                            }}
                          >
                            OK
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </Modal>
              </View>

              <View>
                <Modal
                  style={{ borderRadius: metrics.dimen_10 }}
                  isVisible={this.state.isPayBills}
                  onBackdropPress={() =>
                    this.setState({
                      isPayBills: false,
                    })
                  }
                >
                  <View
                    style={{
                      width: "90%",
                      alignSelf: "center",
                      backgroundColor: colors.white,
                      borderRadius: 20,
                      margin: metrics.dimen_20,
                      flexDirection: "column",
                      height: 250,
                      justifyContent: "center",
                    }}
                  >
                    <Text
                      style={{
                        fontSize: metrics.text_large,
                        color: colors.heading_black_text,
                        textAlign: "center",
                        marginTop: 10,
                        fontFamily: metrics.quicksand_bold,
                      }}
                    >
                      {"Pay Bills"}
                    </Text>

                    <FlatList
                      showsVerticalScrollIndicator={false}
                      data={PayBillsData}
                      numColumns={3}
                      keyExtractor={(item, index) => index.toString()}
                      renderItem={({ item }) => (
                        <View
                          style={{
                            // backgroundColor: colors.light_grey_backgroud,
                            margin: metrics.dimen_5,
                            flex: 1 / 3,
                            justifyContent: "center",
                            height: metrics.dimen_100,
                            width: metrics.dimen_100,
                            // borderRadius: metrics.dimen_7,
                          }}
                        >
                          <TouchableOpacity
                            onPress={() => this.goPayBills(item)}
                          >
                            <Image
                              source={item.image}
                              style={{
                                height: metrics.dimen_40,
                                width: metrics.dimen_40,
                                alignSelf: "center",
                              }}
                            />
                            <Text
                              style={{
                                fontSize: metrics.text_medium,
                                fontFamily: metrics.quicksand_semibold,
                                color: colors.heading_black_text,
                                textAlign: "center",
                                marginTop: 2,
                              }}
                            >
                              {item.name}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      )}
                    />
                  </View>
                </Modal>
              </View>
            </View>
          </ScrollView>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whitesmoke,
  },
  image_user_style: {
    height: metrics.dimen_40,
    width: metrics.dimen_40,
    resizeMode: "contain",
  },
  user_text: {
    fontFamily: metrics.quicksand_medium,
    fontSize: metrics.text_normal,
  },
  scan_bell_style: {
    height: metrics.dimen_20,
    width: metrics.dimen_20,
    resizeMode: "contain",
    alignSelf: "center",
    tintColor: colors.white,
  },
  available_bal: {
    fontSize: metrics.text_22,
    color: colors.theme_caribpay,
    marginTop: 5,
    // fontWeight: "bold",
    fontFamily: metrics.quicksand_bold,
    textAlignVertical: "center",
  },
  horizontalLine: {
    borderBottomColor: "#D3D3D3",
    borderBottomWidth: 0.5,
    width: "90%",
    alignSelf: "center",
    marginTop: 20,
  },
  bottomview: {
    bottom: 20,
    position: "absolute",
    color: colors.theme_caribpay,
    height: 40,
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_10,
    flexDirection: "row",
  },
  bottomview2: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_5,
  },
});
