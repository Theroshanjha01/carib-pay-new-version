import React from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import { slides } from "../constant.js";
import AppIntroSlider from "react-native-app-intro-slider";
import Metrics from "../Themes/Metrics.js";
import metrics from "../Themes/Metrics.js";
import colors from "../Themes/Colors.js";
import DeviceInfo from "react-native-device-info";
import { getDeviceInfoApi } from "./CaribMall/Utils/api.constants.js";
import fetchPost from "../fetchPost.js";
import fetchGet from "../fetchGet.js";
// import Colors from '../Themes/Colors.js';

export default class IntroSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _renderItem = ({ item }) => {
    return (
      <View style={{ flex: 1, color: colors.lightWhite }}>
        {item.skip && (
          <View
            style={{
              alignItems: "flex-end",
              height: 40,
              justifyContent: "center",
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Login")}
            >
              <Text style={{ textAlign: "right", marginRight: 20 }}>SKIP</Text>
            </TouchableOpacity>
          </View>
        )}
        <View
          style={{ flex: 1, justifyContent: "center", alignContent: "center" }}
        >
          <Image
            style={{
              height: Metrics.dimen_220,
              width: Metrics.dimen_220,
              alignSelf: "center",
              resizeMode: "contain",
            }}
            source={item.image}
          />
          <Text
            style={{
              color: "#283142",
              fontSize: metrics.text_22,
              textAlign: "center",
              marginTop: 20,
              fontWeight: metrics.quicksand_semibold,
              fontWeight: "bold",
            }}
          >
            {item.title}
          </Text>
          <Text
            style={{
              width: "80%",
              alignSelf: "center",
              color: "#808080",
              fontSize: 18,
              textAlign: "center",
              marginTop: 20,
              fontFamily: Metrics.quicksand_bold,
            }}
          >
            {item.text}
          </Text>
          {!item.skip && (
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Login")}
            >
              <View
                style={{
                  backgroundColor: colors.theme_caribpay,
                  justifyContent: "center",
                  alignSelf: "center",
                  width: "50%",
                  borderRadius: 20,
                  height: 40,
                  marginTop: 100,
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: 15,
                    color: "white",
                    fontWeight: "bold",
                  }}
                >
                  GET STARTED
                </Text>
              </View>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };

  _onDone = () => {};

  // async getDeviceInfo() {
  //   let device_id =  DeviceInfo.getUniqueId();
  //   const response = await fetchGet(`${getDeviceInfoApi}?device_id=${device_id}`);
  //   console.log('responsed', response);
  // }
  render() {
    return (
      <AppIntroSlider
        renderItem={this._renderItem}
        data={slides}
        onDone={this._onDone}
        activeDotStyle={{ backgroundColor: colors.theme_caribpay }}
      />
    );
  }
}
