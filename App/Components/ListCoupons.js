import * as React from "react";
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  BackHandler,
} from "react-native";
import colors from "../Themes/Colors";
import metrics from "../Themes/Metrics";

export default class ListCoupons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isCategory: false,
      isCategorySub: false,
      isItemClicked: false,
      categoryData: [],
      couponName: this.props.navigation.state.params.name,
      iamgeUrl: this.props.navigation.state.params.image,
    };
  }

  componentDidMount() {
    let dataCategoryUpto40K = [
      {
        title: "Voucher Worth",
        vouceherwrth: "$ 1,000",
        caption: "15%  Cashback",
        image: this.state.iamgeUrl,
      },
      {
        title: "Voucher Worth",
        vouceherwrth: "$ 5,000",
        caption: "15%  Cashback",
        image: this.state.iamgeUrl,
      },
      {
        title: "Voucher Worth",
        vouceherwrth: "$ 2,000",
        caption: "15%  Cashback",
        image: this.state.iamgeUrl,
      },
      {
        title: "Voucher Worth",
        vouceherwrth: "$ 1,000",
        caption: "15%  Cashback",
        image: this.state.iamgeUrl,
      },
      {
        title: "Voucher Worth",
        vouceherwrth: "$ 4,000",
        caption: "15%  Cashback",
        image: this.state.iamgeUrl,
      },
      {
        title: "Voucher Worth",
        vouceherwrth: "$ 600",
        caption: "15%  Cashback",
        image: this.state.iamgeUrl,
      },
    ];

    let data = dataCategoryUpto40K.map((item, index) => {
      item.idNum = ++index;
      return { ...item };
    });

    this.setState({
      categoryData: data,
    });

    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  handleSelection = (item, idNum) => {
    var selectedid = this.state.selectedID;
    if (selectedid === idNum) {
      this.setState({
        selectedID: null,
      });
    } else {
      this.setState({
        selectedID: idNum,
      });
    }
  };

  render() {
    return (
      <View style={styles.scene}>
        <ScrollView>
          <View
            style={{
              height: metrics.dimen_55,
              backgroundColor: colors.theme_caribpay,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity
                style={{
                  marginLeft: 10,
                  height: metrics.dimen_24,
                  width: metrics.dimen_24,
                  resizeMode: "contain",
                  alignSelf: "center",
                }}
                onPress={() => this.props.navigation.goBack(null)}
              >
                <Image
                  style={{
                    height: metrics.dimen_24,
                    width: metrics.dimen_24,
                    resizeMode: "contain",
                    alignSelf: "center",
                  }}
                  source={require("../Images/leftarrow.png")}
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: metrics.text_17,
                  fontFamily: metrics.quicksand_semibold,
                  color: colors.white,
                  marginLeft: 10,
                  alignSelf: "center",
                  marginTop: -5,
                }}
              >
                {this.state.couponName + " Vouchers"}
              </Text>
            </View>

            <View style={{ flexDirection: "row", marginRight: 15 }}>
              <Image
                style={{
                  marginLeft: 10,
                  height: metrics.dimen_28,
                  width: metrics.dimen_28,
                  resizeMode: "contain",
                  alignSelf: "center",
                  tintColor: colors.white,
                }}
                source={require("../Images/find.png")}
              />
              <Image
                style={{
                  marginLeft: 15,
                  height: metrics.dimen_22,
                  width: metrics.dimen_22,
                  resizeMode: "contain",
                  alignSelf: "center",
                  tintColor: colors.white,
                }}
                source={require("../Images/heart.png")}
              />
              <Image
                style={{
                  marginLeft: 15,
                  height: metrics.dimen_24,
                  width: metrics.dimen_24,
                  resizeMode: "contain",
                  alignSelf: "center",
                  tintColor: colors.white,
                }}
                source={require("../Images/supermarket.png")}
              />
            </View>
          </View>

          <View
            style={{
              width: "95%",
              alignSelf: "center",
              borderRadius: 10,
              marginTop: 10,
            }}
          >
            <Image
              style={{
                width: "100%",
                height: metrics.dimen_200,
                borderRadius: 10,
              }}
              source={{ uri: this.state.iamgeUrl }}
            />
          </View>

          <View style={{ marginBottom: 20 }}>
            <Text
              style={{
                marginTop: 10,
                marginLeft:10,
                fontSize: metrics.text_17,
                fontFamily: metrics.quicksand_bold,
              }}
            >
              {"Pick a voucher , EMI available!"}
            </Text>

            <FlatList
              data={this.state.categoryData}
              keyExtractor={(item, index) => index.toString()}
              numColumns={2}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={{
                    margin: 10,
                    flex: 1 / 2,
                    backgroundColor: colors.white,
                    borderRadius: 10,
                  }}
                  onPress={() =>
                    this.props.navigation.navigate("CouponsDetails", {
                      data: item,
                      brandName:this.state.couponName
                    })
                  }
                >
                  <View
                    style={{
                      margin: 10,
                      flex: 1 / 2,
                      backgroundColor: colors.white,
                      borderRadius: 10,
                    }}
                  >
                    <Image
                      style={{
                        height: 100,
                        width: 100,
                        alignSelf: "center",
                        borderRadius: 10,
                        resizeMode: "contain",
                      }}
                      source={{ uri: item.image }}
                    />
                    <Text
                      style={{
                        fontSize: metrics.text_17,
                        fontFamily: metrics.quicksand_bold,
                        color: colors.app_blue,
                        alignSelf: "center",
                      }}
                    >
                      {item.title}
                    </Text>
                    <Text
                      style={{
                        fontSize: metrics.text_large,
                        fontFamily: metrics.quicksand_bold,
                        color: colors.app_black_text,
                        alignSelf: "center",
                      }}
                    >
                      {item.vouceherwrth}
                    </Text>

                    <Text
                      style={{
                        fontSize: metrics.text_16,
                        fontFamily: metrics.quicksand_bold,
                        color: colors.theme_orange_color_caribpay,
                        alignSelf: "center",
                      }}
                    >
                      {item.caption}
                    </Text>

                    <View
                      style={{
                        alignItems: "center",
                        height: 30,
                        borderRadius: 15,
                        justifyContent: "center",
                        width: "50%",
                        backgroundColor: colors.theme_caribpay,
                        alignSelf: "center",
                        marginTop: 10,
                        marginBottom: 10,
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: metrics.quicksand_bold,
                          color: colors.white,
                        }}
                      >
                        Buy Now
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>

          {/* {this.state.isCategory && (
            <View style={{ marginBottom: 20 }}>
              <FlatList
                data={this.state.categoryData}
                keyExtractor={(item, index) => index.toString()}
                extraData={this.state.selectedID}
                ItemSeparatorComponent={() => (
                  <View
                    style={{
                      borderBottomWidth: 0.5,
                      borderBottomColor: colors.app_gray,
                      marginTop: 15,
                      marginBottom: 15,
                    }}
                  />
                )}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    onPress={() => this.handleSelection(item, item.idNum)}
                  >
                    <View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                        }}
                      >
                        <View style={{ flexDirection: "row" }}>
                          <Image
                            style={{
                              width: metrics.dimen_40,
                              height: metrics.dimen_40,
                              margin: 10,
                              //   tintColor: colors.theme_caribpay,
                            }}
                            source={item.image}
                          />

                          <View style={{ marginLeft: 10 }}>
                            <Text
                              style={{
                                fontSize: metrics.text_normal,
                                fontFamily: metrics.quicksand_bold,
                                marginTop: 5,
                              }}
                            >
                              {item.title}
                            </Text>

                            <Text
                              style={{
                                fontSize: metrics.text_normal,
                                fontFamily: metrics.quicksand_bold,
                                color: colors.app_gray,
                              }}
                            >
                              {item.caption}
                            </Text>
                          </View>
                        </View>

                        <View style={{ margin: 10 }}>
                          <TouchableOpacity
                            onPress={() =>
                              this.handleSelection(item, item.idNum)
                            }
                          >
                            <Image
                              style={{
                                height: metrics.dimen_15,
                                width: metrics.dimen_15,
                                resizeMode: "contain",
                                alignSelf: "center",
                                tintColor: colors.app_gray,
                              }}
                              source={
                                item.idNum == this.state.selectedID
                                  ? require("../Images/upbtn.png")
                                  : require("../Images/downbtn.png")
                              }
                            />
                          </TouchableOpacity>
                        </View>
                      </View>

                      {item.idNum == this.state.selectedID && (
                        <FlatList
                          data={fashionSubData}
                          keyExtractor={(item, index) => index.toString()}
                          numColumns={3}
                          renderItem={({ item }) => (
                            <View style={{ margin: 10, flex: 1 / 3 }}>
                              <Image
                                style={{
                                  height: 60,
                                  width: 60,
                                  alignSelf: "center",
                                  //   borderRadius: 30,
                                  resizeMode: "contain",
                                }}
                                source={{ uri: item.image }}
                              />
                              <Text
                                style={{
                                  fontSize: metrics.text_medium,
                                  fontFamily: metrics.quicksand_bold,
                                  color: colors.app_gray,
                                  alignSelf: "center",
                                }}
                              >
                                {item.title}
                              </Text>
                              <Text
                                style={{
                                  fontSize: metrics.text_medium,
                                  fontFamily: metrics.quicksand_bold,
                                  color: colors.green,
                                  alignSelf: "center",
                                }}
                              >
                                {item.caption}
                              </Text>
                            </View>
                          )}
                        />
                      )}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          )}

          <View>
            <View>
              <Text
                style={{
                  margin: 10,
                  fontSize: metrics.text_17,
                  fontFamily: metrics.quicksand_bold,
                }}
              >
                {"Just Arrived Brands >> "}
              </Text>
            </View>

            <FlatList
              data={justArrviedData}
              keyExtractor={(item, index) => index.toString()}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item }) => (
                <View style={{ margin: 10 }}>
                  <Image
                    style={{
                      height: 150,
                      width: 150,
                      alignSelf: "center",
                      borderRadius: 7,
                    }}
                    source={{ uri: item.image_url }}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      fontFamily: metrics.quicksand_bold,
                      color: colors.app_gray,
                      alignSelf: "center",
                    }}
                  >
                    {item.cashback}
                  </Text>
                </View>
              )}
            />
          </View>

          <View style={{ marginBottom: 20 }}>
            <Text
              style={{
                margin: 10,
                fontSize: metrics.text_17,
                fontFamily: metrics.quicksand_bold,
              }}
            >
              {"Upto $4,000 Cashback"}
            </Text>

            <FlatList
              data={dataCategoryUpto40K}
              keyExtractor={(item, index) => index.toString()}
              numColumns={3}
              renderItem={({ item }) => (
                <View style={{ margin: 10, flex: 1 / 3 }}>
                  <Image
                    style={{
                      height: 60,
                      width: 60,
                      alignSelf: "center",
                      borderRadius: 30,
                      resizeMode: "contain",
                    }}
                    source={item.image}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_medium,
                      fontFamily: metrics.quicksand_bold,
                      color: colors.app_gray,
                      alignSelf: "center",
                    }}
                  >
                    {item.title}
                  </Text>
                </View>
              )}
            /> */}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
    backgroundColor: colors.light_grey_backgroud,
  },
});
