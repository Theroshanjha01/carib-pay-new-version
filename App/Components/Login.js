import React from "react";
import {
  BackHandler,
  Dimensions,
  FlatList,
  Image,
  Keyboard,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Input } from "react-native-elements";
import metrics from "../Themes/Metrics.js";
import colors from "../Themes/Colors.js";
import Toast from "react-native-easy-toast";
import RBSheet from "react-native-raw-bottom-sheet";
import AsyncStorage from "@react-native-community/async-storage";
import { showAlert } from "../Utils.js";
import CountryData from "country-data";
import TouchID from "react-native-touch-id";
import SmoothPinCodeInput from "react-native-smooth-pincode-input";
import Modal from "react-native-modal";
import RadioButton from "react-native-radio-button";
import NetInfo from "@react-native-community/netinfo";
import messaging from "@react-native-firebase/messaging";
import DeviceInfo from "react-native-device-info";
import fetchPost from "../fetchPost.js";
import { logWithTouchApi } from "./CaribMall/Utils/api.constants.js";

const axios = require("axios");

var Spinner = require("react-native-spinkit");

const optionalConfigObject = {
  title: "Biometric Authentication", // Android
  imageColor: colors.app_dark_green_color, // Android
  imageErrorColor: "#ff0000", // Android
  sensorDescription: "Touch the fingerprint sensor", // Android
  sensorErrorDescription: "Failed", // Android
  cancelText: "Cancel", // Androi
  fallbackLabel: "Show Passcode", // iOS (if empty, then label is hidden)
  unifiedErrors: false, // use unified error messages (default false)
  passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
};

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      touchIdEnable: this.props.navigation.state.params
        ?.FaceIDEnabletouchIdEnable,
      FaceIDEnable: this.props.navigation.state.params
        ?.FaceIDEnabletouchIdEnable,
      passcode: this.props.navigation.state.params?.passcode,
      visible: false,
      currencycode: "",
      email: "",
      phone: "",
      password: "",
      hide: true,
      fromMobilNo: true,
      fromEmail: false,
      countryDataSource: [],
      newData: [],
      text: "",
      arrayholder: [],
      showAlert: true,
      isoName: "",
      code: "",
      wrong: false,
      invalid: false,
      userblocked: false,
      showKeyboard: false,
      multipleLogin: false,
      loginAttempts: 0,
      deviceID: "",
      fcmToken: "",
      connection_Status: false,
      user_id: "",
    };
    this._keyboardDidHide = this._keyboardDidHide.bind(this);
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
  }

  async componentDidMount() {
    NetInfo.fetch().then((state) => {
      if (state.isConnected == false) {
        this.RBSheetAdd.open();
      } else {
        console.log("connection true");
      }
    });

    axios
      .get("https://topups.reloadly.com/countries")
      .then((response) => {
        AsyncStorage.setItem(
          "countryDataSource",
          JSON.stringify(response.data)
        );
        this.setState({
          countryDataSource: response.data,
        });
        this.state.arrayholder = response.data;
      })
      .catch((err) => {
        console.warn(err);
      });

    axios
      .get("https://ipinfo.io/json")
      .then((response) => {
        let country_code = response.data.country;
        let countrycallingcode = JSON.stringify(
          CountryData.countries[country_code].countryCallingCodes[0]
        );
        this.setState({
          currencycode: countrycallingcode.replace(/"/g, ""),
          isoName: country_code,
        });
      })
      .catch((error) => {
        console.log(error);
      });

    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.setCredentials();
      }
    );

    // let uniqueId = await AsyncStorage.getItem('DeviceId');

    let uniqueId = DeviceInfo.getUniqueId();
    this.setState(
      {
        deviceID: uniqueId,
      },
      () => AsyncStorage.setItem("DeviceId", this.state.deviceID)
    );

    const authStatus = await messaging().requestPermission({
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      provisional: false,
      sound: true,
    });

    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    console.log("LOG: getToken status: ", enabled, uniqueId);

    if (enabled) {
      const fcm_Token = await messaging().getToken();

      await AsyncStorage.setItem("fcmToken", fcm_Token);

      console.log("LOG: getToken status: ", fcm_Token);

      this.setState({
        deviceID: uniqueId,
        fcmToken: fcm_Token,
      });

      console.log(this.state.deviceID, this.state.fcmToken);
    }

    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  setCredentials = async () => {
    let touchIdEnable = this.state.touchIdEnable;
    // let touchIdEnable = await AsyncStorage.getItem("TouchIDEnable");
    // let FaceIDEnable = await AsyncStorage.getItem("FaceIDEnable");
    let ispasscode = this.state.passcode;
    // let ispasscode = await AsyncStorage.getItem("passcode");
    // ispasscode = JSON.parse(ispasscode);
    let deviceId = await AsyncStorage.getItem("DeviceId");
    let fcm_Token = await AsyncStorage.getItem("fcmToken");

    this.setState({
      deviceID: deviceId == null ? this.state.deviceID : deviceId,
      fcmToken: fcm_Token,
    });

    if (ispasscode != null && !touchIdEnable) {
      this.setState({
        passcode: ispasscode,
      });
      setTimeout(() => {
        this.RBSheetPasscode.open();
      }, 300);

      console.log("test 0", ispasscode);
    }
    // if (touchIdEnable == "1") {
    if (touchIdEnable) {
      TouchID.authenticate("Please authenticate", optionalConfigObject).then(
        (success) => {
          if (success == true) {
            let respo = this.CallApiOnTouchId();
            // this.storeloginWithPasscode(respo.data.response);
            // console.warn("check in", respo);
            // this.props.navigation.navigate("Home");
          }
        }
      );
    }
    // if (FaceIDEnable == "1") {
    //   TouchID.authenticate("Please authenticate", optionalConfigObject).then(
    //     (success) => {
    //       if (success == true) {
    //         this.props.navigation.navigate("Home");
    //       }
    //     }
    //   );
    // }
  };
  async CallApiOnTouchId() {
    let phone_device_id = DeviceInfo.getUniqueId();
    console.log("phone device id ==> ", phone_device_id);
    let postData = {
      device_id: phone_device_id,

    };

    console.log("parmas  enable touch -- > ", postData);

    axios({
      method: "post",
      url: "https://sandbox.caribpayintl.com/api/login-with-touch",
      headers: { Authorization: this.state.token },
      data: postData,
    })
      .then((response) => {
        console.log(" response enable touch -- > ", response.data.response);
        this.storeloginWithPasscode(response.data.response)
      })
      .catch((err) => {
        // this.setState({ visible: false });
        console.log(err, "error hai " + err);
      });
  }
  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow() {
    this.setState({
      showKeyboard: true,
    });
  }

  _keyboardDidHide() {
    this.setState({
      showKeyboard: false,
    });
  }

  handleBackPress = async () => {
    BackHandler.exitApp();
  };

  SearchFilterFunction(text) {
    const newData = this.state.arrayholder.filter((item) => {
      const itemData = `${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({ countryDataSource: newData, text: text });
  }

  renderCountries = () => {
    return (
      <View>
        <View
          style={{
            height: metrics.dimen_60,
            backgroundColor: colors.theme_caribpay,
            flexDirection: "row",
          }}
        >
          <TouchableOpacity
            style={{
              height: metrics.dimen_25,
              width: metrics.dimen_25,
              marginStart: metrics.dimen_10,
              alignSelf: "center",
            }}
            onPress={() => this.RBSheetAlert.close()}
          >
            <Image
              style={{
                height: metrics.dimen_25,
                width: metrics.dimen_25,
                alignSelf: "center",
                paddingHorizontal: 10,
              }}
              source={require("../Images/leftarrow.png")}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: metrics.text_16,
              color: "white",
              fontWeight: "bold",
              textAlign: "center",
              alignSelf: "center",
              marginLeft: 10,
            }}
          >
            Select Country
          </Text>
        </View>
        <View
          style={{
            marginTop: metrics.dimen_5,
            width: "95%",
            alignSelf: "center",
          }}
        >
          <Input
            containerStyle={{
              alignSelf: "center",
              backgroundColor: "#DCDCDC",
              borderRadius: 10,
              height: 45,
            }}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            placeholder="Search"
            placeholderTextColor="#9D9D9F"
            inputStyle={{ color: colors.black, fontSize: metrics.text_16 }}
            value={this.state.text}
            onChangeText={(text) => this.SearchFilterFunction(text)}
            leftIcon={
              <Image
                style={{
                  width: metrics.dimen_25,
                  height: metrics.dimen_25,
                  resizeMode: "contain",
                }}
                source={require("../Images/search.png")}
              />
            }
          />
        </View>

        <FlatList
          style={{ marginTop: metrics.dimen_7 }}
          showsVerticalScrollIndicator={false}
          data={this.state.countryDataSource}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => this.renderItemCountries(item)}
        />
      </View>
    );
  };

  getCodesByList = (item) => {
    this.setState({
      currencycode: item.callingCodes[0],
      isoName: item.isoName,
    });
    this.RBSheetAlert.close();
  };

  renderItemCountries = (item) => {
    return (
      <View>
        <TouchableOpacity onPress={() => this.getCodesByList(item)}>
          <View style={{ flexDirection: "row", margin: metrics.dimen_15 }}>
            <Image
              style={{ height: 30, width: 30, resizeMode: "contain" }}
              source={{
                uri:
                  "https://www.countryflags.io/" +
                  item.isoName.toLocaleLowerCase() +
                  "/flat/64.png",
              }}
            />
            <Text
              style={{
                color: colors.app_black_text,
                fontSize: metrics.text_normal,
                paddingHorizontal: 10,
              }}
            >
              {item.name}
            </Text>
            <Text
              style={{
                color: colors.app_black_text,
                fontSize: metrics.text_description,
              }}
            >
              {" (" + item.callingCodes[0] + ")"}
            </Text>
          </View>
          <View
            style={{
              borderBottomColor: colors.light_grey_backgroud,
              borderBottomWidth: 1,
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  renderPasscode = () => {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <View
            style={{
              height: metrics.dimen_60,
              backgroundColor: colors.carib_pay_blue,
              flexDirection: "row",
            }}
          >
            <TouchableOpacity
              style={{
                height: metrics.dimen_25,
                width: metrics.dimen_25,
                marginStart: metrics.dimen_10,
                alignSelf: "center",
              }}
              onPress={() => this.RBSheetPasscode.close()}
            >
              <Image
                style={{
                  height: metrics.dimen_25,
                  width: metrics.dimen_25,
                  alignSelf: "center",
                  paddingHorizontal: 10,
                }}
                source={require("../Images/close.png")}
              />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: metrics.text_16,
                color: "white",
                // fontWeight: "bold",
                fontFamily: metrics.quicksand_bold,
                textAlign: "center",
                alignSelf: "center",
                marginLeft: 10,
              }}
            >
              Passcode
            </Text>
          </View>

          <Text
            style={{
              fontSize: metrics.text_heading,
              color: colors.heading_black_text,
              // fontWeight: "700",
              fontFamily: metrics.quicksand_bold,
              textAlign: "center",
              marginTop: metrics.dimen_10,
              marginBottom: metrics.dimen_10,
            }}
          >
            Enter your Passcode
          </Text>
          <Image
            style={{
              height: metrics.dimen_100,
              width: metrics.dimen_100,
              resizeMode: "contain",
              alignSelf: "center",
              marginTop: metrics.dimen_5,
              marginBottom: metrics.dimen_10,
            }}
            source={require("../Images/pincode.png")}
          />
          <Text
            style={{
              fontSize: metrics.text_description,
              color: colors.app_gray,
              textAlign: "center",
              marginTop: metrics.dimen_5,
              fontFamily: metrics.quicksand_semibold,
            }}
          >
            Passcode required to login in CaribPay
          </Text>
          <View style={{ marginTop: metrics.dimen_20, alignSelf: "center" }}>
            <SmoothPinCodeInput
              placeholder=""
              cellSize={65}
              cellSpacing={7}
              keyboardType="phone-pad"
              cellStyle={{
                borderWidth: 2,
                borderRadius: 7,
                borderColor: colors.theme_caribpay,
                // backgroundColor: "azure",
              }}
              cellStyleFocused={{
                borderColor: "lightseagreen",
                backgroundColor: "lightcyan",
              }}
              textStyle={{
                fontSize: 24,
                color: colors.black,
                fontWeight: "bold",
              }}
              textStyleFocused={{
                color: "crimson",
              }}
              value={this.state.code}
              onFulfill={() =>
                setTimeout(() => {
                  this.onSetPasscodeClick();
                }, 500)
              }
              onTextChange={(code) => this.setState({ code })}
            />
          </View>
          {/* <TouchableOpacity onPress={() => this.OnforgotPasscode()}>
            <Text
              style={{
                fontSize: 14,
                color: colors.place_holder_color,
                fontWeight: 'bold',
                marginTop: 20,
                marginHorizontal: 20,
                textAlign: 'right',
              }}>
              Forgot Passcode ?
            </Text>
          </TouchableOpacity> */}
        </View>
      </SafeAreaView>
    );
  };

  // OnforgotPasscode = () => {
  //   this.RBSheetPasscode.close()
  //   this.props.navigation.navigate("ForgetPasscode")
  // }

  onSetPasscodeClick = async () => {
    this.setState({ visible: true });
    let data = {
      device_id: this.state.deviceID,
      passcode: this.state.code,
      // user_id: this.state.passcode?.user_id,
    };
    console.log("postdata login with pcode", data);
    axios({
      method: "post",
      url: "https://sandbox.caribpayintl.com/api/login-with-passcode",
      data: data,
    })
      .then((response) => {
        console.log("response login-with-passcode ", response.data);
        this.storeloginWithPasscode(response.data.response);
      })
      .catch((err) => {
        console.log("catch error ---> ", err);
        this.setState({ visible: false });

        // this.setState({
        //   wrong: true,
        // });
      });
  };

  OnLogin = () => {
    if (this.state.fromMobilNo) {
      if (this.state.currencycode == "") {
        this.refs.toast.show("Please select country");
      } else if (this.state.phone == "") {
        this.refs.toast.show("Please enter the phone number");
      } else if (this.state.password == "") {
        this.refs.toast.show("Password Required");
      } else {
        this.setState({
          visible: true,
        });

        let loginwithPhoneData = {
          email: this.state.currencycode + this.state.phone,
          password: this.state.password,
          device_id: this.state.deviceID,
        };

        console.log("loginwithPhoneData ---", loginwithPhoneData);
        axios
          .post(
            "https://sandbox.caribpayintl.com/api/login",
            loginwithPhoneData
          )
          .then((response) => {
            console.log("loginwithphone data ---> ", response.data.response);
            if (response.data.response.status == 200) {
              this.storeLoginData(response.data.response);
            }
          })
          .catch((error) => {
            if (error.response) {
              console.log("response", error.response.data);
              let obj = error.response.data;
              if (obj.hasOwnProperty("response")) {
                if (
                  error.response.data.response.status == 400 &&
                  error.response.data.response.message ==
                    "Invalid email & credentials"
                ) {
                  this.setState({
                    visible: false,
                    invalid: true,
                  });
                } else if (
                  error.response.data.response.status == 400 &&
                  error.response.data.response.message !=
                    "Invalid email & credentials"
                ) {
                  this.setState({
                    visible: false,
                    userblocked: true,
                  });
                }
              } else {
                if (
                  error.response.data.success.status == 400 &&
                  error.response.data.success.message ==
                    "Invalid email & credentials"
                ) {
                  this.setState({
                    visible: false,
                    invalid: true,
                  });
                } else {
                  this.setState({
                    visible: false,
                    multipleLogin: true,
                    phone_number: error.response.data.success.phone,
                    user_id: error.response.data.success.user_id,
                  });
                }
              }
            }
          });
      }
    } else {
      var patt = new RegExp(
        "^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$"
      );
      if (this.state.email == "") {
        this.refs.toast.show("Email Required!");
      } else if (patt.test(this.state.email) == false) {
        this.setState({
          emailInvalid: true,
        });
      } else if (this.state.password == "") {
        this.refs.toast.show("Password Required");
      } else {
        this.setState({
          visible: true,
        });
        let loginwithEmailData = {
          email: this.state.email,
          password: this.state.password,
          device_id: this.state.deviceID,
        };
        axios({
          method: "post",
          url: "https://sandbox.caribpayintl.com/api/login",
          data: loginwithEmailData,
        })
          .then((response) => {
            console.log("response data is ---", response.data.data);
            if (response.data.response.status == 200) {
              this.storeLoginData(response.data.response);
            }
          })
          .catch((error) => {
            if (error.response) {
              console.log("response", error.response.data);
              let obj = error.response.data;
              if (obj.hasOwnProperty("response")) {
                if (
                  error.response.data.response.status == 400 &&
                  error.response.data.response.message ==
                    "Invalid email & credentials"
                ) {
                  this.setState({
                    visible: false,
                    invalid: true,
                  });
                } else if (
                  error.response.data.response.status == 400 &&
                  error.response.data.response.message !=
                    "Invalid email & credentials"
                ) {
                  this.setState({
                    visible: false,
                    userblocked: true,
                  });
                }
              } else {
                if (
                  error.response.data.success.status == 400 &&
                  error.response.data.success.message ==
                    "Invalid email & credentials"
                ) {
                  this.setState({
                    visible: false,
                    invalid: true,
                  });
                } else {
                  this.setState({
                    visible: false,
                    multipleLogin: true,
                    phone_number: error.response.data.success.phone,
                    user_id: error.response.data.success.user_id,
                  });
                }
              }
            }
          });
      }
    }
  };

  storeloginWithPasscode = async (data) => {
    console.log("data -->", data);
    console.log("data -->", data.email);

    let pwd = await AsyncStorage.getItem("password");
    console.log("passwrd--->", pwd);

    await AsyncStorage.setItem("first_name", data.first_name);
    await AsyncStorage.setItem(
      "middle_name",
      data.middle_name == null ? "" : data.middle_name
    );
    await AsyncStorage.setItem("last_name", data.last_name);
    await AsyncStorage.setItem("email", data.email);
    await AsyncStorage.setItem("formattedPhone", data.formattedPhone);
    await AsyncStorage.setItem("id", JSON.stringify(data.user_id));
    await AsyncStorage.setItem("token", data.token);
    await AsyncStorage.setItem(
      "picture",
      data.picture == null ? "" : data.picture
    );
    // await AsyncStorage.setItem("user_status", data.user_status);
    await AsyncStorage.setItem("password", this.state.password);
    await AsyncStorage.setItem("caribpay_id", data.caribpay_id);

    let postData = { user_id: data.user_id };
    // this.registerDevice(data.user_id, data.token);
    axios
      .get("https://sandbox.caribpayintl.com/api/available-balance", {
        params: postData,
        headers: { Authorization: data.token },
      })
      .then((response) => {
        let datas = response.data.wallets;
        const filterData = datas.filter((roshan) => roshan.is_default == "Yes");
        let currency_symbol = filterData[0].curr_code;
        let balance = filterData[0].balance;
        let available_bal = parseFloat(balance).toFixed(2);
        this.setState({
          available_Balance: available_bal,
          currency_symbol: currency_symbol,
        });
        console.log("wallets--->", datas);
        this.getSetBalanceWallet(datas);
        setTimeout(() => {
          this.setState({
            visible: false,
            phone: "",
            password: "",
            loginAttempts: 0,
          });
        }, 200);

        this.RBSheetPasscode.close();
        let loginPostData = {
          email: data.email,
          password: pwd,
        };

        console.log("params caribmall ----> ", loginPostData);
        axios({
          method: "post",
          url: "https://caribmall.com/api/auth/login",
          data: loginPostData,
        }).then((response) => {
          console.log(
            "caribmall login auth api response data ---> ",
            response.data
          );
          AsyncStorage.setItem(
            "carib_mall_data",
            JSON.stringify(response.data.data)
          );
        });
        this.setState({
          code: "",
        });
        this.props.navigation.navigate("Home");
      })
      .catch((error) => {
        console.log("error in balance getting --- > ", error);
      });
  };

  makeid = (length) => {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  };

  storeLoginData = async (data) => {
    console.log("data -->", data);
    await AsyncStorage.setItem("first_name", data.first_name);
    await AsyncStorage.setItem(
      "middle_name",
      data.middle_name == null ? "" : data.middle_name
    );
    await AsyncStorage.setItem("last_name", data.last_name);
    await AsyncStorage.setItem("email", data.email);
    await AsyncStorage.setItem("formattedPhone", data.formattedPhone);
    await AsyncStorage.setItem("id", JSON.stringify(data.user_id));
    await AsyncStorage.setItem("token", data.token);
    await AsyncStorage.setItem("picture", data.picture);
    await AsyncStorage.setItem("user_status", data.user_status);
    await AsyncStorage.setItem("password", this.state.password);
    await AsyncStorage.setItem("caribpay_id", data.caribpay_id);

    console.log("password ---> ", this.state.password);

    let postData = { user_id: data.user_id };
    this.registerDevice(data.user_id, data.token);
    axios
      .get("https://sandbox.caribpayintl.com/api/available-balance", {
        params: postData,
        headers: { Authorization: data.token },
      })
      .then((response) => {
        let datas = response.data.wallets;
        const filterData = datas.filter((roshan) => roshan.is_default == "Yes");
        let currency_symbol = filterData[0].curr_code;
        let balance = filterData[0].balance;
        let available_bal = parseFloat(balance).toFixed(2);
        this.setState({
          available_Balance: available_bal,
          currency_symbol: currency_symbol,
        });
        console.log("wallets--->", datas);
        this.getSetBalanceWallet(datas);
        setTimeout(() => {
          this.setState({
            visible: false,
            phone: "",
            password: "",
            loginAttempts: 0,
          });
        }, 200);

        let loginPostData = {
          email: data.formattedPhone,
          password: this.state.password,
        };

        console.log("params----> ", loginPostData);
        axios({
          method: "post",
          url: "https://caribmall.com/api/auth/login",
          data: loginPostData,
        })
          .then((response) => {
            this.props.navigation.navigate("Home");
            console.log(
              "caribmall login auth api response data ---> ",
              response.data
            );
            AsyncStorage.setItem(
              "carib_mall_data",
              JSON.stringify(response.data.data)
            );
          })
          .catch((error) => {
            if (error.response) {
              console.log("yes it is unlogin ---> ", error.response.status);
              console.log(
                "login params under register password ---> ",
                loginPostData.password
              );
              if (error.response.status == 401) {
                let registerCMData = {
                  name: data.first_name,
                  email: data.email,
                  phone: data.formattedPhone,
                  password: loginPostData.password,
                  subscribe: true,
                  agree: 1,
                  verification_token: this.makeid(40),
                };
                console.log(
                  "register params ---> carib maall ",
                  registerCMData
                );
                axios({
                  method: "post",
                  url: "https://caribmall.com/api/auth/register",
                  data: registerCMData,
                })
                  .then((response) => {
                    console.log(
                      "caribmall register  response data ---> ",
                      response.data.data
                    );
                    AsyncStorage.setItem(
                      "carib_mall_data",
                      JSON.stringify(response.data.data)
                    );
            this.props.navigation.navigate("Home");

                  })
                  .catch((error) => {
                    if (error.response) {
                      console.log(
                        "error register caribmall -->",
                        error.response.data
                      );
                    }
                  });
              }
            }
          });
      })
      .catch((error) => {
        console.log("error in balance getting --- > ", error);
      });
  };

  registerDevice = (id, token) => {
    this.setState({ visible: true });
    let data = {
      user_id: id,
      device_id: this.state.deviceID,
      fcm_token: this.state.fcmToken,
    };
    console.log("postdata register device ---> ", data);
    axios({
      method: "post",
      url: "https://sandbox.caribpayintl.com/api/register-device",
      headers: { Authorization: token },
      data: data,
    })
      .then((response) => {
        console.log("response register device --> ", response.data);
        // this.setState({ visible: false })
      })
      .catch((err) => {
        this.setState({ visible: false });
        alert(err);
      });
  };

  setWalletdata = async (data) => {
    await AsyncStorage.setItem("walletData", JSON.stringify(data));
  };

  getSetBalanceWallet = async (data) => {
    await AsyncStorage.setItem(
      "available_balance",
      this.state.available_Balance
    );
    await AsyncStorage.setItem("symbol", this.state.currency_symbol);
    {
      this.setWalletdata(data);
    }
  };

  render() {
    let name = this.state.isoName;
    var inshort = name.toLocaleLowerCase();
    return (
      <SafeAreaView style={styles.mainContainer}>
        <View style={{ flex: 1 }}>
          <View
            style={{
              backgroundColor: colors.theme_caribpay,
              height: metrics.dimen_240,
            }}
          >
            <View style={{ margin: metrics.dimen_20 }}>
              <Image
                style={styles.imageStyle}
                source={require("../Images/logo.png")}
              />
              <Text
                style={{
                  fontSize: metrics.text_header,
                  color: colors.app_light_gray,
                  fontFamily: metrics.quicksand_regular,
                  fontWeight: "bold",
                }}
              >
                Welcome
              </Text>
              <Text
                style={{
                  fontSize: metrics.text_21,
                  color: colors.white,
                  // fontWeight: 'bold',
                  fontFamily: metrics.quicksand_bold,
                }}
              >
                Sign In
              </Text>
            </View>
          </View>

          <View
            style={{
              position: "absolute",
              top: metrics.dimen_160,
              width: "100%",
              height: Dimensions.get("screen").height - 200,
              backgroundColor: colors.white,
              borderRadius: 40,
            }}
          >
            <ScrollView style={{ marginBottom: 20 }}>
              <View
                style={{
                  flexDirection: "column",
                  marginHorizontal: metrics.dimen_20,
                }}
              >
                <View style={{ flexDirection: "row", marginTop: 20 }}>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-evenly",
                    }}
                  >
                    <RadioButton
                      size={metrics.dimen_12}
                      animation={"bounceIn"}
                      isSelected={this.state.fromMobilNo}
                      innerColor={colors.theme_caribpay}
                      outerColor={colors.theme_caribpay}
                      onPress={() =>
                        this.setState({
                          fromMobilNo: true,
                          fromEmail: false,
                        })
                      }
                    />
                    <Text
                      style={{
                        fontSize: metrics.text_14,
                        fontFamily: metrics.quicksand_semibold,
                        color: colors.black,
                        marginLeft: metrics.dimen_10,
                      }}
                    >
                      Mobile No.
                    </Text>
                  </View>

                  <View
                    style={{
                      justifyContent: "center",
                      marginLeft: metrics.dimen_20,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: metrics.text_header,
                        color: colors.heading_black_text,
                        textAlignVertical: "center",
                      }}
                    >
                      |
                    </Text>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      marginLeft: metrics.dimen_30,
                    }}
                  >
                    <RadioButton
                      size={metrics.dimen_12}
                      animation={"bounceIn"}
                      isSelected={this.state.fromEmail}
                      innerColor={colors.theme_caribpay}
                      outerColor={colors.theme_caribpay}
                      onPress={() =>
                        this.setState({
                          fromEmail: true,
                          fromMobilNo: false,
                        })
                      }
                    />
                    <Text
                      style={{
                        fontSize: metrics.text_14,
                        fontFamily: metrics.quicksand_semibold,
                        color: colors.black,
                        marginLeft: metrics.dimen_10,
                      }}
                    >
                      Email
                    </Text>
                  </View>
                </View>

                {this.state.fromMobilNo && (
                  <View
                    style={{
                      flexDirection: "column",
                      marginTop: 20,
                      width: "90%",
                    }}
                  >
                    <Text
                      style={{
                        fontSize: metrics.text_header,
                        fontFamily: metrics.quicksand_semibold,
                        color: colors.black,
                      }}
                    >
                      {"Mobile No."}
                    </Text>
                    <View style={{ flexDirection: "row", marginTop: 10 }}>
                      <Input
                        placeholderTextColor={colors.place_holder_color}
                        placeholder={"Code"}
                        value={this.state.currencycode}
                        disabled={true}
                        onChangeText={(text) =>
                          this.setState({ currencycode: text })
                        }
                        leftIcon={
                          <TouchableOpacity
                            onPress={() => this.RBSheetAlert.open()}
                          >
                            {this.state.isoName == "" ? (
                              <Image
                                style={{ height: 20, width: 20 }}
                                source={require("../Images/globe.png")}
                              />
                            ) : (
                              <Image
                                style={{ height: 30, width: 30 }}
                                source={{
                                  uri:
                                    "https://www.countryflags.io/" +
                                    inshort +
                                    "/flat/64.png",
                                }}
                              />
                            )}
                          </TouchableOpacity>
                        }
                        containerStyle={{
                          width: "30%",
                          height: 41,
                          minHeight: 41,
                        }}
                        inputContainerStyle={{
                          borderBottomWidth: 0.5,
                          height: 41,
                          minHeight: 41,
                        }}
                        inputStyle={{ fontSize: 14, height: 41, minHeight: 41 }}
                        style={{ height: 42, minHeight: 42 }}
                      />
                      {/* <Text style={{ fontSize: metrics.text_normal, color: colors.black, marginTop: 10 }}>Mobile Number</Text> */}
                      <Input
                        placeholder={"Mobile No."}
                        placeholderTextColor={colors.place_holder_color}
                        containerStyle={{ width: "70%", height: 40 }}
                        keyboardType="phone-pad"
                        returnKeyLabel="Done"
                        returnKeyType="done"
                        value={this.state.phone}
                        inputContainerStyle={{ borderBottomWidth: 0.5 }}
                        inputStyle={{ fontSize: 14 }}
                        onChangeText={(text) => this.setState({ phone: text })}
                      />
                    </View>
                  </View>
                )}
                {this.state.fromEmail && (
                  <View>
                    <Text
                      style={{
                        fontSize: metrics.text_header,
                        fontFamily: metrics.quicksand_semibold,
                        color: colors.black,
                        marginTop: 20,
                      }}
                    >
                      Email
                    </Text>

                    <Input
                      placeholder={"Email Id"}
                      placeholderTextColor={colors.place_holder_color}
                      value={this.state.email}
                      onChangeText={(text) => this.setState({ email: text })}
                      containerStyle={{ width: "100%", height: 50 }}
                      inputContainerStyle={{ borderBottomWidth: 0.2 }}
                      inputStyle={{ fontSize: 14 }}
                    />
                  </View>
                )}
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    fontFamily: metrics.quicksand_semibold,
                    color: colors.black,
                    marginTop: 15,
                  }}
                >
                  Password
                </Text>
                <Input
                  placeholder={"Password"}
                  value={this.state.password}
                  placeholderTextColor={colors.place_holder_color}
                  onChangeText={(text) => this.setState({ password: text })}
                  secureTextEntry={this.state.hide ? true : false}
                  rightIcon={
                    <TouchableOpacity
                      onPress={() => this.setState({ hide: !this.state.hide })}
                    >
                      <Image
                        style={{ height: 20, width: 20, marginRight: 10 }}
                        source={
                          this.state.hide
                            ? require("../Images/hide.png")
                            : require("../Images/eye.png")
                        }
                      />
                    </TouchableOpacity>
                  }
                  containerStyle={{ width: "100%", height: 50 }}
                  inputContainerStyle={{ borderBottomWidth: 0.2 }}
                  inputStyle={{ fontSize: 14 }}
                />
              </View>

              <Spinner
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignSelf: "center",
                }}
                isVisible={this.state.visible}
                size={70}
                type={"ThreeBounce"}
                color={colors.black}
              />

              <TouchableOpacity onPress={() => this.OnLogin()}>
                <View elevation={5} style={styles.buttonStyle}>
                  <Image
                    style={{ height: 15, width: 15, alignSelf: "center" }}
                    source={require("../Images/checkbox.png")}
                  />
                  <Text
                    style={{
                      fontSize: 15,
                      color: "white",
                      // fontWeight: 'bold',
                      fontFamily: metrics.quicksand_bold,
                      textAlign: "center",
                      alignSelf: "center",
                      marginLeft: 10,
                    }}
                  >
                    {"Login "}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("ForgetPassword", {
                    fromLogin: "1",
                  })
                }
              >
                {/* <TouchableOpacity onPress={() => this.setState({
                                tryAgain: true
                            })}> */}

                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: metrics.quicksand_semibold,
                    color: colors.app_gray,
                    // fontWeight: "bold",
                    marginTop: 20,
                    marginHorizontal: 20,
                    textAlign: "right",
                  }}
                >
                  Forgot Password ?
                </Text>
              </TouchableOpacity>

              <Toast
                ref="toast"
                style={{ backgroundColor: "black" }}
                position="center"
                positionValue={200}
                fadeInDuration={200}
                fadeOutDuration={1000}
                opacity={0.8}
                textStyle={{ color: "white" }}
              />

              <RBSheet
                ref={(ref) => {
                  this.RBSheetAlert = ref;
                }}
                height={Dimensions.get("screen").height}
                duration={0}
              >
                {this.renderCountries()}
              </RBSheet>

              <RBSheet
                ref={(ref) => {
                  this.RBSheetPasscode = ref;
                }}
                height={Dimensions.get("screen").height}
                duration={0}
              >
                {this.renderPasscode()}
              </RBSheet>

              <RBSheet
                ref={(ref) => {
                  this.RBSheetAdd = ref;
                }}
                closeOnDragDown={true}
                height={Dimensions.get("window").height - 30}
                closeOnPressMask={true}
                customStyles={{
                  wrapper: {
                    backgroundColor: "transparent",
                  },
                  draggableIcon: {
                    backgroundColor: colors.theme_caribpay,
                  },
                  container: {
                    borderTopRightRadius: 20,
                    borderTopLeftRadius: 20,
                    backgroundColor: colors.whitesmoke,
                  },
                }}
              >
                <View
                  style={{
                    flex: 1,
                    margin: metrics.dimen_10,
                    justifyContent: "center",
                  }}
                >
                  <Image
                    style={{
                      height: 100,
                      width: 100,
                      resizeMode: "contain",
                      alignSelf: "center",
                    }}
                    source={require("../Images/wifi.png")}
                  />
                  <Text
                    style={{
                      color: colors.sub_cat_menu,
                      fontSize: metrics.text_21,
                      textAlign: "center",
                      alignSelf: "center",
                      marginTop: 10,
                      fontWeight: "bold",
                    }}
                  >
                    Whoops!
                  </Text>
                  <Text
                    style={{
                      color: colors.sub_cat_menu,
                      fontSize: metrics.text_normal,
                      textAlign: "center",
                      alignSelf: "center",
                      fontWeight: "bold",
                    }}
                  >
                    {
                      "No Internet Connections \n Please check your internet Settings"
                    }
                    !
                  </Text>
                </View>
              </RBSheet>

              {/* <View>
                                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.invalid} >
                                    <View style={{ width: "80%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20 }}>
                                        <View style={{ justifyContent: 'center', height: metrics.dimen_60, backgroundColor: colors.carib_pay_blue }}>
                                            <Text style={{ color: colors.white, fontSize: metrics.text_header, textAlign: 'center', textAlignVertical: 'center' }}>{"Warning!"}</Text>
                                        </View>
                                        <View style={{ alignSelf: 'center', margin: metrics.dimen_15, flexDirection: 'column' }}>
                                            <Text style={{ color: 'red', fontSize: metrics.text_medium, textAlign: 'center' }}>{"You have made " + this.state.loginAttempts + " unsuccessfull attempts(s)" + "\n Remaining Attempt(s) : " + (3 - parseInt(this.state.loginAttempts)) + " "}</Text>
                                            <Text style={{ color: colors.heading_black_text, fontSize: metrics.text_header, textAlign: 'center' }}>{"Invalid Credentials, Try again with correct credentials!"}</Text>
                                            <TouchableOpacity onPress={() => this.setState({
                                                invalid: false
                                            })}>
                                                <View style={{ height: metrics.dimen_30, width: 80, backgroundColor: colors.carib_pay_blue, alignSelf: 'center', marginTop: 20, justifyContent: 'center' }}>
                                                    <Text style={{ color: colors.white, fontSize: metrics.text_normal, textAlign: 'center' }}>{"Try Again"}</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </Modal>
                            </View> */}

              <View>
                <Modal
                  style={{ borderRadius: metrics.dimen_10 }}
                  isVisible={this.state.invalid}
                >
                  <View
                    style={{
                      width: "90%",
                      alignSelf: "center",
                      backgroundColor: colors.white,
                      borderRadius: metrics.dimen_10,
                      margin: metrics.dimen_20,
                      flexDirection: "column",
                      height: 350,
                      justifyContent: "center",
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View>
                        <Image
                          style={{
                            height: metrics.dimen_80,
                            width: metrics.dimen_100,
                            resizeMode: "contain",
                            alignSelf: "center",
                            marginTop: 20,
                          }}
                          source={require("../Images/logo.png")}
                        />
                        {this.state.fromEmail ? (
                          <Text
                            style={{
                              fontSize: metrics.text_heading,
                              color: colors.black,
                              fontWeight: "bold",
                              textAlign: "center",
                              margin: 15,
                            }}
                          >
                            Your Email ID or password is incorrect.
                          </Text>
                        ) : (
                          <Text
                            style={{
                              fontSize: metrics.text_heading,
                              color: colors.black,
                              fontWeight: "bold",
                              textAlign: "center",
                              margin: 15,
                            }}
                          >
                            Your mobile no. or password is incorrect.
                          </Text>
                        )}
                        <Text
                          style={{
                            fontSize: metrics.text_normal,
                            color: colors.app_gray,
                            margin: 15,
                            fontWeight: "bold",
                            textAlign: "center",
                          }}
                        >
                          Try again or tap on 'Forgot Password' to reset your
                          password
                        </Text>
                      </View>
                      <View style={styles.bottomview}>
                        <TouchableOpacity
                          onPress={() => this.setState({ invalid: false })}
                        >
                          <Text
                            style={{
                              fontSize: 15,
                              color: "white",
                              fontWeight: "bold",
                              textAlign: "center",
                              alignSelf: "center",
                              marginLeft: 10,
                            }}
                          >
                            OK
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </Modal>
              </View>

              <View>
                <Modal
                  style={{ borderRadius: metrics.dimen_10 }}
                  isVisible={this.state.multipleLogin}
                >
                  <View
                    style={{
                      width: "90%",
                      alignSelf: "center",
                      backgroundColor: colors.white,
                      borderRadius: metrics.dimen_10,
                      margin: metrics.dimen_20,
                      flexDirection: "column",
                      height: 350,
                      justifyContent: "center",
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View>
                        <Image
                          style={{
                            height: metrics.dimen_80,
                            width: metrics.dimen_100,
                            resizeMode: "contain",
                            alignSelf: "center",
                            marginTop: 20,
                          }}
                          source={require("../Images/logo.png")}
                        />
                        <Text
                          style={{
                            fontSize: metrics.text_heading,
                            color: colors.black,
                            fontWeight: "bold",
                            textAlign: "center",
                            margin: 15,
                          }}
                        >
                          Multiple Login!
                        </Text>
                        <Text
                          style={{
                            fontSize: metrics.text_normal,
                            color: colors.app_gray,
                            margin: 15,
                            fontWeight: "bold",
                            textAlign: "center",
                          }}
                        >
                          You are login on other device please logout first from
                          that device then login again !
                        </Text>
                      </View>
                      <View
                        style={{
                          margin: metrics.dimen_15,
                          flexDirection: "row",
                          justifyContent: "center",
                        }}
                      >
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({ multipleLogin: false })
                          }
                        >
                          <View
                            style={{
                              width: metrics.dimen_120,
                              backgroundColor: colors.carib_pay_blue,
                              borderRadius: metrics.dimen_7,
                              height: metrics.dimen_40,
                              justifyContent: "center",
                            }}
                          >
                            <Text
                              style={{
                                paddingHorizontal: metrics.dimen_10,
                                fontSize: metrics.text_header,
                                color: colors.white,
                                textAlign: "center",
                                fontWeight: "900",
                              }}
                            >
                              Later
                            </Text>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({ multipleLogin: false }, () =>
                              this.props.navigation.navigate("PasswordOTP", {
                                otp: "",
                                data: this.state.phone_number,
                                // phone: this.state.phone_number,
                                fromLogin: "2",
                                fieldType: "phone",
                                user_id: this.state.user_id,
                              })
                            )
                          }
                        >
                          <View
                            style={{
                              width: metrics.dimen_120,
                              backgroundColor: colors.carib_pay_blue,
                              borderRadius: metrics.dimen_7,
                              height: metrics.dimen_40,
                              marginLeft: 10,
                              justifyContent: "center",
                            }}
                          >
                            <Text
                              style={{
                                paddingHorizontal: metrics.dimen_10,
                                fontSize: metrics.text_header,
                                color: colors.white,
                                textAlign: "center",
                                fontWeight: "900",
                              }}
                            >
                              Logout
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </Modal>
              </View>

              <View>
                <Modal
                  style={{ borderRadius: metrics.dimen_10 }}
                  isVisible={this.state.emailInvalid}
                >
                  <View
                    style={{
                      width: "90%",
                      alignSelf: "center",
                      backgroundColor: colors.white,
                      borderRadius: metrics.dimen_10,
                      margin: metrics.dimen_20,
                      flexDirection: "column",
                      height: 200,
                      justifyContent: "center",
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View>
                        <Image
                          style={{
                            height: metrics.dimen_80,
                            width: metrics.dimen_100,
                            resizeMode: "contain",
                            alignSelf: "center",
                            marginTop: 10,
                          }}
                          source={require("../Images/logo.png")}
                        />
                        <Text
                          style={{
                            fontSize: metrics.text_normal,
                            color: colors.black,
                            margin: 15,
                            fontWeight: "bold",
                            textAlign: "center",
                          }}
                        >
                          Enter the Email ID in correct Format.
                        </Text>
                      </View>
                      <View style={styles.bottomview}>
                        <TouchableOpacity
                          onPress={() => this.setState({ emailInvalid: false })}
                        >
                          <Text
                            style={{
                              fontSize: 15,
                              color: "white",
                              fontWeight: "bold",
                              textAlign: "center",
                              alignSelf: "center",
                              marginLeft: 10,
                            }}
                          >
                            OK
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </Modal>
              </View>

              <View>
                <Modal
                  style={{ borderRadius: metrics.dimen_10 }}
                  isVisible={this.state.wrong}
                >
                  <View
                    style={{
                      width: "80%",
                      alignSelf: "center",
                      backgroundColor: colors.white,
                      borderRadius: metrics.dimen_5,
                      margin: metrics.dimen_20,
                    }}
                  >
                    <View
                      style={{
                        justifyContent: "center",
                        height: metrics.dimen_60,
                        backgroundColor: colors.carib_pay_blue,
                      }}
                    >
                      <Text
                        style={{
                          color: colors.white,
                          fontWeight: "bold",
                          fontSize: metrics.text_header,
                          textAlign: "center",
                        }}
                      >
                        {"CaribPay"}
                      </Text>
                    </View>
                    <View
                      style={{
                        alignSelf: "center",
                        margin: metrics.dimen_15,
                        flexDirection: "column",
                      }}
                    >
                      <Image
                        style={{
                          height: metrics.dimen_80,
                          width: metrics.dimen_100,
                          resizeMode: "contain",
                          alignSelf: "center",
                        }}
                        source={require("../Images/danger.png")}
                      />
                      <Text
                        style={{
                          color: colors.heading_black_text,
                          fontSize: metrics.text_header,
                          textAlign: "center",
                          marginTop: 10,
                        }}
                      >
                        {"Passcode Invalid."}
                      </Text>

                      <TouchableOpacity
                        onPress={() =>
                          this.setState({ wrong: false, code: "" })
                        }
                      >
                        <View
                          style={{
                            height: metrics.dimen_35,
                            width: 200,
                            backgroundColor: colors.carib_pay_blue,
                            borderRadius: metrics.dimen_7,
                            alignSelf: "center",
                            marginTop: 20,
                            justifyContent: "center",
                          }}
                        >
                          <Text
                            style={{
                              color: colors.white,
                              fontSize: metrics.text_normal,
                              textAlign: "center",
                              padding: 10,
                            }}
                          >
                            {"Try Again"}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </Modal>
              </View>

          

              <View>
                <Modal
                  style={{ borderRadius: metrics.dimen_10 }}
                  isVisible={this.state.userblocked}
                >
                  <View
                    style={{
                      width: "90%",
                      alignSelf: "center",
                      backgroundColor: colors.white,
                      borderRadius: metrics.dimen_10,
                      margin: metrics.dimen_20,
                      flexDirection: "column",
                      height: 350,
                      justifyContent: "center",
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View>
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({
                              userblocked: false,
                            })
                          }
                        >
                          <Image
                            style={{
                              height: metrics.dimen_16,
                              width: metrics.dimen_16,
                              resizeMode: "contain",
                              margin: 15,
                            }}
                            source={require("../Images/cross-sign.png")}
                          />
                        </TouchableOpacity>

                        <Image
                          style={{
                            height: metrics.dimen_100,
                            width: metrics.dimen_100,
                            resizeMode: "contain",
                            alignSelf: "center",
                            marginTop: 10,
                          }}
                          source={require("../Images/sorrypic.jpg")}
                        />

                        <Text
                          style={{
                            fontSize: metrics.text_heading,
                            color: colors.black,
                            fontWeight: "bold",
                            textAlign: "center",
                            margin: 15,
                          }}
                        >
                          Your account has been suspended due to many invalid
                          login attempt.
                        </Text>
                        <Text
                          style={{
                            fontSize: metrics.text_normal,
                            color: colors.app_gray,
                            margin: 15,
                            fontWeight: "bold",
                            textAlign: "center",
                          }}
                        >
                          Please contact support centre at
                          support@caribpayintl.com
                        </Text>
                      </View>
                      {/* <View style={styles.bottomview}>
                        <TouchableOpacity
                          onPress={() => this.setState({ userblocked: false })}
                        >
                          <Text
                            style={{
                              fontSize: 15,
                              color: "white",
                              fontWeight: "bold",
                              textAlign: "center",
                              alignSelf: "center",
                              marginLeft: 10,
                            }}
                          >
                            OK
                          </Text>
                        </TouchableOpacity>
                      </View> */}
                    </View>
                  </View>
                </Modal>
              </View>
            </ScrollView>
          </View>
          {!this.state.showKeyboard && (
            <View
              style={{ position: "absolute", bottom: 10, alignSelf: "center" }}
            >
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Register")}
              >
                <Text
                  style={{
                    fontSize: metrics.text_header,
                    color: colors.carib_pay_blue,
                    fontFamily: metrics.quicksand_bold,
                    // fontWeight: "bold",
                    marginTop: 20,
                    marginHorizontal: metrics.dimen_20,
                  }}
                >
                  Don't Have an Account ?
                  <Text
                    style={{
                      fontSize: metrics.text_large,
                      color: colors.theme_caribpay,
                      fontFamily: metrics.quicksand_semibold,
                      // fontWeight: "bold",
                      textAlign: "center",
                      marginTop: 20,
                    }}
                  >
                    {" "}
                    Register
                  </Text>
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  imageStyle: {
    height: metrics.dimen_60,
    width: metrics.dimen_60,
    resizeMode: "contain",
    marginTop: 5,
  },
  textStyle: {
    color: colors.app_gray,
    fontSize: metrics.text_description,
    marginLeft: 15,
    marginTop: 15,
  },
  buttonStyle: {
    flexDirection: "row",
    width: "80%",
    borderRadius: 25,
    justifyContent: "center",
    alignSelf: "center",
    height: 50,
    marginTop: metrics.dimen_20,
    backgroundColor: colors.theme_caribpay,
    shadowColor: "#f2f2f2",
    shadowOpacity: 1,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1,
    },
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 50,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 25,
    marginTop: 10,
  },
});
