import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  FlatList,
  BackHandler,
  PanResponder,
} from "react-native";
import metrics from "../Themes/Metrics";
import colors from "../Themes/Colors";
import { Input } from "react-native-elements";
import Toast, { DURATION } from "react-native-easy-toast";
const axios = require("axios");
import RBSheet from "react-native-raw-bottom-sheet";
import Modal from "react-native-modal";
import CountryData from "country-data";

export default class MobileReload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: this.props.navigation.state.params.user_id,
      token: this.props.navigation.state.params.token,
      currencycode: "",
      phone: "",
      countryDataSource: [],
      isoOperators: [],
      isoName: "",
      fromPostPaid: false,
      fromPrepaid: true,
      arrayholder: [],
      isComingSoon: false,
    };
    console.disableYellowBox = true;
  }

  componentDidMount() {
    axios
      .get("https://topups.reloadly.com/countries")
      .then((response) => {
        this.setState({
          countryDataSource: response.data,
        });
        this.state.arrayholder = response.data;
      })
      .catch((err) => {
        console.warn(err);
      });

    axios
      .get("https://ipinfo.io/json")
      .then((response) => {
        let country_code = response.data.country;
        let countrycallingcode = JSON.stringify(
          CountryData.countries[country_code].countryCallingCodes[0]
        );
        this.setState({
          currencycode: countrycallingcode.replace(/"/g, ""),
          isoName: country_code,
        });
      })
      .catch((error) => {
        console.log(error);
      });

    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  getCodesByList = (item) => {
    this.setState({
      currencycode: item.callingCodes[0],
      isoName: item.isoName,
    });
    this.RBSheetAlert.close();
    // this.getOperatorIds(item);
  };

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  renderHeader() {
    return (
      <View
        style={{
          backgroundColor: colors.light_grey_backgroud,
          height: 40,
          marginTop: metrics.dimen_20,
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Text
          style={{
            fontSize: metrics.text_description,
            color: colors.heading_black_text,
            alignSelf: "center",
            marginLeft: 10,
          }}
        >
          Select from Recents
        </Text>
        <Text
          style={{
            fontSize: metrics.text_description,
            color: colors.carib_pay_blue,
            alignSelf: "center",
            marginRight: 10,
          }}
        >
          View all
        </Text>
      </View>
    );
  }

  onProceedtoNext = () => {
    if (this.state.currencycode == "") {
      this.refs.toast.show("please select the country first");
    } else if (this.state.phone == "") {
      this.refs.toast.show("please enter your phone number");
    } else if (this.state.phone.length < 4) {
      this.refs.toast.show("Invalid Phone Number");
    } else {
      this.props.navigation.navigate("Topup", {
        user_id: this.state.user_id,
        token: this.state.token,
        phone: this.state.currencycode + this.state.phone,
        isoName: this.state.isoName,
      });
    }
  };

  renderItemCountries = (item) => {
    return (
      <View>
        <TouchableOpacity onPress={() => this.getCodesByList(item)}>
          <View style={{ flexDirection: "row", margin: metrics.dimen_15 }}>
            <Image
              style={{ height: 30, width: 30, resizeMode: "contain" }}
              source={{
                uri:
                  "https://www.countryflags.io/" +
                  item.isoName.toLocaleLowerCase() +
                  "/flat/64.png",
              }}
            />
            <Text
              style={{
                color: colors.app_black_text,
                fontSize: metrics.text_normal,
                paddingHorizontal: 10,
              }}
            >
              {item.name}
            </Text>
            <Text
              style={{
                color: colors.app_black_text,
                fontSize: metrics.text_description,
              }}
            >
              {" (" + item.callingCodes[0] + ")"}
            </Text>
          </View>
          <View
            style={{
              borderBottomColor: colors.light_grey_backgroud,
              borderBottomWidth: 1,
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  SearchFilterFunction(text) {
    const newData = this.state.arrayholder.filter((item) => {
      const itemData = `${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({ countryDataSource: newData, text: text });
  }

  renderCountries = () => {
    return (
      <View>
        <View
          style={{
            height: metrics.dimen_60,
            backgroundColor: colors.carib_pay_blue,
            flexDirection: "row",
          }}
        >
          <TouchableOpacity
            style={{
              height: metrics.dimen_25,
              width: metrics.dimen_25,
              marginStart: metrics.dimen_10,
              alignSelf: "center",
            }}
            onPress={() => this.RBSheetAlert.close()}
          >
            <Image
              style={{
                height: metrics.dimen_25,
                width: metrics.dimen_25,
                alignSelf: "center",
                paddingHorizontal: 10,
              }}
              source={require("../Images/leftarrow.png")}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: metrics.text_16,
              color: "white",
              fontWeight: "bold",
              textAlign: "center",
              alignSelf: "center",
              marginLeft: 10,
            }}
          >
            Select Country
          </Text>
        </View>
        <View
          style={{
            marginTop: metrics.dimen_5,
            width: "95%",
            alignSelf: "center",
          }}
        >
          <Input
            containerStyle={{
              alignSelf: "center",
              backgroundColor: "#DCDCDC",
              borderRadius: 10,
              height: 45,
            }}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            placeholder="Search"
            placeholderTextColor="#9D9D9F"
            inputStyle={{
              color: colors.black,
              fontSize: metrics.text_description,
            }}
            value={this.state.text}
            onChangeText={(text) => this.SearchFilterFunction(text)}
            leftIcon={
              <Image
                style={{
                  width: metrics.dimen_25,
                  height: metrics.dimen_25,
                  resizeMode: "contain",
                }}
                source={require("../Images/search.png")}
              />
            }
          />
        </View>

        <FlatList
          style={{ marginTop: metrics.dimen_7 }}
          showsVerticalScrollIndicator={false}
          data={this.state.countryDataSource}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => this.renderItemCountries(item)}
        />
      </View>
    );
  };

  render() {
    let name = this.state.isoName;
    var inshort = name.toLocaleLowerCase();
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.header}>
          <TouchableOpacity
            style={styles.headerleftImage}
            onPress={() => this.props.navigation.goBack(null)}
          >
            <Image
              style={styles.headerleftImage}
              source={require("../Images/leftarrow.png")}
            />
          </TouchableOpacity>
          <Text style={styles.headertextStyle}>Mobile Reload</Text>
        </View>
        <View style={styles.absouluteview}>
          <View style={{ flex: 1 }}>
            {/* 
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: metrics.dimen_20 }}>
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{ color: '#323232', fontWeight: '500', fontSize: metrics.text_header }}>Mobile Top-up</Text>
                            </View>

                            <View style={{ height: metrics.dimen_30, width: metrics.dimen_30, borderRadius: metrics.dimen_30 / 2, backgroundColor: "#f2f2f2", justifyContent: 'center' }}>
                                <Image style={{ height: metrics.dimen_15, width: metrics.dimen_15, alignSelf: 'center' }}
                                    source={require('../Images/refresh.png')}></Image>
                            </View>
                        </View> */}

            {/* <View style={{ flexDirection: 'row', backgroundColor: colors.theme_caribpay, height: metrics.dimen_30, alignSelf: 'center', width: '95%', margin: 2, borderRadius: metrics.dimen_10 }}> */}
            {/* <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromPrepaid ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromPostPaid: false, fromPrepaid: true, isComingSoon: false })}> */}
            {/* <View> */}
            <Text
              style={{
                fontSize: metrics.text_large,
                color: colors.black,
                marginLeft: 22,
                marginTop: 20,
                fontFamily: metrics.quicksand_bold,
              }}
            >
              Prepaid
            </Text>
            {/* </View> */}
            {/* </TouchableOpacity> */}
            {/* <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromPostPaid ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromPostPaid: true, fromPrepaid: false, isComingSoon: true })}>
                                <View style={{ justifyContent: 'center', flex: 1 / 2, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromPostPaid ? colors.light_grey_backgroud : colors.transparent }}>
                                    <Text style={{ fontSize: metrics.text_description, color: this.state.fromPostPaid ? colors.black : colors.whitesmoke, textAlign: 'center' }}>Postpaid</Text>
                                </View>
                            </TouchableOpacity> */}
            {/* </View> */}

            <View style={{ flexDirection: "column", margin: metrics.dimen_10 }}>
              <View style={{ flexDirection: "row" }}>
                <View style={{ height: 40, width: "32%" }}>
                  <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
                    <Input
                      placeholder={"Code"}
                      value={this.state.currencycode}
                      disabled={true}
                      onChangeText={(text) =>
                        this.setState({ currencycode: text })
                      }
                      leftIcon={
                        <TouchableOpacity
                          onPress={() => this.RBSheetAlert.open()}
                        >
                          {this.state.isoName == "" ? (
                            <Image
                              style={{ height: 20, width: 20 }}
                              source={require("../Images/globe.png")}
                            />
                          ) : (
                            <Image
                              style={{ height: 30, width: 30 }}
                              source={{
                                uri:
                                  "https://www.countryflags.io/" +
                                  inshort +
                                  "/flat/64.png",
                              }}
                            />
                          )}
                        </TouchableOpacity>
                      }
                      containerStyle={{ width: "100%", height: 40 }}
                      inputContainerStyle={{ borderBottomWidth: 0 }}
                      inputStyle={{ fontSize: 13 }}
                    />
                  </TouchableOpacity>
                </View>

                <Input
                  placeholder={"Mobile No."}
                  containerStyle={{ width: "70%", height: 50 }}
                  keyboardType="number-pad"
                
                  value={this.state.phone}
                  inputContainerStyle={{ borderBottomWidth: 0 }}
                  inputStyle={{ fontSize: 13 }}
                  onChangeText={(text) => this.setState({ phone: text })}
                />
              </View>
              <View style={styles.horizontalLine} />
              <Text
                style={{
                  color: "#323232",
                  fontFamily:metrics.quicksand_semibold,
                  fontSize: metrics.text_description,
                  marginTop: 5,
                  marginLeft: 9,
                }}
              >
                Select Country and Enter Mobile Number
              </Text>
            </View>
          </View>

          <Toast
            ref="toast"
            style={{ backgroundColor: "black" }}
            position="center"
            positionValue={200}
            fadeInDuration={200}
            fadeOutDuration={1000}
            opacity={0.8}
            textStyle={{ color: "white" }}
          />

          <View>
            <Modal
              style={{ borderRadius: metrics.dimen_10 }}
              isVisible={this.state.isComingSoon}
            >
              <View
                style={{
                  width: "90%",
                  alignSelf: "center",
                  backgroundColor: colors.white,
                  borderRadius: metrics.dimen_10,
                  margin: metrics.dimen_20,
                  flexDirection: "column",
                  height: 200,
                  justifyContent: "center",
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Image
                      style={{
                        height: 150,
                        width: 150,
                        alignSelf: "center",
                        marginTop: -20,
                      }}
                      source={require("../Images/soon.png")}
                    />
                    <Text
                      style={{
                        fontSize: metrics.text_heading,
                        color: colors.black,
                        fontWeight: "bold",
                        textAlign: "center",
                        marginTop: -30,
                      }}
                    >
                      Postpaid Services
                    </Text>
                  </View>
                  <View style={styles.bottomview2}>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          isComingSoon: false,
                          fromPostPaid: false,
                          fromPrepaid: true,
                        })
                      }
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          color: "white",
                          fontWeight: "bold",
                          textAlign: "center",
                          alignSelf: "center",
                          marginLeft: 10,
                        }}
                      >
                        {"OK "}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
          </View>

          <RBSheet
            ref={(ref) => {
              this.RBSheetAlert = ref;
            }}
            height={Dimensions.get("screen").height}
            duration={0}
          >
            {this.renderCountries()}
          </RBSheet>
        </View>
        <View style={styles.bottomview}>
          <TouchableOpacity onPress={() => this.onProceedtoNext()}>
            <Text
              style={{
                fontSize: 15,
                color: "white",
                fontFamily:metrics.quicksand_semibold,
                textAlign: "center",
                alignSelf: "center",
                marginLeft: 10,
              }}
            >
              {"Proceed to Top-up "}
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: metrics.dimen_70,
    backgroundColor: colors.theme_caribpay,
    flexDirection: "row",
    paddingHorizontal: metrics.dimen_20,
  },
  headerleftImage: {
    height: metrics.dimen_24,
    width: metrics.dimen_24,
    alignSelf: "center",
    resizeMode: "contain",
    tintColor: colors.whitesmoke,
    marginBottom: metrics.dimen_15,
  },
  headertextStyle: {
    fontSize: metrics.text_17,
    color: colors.white,
    marginBottom: metrics.dimen_15,
    fontFamily: metrics.quicksand_bold,
    textAlignVertical: "center",
    paddingHorizontal: metrics.dimen_20,
  },
  absouluteview: {
    position: "absolute",
    top: 51,
    borderTopRightRadius: metrics.dimen_20,
    borderTopLeftRadius: metrics.dimen_20,
    width: "100%",
    height: Dimensions.get("screen").height,
    backgroundColor: colors.white,
  },
  container: { flexDirection: "row", margin: metrics.dimen_20 },
  image_style: {
    height: metrics.dimen_30,
    width: metrics.dimen_30,
    resizeMode: "contain",
  },
  headingg: { fontSize: metrics.text_header, color: colors.black },
  descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.carib_pay_blue,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 20,
  },
  arrowstyle: {
    height: metrics.dimen_15,
    width: metrics.dimen_15,
    resizeMode: "contain",
  },
  horizontalLine: {
    borderBottomColor: "#D3D3D3",
    borderBottomWidth: 1,
    width: "95%",
    alignSelf: "center",
  },
  bottomview2: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_5,
  },
});
