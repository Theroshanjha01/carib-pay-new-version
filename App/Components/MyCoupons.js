import React from "react";
import { View, Text, Image, TouchableOpacity, FlatList } from "react-native";
import { Input } from "react-native-elements";
import colors from "../Themes/Colors";
import metrics from "../Themes/Metrics";
let expiryToday = [
  {
    idNum: 1,
    brand: "Pantaloons",
    title: "30% off in your any purchase",
    validity: "3rd Dec 2020",
    discount_rate: "30%",
    image:
      "https://content.pantaloons.com/uploads/2019/12/11135526/17-06-2019-pantaloons-logo.jpg",
  },
  {
    idNum: 2,
    brand: "Levis",
    title: "10% off in your any purchase",
    validity: "3rd Dec 2020",
    discount_rate: "10%",
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTFkDeyb7yHWMdWd_5pQnCJ2CptGyAzl6DESA&usqp=CAU",
  },
  {
    idNum: 3,
    brand: "UCB",
    title: "40% off in your any purchase",
    validity: "3rd Dec 2020",
    discount_rate: "40%",
    image:
      "https://www.benetton.com/on/demandware.static/Sites-UCB_world-Site/-/default/dwbf622814/favicons/favicon-512x512.png",
  },
];

let expirySoon = [
  {
    idNum:6,
    brand: "Pantaloons",
    title: "30% off in your any purchase",
    validity: "13th Nov 2020",
    discount_rate: "30%",
    image:
      "https://content.pantaloons.com/uploads/2019/12/11135526/17-06-2019-pantaloons-logo.jpg",
  },
  {
    idNum:6,
    brand: "Levis",
    title: "10% off in your any purchase",
    validity: "13th Nov 2020",
    discount_rate: "10%",
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTFkDeyb7yHWMdWd_5pQnCJ2CptGyAzl6DESA&usqp=CAU",
  },
  {
    idNum:6,
    color: "#D0CECD",
    brand: "UCB",
    title: "40% off in your any purchase",
    validity: "13th Nov 2020",
    discount_rate: "40%",
    image:
      "https://www.benetton.com/on/demandware.static/Sites-UCB_world-Site/-/default/dwbf622814/favicons/favicon-512x512.png",
  },
];

export default class MyCoupons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isExpToday: true,
      isExpSoon: false,
    };
  }
  componentDidMount() {}

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: colors.white }}>
        <Input
          containerStyle={{
            alignSelf: "center",
            backgroundColor: colors.light_grey_backgroud,
            borderRadius: 5,
            height: 35,
            width: "95%",
            marginTop: 10,
            marginBottom: 5,
          }}
          inputContainerStyle={{ borderBottomWidth: 0 }}
          placeholder="Search coupons"
          placeholderTextColor="#9D9D9F"
          inputStyle={{
            color: colors.black,
            fontSize: metrics.text_normal,
            marginTop: -15,
            fontFamily: metrics.quicksand_bold,
          }}
          //   onChangeText={(text) =>
          //     this.setState({ search_text: text }, () =>
          //       setTimeout(this.doProductSearch, 500)
          //     )
          //   }
          rightIcon={
            <Image
              style={{
                marginTop: -10,
                width: metrics.dimen_25,
                height: metrics.dimen_25,
                resizeMode: "contain",
                tintColor: colors.app_gray,
              }}
              source={require("../Images/find.png")}
            />
          }
        />
        <View
          style={{
            height: 50,
            backgroundColor: colors.white,
            borderRadius: 5,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <TouchableOpacity
            onPress={() =>
              this.setState({ isExpToday: true, isExpSoon: false })
            }
          >
            <View
              style={{
                justifyContent: "center",
                alignSelf: "center",
                marginLeft: 20,
                height: 50,
                borderBottomColor: this.state.isExpToday
                  ? colors.app_yellow
                  : colors.transparent,
                borderBottomWidth: 2,
              }}
            >
              <Text
                style={{
                  fontFamily: metrics.quicksand_semibold,
                  color: this.state.isExpToday
                    ? colors.app_yellow
                    : colors.app_gray,
                  fontSize: metrics.text_17,
                  alignSelf: "center",
                }}
              >
                Expiring Today
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() =>
              this.setState({ isExpToday: false, isExpSoon: true })
            }
          >
            <View
              style={{
                justifyContent: "center",
                alignSelf: "center",
                marginRight: 20,
                height: 50,
                borderBottomColor: this.state.isExpSoon
                  ? colors.app_yellow
                  : colors.transparent,
                borderBottomWidth: 2,
              }}
            >
              <Text
                style={{
                  fontFamily: metrics.quicksand_semibold,
                  color: this.state.isExpSoon
                    ? colors.app_yellow
                    : colors.app_gray,
                  fontSize: metrics.text_17,
                  alignSelf: "center",
                }}
              >
                Expiring Soon
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <FlatList
          data={this.state.isExpToday ? expiryToday : expirySoon}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("MyCouponsDetails", {
                  data: item,
                  brandName: item.brand,
                })
              }
            >
              <View style={{ margin: 20 }}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    backgroundColor:
                      item.idNum == 1
                        ? "#F8CEAB"
                        : item.idNum == 2
                        ? colors.lightgreen
                        : item.idNum == 3 ? "#77b5fe" : "#A8A8A8",
                    height: 100,
                    borderTopLeftRadius: 10,
                    borderTopRightRadius: 10,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      margin: 10,
                    }}
                  >
                    <Image
                      source={{ uri: item.image }}
                      style={{
                        height: 50,
                        width: 50,
                        borderRadius: 25,
                        alignSelf: "center",
                      }}
                    />
                    <View
                      style={{
                        flexDirection: "column",
                        marginLeft: 5,
                        alignSelf: "center",
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: metrics.quicksand_bold,
                          color: colors.white,
                          marginTop: -10,
                          marginBottom: 10,
                        }}
                      >
                        {item.brand.toLocaleUpperCase()}
                      </Text>
                      <Text
                        style={{
                          fontFamily: metrics.quicksand_bold,
                          color: colors.white,
                        }}
                      >
                        {item.title}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{ flexDirection: "column", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: metrics.quicksand_bold,
                        color: colors.white,
                        fontSize: 25,
                        alignSelf: "center",
                        marginTop: -15,
                      }}
                    >
                      {item.discount_rate}
                    </Text>

                    <Text
                      style={{
                        fontFamily: metrics.quicksand_bold,
                        color: colors.white,
                        alignSelf: "center",
                      }}
                    >
                      Discount
                    </Text>
                  </View>

                  <View />
                </View>
                <View
                  style={{
                    backgroundColor: colors.white,
                    borderColor: colors.light_grey_backgroud,
                    borderWidth: 1,
                  }}
                >
                  <Text
                    style={{
                      fontFamily: metrics.quicksand_bold,
                      color: colors.app_gray,
                      margin: 10,
                      marginLeft: 5,
                      // alignSelf: "center",
                    }}
                  >
                    {"Valid until : " + item.validity}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}
