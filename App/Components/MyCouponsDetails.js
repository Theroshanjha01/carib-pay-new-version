import * as React from "react";
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  BackHandler,
  Clipboard,
} from "react-native";
import colors from "../Themes/Colors";
import metrics from "../Themes/Metrics";
import Modal from "react-native-modal";
import RBSheet from "react-native-raw-bottom-sheet";
import Toast, { DURATION } from "react-native-easy-toast";
import StepIndicator from "react-native-step-indicator";
const labels = ["", "", ""];
const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: "#fe7013",
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: "#fe7013",
  stepStrokeUnFinishedColor: "#aaaaaa",
  separatorFinishedColor: "#fe7013",
  separatorUnFinishedColor: "#aaaaaa",
  stepIndicatorFinishedColor: "#fe7013",
  stepIndicatorUnFinishedColor: "#ffffff",
  stepIndicatorCurrentColor: "#ffffff",
  stepIndicatorLabelFontSize: 14,
  currentStepIndicatorLabelFontSize: 14,
  stepIndicatorLabelCurrentColor: "#fe7013",
  stepIndicatorLabelFinishedColor: "#ffffff",
  stepIndicatorLabelUnFinishedColor: "#aaaaaa",
  labelColor: colors.app_gray,
  labelSize: 14,
  currentStepLabelColor:colors.theme_orange_color_caribpay,
};

export default class MyCouponsDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.navigation.state.params.data,
      brandName: this.props.navigation.state.params.brandName,
      currentPosition:3
    };
  }

  componentDidMount() {
    console.log(this.state.data);
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  saveVoucherCode = () => {
    Clipboard.setString("ROXL 3409 ABC9 PPXZ TTRQ");
    this.refs.toast.show("Copy code successfully!");
  };

  renderTncs = () => {
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            margin: 10,
          }}
        >
          <Text
            style={{
              fontFamily: metrics.quicksand_bold,
              fontSize: metrics.text_16,
            }}
          >
            {"Terms & Conditions"}
          </Text>
          <TouchableOpacity onPress={() => this.RBSheetAlert.close()}>
            <Text
              style={{
                fontFamily: metrics.quicksand_bold,
                color: colors.app_orange,
              }}
            >
              {"Cancel"}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ margin: 5 }}>
          <Text style={styles.textStyle}>
            1. User get 5% Cashback upto $200 on shopping from CaribPay Mall
          </Text>
          <Text style={styles.textStyle}>
            2. Minimum transaction value is $50
          </Text>
          <Text style={styles.textStyle}>
            3. Voucher is valid for one time usage per user
          </Text>
          <Text style={styles.textStyle}>
            4. Cashback will be credited within 24 hours
          </Text>
          <Text style={styles.textStyle}>
            5. Voucher is valid for selected products
          </Text>
          <Text style={styles.textStyle}>6. Voucher is valid for 30 Days</Text>
          <Text style={styles.textStyle}>
            7. Voucher is applicable on only CaribPay App
          </Text>
          <Text style={styles.textStyle}>
            8. Cancelled order will not be eligible for Cashback
          </Text>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.scene}>
        <ScrollView>
          <View
            style={{
              width: "100%",
              alignSelf: "center",
              //   borderRadius: 10,
            }}
          >
            <Image
              style={{
                width: "100%",
                height: metrics.dimen_200,
                // borderRadius: 10,
              }}
              source={{ uri: this.state.data.image }}
            />

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                position: "absolute",
                top: 10,
              }}
            >
              <TouchableOpacity
                onPress={() => this.props.navigation.goBack(null)}
              >
                <Image
                  style={{
                    height: 24,
                    width: 24,
                    resizeMode: "center",
                    alignSelf: "center",
                    marginLeft: 10,
                    tintColor: colors.app_gray,
                  }}
                  source={require("../Images/leftarrow.png")}
                />
              </TouchableOpacity>

             
            </View>
          </View>

          <Image
            style={{
              width: 60,
              height: 60,
              borderRadius: 10,
              borderColor: colors.app_gray,
              borderWidth: 1.5,
              position: "absolute",
              top: 160,
              left: 15,
            }}
            source={{ uri: this.state.data.image }}
          />

          <View style={{ marginTop: 20 }}>
            <Text
              style={{
                marginTop: 20,
                marginLeft: 10,
                fontSize: metrics.text_16,
                fontFamily: metrics.quicksand_bold,
              }}
            >
              {"You Won"}
            </Text>

            <Text
              style={{
                marginLeft: 10,
                fontSize: 22,
                fontFamily: metrics.quicksand_bold,
                color: colors.black,
              }}
            >
              {this.state.brandName + " Fashion Voucher - "}
            </Text>

            <Text
              style={{
                marginLeft: 10,
                fontSize: 22,
                fontFamily: metrics.quicksand_bold,
                color: colors.black,
              }}
            >
              {this.state.data.title}
            </Text>

            <Text
              style={{
                marginLeft: 10,
                fontSize: metrics.text_normal,
                marginTop: 5,
                fontFamily: metrics.quicksand_medium,
                color: colors.theme_orange_color_caribpay,
              }}
            >
              {"Valid until " + this.state.data.validity}
            </Text>

            <Text
              style={{
                marginLeft: 10,
                fontSize: metrics.text_normal,
                marginTop: 40,
                fontFamily: metrics.quicksand_bold,
                color: colors.app_gray,
              }}
            >
              {
                "To avail this voucher , use the following voucher code on shopping of minimum order $300"
              }
            </Text>

            <Text
              style={{
                marginLeft: 10,
                fontSize: metrics.text_normal,
                marginTop: 10,
                fontFamily: metrics.quicksand_bold,
                color: colors.app_gray,
                textDecorationLine: "underline",
              }}
            >
              {"More Details"}
            </Text>

            <TouchableOpacity onPress={() => this.RBSheetAlert.open()}>
              <Text
                style={{
                  marginLeft: 10,
                  fontSize: metrics.text_normal,
                  marginTop: 2,
                  fontFamily: metrics.quicksand_bold,
                  color: colors.app_gray,
                  marginTop: 10,
                  textDecorationLine: "underline",
                }}
              >
                {"Terms & Conditions"}
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              // position: "absolute",
              // bottom: 70,
              height: 50,
              // left: 10,
              flexDirection: "row",
              borderRadius: 25,
              // justifyContent: "center",
              marginTop: 30,
              marginLeft: 10,
            }}
          >
            <TouchableOpacity
              style={{
                height: 50,
                flexDirection: "row",
                borderRadius: 25,
                // justifyContent: "center",
              }}
              onPress={() => this.saveVoucherCode()}
            >
              <Image
                style={{
                  height: metrics.dimen_24,
                  width: metrics.dimen_24,
                  resizeMode: "contain",
                  alignSelf: "center",
                }}
                source={require("../Images/copy-content.png")}
              />
              <View style={{ marginLeft: 10 }}>
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    fontFamily: metrics.quicksand_bold,
                    color: colors.app_gray,
                  }}
                >
                  {"Tap to copy code & avail offer"}
                </Text>
                <Text
                  style={{
                    fontSize: metrics.text_normal,
                    fontFamily: metrics.quicksand_bold,
                    color: colors.black,
                  }}
                >
                  {"ROXL 3409 ABC9 PPXZ TTRQ"}
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{
              borderBottomColor: colors.light_grey_backgroud,
              borderBottomWidth: 4,
              width: "100%",
              alignSelf: "center",
              marginTop: 20,
            }}
          />

<View style={{ margin: 10, marginBottom: 80 }}>
            <Text
              style={{
                fontSize: metrics.text_17,
                fontFamily: metrics.quicksand_bold,
                marginBottom: 5,
              }}
            >
              How to Redeem
            </Text>

            <View style={{ height: 200, flexDirection: "row" }}>
              <StepIndicator
                customStyles={customStyles}
                direction={"vertical"}
                stepCount={3}
                currentPosition={this.state.currentPosition}
                labels={labels}
                style={{ marginLeft: -15 }}
              />
              <View
                style={{
                  flexDirection: "column",
                  marginLeft: 30,
                  marginTop: 15,
                }}
              >
                <Text style={styles.textStyle}>
                  Visit the store to redeem the voucher.
                </Text>
                <Text style={{...styles.textStyle,marginTop:30}}>
                  Open the voucher from MY Coupons section of the CaribPay App.
                </Text>
                <Text style={{...styles.textStyle,marginTop:30}}>
                  Share the voucher at the cashier at the time of payment.
                </Text>
              </View>
            </View>
          </View>
        </ScrollView>

        <View
          style={{
            position: "absolute",
            bottom: 10,
            left: 20,
            right: 20,
            height: 50,
            backgroundColor: colors.theme_caribpay,
            borderRadius: 25,
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={() =>
              this.setState({
                isComingSoon: true,
              })
            }
          >
            <Text
              style={{
                fontSize: metrics.text_16,
                fontFamily: metrics.quicksand_bold,
                color: colors.white,
                alignSelf: "center",
              }}
            >
              {"REDEEM NOW"}
            </Text>
          </TouchableOpacity>
        </View>

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.isComingSoon}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                // margin: metrics.dimen_10,
                flexDirection: "column",
                height: 200,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isComingSoon: false,
                    })
                  }
                >
                  <Image
                    style={{
                      height: metrics.dimen_16,
                      width: metrics.dimen_16,
                      resizeMode: "contain",
                      margin: 15,
                    }}
                    source={require("../Images/cross-sign.png")}
                  />
                </TouchableOpacity>
                <Image
                  style={{
                    height: 150,
                    width: 150,
                    resizeMode: "cover",
                    alignSelf: "center",
                    marginTop: -30,
                  }}
                  source={require("../Images/soon.png")}
                />
                <Text
                  style={{
                    fontSize: metrics.text_heading,
                    color: colors.black,
                    fontFamily: metrics.quicksand_bold,
                    textAlign: "center",
                    marginTop: -20,
                  }}
                >
                  {"REDEEM NOW"}
                </Text>
              </View>
            </View>
          </Modal>
        </View>
        <Toast
          ref="toast"
          style={{ backgroundColor: "black" }}
          position="center"
          positionValue={200}
          fadeInDuration={200}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: "white" }}
        />

        <RBSheet
          ref={(ref) => {
            this.RBSheetAlert = ref;
          }}
          height={400}
          closeOnDragDown={true}
          closeOnPressMask={false}
          customStyles={{
            wrapper: {
              backgroundColor: "transparent",
            },
            draggableIcon: {
              backgroundColor: "#000",
            },
            container: {
              borderTopLeftRadius: 30,
              borderTopRightRadius: 30,
            },
          }}
        >
          {this.renderTncs()}
        </RBSheet>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
    backgroundColor: colors.white,
  },
  textStyle: {
    fontFamily: metrics.quicksand_semibold,
    fontSize: metrics.text_normal,
    margin: 5,
  },
});
