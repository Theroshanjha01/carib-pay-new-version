import React from 'react';
import { Text, FlatList, View, Image, TouchableOpacity, BackHandler } from 'react-native'
import metrics from '../Themes/Metrics';
import colors from '../Themes/Colors.js';
var Spinner = require('react-native-spinkit');
const axios = require('axios');
import Modal from 'react-native-modal';
const moment = require('moment');



export default class NotificationDetails extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.navigation.state.params.data,
            token: this.props.navigation.state.params.token,
            title: '',
            content: '',
            date: '', type: ''
        }
    }

    componentDidMount() {
        this.getNotificationDetails();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }


    getNotificationDetails = () => {
        this.setState({
            spinvisible: true
        })

        let postData = {
            user_id: this.state.data.user,
            message_id: this.state.data.id
        }

        console.log("postData", postData)

        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/message_detail',
            headers: { 'Authorization': this.state.token },
            data: postData
        }).then((response) => {
            console.log("notification detailed response ---> ", response.data.success.data)
            this.setState({
                title: response.data.success.data.title,
                content: response.data.success.data.content,
                date: response.data.success.data.created_at,
                type: response.data.success.data.type,
                spinvisible: false
            })
        }).catch((err) => {
            console.log("notification detailed response  error   --- > ", err)
            this.setState({
                spinvisible: false
            })

        })
    }



    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.light_grey_backgroud }}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                    <Image style={{ margin: 10, height: metrics.dimen_30, width: metrics.dimen_30, resizeMode: 'contain', tintColor: colors.black }}
                        source={require('../Images/leftarrow.png')} />
                </TouchableOpacity>

                {!this.state.spinvisible && this.state.type != 'push' && < View style={{ justifyContent: 'center', backgroundColor: colors.light_grey_backgroud, height: 200 }}>
                    <Image source={require("../Images/Pic.png")}
                        style={{ height: 100, width: 100, alignSelf: 'center', resizeMode: 'contain' }}></Image>
                    {/* <Text style={{ fontSize: 13, color: colors.app_gray, fontWeight: '500', marginTop: 3, textAlign: 'center' }}>{"Image not available"}</Text> */}
                </View>}
                <View style={{ backgroundColor: colors.white, height: 70, justifyContent: 'center' }}>
                    <Text style={{ fontSize: 20, color: colors.black, fontFamily:metrics.quicksand_bold, marginLeft: 10 }}>{this.state.title}</Text>
                    {this.state.date != '' && <Text style={{ textAlign: 'right', fontSize: 13, color: colors.app_gray, fontFamily:metrics.quicksand_semibold, marginRight: 20 }}>{moment(this.state.date).format("MM-DD-YY , h:mm:ss a")}</Text>}
                </View>
                <View style={{ backgroundColor: colors.white, height: 70, justifyContent: 'center', marginTop: 10 }}>
                    <Text style={{ fontSize: 13, color: colors.app_gray,fontFamily:metrics.quicksand_semibold, margin: 10 }}>{this.state.content}</Text>
                </View>

                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.spinvisible}>
                    <View style={{ width: "50%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 100 }}>
                        <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinvisible} size={70} type={"ThreeBounce"} color={colors.black} />
                    </View>
                </Modal>



            </View >
        )
    }
}