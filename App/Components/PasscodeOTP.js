import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    BackHandler,
    SafeAreaView,
} from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
import ProgressDialog from '../ProgressDialog.js';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
const axios = require('axios');
import Modal from 'react-native-modal';
import auth from '@react-native-firebase/auth';
import Toast, { DURATION } from 'react-native-easy-toast';

export default class PasscodeOTP extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            otp: this.props.navigation.state.params.otp,
            code: '',
            data: this.props.navigation.state.params.data,
            phone: this.props.navigation.state.params.phone,
            fieldType: this.props.navigation.state.params.fieldType,
            remainingTime: 60,
            verificationId: '',
            invalid: false,
            user_id: this.props.navigation.state.params.user_id,
            token: this.props.navigation.state.params.token
        };
    }

    componentDidMount() {
        console.log('data', this.state.data, this.state.user_id , this.state.token);
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        setInterval(() => {
            this.setState(prevState => {
                if (this.state.remainingTime > 0) {
                    return { remainingTime: prevState.remainingTime - 1 };
                } else {
                    return null;
                }
            });
        }, 1000);
        this.verifyPhoneNumber();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        clearInterval();
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    };

    onConfirmClick = () => {
        this.setState({ visible: true });
        let data = this.state.data;
        let postData = { email: data };
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/forgot-password',
            data: postData,
        })
            .then(response => {
                console.log(response.data);
                if (response.data.status == 200) {
                    console.log('otp', response.data.otp);
                    this.setState({ remainingTime: 60, visible: false });
                } else if (response.data.status == 201) {
                    this.setState({ visible: false });
                    this.setState({
                        invalid: true,
                    });
                } else {
                    this.setState({ visible: false });
                    // showAlert("CaribPay", "Something went wrong")
                }
            })
            .catch(err => {
                this.setState({ visible: false });
            });
    };

    verifyOTP = async () => {
        try {
            if (this.state.code == '' || this.state.code?.length !== 6) {
                // this.logoutDevice();
                this.refs.toast.show('OTP Required.', DURATION.LENGTH_LONG);
            } else {
                this.setState({ visible: true });
                const credential = await auth.PhoneAuthProvider.credential(
                    this.state.verificationId,
                    this.state.code,
                );
                console.log('LOG: verifyCode: credential: ', credential);
                if (credential?.token?.length > 0) {
                    const userCredential = await auth().signInWithCredential(credential);
                    this.setState({ visible: false });
                    this.props.navigation.navigate('SetPasscode', {
                        user_id: this.state.user_id,
                        token: this.state.token,
                        fromForgetpassword:true
                    })
                } else {
                    this.setState({ visible: false });
                    this.setState({
                        invalid: true,
                    });
                    this.refs.toast.show(
                        'Unable to verify user authentication.',
                        DURATION.LENGTH_LONG,
                    );
                }
            }
        } catch (error) {
            console.log('LOG: verifyOTP: ', error);
            this.setState({ visible: false });
            this.setState({
                invalid: true,
            });
            switch (error?.code) {
                case 'auth/invalid-verification-code':
                    this.refs.toast.show(
                        'Invalid Verification Code.',
                        DURATION.LENGTH_LONG,
                    );
                    break;
                default:
                    this.refs.toast.show(
                        error?.userInfo?.message || 'Invalid Verification Code.',
                        DURATION.LENGTH_LONG,
                    );
                    break;
            }
        }
    };

    verifyPhoneNumber = async () => {
        try {
            this.setState({ visible: true });
            const confirmation = auth()
                .verifyPhoneNumber(this.state.data, 120, true)
                .on(
                    'state_changed',
                    phoneAuthSnapshot => {
                        console.log('LOG: verifyPhoneNumber: state_changed: ', {
                            phoneAuthSnapshot,
                            phone: this.state.data,
                            verificationId: this.state.verificationId,
                        });
                        this.setState({ visible: false });

                        switch (phoneAuthSnapshot.state) {
                            case auth.PhoneAuthState.CODE_SENT: // or 'sent'
                                if (phoneAuthSnapshot?.verificationId?.length) {
                                    this.refs.toast.show(
                                        'OTP Sent Successfully.',
                                        DURATION.LENGTH_LONG,
                                    );
                                    this.setState({
                                        remainingTime: 60,
                                        verificationId: phoneAuthSnapshot.verificationId,
                                    });
                                } else {
                                    this.refs.toast.show(
                                        'OTP Sent Failed.',
                                        DURATION.LENGTH_LONG,
                                    );
                                }
                                break;

                            case auth.PhoneAuthState.ERROR: // or 'error'
                                if (phoneAuthSnapshot?.error?.code) {
                                    switch (phoneAuthSnapshot.error.code) {
                                        case 'auth/invalid-verification-code':
                                            this.refs.toast.show(
                                                'Invalid Verification Code.',
                                                DURATION.LENGTH_LONG,
                                            );
                                            break;

                                        case 'auth/too-many-requests':
                                            this.refs.toast.show(
                                                'Too many request in short period, please try again in an hour.',
                                                DURATION.LENGTH_LONG,
                                            );
                                            break;

                                        default:
                                            this.refs.toast.show(
                                                phoneAuthSnapshot.error.userInfo?.message ||
                                                'Invalid Verification Code.',
                                                DURATION.LENGTH_LONG,
                                            );
                                            break;
                                    }
                                } else {
                                    this.refs.toast.show(
                                        'Unable to send verification code, please try again later.',
                                        DURATION.LENGTH_LONG,
                                    );
                                }

                                break;

                            // ---------------------
                            // ANDROID ONLY EVENTS
                            // ---------------------
                            case auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT: // or 'timeout'
                                // proceed with your manual code input flow, same as you would do in CODE_SENT if you were on IOS
                                break;

                            case auth.PhoneAuthState.AUTO_VERIFIED: // or 'verified'
                                // auto verified means the code has also been automatically confirmed as correct/received
                                // phoneAuthSnapshot.code will contain the auto verified sms code - no need to ask the user for input.

                                if (phoneAuthSnapshot.code?.length === 6) {
                                    this.setState({ code: phoneAuthSnapshot.code });
                                    this.refs.toast.show(
                                        'Auto Verification successfull.',
                                        DURATION.LENGTH_LONG,
                                    );
                                    this.setState({ visible: false });
                                    this.props.navigation.navigate('SetPasscode', {
                                        user_id: this.state.data,
                                        token: this.state.token,
                                    })
                                } else {
                                    this.refs.toast.show(
                                        'Failed to send OTP.',
                                        DURATION.LENGTH_LONG,
                                    );
                                }
                                break;
                        }
                    },
                    error => {
                        this.setState({ visible: false });
                        console.log('phoneAuthSnapshot: state_changed error: ', error);
                        console.log(
                            'phoneAuthSnapshot: state_changed error: ',
                            error.verificationId,
                        );
                    },
                    phoneAuthSnapshot => {
                        this.setState({ visible: false });
                        console.log('phoneAuthSnapshot: ', phoneAuthSnapshot);
                    },
                );
        } catch (error) {
            console.log('LOG: verifyPhoneNumber: TRY-CATCH: ', error);
            this.setState({ visible: false });
        }
    };

    logoutDevice = async () => {
        try {
            this.setState({ visible: false });
            let data = {
                user_id: this.state.user_id,
            };
            console.log('postdata register device ---> ', data);
            axios({
                method: 'post',
                url: 'https://sandbox.caribpayintl.com/api/logout',
                data: data,
            })
                .then(response => {
                    console.log('response logoutDevice --> ', response.data);
                    this.refs.toast.show(
                        'OTP verified, please login now!',
                        DURATION.LENGTH_LONG,
                    );
                    this.props.navigation.goBack(null);
                })
                .catch(err => {
                    console.log('catch logoutDevice --> ', err);
                    alert(err);
                });
        } catch (error) {
            console.log('LOG: logoutDevice: ', error);
            this.setState({ visible: false });
        }
    };

    onOTPVerify = () => {
        if (this.state.code == '') {
            this.refs.toast.show('Proper OTP Required!', DURATION.LENGTH_LONG);
        } else {
            let otp_entered = this.state.code;
            if (otp_entered == this.state.otp) {
                this.props.navigation.navigate('SetPasscode', {
                    user_id: this.state.data,
                    token: this.state.token,
                })
            } else {
                this.setState({
                    invalid: true,
                });
            }
        }
    };

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <ProgressDialog visible={this.state.visible} />
                    <View
                        style={{
                            backgroundColor: colors.theme_caribpay,
                            height: metrics.newView.upperview,
                        }}>
                        <View style={{ margin: metrics.dimen_20 }}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.goBack(null)}>
                                <View
                                    style={{
                                        height: metrics.dimen_40,
                                        width: metrics.dimen_40,
                                        justifyContent: 'center',
                                    }}>
                                    <Image
                                        style={{
                                            height: metrics.dimen_25,
                                            width: metrics.dimen_25,
                                            tintColor: 'white',
                                        }}
                                        source={require('../Images/leftarrow.png')}
                                    />
                                </View>
                            </TouchableOpacity>
                            <Image
                                style={styles.imageStyle}
                                source={require('../Images/logo.png')}
                            />
                            <Text
                                style={{
                                    fontSize: metrics.text_heading,
                                    color: colors.white,
                                    fontWeight: 'bold',
                                }}>
                                Enter OTP
              </Text>
                        </View>
                    </View>

                    <View style={styles.curveview}>
                        <Text
                            style={{
                                textAlignVertical: 'center',
                                textAlign: 'center',
                                color: colors.theme_caribpay,
                                fontWeight: 'bold',
                                marginTop: metrics.dimen_20,
                            }}>
                            {'Enter your 6-Digit OTP sent on \n' + this.state.data}
                        </Text>
                        <View
                            style={{ marginTop: 30, alignSelf: 'center', marginBottom: 30 }}>
                            <SmoothPinCodeInput
                                placeholder=""
                                cellSize={44}
                                codeLength={6}
                                cellSpacing={6}
                                keyboardType="phone-pad"
                                returnKeyLabel="Done"
                                returnKeyType="done"
                                cellStyle={{
                                    borderWidth: 2,
                                    borderRadius: 7,
                                    borderColor: colors.theme_caribpay,
                                    backgroundColor: colors.light_grey_backgroud,
                                }}
                                cellStyleFocused={{
                                    borderColor: 'lightseagreen',
                                    backgroundColor: 'lightcyan',
                                }}
                                textStyle={{
                                    fontSize: 24,
                                    color: colors.black,
                                    fontWeight: 'bold',
                                }}
                                textStyleFocused={{
                                    color: 'crimson',
                                }}
                                value={this.state.code}
                                // onFulfill={() => setTimeout(() => { this.onSetPasscodeClick() }, 500)}
                                onTextChange={code => this.setState({ code })}
                            />
                        </View>

                        <Text
                            style={{
                                textAlignVertical: 'center',
                                textAlign: 'center',
                                color: colors.heading_black_text,
                                width: 300,
                                marginBottom: metrics.dimen_40,
                                alignSelf: 'center',
                            }}>
                            {this.state.fromLogin == '1'
                                ? 'Once your phone number is verified, you can reset your password.'
                                : this.state.fromLogin == '2'
                                    ? 'Once your phone number is verified, you can login using this device.'
                                    : 'Once your phone number is verified, you can change your passcode.'}
                        </Text>
                        {this.state.remainingTime > 0 && (
                            <Text
                                style={{
                                    textAlignVertical: 'center',
                                    textAlign: 'center',
                                    color: colors.theme_ornage_color,
                                    fontWeight: '400',
                                    marginBottom: metrics.dimen_40,
                                }}>
                                {this.state.remainingTime + ' Seconds Left'}
                            </Text>
                        )}

                        <TouchableOpacity onPress={() => this.verifyOTP()}>
                            <View elevation={1} style={styles.buttonStyle}>
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: 'white',
                                        fontWeight: 'bold',
                                        textAlign: 'center',
                                        alignSelf: 'center',
                                    }}>
                                    Verify
                </Text>
                            </View>
                        </TouchableOpacity>

                        {this.state.remainingTime == 0 && (
                            <TouchableOpacity onPress={() => this.verifyPhoneNumber()}>
                                <Text style={{ ...styles.info, marginTop: metrics.dimen_20 }}>
                                    Not received your code ?{' '}
                                    <Text style={{ ...styles.info, color: colors.theme_caribpay }}>
                                        Resend Code
                  </Text>
                                </Text>
                            </TouchableOpacity>
                        )}
                    </View>

                    <Toast
                        ref="toast"
                        style={{ backgroundColor: 'black' }}
                        position="center"
                        positionValue={200}
                        fadeInDuration={200}
                        fadeOutDuration={1000}
                        opacity={0.8}
                        textStyle={{ color: 'white' }}
                    />

                    <View>
                        <Modal
                            style={{ borderRadius: metrics.dimen_10 }}
                            isVisible={this.state.invalid}>
                            <View
                                style={{
                                    width: '90%',
                                    alignSelf: 'center',
                                    backgroundColor: colors.white,
                                    borderRadius: metrics.dimen_10,
                                    margin: metrics.dimen_20,
                                    flexDirection: 'column',
                                    height: 250,
                                    justifyContent: 'center',
                                }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Image
                                            style={{
                                                height: metrics.dimen_80,
                                                width: metrics.dimen_100,
                                                resizeMode: 'contain',
                                                alignSelf: 'center',
                                                marginTop: 20,
                                            }}
                                            source={require('../Images/logo.png')}
                                        />
                                        <Text
                                            style={{
                                                fontSize: metrics.text_heading,
                                                color: colors.black,
                                                fontWeight: 'bold',
                                                textAlign: 'center',
                                                margin: 15,
                                            }}>
                                            {'INVALID!'}
                                        </Text>
                                        <Text
                                            style={{
                                                fontSize: metrics.text_normal,
                                                color: colors.app_gray,
                                                margin: 15,
                                                fontWeight: 'bold',
                                                textAlign: 'center',
                                                width: 300,
                                                alignSelf: 'center',
                                            }}>
                                            {' '}
                                            {'You have entered wrong OTP!'}
                                        </Text>
                                    </View>
                                    <View style={styles.bottomview2}>
                                        <TouchableOpacity
                                            onPress={() => this.setState({ invalid: false })}>
                                            <Text
                                                style={{
                                                    fontSize: 15,
                                                    color: 'white',
                                                    fontWeight: 'bold',
                                                    textAlign: 'center',
                                                    alignSelf: 'center',
                                                    marginLeft: 10,
                                                }}>
                                                {"Retry "}
                      </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </Modal>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    imageStyle: {
        height: metrics.dimen_60,
        width: metrics.dimen_60,
        resizeMode: 'contain',
        marginTop: 5,
    },
    textStyle: {
        color: colors.app_gray,
        fontSize: metrics.text_description,
        marginLeft: 15,
        marginTop: 15,
    },
    buttonStyle: {
        width: '80%',
        borderRadius: metrics.dimen_10,
        justifyContent: 'center',
        alignSelf: 'center',
        height: 40,
        backgroundColor: colors.theme_caribpay,
        shadowColor: '#f2f2f2',
        shadowOpacity: 1,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1,
        },
    },
    info: {
        textAlign: 'center',
        fontSize: metrics.text_description,
        color: colors.carib_pay_blue,
        marginBottom: metrics.dimen_20,
        width: '60%',
        alignSelf: 'center',
    },
    curveview: {
        height: metrics.newView.curveview,
        position: 'absolute',
        top: metrics.newView.curvetop,
        width: '100%',
        backgroundColor: colors.white,
        borderRadius: 40,
    },
    bottomview2: {
        bottom: 10,
        position: 'absolute',
        height: 40,
        backgroundColor: colors.theme_caribpay,
        width: '90%',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: metrics.dimen_5,
    },
});
