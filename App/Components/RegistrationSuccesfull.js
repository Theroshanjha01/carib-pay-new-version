import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, BackHandler } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import metrics from '../Themes/Metrics.js';
import colors from "../Themes/Colors";


export default class RegistrationSuccesfull extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isReset: this.props.navigation.state.params.isReset,
            user_id: this.props.navigation.state.params.user_id
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        BackHandler.exitApp();
    }

    onFinishBtnClick = () => {
        this.props.navigation.navigate('AccountVerification', { user_id: this.state.user_id, fromSignup: true })
    }



    render() {
        return (
            <View style={styles.container}>
                <LinearGradient colors={['#f9fdff', '#d7f3f7', '#b4e6f1']} style={{ flex: 1 }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                        <Image style={{ height: metrics.dimen_120, width: metrics.dimen_120, alignSelf: 'center', resizeMode: 'contain' }}
                            source={require('../Images/success.png')}></Image>
                        {this.state.isReset == "0" ?
                            <View>
                                <Text style={styles.info}>{"Your account is created \n successfully. Please upload your KYC verification documents."}</Text>
                            </View> :
                            <Text style={styles.info}>{"Your account password is reset \n successfully.Thank you for join CaribPay \n Let's Login."}</Text>
                        }
                        {this.state.isReset == "0" ?
                            <TouchableOpacity onPress={() => this.onFinishBtnClick()}>
                                <View elevation={1} style={styles.buttonStyle}>
                                    <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center' }}>Proceed to KYC</Text>
                                </View>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                                <View elevation={1} style={styles.buttonStyle}>
                                    <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center' }}>Login</Text>
                                </View>
                            </TouchableOpacity>
                        }

                    </View>






                </LinearGradient>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DCDCDC'
    },
    info: {
        textAlign: 'center', fontSize: metrics.text_16, fontFamily: metrics.quicksand_bold, fontWeight: 'bold', marginBottom: metrics.dimen_20, marginTop: metrics.dimen_40
    }, buttonStyle: {
        width: '90%',
        borderRadius: 5,
        justifyContent: 'center',
        alignSelf: 'center',
        height: 40,
        backgroundColor: colors.theme_caribpay,
        shadowColor: "#f2f2f2",
        shadowOpacity: 1,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    }
});
