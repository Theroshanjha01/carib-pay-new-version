import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
var Spinner = require('react-native-spinkit');
import { Input } from 'react-native-elements';
const axios = require('axios');
import { showAlert } from '../Utils.js';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';


export default class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: '',
      confirmNewPassword: '',
      visible: false,
      email: this.props.navigation.state.params.email,
      phone: this.props.navigation.state.params.phone,
      fieldType: this.props.navigation.state.params.fieldType,
      data:this.props.navigation.state.params.data,
      show1: true,
      show2: true,
      show3: false,
      NoUserTrue: false,

    };
  }

  componentDidMount() {
    console.log("data value is -->",this.state.data , this.state.email)
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  onSubmitClick = () => {
    if (this.state.newPassword == '') {
      showAlert('CaribPay', 'Required New Password.');
    } else if (this.state.confirmNewPassword == '') {
      showAlert('CaribPay', 'Required Confirm Password.');
    } else if (this.state.confirmNewPassword != this.state.newPassword) {
      showAlert('CaribPay', 'Oops! Password Mismatchs.');
    } else {
      this.setState({ visible: true });
      let postData = {
        [this.state.fieldType ? 'email' : 'email']: this.state.phone,
        password: this.state.confirmNewPassword,
      };
      console.log(postData);
      axios({
        method: 'post',
        url: 'https://sandbox.caribpayintl.com/api/reset-password',
        data: postData,
      })
        .then(response => {
          console.log(response.data);
          if (response.data.status == 200) {
            // let apiToken;
            // axios
            //   .post(
            //     'https://caribmall.com/api/auth/login',
            //     {
            //       email: this.state,
            //       password: '123456789',
            //     },
            //   )
            //   .then(response => {
            //     console.log('caribmall login auth api response data ---> ', response.data.data['api_token']);
            //     apiToken = response.data.data['api_token']
            //   }).catch((error) => {
            //     console.log("caribmall login auth api error --> ", error)
            //   })


            console.log("carib mall forget password params-->", this.state.data , this.state.confirmNewPassword)

              axios
              .post(
                'https://caribmall.com/api/auth/password_forget',
                {
                  phone: this.state.email,
                  password: this.state.confirmNewPassword,
                },
              )
              .then(response => {
                console.log('caribmall pswd forget response ---> ', response.data);
              }).catch((error) => {
                console.log("caribmall pswd forget response error --> ", error)
              })

            // axios
            //   .put(
            //     'https://caribmall.com/api/password/update?api_token=' + apiToken,
            //     {
            //       password: this.state.confirmNewPassword,
            //       password_confirmation: this.state.confirmNewPassword,
            //     },
            //   )
            //   .then(response => {
            //     console.log('caribmall pswd update response ---> ', response.data);
            //   }).catch((error) => {
            //     console.log("caribmall pswd update response error --> ", error)
            //   })
            this.setState({ visible: false });
            this.setState({ show3: true });

          } else {
            this.setState({ visible: false, NoUserTrue: true });
            // alert(response.data.message);
          }
        })
        .catch(err => {
          this.setState({ visible: false });
          // alert(err);
        });
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.header}>
          <TouchableOpacity
            style={styles.headerleftImage}
            onPress={() => this.props.navigation.goBack(null)}>
            <Image
              style={styles.headerleftImage}
              source={require('../Images/leftarrow.png')}
            />
          </TouchableOpacity>
          <Text style={styles.headertextStyle}>Reset Password</Text>
        </View>

        <View style={styles.absouluteview}>
          <Image
            style={styles.imageStyle}
            source={require('../Images/logo.png')}
          />
          <Text
            style={{
              fontSize: metrics.text_normal,
              textAlign: 'center',
              color: colors.carib_pay_blue,
            }}>
            Enter Your New Password
          </Text>

          <Input
            placeholder={'New Password'}
            containerStyle={{
              width: '90%',
              height: metrics.dimen_50,
              borderColor: colors.app_gray,
              borderWidth: 1,
              borderRadius: metrics.dimen_7,
              marginTop: metrics.dimen_10,
              alignSelf: 'center',
            }}
            value={this.state.newPassword}
            secureTextEntry={this.state.show1 ? true : false}
            leftIcon={
              <Image
                style={{ height: 20, width: 20 }}
                source={require('../Images/key.png')}
              />
            }
            rightIcon={
              <TouchableOpacity
                onPress={() => this.setState({ show1: !this.state.show1 })}>
                <Image
                  style={{ height: 20, width: 20, marginRight: 10 }}
                  source={
                    this.state.show1
                      ? require('../Images/hide.png')
                      : require('../Images/eye.png')
                  }
                />
              </TouchableOpacity>
            }
            inputContainerStyle={{
              borderBottomWidth: 0,
              justifyContent: 'center',
            }}
            inputStyle={{ fontSize: 13 }}
            onChangeText={text => this.setState({ newPassword: text })}
          />

          <Input
            placeholder={'Confirm New Password'}
            containerStyle={{
              width: '90%',
              height: metrics.dimen_50,
              borderColor: colors.app_gray,
              borderWidth: 1,
              borderRadius: metrics.dimen_7,
              marginTop: metrics.dimen_10,
              alignSelf: 'center',
            }}
            value={this.state.confirmNewPassword}
            secureTextEntry={this.state.show2 ? true : false}
            leftIcon={
              <Image
                style={{ height: 20, width: 20 }}
                source={require('../Images/key.png')}
              />
            }
            rightIcon={
              <TouchableOpacity
                onPress={() => this.setState({ show2: !this.state.show2 })}>
                <Image
                  style={{ height: 20, width: 20, marginRight: 10 }}
                  source={
                    this.state.show2
                      ? require('../Images/hide.png')
                      : require('../Images/eye.png')
                  }
                />
              </TouchableOpacity>
            }
            inputContainerStyle={{
              borderBottomWidth: 0,
              justifyContent: 'center',
            }}
            inputStyle={{ fontSize: 13 }}
            onChangeText={text => this.setState({ confirmNewPassword: text })}
          />

          <Spinner
            style={{ justifyContent: 'center', alignSelf: 'center' }}
            isVisible={this.state.visible}
            size={70}
            type={'ThreeBounce'}
            color={colors.black}
          />
        </View>

        <View
          style={{
            ...styles.bottomview,
            backgroundColor: colors.carib_pay_blue,
          }}>
          <TouchableOpacity onPress={() => this.onSubmitClick()}>
            <Text
              style={{
                fontSize: 15,
                color: 'white',
                fontWeight: 'bold',
                textAlign: 'center',
                alignSelf: 'center',
                marginLeft: 10,
              }}>
              SUBMIT
            </Text>
          </TouchableOpacity>
        </View>




        <View>
          <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.NoUserTrue}>
            <View style={{ width: "90%", alignSelf: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_10, margin: metrics.dimen_20, flexDirection: 'column', height: 320, justifyContent: 'center' }}>
              <View style={{ flex: 1 }}>
                <View>
                  <Image style={{ height: metrics.dimen_80, width: metrics.dimen_100, resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }}
                    source={require("../Images/logo.png")}></Image>
                  <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, margin: 15, fontWeight: 'bold', textAlign: 'center', width: 300, alignSelf: "center" }}>Email Address / Phone Number does not match.</Text>
                </View>
                <View style={styles.bottomview2}>
                  <TouchableOpacity onPress={() => this.setState({ NoUserTrue: false })}>
                    <Text style={{ fontSize: 15, color: 'white', fontWeight: 'bold', textAlign: 'center', alignSelf: 'center', marginLeft: 10 }}>{"OK "}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </View>










        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.show3}>
            <View
              style={{
                width: '90%',
                alignSelf: 'center',
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                margin: metrics.dimen_20,
                flexDirection: 'column',
                height: 270,
                justifyContent: 'center',
              }}>
              <View style={{ flex: 1 }}>
                <View>
                  <Image
                    style={{
                      height: metrics.dimen_80,
                      width: metrics.dimen_100,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                      marginTop: 20,
                    }}
                    source={require('../Images/successfully.png')}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_heading,
                      color: colors.black,
                      fontWeight: 'bold',
                      textAlign: 'center',
                      margin: 10,
                    }}>
                    Your password has been successfully Reset.
                  </Text>
                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      color: colors.app_gray,
                      margin: 10,
                      fontWeight: 'bold',
                      textAlign: 'center',
                      width: 200,
                      alignSelf: 'center',
                    }}>
                    Let's Login.
                  </Text>
                </View>
                <View
                  style={[
                    styles.bottomview,
                    { backgroundColor: colors.white, flexDirection: 'row' },
                  ]}>
                  <View
                    style={{
                      width: '90%',
                      backgroundColor: colors.theme_caribpay,
                      borderRadius: 10,
                      justifyContent: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({ show3: false }, () =>
                          this.props.navigation.navigate('Login'),
                        )
                      }>
                      <Text
                        style={{
                          fontSize: 15,
                          color: 'white',
                          fontWeight: 'bold',
                          textAlign: 'center',
                          alignSelf: 'center',
                        }}>
                        Login
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: metrics.dimen_70,
    backgroundColor: colors.carib_pay_blue,
    flexDirection: 'row',
    paddingHorizontal: metrics.dimen_20,
  },
  headerleftImage: {
    height: metrics.dimen_24,
    width: metrics.dimen_24,
    alignSelf: 'center',
    resizeMode: 'contain',
    tintColor: colors.whitesmoke,
    marginBottom: metrics.dimen_15,
  },
  headertextStyle: {
    fontSize: metrics.text_16,
    fontWeight: 'bold',
    color: colors.white,
    marginBottom: metrics.dimen_15,
    fontFamily: metrics.quicksand_bold,
    textAlignVertical: 'center',
    paddingHorizontal: metrics.dimen_20,
  },
  absouluteview: {
    position: 'absolute',
    top: 51,
    borderTopRightRadius: metrics.dimen_20,
    borderTopLeftRadius: metrics.dimen_20,
    width: '100%',
    height: Dimensions.get('screen').height,
    backgroundColor: colors.white,
  },
  container: { flexDirection: 'row', margin: metrics.dimen_15 },
  image_style: {
    height: metrics.dimen_30,
    width: metrics.dimen_30,
    resizeMode: 'contain',
  },
  headingg: { fontSize: metrics.text_header, color: colors.black },
  imageStyle: {
    height: metrics.dimen_200,
    width: metrics.dimen_200,
    alignSelf: 'center',
    resizeMode: 'contain',
    marginTop: 5,
  },
  descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
  arrowstyle: {
    height: metrics.dimen_15,
    width: metrics.dimen_15,
    resizeMode: 'contain',
  },
  horizontalLine: {
    borderBottomColor: '#D3D3D3',
    borderBottomWidth: 0.4,
    width: '90%',
    alignSelf: 'center',
  },
  bottomview: {
    bottom: 10,
    position: 'absolute',
    height: 40,
    backgroundColor: colors.app_light_yellow_color,
    width: '90%',
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: metrics.dimen_5,
  },
  bottomview2: { bottom: 10, position: 'absolute', height: 40, backgroundColor: colors.theme_caribpay, width: '90%', justifyContent: 'center', alignSelf: 'center', borderRadius: metrics.dimen_5 },

});
