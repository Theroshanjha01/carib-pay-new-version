import React from 'react';
import { View, Image, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';
import Modal from 'react-native-modal';
import Svg, {
    Circle,
} from 'react-native-svg';

export default class Reward extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isComingSoon: false
        }
    }
    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ backgroundColor: colors.theme_caribpay, height: 55, flexDirection: 'row' }}>
                    <TouchableOpacity
                        style={{ height: 24, width: 24, resizeMode: 'contain', alignSelf: 'center', marginHorizontal: 10 }}
                        onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={{ height: 24, width: 24, resizeMode: 'contain', alignSelf: 'center', marginHorizontal: 10 }}
                            source={require("../Images/leftarrow.png")}></Image></TouchableOpacity>
                    <Text style={{ fontSize: metrics.text_large, alignSelf: 'center', marginHorizontal: 100, color: colors.white,fontFamily:metrics.quicksand_bold }}>My Rewards</Text>
                </View>
                {/* <View style={{ backgroundColor: colors.theme_caribpay, height: 120, borderBottomLeftRadius: 50, borderBottomRightRadius: 50 }}>
                    <Image style={{ height: 80, width: 80, resizeMode: 'contain', alignSelf: 'center', marginTop: 5 }}
                        source={require("../Images/giftreward.png")}></Image>
                </View> */}

                <View style={styles.tvscreenBottom}>
                    <Image style={{
                        height: 80, width: 80, position: 'absolute', top: 120, alignSelf: 'center', resizeMode: 'contain', transform: [
                            { scaleX: 0.6 },
                            { scaleY: 2.1 }
                        ]
                    }}
                        source={require("../Images/giftreward.png")}></Image>
                </View>



                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Image style={{ height: 180, width: 300, resizeMode: 'contain', alignSelf: 'center', marginTop: 10 }}
                        source={require("../Images/rewardss.png")}></Image>
                    <Text style={{ textAlign: 'center', color: colors.black, fontSize: metrics.text_header,fontFamily:metrics.quicksand_bold }}>Cashback Rewards Await!</Text>
                    <Text style={{ textAlign: 'center', width: 300,fontFamily:metrics.quicksand_bold, alignSelf: 'center', color: colors.app_gray }}>Check out our ongoing promotions to get cashback rewards!</Text>
                </View>

                <View style={{ position: 'absolute', justifyContent: 'center', bottom: 40, backgroundColor: colors.theme_caribpay, borderRadius: 20, height: 40, width: "90%", alignSelf: 'center' }}>
                    <TouchableOpacity onPress={() => this.setState({
                        isComingSoon: true
                    })}>
                        <Text style={{ alignSelf: 'center', color: colors.white, fontFamily:metrics.quicksand_bold }}>View Promotions</Text>
                    </TouchableOpacity>
                </View>

                <View>
                  <Modal
                    style={{ borderRadius: metrics.dimen_10 }}
                    isVisible={this.state.isComingSoon}
                  >
                    <View
                      style={{
                        width: "90%",
                        alignSelf: "center",
                        backgroundColor: colors.white,
                        borderRadius: metrics.dimen_10,
                        flexDirection: "column",
                        height: 200,
                        justifyContent: "center",
                      }}
                    >
                      
            
                        <View style={{ flex: 1 }}>
                        
                        <TouchableOpacity onPress={()=>this.setState({
                          isComingSoon:false
                        })}><Image
                            style={{
                              height: metrics.dimen_16,
                              width: metrics.dimen_16,
                              resizeMode: "contain",
                              margin:15
                            }}
                            source={require("../Images/cross-sign.png")}></Image></TouchableOpacity>


                          <Image
                            style={{
                              height: 150,
                              width: 150,
                              resizeMode: "cover",
                              alignSelf: "center",
                              marginTop: -30,
                            }}
                            source={require("../Images/soon.png")}
                          />
                          <Text
                            style={{
                              fontSize: metrics.text_heading,
                              color: colors.black,
                              fontFamily:metrics.quicksand_bold,
                              textAlign: "center",
                              marginTop: -20,
                            }}
                          >
                            {"Promotions"}
                          </Text>
                        </View>
                      </View>
                  </Modal>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    bottomview2: {
        bottom: 10,
        position: 'absolute',
        height: 40,
        backgroundColor: colors.theme_caribpay,
        width: '90%',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: metrics.dimen_5,
    },
    tvscreenBottom: {
        width: 200,
        height: 350,
        alignSelf: 'center',
        backgroundColor: colors.theme_caribpay,
        borderBottomRightRadius: 175,
        borderBottomLeftRadius: 175,
        position: 'absolute',
        top: -50,
        transform: [
            { scaleX: 2 },
            { scaleY: .5 }
        ]
    },
});