import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  Text,
  Image,
  TouchableOpacity,
  BackHandler,
  StyleSheet,
} from "react-native";
import SmoothPinCodeInput from "react-native-smooth-pincode-input";
import colors from "../Themes/Colors";
import metrics from "../Themes/Metrics";
var Spinner = require("react-native-spinkit");
const axios = require("axios");
import DeviceInfo from "react-native-device-info";
import AsyncStorage from "@react-native-community/async-storage";
import Modal from "react-native-modal";
import DropdownAlert from "react-native-dropdownalert";
import Toast from "react-native-easy-toast";

export default class SetPasscode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: "",
      visible: false,
      deviceID: "",
      user_id: this.props.navigation.state.params.user_id,
      token: this.props.navigation.state.params.token,
      enabled: false,
      fromForgetpassword: this.props.navigation.state.params.fromForgetpassword,
    };
    console.disableYellowBox = true;
  }

  componentDidMount() {
    let uniqueId = DeviceInfo.getUniqueId();
    this.setState({
      deviceID: uniqueId,
    });
    // this.requestUserPermission();
    // this.recvmessages();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  //
  // async requestUserPermission() {
  //     const authStatus = await messaging().requestPermission();
  //     const enabled =
  //         authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
  //         authStatus === messaging.AuthorizationStatus.PROVISIONAL;

  //     if (enabled) {
  //         console.log('Authorization status:', authStatus);
  //         let fcmtoken = await firebase.messaging.getToken()
  //         console.log("fcmm token is------> ",fcmtoken)
  //     }

  // messaging().onNotificationOpenedApp(remoteMessage => {
  //     console.log(
  //       'Notification caused app to open from background state:',
  //       remoteMessage.notification,
  //     );
  //     navigation.navigate(remoteMessage.data.type);
  //   });

  //   messaging()
  //     .getInitialNotification()
  //     .then(remoteMessage => {
  //       if (remoteMessage) {
  //         console.log(
  //           'Notification caused app to open from quit state:',
  //           remoteMessage.notification,
  //         );
  //         // setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
  //       }

  //     });
  // }

  // async getToken() {
  //     let fcmToken = await AsyncStorage.getItem('fcmToken');
  //     if (!fcmToken) {
  //         fcmToken = await firebase.messaging().getToken();
  //         if (fcmToken) {
  //             await AsyncStorage.setItem('fcmToken', fcmToken);
  //             let data = {
  //                 user_id: this.state.user_id,
  //                 device_id: this.state.deviceID,
  //                 fcm_token: fcmToken
  //             }
  //             axios({
  //                 method: 'post',
  //                 url: 'https://sandbox.caribpayintl.com/api/register-device',
  //                 headers: { 'Authorization': this.state.token },
  //                 data: data,
  //             }).then((response) => {
  //                 if (response.data.success.status == 200) {
  //                     console.log("success message ---> ",response.data.success.message)
  //                 }
  //             }).catch((err) => {
  //                 this.setState({ visible: false })
  //                 alert(err)
  //             });
  //         }
  //     }
  // }

  // async requestPermission() {
  //     try {
  //         await firebase.messaging().requestPermission();
  //         this.getToken();
  //     } catch (error) {
  //         console.log('permission rejected');
  //     }
  // }

  onSetPasscodeClick = () => {
    if (this.state.reconfirmcode == this.state.code) {
      this.setState({ visible: true });
      let data = {
        device_id: this.state.deviceID,
        passcode: this.state.code,
        user_id: this.state.user_id,
      };
      console.log("test: ", data, this.state.token);
      axios({
        method: "post",
        url: "https://sandbox.caribpayintl.com/api/set-passcode",
        headers: { Authorization: this.state.token },
        data: data,
      })
        .then((response) => {
          if (response.data.success.status == 200) {
            AsyncStorage.setItem("DeviceId", this.state.deviceID);
            AsyncStorage.setItem(
              "passcode",
              JSON.stringify({
                user_id: this.state.user_id,
                passcode: this.state.code,
              })
            );
            this.setState({
              visible: false,
              enabled: true,
            });
          }
        })
        .catch((err) => {
          this.setState({ visible: false });
          alert(err);
        });
    } else {
      this.refs.toast.show("Sorry passcode mismatch. Try again.")
    }
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1, color: "#FFFFFF" }}>
        <View style={{ backgroundColor: colors.theme_caribpay }}>
          <View
            style={{
              flexDirection: "row",
              height: metrics.dimen_55,
              margin: 5,
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}
            >
              <View
                style={{
                  height: metrics.dimen_40,
                  width: metrics.dimen_40,
                  justifyContent: "center",
                  marginHorizontal: 5,
                }}
              >
                <Image
                  style={{
                    height: metrics.dimen_25,
                    width: metrics.dimen_25,
                    tintColor: "white",
                    marginTop: 10,
                  }}
                  source={require("../Images/leftarrow.png")}
                />
              </View>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: metrics.text_heading,
                color: colors.white,
                fontFamily: metrics.quicksand_bold,
                alignSelf: "center",
                marginTop: -5,
              }}
            >
              {this.state.fromForgetpassword ? "Reset Passcode" : "Passcode"}
            </Text>
          </View>
        </View>
        <View style={{ margin: 20, flex: 1 }}>
          <Image
            style={{
              height: metrics.dimen_80,
              width: metrics.dimen_80,
              resizeMode: "contain",
              alignSelf: "center",
            }}
            source={require("../Images/pincode.png")}
          />
          <Text
            style={{
              fontSize: metrics.text_heading,
              color: colors.heading_black_text,
              fontFamily: metrics.quicksand_semibold,
              marginTop: 20,
              alignSelf:'center'

            }}
          >
            {this.state.fromForgetpassword
              ? "Reset "
              : "Enter " + "your Passcode"}
          </Text>
          {/* <Text style={{ fontSize: metrics.text_description, color: colors.app_gray, textAlign: 'center', marginTop: 10 }}>Now you can also login into the CaribPay App by using this passcode.</Text> */}
          <View style={{ marginTop: 15, alignSelf: "center" }}>
            {/* <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, fontWeight: '700' }}>Set</Text> */}
            <SmoothPinCodeInput
              placeholder=""
              cellSize={55}
              cellSpacing={7}
              password="*"
              keyboardType="phone-pad"
              cellStyle={{
                borderWidth: 2,
                borderRadius: 7,
                borderColor: colors.theme_caribpay,
                // backgroundColor: 'azure',
              }}
              cellStyleFocused={{
                borderColor: "lightseagreen",
                backgroundColor: "lightcyan",
              }}
              textStyle={{
                fontSize: 24,
                color: colors.black,
                fontWeight: "bold",
              }}
              textStyleFocused={{
                color: "crimson",
              }}
              value={this.state.code}
              onTextChange={(code) => this.setState({ code: code })}
            />
          </View>
          <Text
            style={{
              fontSize: metrics.text_heading,
              color: colors.heading_black_text,
              marginTop: 10,
              fontFamily: metrics.quicksand_semibold,
              alignSelf:'center'
            }}
          >
            Re-enter your Passcode
          </Text>
          <View style={{ marginTop: 15, alignSelf: "center" }}>
            <SmoothPinCodeInput
              cellSize={55}
              cellSpacing={7}
              keyboardType="phone-pad"
              password="*"
              cellStyle={{
                borderWidth: 2,
                borderRadius: 7,
                borderColor: colors.theme_caribpay,
                // backgroundColor: 'azure',
              }}
              cellStyleFocused={{
                borderColor: "lightseagreen",
                backgroundColor: "lightcyan",
              }}
              textStyle={{
                fontSize: 24,
                color: colors.black,
                fontWeight: "bold",
              }}
              textStyleFocused={{
                color: "crimson",
              }}
              value={this.state.reconfirmcode}
              onTextChange={(code) => this.setState({ reconfirmcode: code })}
            />
          </View>

          <Text
            style={{
              fontSize: metrics.text_description,
              color: colors.app_gray,
              textAlign: "center",
              marginTop: 20,
              fontFamily: metrics.quicksand_semibold,
            }}
          >
            Now you can also login into the CaribPay App by using this passcode.
          </Text>
          <View>
            <Modal
              style={{ borderRadius: metrics.dimen_10 }}
              isVisible={this.state.enabled}
            >
              <View
                style={{
                  width: "90%",
                  alignSelf: "center",
                  backgroundColor: colors.white,
                  borderRadius: metrics.dimen_10,
                  margin: metrics.dimen_20,
                  flexDirection: "column",
                  height: 220,
                  justifyContent: "center",
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Image
                      style={{
                        height: metrics.dimen_80,
                        width: metrics.dimen_100,
                        resizeMode: "contain",
                        alignSelf: "center",
                        marginTop: 20,
                      }}
                      source={require("../Images/successfully.png")}
                    />
                    <Text
                      style={{
                        fontSize: metrics.text_heading,
                        color: colors.black,
                        // fontWeight: "bold",
                        fontFamily:metrics.quicksand_bold,
                        textAlign: "center",
                        margin: 15,
                      }}
                    >
                      {" "}
                      {"Passcode Set Successfully!"}
                    </Text>
                  </View>
                  <View style={styles.bottomview2}>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({ enabled: false }, () =>
                          this.state.fromForgetpassword
                            ? this.props.navigation.navigate("LockScreen")
                            : this.props.navigation.goBack(null)
                        )
                      }
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          color: "white",
                          fontWeight: "bold",
                          textAlign: "center",
                          alignSelf: "center",
                          marginLeft: 10,
                        }}
                      >
                        {"OK "}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
          </View>

          <DropdownAlert ref={(ref) => (this.dropDownAlertRef = ref)} />

          <Spinner
            style={{ justifyContent: "center", alignSelf: "center" }}
            isVisible={this.state.visible}
            size={70}
            type={"ThreeBounce"}
            color={colors.black}
          />

          <View
            style={{
              bottom: 5,
              position: "absolute",
              height: 40,
              width: "90%",
              justifyContent: "center",
              alignSelf: "center",
              borderRadius: 20,
              backgroundColor: colors.carib_pay_blue,
            }}
          >
            <TouchableOpacity onPress={() => this.onSetPasscodeClick()}>
              <Text
                style={{
                  fontSize: 15,
                  color: "white",
                  fontFamily: metrics.quicksand_bold,
                  textAlign: "center",
                  alignSelf: "center",
                  marginLeft: 10,
                }}
              >
                Set Passcode
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <Toast
          ref="toast"
          style={{ backgroundColor: "black" }}
          position="bottom"
          positionValue={200}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: "#ffffff" }}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  bottomview2: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 20,
  },
});
