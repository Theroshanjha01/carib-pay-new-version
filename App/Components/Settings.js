import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Share,
  BackHandler,
} from "react-native";
import metrics from "../Themes/Metrics.js";
import colors from "../Themes/Colors.js";
import ToggleSwitch from "toggle-switch-react-native";
import TouchID from "react-native-touch-id";
import { showAlert } from "../Utils.js";
import AsyncStorage from "@react-native-community/async-storage";
import Modal from "react-native-modal";
import fetchPost from "../fetchPost.js";
import { touchAPi } from "./CaribMall/Utils/api.constants.js";
import DeviceInfo from "react-native-device-info";
const axios = require("axios");


const optionalConfigObject = {
  title: "Authentication Required", // Android
  imageColor: colors.app_dark_green_color, // Android
  imageErrorColor: "#ff0000", // Android
  sensorDescription: "Touch the fingerprint sensor", // Android
  sensorErrorDescription: "Failed", // Android
  cancelText: "Cancel", // Android
  fallbackLabel: "Show Passcode", // iOS (if empty, then label is hidden)
  unifiedErrors: false, // use unified error messages (default false)
  passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
};

export default class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPasscodeEnabled: false,
      isTouchIdEnabled: false,
      IsAuthenticated: false,
      token: this.props.navigation.state.params.token,
      user_id: "",
    };
  }

  componentDidMount() {
    this.reloadData();
    this.getData();
  }

  getData = () => {
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.reloadData();
      }
    );
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  };

  reloadData = async () => {
    let touchid = await AsyncStorage.getItem("TouchIDEnable");
    // let faceid = await AsyncStorage.getItem('FaceIDEnable');
    let passcode = await AsyncStorage.getItem("passcode");
    passcode = JSON.parse(passcode);

    let user_id = await AsyncStorage.getItem("id");
    console.log(user_id, this.state.token);

    this.setState({
      user_id: user_id,
    });

    if (touchid == "1") {
      this.setState({
        isTouchIdEnabled: true,
      });
    }
    // if (faceid == "1") {
    //     this.setState({
    //         isEnabledFaceID: true
    //     })
    // }
    if (passcode?.passcode != null && passcode?.user_id == user_id) {
      this.setState({
        isPasscodeEnabled: true,
      });
    }
  };

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  _pressHandler() {
    const isSupportedObject = {
      unifiedErrors: false, // use unified error messages (default false)
      passcodeFallback: false, // if true is passed, itwill allow isSupported to return an error if the device is not enrolled in touch id/face id etc. Otherwise, it will just tell you what method is supported, even if the user is not enrolled.  (default false)
    };

    TouchID.isSupported(isSupportedObject)
      .then(async (biometryType) => {
        if (biometryType === "FaceID") {
          console.log("FaceID is supported.");
          TouchID.authenticate(
            "Please enable your Face ID",
            optionalConfigObject
          )
            .then(async (success) => {
              console.log(success);
              let response = this.CallApiOnTouchId();
              console.log(response);
              AsyncStorage.setItem("FaceIDEnable", "1");
              showAlert("CaribPay", "Authenticated Successfully");
            })
            .catch((error) => {
              showAlert("CaribPay", "Authentication Failed");
            });
        } else {
          console.log("TouchID is supported.");
          TouchID.authenticate(
            "Please Enable your Touch ID",
            optionalConfigObject
          )
            .then(async (success) => {
              console.log(success);
              let response = this.CallApiOnTouchId();
              console.log(response);
              AsyncStorage.setItem("TouchIDEnable", "1");
              this.setState({
                IsAuthenticated: true,
              });
            })
            .catch((error) => {
              showAlert("CaribPay", "Authentication Failed");
            });
        }
      })
      .catch((error) => {
        // Failure code
        console.log(error);
        showAlert("CaribPay", "Not Supported");
      });
  }

  async CallApiOnTouchId() {
    console.log(
      "info coming ---> ",
      DeviceInfo.getUniqueId(),
      this.state.user_id
    );

    //  let body = {
    //       device_id: DeviceInfo.getUniqueId(),
    //       user_id: this.state.user_id,
    //       touch_status: 1,
    //     };
    //     console.log("body enable touch -->>>>> ", body);
    //     const response = await fetchPost(touchAPi, body, this.state.token, true);
    //     console.log(response);
    //     return response;

    let phone_device_id = DeviceInfo.getUniqueId();
    console.log("phone device id ==> ", phone_device_id);
    let postData = {
      device_id: phone_device_id,
      user_id: this.state.user_id,
      touch_status: 1,
    };

    console.log("parmas  enable touch -- > ", postData);

    axios({
      method: "post",
      url: "https://sandbox.caribpayintl.com/api/enable-touch",
      headers: { Authorization: this.state.token },
      data: postData,
    })
      .then((response) => {
        console.log(" response enable touch -- > ", response.data);
      })
      .catch((err) => {
        // this.setState({ visible: false });
        console.log(err, "error hai " + err);
      });
  }
  _onFaceID() {
    const isSupportedObject = {
      unifiedErrors: false, // use unified error messages (default false)
      passcodeFallback: false, // if true is passed, itwill allow isSupported to return an error if the device is not enrolled in touch id/face id etc. Otherwise, it will just tell you what method is supported, even if the user is not enrolled.  (default false)
    };

    TouchID.isSupported(isSupportedObject).then((biometryType) => {
      if (biometryType === "FaceID") {
        console.log("FaceID is supported.");
      } else {
        showAlert("CaribPay", "FaceID is not Supported");
      }
    });
  }

  referPeoples = async () => {
    if (Platform.OS === "ios") {
      try {
        const result = await Share.share({
          message:
            "Hey! Download CaribPay for Fast & Easy Payment  https://share.caribpayintl.com",
          title: "CaribPay",
        });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
          } else {
          }
        } else if (result.action === Share.dismissedAction) {
        }
      } catch (error) {
        alert(error.message);
      }
    } else {
      try {
        const result = await Share.share({
          message:
            "Hey! Download CaribPay for Fast & Easy Payment  https://share.caribpayintl.com",
          title: "CaribPay",
        });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
          } else {
          }
        } else if (result.action === Share.dismissedAction) {
        }
      } catch (error) {
        alert(error.message);
      }
    }
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.header}>
          <TouchableOpacity
            style={styles.headerleftImage}
            onPress={() => this.props.navigation.goBack(null)}
          >
            <Image
              style={styles.headerleftImage}
              source={require("../Images/leftarrow.png")}
            />
          </TouchableOpacity>
          <Text style={styles.headertextStyle}>Settings</Text>
        </View>

        <View style={styles.absouluteview}>
          <View style={styles.container}>
            <View style={{ justifyContent: "center" }}>
              <Image
                style={styles.image_style}
                source={require("../Images/dial.png")}
              />
            </View>
            <View
              style={{
                flexDirection: "column",
                flex: 1,
                marginLeft: metrics.dimen_20,
                justifyContent: "center",
              }}
            >
              <Text style={styles.headingg}>Enable Passcode</Text>
            </View>
            <ToggleSwitch
              isOn={this.state.isPasscodeEnabled}
              onColor={colors.carib_pay_blue}
              offColor={colors.light_grey_backgroud}
              // label="Example label"
              // labelStyle={{ color: "black", fontWeight: "900" }}
              size="medium"
              onToggle={(isOn) =>
                this.setState({
                  isPasscodeEnabled: !this.state.isPasscodeEnabled,
                })
              }
            />
          </View>
          <View style={styles.horizontalLine} />

          {this.state.isPasscodeEnabled ? (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("ForgetPassword", {
                  fromLogin: "0",
                })
              }
            >
              <View style={styles.container}>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.image_style}
                    source={require("../Images/dial.png")}
                  />
                </View>
                <View
                  style={{
                    flexDirection: "column",
                    flex: 1,
                    marginLeft: metrics.dimen_20,
                    justifyContent: "center",
                  }}
                >
                  <Text style={styles.headingg}>Change Passcode</Text>
                </View>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.arrowstyle}
                    source={require("../Images/rightarrow.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("SetPasscode", {
                  user_id: this.state.user_id,
                  token: this.state.token,
                })
              }
            >
              <View style={styles.container}>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.image_style}
                    source={require("../Images/dial.png")}
                  />
                </View>
                <View
                  style={{
                    flexDirection: "column",
                    flex: 1,
                    marginLeft: metrics.dimen_20,
                    justifyContent: "center",
                  }}
                >
                  <Text style={styles.headingg}>Set Passcode</Text>
                </View>
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.arrowstyle}
                    source={require("../Images/rightarrow.png")}
                  />
                </View>
              </View>
            </TouchableOpacity>
          )}

          <View style={styles.horizontalLine} />

          <View style={styles.container}>
            <View style={{ justifyContent: "center" }}>
              <Image
                style={styles.image_style}
                source={require("../Images/fingerprint.png")}
              />
            </View>
            <View
              style={{
                flexDirection: "column",
                flex: 1,
                marginLeft: metrics.dimen_20,
                justifyContent: "center",
              }}
            >
              <Text style={styles.headingg}>Enable Touch ID</Text>
            </View>
            <ToggleSwitch
              isOn={this.state.isTouchIdEnabled}
              onColor={colors.carib_pay_blue}
              offColor={colors.light_grey_backgroud}
              size="medium"
              onToggle={(isOn) =>
                this.setState(
                  {
                    isTouchIdEnabled: !this.state.isTouchIdEnabled,
                  },
                  () =>
                    this.state.isTouchIdEnabled
                      ? this._pressHandler()
                      : AsyncStorage.setItem("TouchIDEnable", "0")
                )
              }
            />
          </View>

          <View style={styles.horizontalLine} />

          {/* <View style={styles.container}>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={styles.image_style} source={require('../Images/identity.png')}></Image>
                        </View>
                        <View style={{ flexDirection: 'column', flex: 1, marginLeft: metrics.dimen_20, justifyContent: 'center' }}>
                            <Text style={styles.headingg}>Enable Face ID</Text>
                        </View>
                        <ToggleSwitch
                            isOn={this.state.isEnabledFaceID}
                            onColor={colors.carib_pay_blue}
                            offColor={colors.light_grey_backgroud}
                            // label="Example label"
                            // labelStyle={{ color: "black", fontWeight: "900" }}
                            size="medium"
                            onToggle={isOn => this.setState({
                                isEnabledFaceID: !this.state.isEnabledFaceID
                            }, () => this.state.isEnabledFaceID ? this._onFaceID() : AsyncStorage.setItem("FaceIDEnable", "0"))}/>
                    </View>


                    <View style={styles.horizontalLine} /> */}

          <TouchableOpacity onPress={() => this.referPeoples()}>
            <View style={styles.container}>
              <View style={{ justifyContent: "center" }}>
                <Image
                  style={styles.image_style}
                  source={require("../Images/share.png")}
                />
              </View>
              <View
                style={{
                  flexDirection: "column",
                  flex: 1,
                  marginLeft: metrics.dimen_20,
                  justifyContent: "center",
                }}
              >
                <Text style={styles.headingg}>{"Share App"}</Text>
              </View>
              <View style={{ justifyContent: "center" }}>
                <Image
                  style={styles.arrowstyle}
                  source={require("../Images/rightarrow.png")}
                />
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.horizontalLine} />

          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("TermsNConditions", {
                token: this.state.token,
                fromPrivacy: "0",
              })
            }
          >
            <View style={styles.container}>
              <View style={{ justifyContent: "center" }}>
                <Image
                  style={styles.image_style}
                  source={require("../Images/paper.png")}
                />
              </View>
              <View
                style={{
                  flexDirection: "column",
                  flex: 1,
                  marginLeft: metrics.dimen_20,
                  justifyContent: "center",
                }}
              >
                <Text style={styles.headingg}>{"Terms & Conditions"}</Text>
              </View>
              <View style={{ justifyContent: "center" }}>
                <Image
                  style={styles.arrowstyle}
                  source={require("../Images/rightarrow.png")}
                />
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.horizontalLine} />

          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("TermsNConditions", {
                token: this.state.token,
                fromPrivacy: "1",
              })
            }
          >
            <View style={styles.container}>
              <View style={{ justifyContent: "center" }}>
                <Image
                  style={styles.image_style}
                  source={require("../Images/policy.png")}
                />
              </View>
              <View
                style={{
                  flexDirection: "column",
                  flex: 1,
                  marginLeft: metrics.dimen_20,
                  justifyContent: "center",
                }}
              >
                <Text style={styles.headingg}>{"Privacy Notice"}</Text>
              </View>
              <View style={{ justifyContent: "center" }}>
                <Image
                  style={styles.arrowstyle}
                  source={require("../Images/rightarrow.png")}
                />
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.horizontalLine} />

          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("TermsNConditions", {
                token: this.state.token,
                fromPrivacy: "2",
              })
            }
          >
            <View style={styles.container}>
              <View style={{ justifyContent: "center" }}>
                <Image
                  style={{ ...styles.image_style, tintColor: colors.black }}
                  source={require("../Images/stand.png")}
                />
              </View>
              <View
                style={{
                  flexDirection: "column",
                  flex: 1,
                  marginLeft: metrics.dimen_20,
                  justifyContent: "center",
                }}
              >
                <Text style={styles.headingg}>{"About Us"}</Text>
              </View>
              <View style={{ justifyContent: "center" }}>
                <Image
                  style={styles.arrowstyle}
                  source={require("../Images/rightarrow.png")}
                />
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.horizontalLine} />

          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("TermsNConditions", {
                token: this.state.token,
                fromPrivacy: "3",
              })
            }
          >
            <View style={styles.container}>
              <View style={{ justifyContent: "center" }}>
                <Image
                  style={{ ...styles.image_style, tintColor: colors.black }}
                  source={require("../Images/contactus.png")}
                />
              </View>
              <View
                style={{
                  flexDirection: "column",
                  flex: 1,
                  marginLeft: metrics.dimen_20,
                  justifyContent: "center",
                }}
              >
                <Text style={styles.headingg}>{"Contact Us"}</Text>
              </View>
              <View style={{ justifyContent: "center" }}>
                <Image
                  style={styles.arrowstyle}
                  source={require("../Images/rightarrow.png")}
                />
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.horizontalLine} />

          <View>
            <Modal
              style={{ borderRadius: metrics.dimen_10 }}
              isVisible={this.state.IsAuthenticated}
            >
              <View
                style={{
                  width: "80%",
                  alignSelf: "center",
                  backgroundColor: colors.white,
                  borderRadius: metrics.dimen_10,
                  margin: metrics.dimen_20,
                  flexDirection: "column",
                  height: 250,
                  justifyContent: "center",
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Image
                      style={{
                        height: metrics.dimen_100,
                        width: metrics.dimen_100,
                        resizeMode: "contain",
                        alignSelf: "center",
                        marginTop: 20,
                      }}
                      source={require("../Images/fprint.png")}
                    />
                    <Text
                      style={{
                        fontSize: metrics.text_normal,
                        color: colors.black,
                        fontWeight: "bold",
                        textAlign: "center",
                        marginTop: 20,
                      }}
                    >
                      {"Your Fingerprint is Authenticated Successfully!"}
                    </Text>
                  </View>
                  <View style={styles.bottomview2}>
                    <TouchableOpacity
                      onPress={() => this.setState({ IsAuthenticated: false })}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          color: "white",
                          fontWeight: "bold",
                          textAlign: "center",
                          alignSelf: "center",
                          marginLeft: 10,
                        }}
                      >
                        {"OK "}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: metrics.dimen_70,
    backgroundColor: colors.carib_pay_blue,
    paddingHorizontal: metrics.dimen_20,
    flexDirection: "row",
  },
  headerleftImage: {
    height: metrics.dimen_24,
    width: metrics.dimen_24,
    alignSelf: "center",
    resizeMode: "contain",
    tintColor: colors.whitesmoke,
    marginBottom: metrics.dimen_15,
  },
  headertextStyle: {
    fontSize: metrics.text_17,
    fontWeight: "200",
    color: colors.white,
    marginBottom: metrics.dimen_15,
    paddingHorizontal: metrics.dimen_20,
    fontFamily: metrics.quicksand_bold,
    textAlignVertical: "center",
  },
  absouluteview: {
    position: "absolute",
    top: 51,
    borderTopRightRadius: metrics.dimen_20,
    borderTopLeftRadius: metrics.dimen_20,
    width: "100%",
    height: Dimensions.get("screen").height,
    backgroundColor: colors.white,
  },
  container: { flexDirection: "row", margin: metrics.dimen_15 },
  image_style: {
    height: metrics.dimen_25,
    width: metrics.dimen_25,
    resizeMode: "contain",
  },
  headingg: {
    fontSize: metrics.text_header,
    color: colors.black,
    textAlignVertical: "center",
  },
  descriiption: {
    fontSize: metrics.text_description,
    color: colors.app_gray,
    fontWeight: "300",
  },
  arrowstyle: {
    height: metrics.dimen_15,
    width: metrics.dimen_15,
    resizeMode: "contain",
  },
  horizontalLine: {
    borderBottomColor: "#D3D3D3",
    borderBottomWidth: 0.4,
    width: "90%",
    alignSelf: "center",
  },
  bottomview2: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.theme_caribpay,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: metrics.dimen_5,
  },
});
