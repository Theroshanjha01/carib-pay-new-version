import React from "react";
import { ActivityIndicator, View, Image } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import colors from "../Themes/Colors";
import DeviceInfo from "react-native-device-info";
import { getDeviceInfoApi } from "./CaribMall/Utils/api.constants";
import fetchPost from "../fetchPost";
import fetchGet from "../fetchGet";

export default class Splash extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    let istoken = await AsyncStorage.getItem("token");
    let passcode;
    // let passcode = await AsyncStorage.getItem('passcode')
    // passcode = JSON.parse(passcode);
    let touchid;
    // let touchid = await AsyncStorage.getItem('TouchIDEnable')
    let user_id = await AsyncStorage.getItem("id");
    let logoutHavingPasscode = await AsyncStorage.getItem(
      "logoutHavingPasscode"
    );
    let resp = await this.getDeviceInfo();
    console.log("respsdasd", resp);

    if (resp.status == "200") {
      touchid = resp.device_info.touch_status;
      passcode = resp.device_info.passcode_status;
    }
    console.log("passcode value is -> , ", passcode, touchid);
    setTimeout(async () => {
      if (istoken == null && logoutHavingPasscode != "1") {
        this.props.navigation.navigate("IntroSlider");
      } else if (
        istoken == null &&
        logoutHavingPasscode == "1" &&
        passcode != null
      ) {
        this.props.navigation.navigate("Login");
      } else if (istoken != null && passcode == 0 && touchid == 0) {
        this.props.navigation.navigate("Home");
      } else if (istoken != null && passcode == 1 && touchid == 0) {
        this.props.navigation.navigate("Login", {
          passcode: true,
        });
      } else if (
        istoken != null &&
        (passcode == null || undefined) &&
        touchid == 0
      ) {
        this.props.navigation.navigate("Login", {
          passcode: true,
        });
      }
      // else if (istoken != null && ((passcode != null || undefined) && (passcode?.passcode != null && passcode?.user_id == user_id)) && touchid != "1" && user_id != null) {
      //     this.props.navigation.navigate('LockScreen')
      // }
      else if (
        istoken == null &&
        (passcode != null || undefined) &&
        touchid == 0 &&
        user_id == null
      ) {
        this.props.navigation.navigate("Login", {
          passcode: true,
        });
      } else if ((istoken != null && touchid) || touchid == 1) {
        this.props.navigation.navigate("Login", {
          FaceIDEnabletouchIdEnable: true,
        });
      } else {
        this.props.navigation.navigate("Login");
      }
    }, 1500);
  }

  async getDeviceInfo() {
    let device_id = DeviceInfo.getUniqueId();
    const response = await fetchGet(
      `${getDeviceInfoApi}?device_id=${device_id}`
    );
    return response;
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignContent: "center",
          backgroundColor: colors.theme_caribpay,
        }}
      >
        <Image
          style={{
            width: 120,
            height: 120,
            resizeMode: "contain",
            alignSelf: "center",
          }}
          source={require("../Images/logo_1024.png")}
        />

        <View
          style={{
            bottom: 15,
            position: "absolute",
            justifyContent: "center",
            alignSelf: "center",
          }}
        >
          <ActivityIndicator
            size="large"
            color={colors.carib_pay_blue}
            animating={true}
          />
        </View>
      </View>
    );
  }
}
