import React, { Component } from 'react';
import {
    SafeAreaView, StyleSheet, Text, View, ScrollView, BackHandler, Image, TouchableOpacity, Dimensions
} from 'react-native';
import metrics from '../Themes/Metrics';
import colors from '../Themes/Colors';
const axios = require('axios');
import HTML from "react-native-render-html";
var Spinner = require('react-native-spinkit');

export default class TermsNConditions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: this.props.navigation.state.params.token,
            fromPrivacy: this.props.navigation.state.params.fromPrivacy,
            htmlContent: '',
            defaultContent: ''
        }
    };

    componentDidMount() {
        this.setState({
            visible: true
        })
        let urlData
        // this.state.fromPrivacy == "0" ? "terms-condition" : this.state.fromPrivacy == "1" ? "privacy-policy" : this.state.fromPrivacy == "2" ? "about-us" : this.state.fromPrivacy == "3" ? 'contact-us' : 'fees-charges'
        if (this.state.fromPrivacy == "0") {
            urlData = "terms-condition"
        } else if (this.state.fromPrivacy == "1") {
            urlData = "privacy-policy"
        }
        else if (this.state.fromPrivacy == "2") {
            urlData = "about-us"
        }
        else if (this.state.fromPrivacy == "3") {
            urlData = "contact-us"
        }
        else if (this.state.fromPrivacy == "4") {
            urlData = "fees-charges"
        }
        else if (this.state.fromPrivacy == "5") {
            urlData = "faq"
        }
        else {
            urlData = "member-benefits"
        }
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let postData = { url: urlData }
        console.log(postData)
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/pages',
            data: postData,
            headers: { 'Authorization': this.state.token },
        }).then((response) => {
            console.log("TNC response -- > ", response.data)
            this.setState({
                htmlContent: response.data.success.data.content,
                visible: false
            })
        }).catch((err) => {
            this.setState({
                visible: false
            })
        });

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    render() {

        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerleftImage} onPress={() => this.props.navigation.goBack(null)}>
                        <Image style={styles.headerleftImage}
                            source={require('../Images/leftarrow.png')}></Image>
                    </TouchableOpacity>
                    {this.state.fromPrivacy == 0 && <Text style={styles.headertextStyle}>{"Terms & Conditions"}</Text>}
                    {this.state.fromPrivacy == 1 && <Text style={styles.headertextStyle}>{"Privacy Notice"}</Text>}
                    {this.state.fromPrivacy == 2 && <Text style={styles.headertextStyle}>{"About Us"}</Text>}
                    {this.state.fromPrivacy == 3 && <Text style={styles.headertextStyle}>{"Contact Us"}</Text>}
                    {this.state.fromPrivacy == 4 && <Text style={styles.headertextStyle}>{"Fees & Charges"}</Text>}
                    {this.state.fromPrivacy == 5 && <Text style={styles.headertextStyle}>{"FAQs"}</Text>}
                    {this.state.fromPrivacy == 6 && <Text style={styles.headertextStyle}>{"Membership Benefits"}</Text>}


                </View>

                <Spinner style={{ justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />
                <ScrollView style={{ flex: 1 }}>
                    {this.state.htmlContent != '' ?
                        <HTML
                            html={this.state.htmlContent}
                            containerStyle={{
                                flex: 1, marginTop: 10, margin: 15
                            }}
                            imagesMaxWidth={Dimensions.get("window").width} />
                        :
                        <HTML
                            html={this.state.defaultContent}
                            containerStyle={{
                                flex: 1, marginTop: 10, margin: 15
                            }}
                            imagesMaxWidth={Dimensions.get("window").width} />
                    }
                </ScrollView>





            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: metrics.dimen_60,
        backgroundColor: colors.theme_caribpay,
        paddingHorizontal: metrics.dimen_15,
        flexDirection: 'row'
    },
    headerleftImage: {
        height: metrics.dimen_24,
        width: metrics.dimen_24,
        alignSelf: 'center',
        resizeMode: 'contain',
        tintColor: colors.whitesmoke,
        // marginBottom: metrics.dimen_15
    },
    headertextStyle: {
        fontSize: metrics.text_17,
        fontWeight: '200',
        color: colors.white,
        // marginBottom: metrics.dimen_15,
        paddingHorizontal: metrics.dimen_20,
        fontFamily: metrics.quicksand_bold, textAlignVertical: 'center', textAlign: 'center'
    }
});
