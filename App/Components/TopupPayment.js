import React from "react";
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import metrics from "../Themes/Metrics.js";
import { Input } from "react-native-elements";
import colors from "../Themes/Colors.js";
import AsyncStorage from "@react-native-community/async-storage";
var Spinner = require("react-native-spinkit");
const axios = require("axios");
// import RNPaypal from 'react-native-paypal-lib';
import Stripe from "react-native-stripe-api";
import {
  paypal_basictoken,
  paypal_gettoken_live,
  paypal_payment_live,
  showAlert,
} from "../Utils.js";
import Modal from "react-native-modal";
import fetchPost from "../fetchPost.js";
import {
  cancelUrl,
  maxValue,
  returnUrl,
} from "./CaribMall/Utils/api.constants.js";

export default class TopupPayment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: this.props.navigation.state.params.amount,
      phone: this.props.navigation.state.params.phone,
      userId: this.props.navigation.state.params.userId,
      token: this.props.navigation.state.params.token,
      operatorName: this.props.navigation.state.params.operatorName,
      operatorID: this.props.navigation.state.params.operatorID,
      isoName: this.props.navigation.state.params.isoName,
      fromPayPal: false,
      fromWallet: true,
      fromCards: false,
      visible: false,
      isVisible: false,
      balance: "",
      RecipientPhone: "",
      txn_date: "",
      client_id: "",
      cvv: "",
      expiryMonth: "",
      expiryYear: "",
      cardNumber: "",
      stripeKey: "",
      updated_balance: "",
      tryAgain: false,
      fromway: "CaribPay",
    };

    console.disableYellowBox = true;
  }

  async componentDidMount() {
    console.log("id---->", this.state.userId);
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    let balance = await AsyncStorage.getItem("available_balance");
    let client_id = await AsyncStorage.getItem("clientIdPaypal");
    this.setState({
      balance: balance,
      client_id: client_id,
    });

    let postData = { method_id: "2", currency_id: "1" };
    axios({
      method: "post",
      url: "https://sandbox.caribpayintl.com/api/deposit/get-stripe-info",
      data: postData,
      headers: { Authorization: this.state.token },
    })
      .then((response) => {
        if (response.data.success.status == 200) {
          this.setState({
            stripeKey: response.data.success.stripe_keys.secret_key,
          });
          console.log(this.state.stripeKey);
        }
      })
      .catch((err) => {
        // alert(err)
      });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  OnProccede = async () => {
    this.setState({ visible: true });
    const apiKey = this.state.stripeKey;
    const client = new Stripe(apiKey);
    const token = await client
      .createToken({
        number: this.state.cardNumber,
        exp_month: this.state.expiryMonth,
        exp_year: this.state.expiryYear,
        cvc: this.state.cvv,
        address_zip: "12345",
      })
      .then((response) => {
        if (response) {
          console.log("response", response.id);
          let postData = {
            amount: this.state.amount,
            totalAmount: this.state.amount,
            currency_id: 1,
            user_id: this.state.userId,
            deposit_payment_id: 2,
            stripeToken: response.id,
          };

          console.log("postdatac--->", postData);
          axios({
            method: "post",
            url:
              "https://sandbox.caribpayintl.com/api/deposit/stripe-payment-store",
            data: postData,
            headers: { Authorization: this.state.token },
          })
            .then((response) => {
              console.log("stripe-payment ", response.data);
              if (response.data.status == 200) {
                let postData1 = {
                  phone: this.state.phone,
                  country: this.state.isoName,
                  amount: this.state.amount,
                  operator_id: this.state.operatorID,
                  user_id: this.state.userId,
                  currency_id: 1,
                };
                console.log(postData1);
                axios({
                  method: "post",
                  url: "https://sandbox.caribpayintl.com/api/top-up",
                  data: postData1,
                  headers: { Authorization: this.state.token },
                })
                  .then((response) => {
                    console.log("rcharge from stripe ---", response.data.data);
                    if (response.data.status == 200) {
                      let createdOn = response.data.data.transactionDate;
                      this.setState({
                        RecipientPhone: response.data.data.recipientPhone,
                        txn_date: createdOn,
                        visible: false,
                      });
                      this.props.navigation.navigate("SuccessFullMobile", {
                        operatorName: this.state.operatorName,
                        RecipientPhone: this.state.RecipientPhone,
                        paymentMode: "PayPal",
                        TxnDate: this.state.txn_date,
                        amount: this.state.amount,
                      });
                    }
                  })
                  .catch((err) => {
                    this.setState({ visible: false });
                    alert(err);
                  });
              }
            })
            .catch((err) => {
              alert(err);
              this.setState({ visible: false });
            });
        } else {
          alert(response.error.message);
          this.setState({ visible: false });
        }
      })
      .catch((err) => {
        console.log(err);
        this.setState({ visible: false });
      });
  };
  getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
  };

  onNext = async () => {
    let postData = {
      phone: this.state.phone,
      country: this.state.isoName,
      amount: this.state.amount,
      operator_id: this.props.navigation.state.params.operatorID,
      user_id: this.state.userId,
      currency_id: 1,
    };
    let requestData = {
      operatorName: this.state.operatorName,
      RecipientPhone: this.state.RecipientPhone,
      paymentMode: "Wallet",
      TxnDate: this.state.txn_date,
      amount: this.state.amount,
    };
    console.log(postData);
    if (this.state.fromWallet) {
      if (parseFloat(this.state.balance) > parseFloat(this.state.amount)) {
        this.setState({
          visible: true,
        });
        axios({
          method: "post",
          url: "https://sandbox.caribpayintl.com/api/top-up",
          data: postData,
          headers: { Authorization: this.state.token },
        })
          .then((response) => {
            console.log(
              "MObile wallet ---> ",
              response.data.status,
              response.data
            );
            console.log("MObile wallet  response ---> ", response.data.data);
            if (response.data.status == 200) {
              this.setState({
                RecipientPhone: response.data.data.recipientPhone,
                txn_date: response.data.data.transactionDate,
                visible: false,
              });
              this.props.navigation.navigate("SuccessFullMobile", requestData);
            } else if (response.data.status == 400) {
              this.setState({
                tryAgain: true,
                message:
                  response.data.message ==
                  "Transaction refused by the operator because recipient phone is not a valid prepaid account"
                    ? "Invalid Prepaid Number!"
                    : "Please Try After Some Time!",
                visible: false,
              });
            }
          })
          .catch((err) => {
            this.setState({ visible: false });
            alert(err);
          });
      } else {
        this.setState({
          isLowBalance:true
        })
      }
    } else if (this.state.fromPayPal) {
      if (parseFloat(this.state.amount) < 10) {
        this.setState({
          limitExceed: true,
        });
        return;
      }
      this.setState({ visible: true });
      // payapl integration
      let cp_id = await AsyncStorage.getItem("caribpay_id");
      console.log(cp_id);
      var dataDetail = JSON.stringify({
        intent: "sale",
        payer: {
          payment_method: "paypal",
        },
        transactions: [
          {
            amount: {
              total: this.state.amount.toString(),
              currency: "USD",
            },
            description: `Mobile recharge is done for mobile ${
              this.state.phone
            } of $ ${this.state.amount}`,
            custom: "Mobile topup",
            invoice_number: this.getRandomInt(maxValue).toString(),
            payment_options: {
              allowed_payment_method: "INSTANT_FUNDING_SOURCE",
            },
            soft_descriptor: cp_id,
            item_list: {
              items: [
                {
                  name: "Mobile Topup",
                  description: `Mobile Topup ${this.state.phone} of $ ${
                    this.state.amount
                  }`,
                  quantity: "1",
                  price: this.state.amount.toString(),
                  tax: "0.00",
                  currency: "USD",
                },
              ],
            },
          },
        ],
        note_to_payer: "Contact us for any questions on your order.",
        redirect_urls: {
          return_url: returnUrl,
          cancel_url: cancelUrl,
        },
      });
      let body = "grant_type=client_credentials";
      const response = await fetchPost(
        paypal_gettoken_live,
        body,
        paypal_basictoken
      );
      console.log(response);

      axios
        .post(
          paypal_payment_live,
          dataDetail, // you can get data details from https://developer.paypal.com/docs/api/payments/v1/
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${response.access_token}`,
            },
          }
        )
        .then((response5) => {
          console.log(response5);
          const { id, links } = response5.data;
          const approvalUrl = links.find((data) => data.rel == "approval_url");
          this.setState({ visible: false });

          let urltosend = `https://sandbox.caribpayintl.com/api/top-up`;

          this.props.navigation.navigate("WebPage", {
            approvalUrl: approvalUrl.href,
            accessToken: response.access_token,
            url: urltosend,
            data: postData,
            paymentId: id,
            from: "mobileTopup",
            token: this.state.token,
            requestData: requestData,
          });
        })
        .catch((err) => {
          console.log({ ...err });
        });
    } else {
      this.setState({
        isComingSoon: true,
      });
    }
  };

  // onNext = () => {
  //     if (this.state.fromWallet == true) {
  //         if (parseFloat(this.state.balance) > parseFloat(this.state.amount)) {
  //             this.setState({
  //                 visible: true
  //             })
  //             let postData = {
  //                 phone: this.state.phone,
  //                 country: this.state.isoName,
  //                 amount: this.state.amount,
  //                 operator_id: this.props.navigation.state.params.operatorID,
  //                 user_id: this.state.userId,
  //                 currency_id: 1
  //             }
  //             console.log(postData)
  //             axios({
  //                 method: 'post',
  //                 url: 'https://sandbox.caribpayintl.com/api/top-up',
  //                 data: postData,
  //                 headers: { 'Authorization': this.state.token },
  //             })
  //                 .then((response) => {
  //                     console.log("MObile wallet ---> ", response.data.status)
  //                     if (response.data.status == 200) {
  //                         this.setState({
  //                             RecipientPhone: response.data.data.recipientPhone,
  //                             txn_date: response.data.data.transactionDate,
  //                             visible: false,
  //                         })
  //                         this.props.navigation.navigate("SuccessFullMobile", { operatorName: this.state.operatorName, RecipientPhone: this.state.RecipientPhone, paymentMode: 'Wallet', TxnDate: this.state.txn_date, amount: this.state.amount })
  //                     }
  //                 }).catch((err) => {
  //                     this.setState({ visible: false })
  //                     alert(err)
  //                 });
  //         } else {
  //             showAlert("CaribPay", "You have Insufficient Balance : " + this.state.balance)
  //         }

  //     } else if (this.state.fromPayPal == true) {
  //         this.setState({ visible: true })
  //         RNPaypal.paymentRequest({
  //             clientId: this.state.client_id,
  //             environment: RNPaypal.ENVIRONMENT.PRODUCTION,
  //             intent: RNPaypal.INTENT.SALE,
  //             price: parseFloat(this.state.amount),
  //             currency: 'USD',
  //             description: 'mobile recharge via paypal',
  //             acceptCreditCards: true
  //         }).then(response => {
  //             if (response.response.state == "approved") {
  //                 console.log("yes it is approved")
  //                 let postData = {
  //                     amount: this.state.amount,
  //                     currencyID: 1,
  //                     userId: this.state.userId,
  //                     methodID: 3,
  //                     details: {
  //                         status: "COMPLETED"
  //                     }
  //                 }
  //                 axios({
  //                     method: 'post',
  //                     url: 'https://sandbox.caribpayintl.com/api/deposit/paypal-payment-store',
  //                     data: postData,
  //                     headers: { 'Authorization': this.state.token },
  //                 }).then((response) => {
  //                     if (response.data.success.status == 200) {
  //                         let postData1 = {
  //                             phone: this.state.phone,
  //                             country: this.state.isoName,
  //                             amount: this.state.amount,
  //                             operator_id: this.props.navigation.state.params.operatorID,
  //                             user_id: this.state.userId,
  //                             currency_id: 1
  //                         }
  //                         console.log(postData1)
  //                         axios({
  //                             method: 'post',
  //                             url: 'https://sandbox.caribpayintl.com/api/top-up',
  //                             data: postData1,
  //                             headers: { 'Authorization': this.state.token },
  //                         })
  //                             .then((response) => {
  //                                 console.log("rcharge from paypal ---", response.data.data)
  //                                 if (response.data.status == 200) {
  //                                     let createdOn = response.data.data.transactionDate
  //                                     this.setState({
  //                                         RecipientPhone: response.data.data.recipientPhone,
  //                                         txn_date: createdOn,
  //                                         visible: false,
  //                                     })
  //                                     this.props.navigation.navigate("SuccessFullMobile", { operatorName: this.state.operatorName, RecipientPhone: this.state.RecipientPhone, paymentMode: 'PayPal', TxnDate: this.state.txn_date, amount: this.state.amount })
  //                                 }
  //                             }).catch((err) => {
  //                                 this.setState({ visible: false })
  //                                 alert(err)
  //                             });
  //                     }
  //                 }).catch((err) => {
  //                     console.log("error in paypal payment store ", err)
  //                 })
  //             } else {
  //                 this.setState({ visible: false })
  //                 alert('Payment Request Failed!')
  //             }
  //         }).catch((error) => {
  //             this.setState({ visible: false })
  //             console.log("error in paymentrequest", error)
  //         })
  //     } else {
  //         this.OnProccede();
  //     }
  // }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.header}>
          <TouchableOpacity
            style={styles.headerleftImage}
            onPress={() => this.props.navigation.goBack(null)}
          >
            <Image
              style={styles.headerleftImage}
              source={require("../Images/leftarrow.png")}
            />
          </TouchableOpacity>
          <Text style={styles.headertextStyle}>Select Option to Pay</Text>
        </View>

        <View style={styles.absouluteview}>
          <View style={{ flex: 1 }}>
            <Modal
              style={{ alignSelf: "center", width: "80%" }}
              isVisible={this.state.visible}
            >
              <View
                style={{
                  backgroundColor: "white",
                  borderRadius: metrics.dimen_10,
                }}
              >
                <View
                  style={{ flexDirection: "column", margin: metrics.dimen_20 }}
                >
                  <Spinner
                    style={{ alignSelf: "center" }}
                    isVisible={this.state.visible}
                    size={70}
                    type={"ThreeBounce"}
                    color={colors.black}
                  />
                  <Text
                    style={{
                      fontSize: metrics.text_header,
                      color: colors.black,
                      textAlign: "center",
                      marginTop: 20,
                    }}
                  >
                    We are processing now, Please wait a while!{" "}
                  </Text>
                </View>
              </View>
            </Modal>

            <View
              style={{
                flexDirection: "column",
                justifyContent: "center",
                margin: metrics.dimen_15,
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <Text
                  style={{
                    color: colors.black,
                    fontSize: metrics.text_header,
                    fontFamily: metrics.quicksand_bold,
                    textAlignVertical: "center",
                  }}
                >
                  {this.state.phone}
                </Text>
                <Text
                  style={{
                    color: colors.black,
                    fontSize: metrics.text_large,
                    fontFamily: metrics.quicksand_bold,
                    marginRight: metrics.dimen_10,
                    textAlignVertical: "center",
                  }}
                >
                  {"$ " + this.state.amount}
                </Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    color: colors.app_yellow_color,
                    fontSize: metrics.text_normal,
                    fontFamily: metrics.quicksand_semibold,
                    textAlignVertical: "center",
                  }}
                >
                  {"Prepaid , "}
                </Text>
                <Text
                  style={{
                    color: colors.app_yellow_color,
                    fontSize: metrics.text_normal,
                    fontFamily: metrics.quicksand_semibold,
                    marginRight: metrics.dimen_10,
                    textAlignVertical: "center",
                  }}
                >
                  {this.state.operatorName}
                </Text>
              </View>
            </View>

            <View style={{ flexDirection: "column", margin: metrics.dimen_15 }}>
              {/* <Text style={{ color: colors.carib_pay_blue, fontSize: metrics.text_16, fontWeight: '600', textAlignVertical: 'center' }}>{"Payment Options"}</Text>
                            <TouchableOpacity onPress={() => this.setState({ fromPayPal: true, fromWallet: false, fromCards: false })}>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <View style={{ width: metrics.dimen_20, height: metrics.dimen_20, borderColor: colors.carib_pay_blue, borderRadius: 20, backgroundColor: this.state.fromPayPal ? colors.carib_pay_blue : 'white', borderWidth: 1 }}>
                                    </View>
                                    <Text style={{ fontSize: metrics.text_header, color: colors.heading_black_text, fontWeight: '500', textAlignVertical: 'center', marginLeft: metrics.dimen_30 }}>PayPal</Text>
                                </View>
                            </TouchableOpacity> */}

              {/* <Text
                style={{
                  fontSize: metrics.text_header,
                  color: colors.heading_black_text,
                  fontFamily: metrics.quicksand_semibold,
                  textAlignVertical: "center",
                }}
              >
                {"You are about to pay money via : " + this.state.fromway}
              </Text> */}

              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    fromPayPal: false,
                    fromWallet: true,
                    fromway: "CaribPay",
                  })
                }
              >
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 10,
                    width: "100%",
                    alignSelf: "center",
                    borderRadius: 5,
                    marginLeft: 20,
                    marginRight: 20,
                    height: metrics.dimen_55,
                    backgroundColor: this.state.fromWallet
                      ? colors.theme_caribpay
                      : colors.light_grey_backgroud,
                  }}
                >
                  <Image
                    style={{
                      height: metrics.dimen_30,
                      width: metrics.dimen_30,
                      resizeMode: "contain",
                      alignSelf: "center",
                      marginLeft: 10,
                    }}
                    source={require("../Images/logo.png")}
                  />

                  <Text
                    style={{
                      fontSize: metrics.text_header,
                      color: this.state.fromWallet
                        ? colors.white
                        : colors.heading_black_text,
                      fontFamily: metrics.quicksand_bold,
                      textAlignVertical: "center",
                      marginLeft: metrics.dimen_15,
                      marginTop: -5,
                    }}
                  >
                    CaribPay
                  </Text>
                  <Text
                    style={{
                      fontSize: metrics.text_header,
                      color: this.state.fromWallet
                        ? colors.white
                        : colors.heading_black_text,
                      fontFamily: metrics.quicksand_bold,
                      textAlignVertical: "center",
                      marginLeft: metrics.dimen_10,
                      marginTop: -5,
                    }}
                  >
                    {"( $ " + this.state.balance + " )"}
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    fromPayPal: true,
                    fromWallet: false,
                    fromway: "PayPal",
                  })
                }
              >
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 10,
                    width: "100%",
                    alignSelf: "center",
                    borderRadius: 5,
                    marginLeft: 20,
                    marginRight: 20,
                    height: metrics.dimen_55,
                    backgroundColor: this.state.fromPayPal
                      ? colors.theme_caribpay
                      : colors.light_grey_backgroud,
                  }}
                >
                  <Image
                    style={{
                      height: metrics.dimen_24,
                      width: metrics.dimen_24,
                      resizeMode: "contain",
                      alignSelf: "center",
                      marginLeft: 10,
                    }}
                    source={require("../Images/paypal_logo.png")}
                  />

                  <Text
                    style={{
                      fontSize: metrics.text_header,
                      color: this.state.fromPayPal
                        ? colors.white
                        : colors.heading_black_text,
                      fontFamily: metrics.quicksand_bold,
                      textAlignVertical: "center",
                      marginLeft: metrics.dimen_15,
                      marginTop: -5,
                    }}
                  >
                    PayPal
                  </Text>
                </View>
              </TouchableOpacity>

              {/* {
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      fromPayPal: false,
                      fromWallet: false,
                      fromCards: true,
                    })
                  }
                >
                  <View style={{ flexDirection: "row", marginTop: 10 }}>
                    <View
                      style={{
                        width: metrics.dimen_20,
                        height: metrics.dimen_20,
                        borderColor: colors.carib_pay_blue,
                        borderRadius: 20,
                        backgroundColor: this.state.fromCards
                          ? colors.carib_pay_blue
                          : "white",
                        borderWidth: 1,
                      }}
                    />
                    <Text
                      style={{
                        fontSize: metrics.text_header,
                        color: colors.heading_black_text,
                        fontWeight: "500",
                        textAlignVertical: "center",
                        marginLeft: metrics.dimen_30,
                      }}
                    >
                      Debit/Credit Cards
                    </Text>
                  </View>
                </TouchableOpacity>
              } */}
            </View>

            {this.state.fromCards && (
              <View
                style={{ flexDirection: "column", margin: metrics.dimen_15 }}
              >
                <Text
                  style={{
                    color: colors.app_yellow_color,
                    fontSize: metrics.text_normal,
                    fontWeight: "600",
                    marginRight: metrics.dimen_10,
                    textAlignVertical: "center",
                    marginBottom: 10,
                  }}
                >
                  Please Enter your Card Detail here to make an payment.
                </Text>

                <Input
                  placeholder={"1245 5634 9980"}
                  containerStyle={{
                    width: "80%",
                    height: 50,
                    borderColor: colors.app_gray,
                    borderWidth: 0.5,
                    borderRadius: 10,
                    alignSelf: "center",
                  }}
                  keyboardType="number-pad"
                  rightIcon={() => (
                    <Image
                      style={{
                        height: 30,
                        width: 30,
                        resizeMode: "contain",
                        alignSelf: "center",
                      }}
                      source={require("../Images/card.png")}
                    />
                  )}
                  value={this.state.cardNumber}
                  inputContainerStyle={{ borderBottomWidth: 0 }}
                  inputStyle={{ fontSize: metrics.dimen_22 }}
                  onChangeText={(text) => this.setState({ cardNumber: text })}
                />

                <View
                  style={{
                    flexDirection: "row",
                    width: "80%",
                    borderColor: colors.app_gray,
                    borderWidth: 0.5,
                    height: 50,
                    marginTop: 10,
                    borderRadius: 10,
                    alignSelf: "center",
                  }}
                >
                  <Input
                    placeholder={"MM"}
                    containerStyle={{
                      flex: 0.5,
                      width: "40%",
                      height: 50,
                      borderRadius: 10,
                    }}
                    keyboardType="number-pad"
                    maxLength={2}
                    value={this.state.expiryMonth}
                    inputContainerStyle={{ borderBottomWidth: 0 }}
                    inputStyle={{ fontSize: metrics.dimen_22 }}
                    onChangeText={(text) =>
                      this.setState({
                        expiryMonth: text,
                      })
                    }
                  />

                  <View
                    style={{
                      borderLeftWidth: 1,
                      height: 35,
                      alignSelf: "center",
                      borderLeftColor: colors.app_gray,
                    }}
                  />

                  <Input
                    placeholder={"YY"}
                    containerStyle={{
                      flex: 0.5,
                      width: "40%",
                      height: 50,
                      borderRadius: 10,
                    }}
                    keyboardType="number-pad"
                    maxLength={2}
                    value={this.state.expiryYear}
                    inputContainerStyle={{ borderBottomWidth: 0 }}
                    inputStyle={{ fontSize: metrics.dimen_22 }}
                    onChangeText={(text) =>
                      this.setState({
                        expiryYear: text,
                      })
                    }
                  />

                  <View
                    style={{
                      borderLeftWidth: 1,
                      height: 35,
                      alignSelf: "center",
                      borderLeftColor: colors.app_gray,
                    }}
                  />

                  <Input
                    placeholder={"CVV"}
                    containerStyle={{
                      width: "40%",
                      flex: 0.5,
                      height: 50,
                      alignSelf: "center",
                      borderRadius: 10,
                    }}
                    keyboardType="number-pad"
                    value={this.state.cvv}
                    inputContainerStyle={{ borderBottomWidth: 0 }}
                    inputStyle={{ fontSize: metrics.dimen_22 }}
                    onChangeText={(text) => this.setState({ cvv: text })}
                  />
                </View>
              </View>
            )}
          </View>

          <View>
            <Modal
              style={{ borderRadius: metrics.dimen_10 }}
              isVisible={this.state.tryAgain}
              out
            >
              <View
                style={{
                  width: "80%",
                  alignSelf: "center",
                  backgroundColor: colors.white,
                  borderRadius: metrics.dimen_5,
                  margin: metrics.dimen_20,
                }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    height: metrics.dimen_60,
                    backgroundColor: colors.carib_pay_blue,
                  }}
                >
                  <Text
                    style={{
                      fontSize: metrics.text_heading,
                      color: colors.white,
                      padding: 20,
                      fontWeight: "bold",
                      textAlign: "center",
                    }}
                  >
                    Mobile-Topup
                  </Text>
                </View>
                <View
                  style={{
                    alignSelf: "center",
                    margin: metrics.dimen_15,
                    flexDirection: "column",
                  }}
                >
                  <Image
                    style={{
                      height: metrics.dimen_80,
                      width: metrics.dimen_80,
                      alignSelf: "center",
                    }}
                    source={require("../Images/warning.png")}
                  />

                  <Text
                    style={{
                      fontSize: metrics.text_normal,
                      color: colors.theme_caribpay,
                      padding: 20,
                      fontWeight: "bold",
                      textAlign: "center",
                    }}
                  >
                    Please Try After Some Time!
                  </Text>

                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        tryAgain: false,
                      })
                    }
                  >
                    <View
                      style={{
                        height: metrics.dimen_40,
                        width: metrics.dimen_120,
                        backgroundColor: colors.carib_pay_blue,
                        alignSelf: "center",
                        marginTop: 10,
                        justifyContent: "center",
                      }}
                    >
                      <Text
                        style={{
                          color: colors.white,
                          fontSize: metrics.text_normal,
                          textAlign: "center",
                        }}
                      >
                        {"OK"}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
          </View>
        </View>

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.isComingSoon}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                // margin: metrics.dimen_10,
                flexDirection: "column",
                height: 200,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isComingSoon: false,
                    })
                  }
                >
                  <Image
                    style={{
                      height: metrics.dimen_16,
                      width: metrics.dimen_16,
                      resizeMode: "contain",
                      margin: 15,
                    }}
                    source={require("../Images/cross-sign.png")}
                  />
                </TouchableOpacity>
                <Image
                  style={{
                    height: 150,
                    width: 150,
                    resizeMode: "cover",
                    alignSelf: "center",
                    marginTop: -30,
                  }}
                  source={require("../Images/soon.png")}
                />
                <Text
                  style={{
                    fontSize: metrics.text_heading,
                    color: colors.black,
                    fontFamily: metrics.quicksand_bold,
                    textAlign: "center",
                    marginTop: -20,
                  }}
                >
                  {"Pay Money via - PayPal"}
                </Text>
              </View>
            </View>
          </Modal>
        </View>

        <View style={styles.bottomview}>
          <TouchableOpacity onPress={() => this.onNext()}>
            <Text
              style={{
                fontSize: 15,
                color: "white",
                fontFamily: metrics.quicksand_bold,
                textAlign: "center",
                alignSelf: "center",
                marginLeft: 10,
              }}
            >
              {"Proceed to Pay ($" + this.state.amount + ") "}
            </Text>
          </TouchableOpacity>
        </View>

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.limitExceed}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                margin: metrics.dimen_20,
                flexDirection: "column",
                height: 250,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <View>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        limitExceed: false,
                      })
                    }
                  >
                    <Image
                      style={{
                        height: metrics.dimen_16,
                        width: metrics.dimen_16,
                        margin: 15,
                      }}
                      source={require("../Images/cross-sign.png")}
                    />
                  </TouchableOpacity>

                  <Image
                    style={{
                      height: 70,
                      width: 70,
                      resizeMode: "cover",
                      alignSelf: "center",
                    }}
                    source={require("../Images/danger.png")}
                  />
                  <Text
                    style={{
                      fontSize: 16,
                      color: colors.black,
                      fontFamily: metrics.quicksand_bold,
                      textAlign: "center",
                      margin: 20,
                    }}
                  >
                    {
                      "Mobile Topup!\nPayPal only accepting minimum 10 USD transaction to proceed. Please choose appropriate."
                    }
                  </Text>
                </View>
              </View>
            </View>
          </Modal>
        </View>

        <View>
          <Modal
            style={{ borderRadius: metrics.dimen_10 }}
            isVisible={this.state.isLowBalance}
          >
            <View
              style={{
                width: "90%",
                alignSelf: "center",
                backgroundColor: colors.white,
                borderRadius: metrics.dimen_10,
                margin: metrics.dimen_20,
                flexDirection: "column",
                height: 250,
                justifyContent: "center",
              }}
            >
              <View style={{ flex: 1 }}>
                <View>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        isLowBalance: false,
                      })
                    }
                  >
                    <Image
                      style={{
                        height: metrics.dimen_16,
                        width: metrics.dimen_16,
                        margin: 15,
                      }}
                      source={require("../Images/cross-sign.png")}
                    />
                  </TouchableOpacity>

                  <Image
                    style={{
                      height: 70,
                      width: 70,
                      resizeMode: "cover",
                      alignSelf: "center",
                    }}
                    source={require("../Images/danger.png")}
                  />
                  <Text
                    style={{
                      fontSize: 16,
                      color: colors.black,
                      fontFamily: metrics.quicksand_bold,
                      textAlign: "center",
                      margin: 20,
                    }}
                  >
                    {"Mobile Topup!\nYou have Insufficient Balance : " +
                      this.state.balance +
                      "!"}
                  </Text>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: metrics.dimen_70,
    backgroundColor: colors.carib_pay_blue,
    flexDirection: "row",
    paddingHorizontal: metrics.dimen_20,
  },
  headerleftImage: {
    height: metrics.dimen_24,
    width: metrics.dimen_24,
    alignSelf: "center",
    resizeMode: "contain",
    tintColor: colors.whitesmoke,
    marginBottom: metrics.dimen_15,
  },
  headertextStyle: {
    fontSize: metrics.text_17,
    color: colors.white,
    marginBottom: metrics.dimen_15,
    fontFamily: metrics.quicksand_bold,
    textAlignVertical: "center",
    paddingHorizontal: metrics.dimen_20,
  },
  absouluteview: {
    position: "absolute",
    top: 51,
    borderTopRightRadius: metrics.dimen_20,
    borderTopLeftRadius: metrics.dimen_20,
    width: "100%",
    height: Dimensions.get("screen").height,
    backgroundColor: colors.white,
  },
  container: { flexDirection: "row", margin: metrics.dimen_20 },
  image_style: {
    height: metrics.dimen_30,
    width: metrics.dimen_30,
    resizeMode: "contain",
  },
  headingg: { fontSize: metrics.text_header, color: colors.black },
  descriiption: { fontSize: metrics.text_description, color: colors.app_gray },
  arrowstyle: {
    height: metrics.dimen_15,
    width: metrics.dimen_15,
    resizeMode: "contain",
  },
  bottomview: {
    bottom: 10,
    position: "absolute",
    height: 40,
    backgroundColor: colors.carib_pay_blue,
    width: "90%",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 20,
  },
  horizontalLine: {
    borderBottomColor: "#D3D3D3",
    borderBottomWidth: 0.4,
    width: "90%",
    alignSelf: "center",
  },
});
