import React from 'react';
import { SafeAreaView, Text, View, FlatList, TouchableOpacity, BackHandler, Image, StyleSheet, Dimensions } from 'react-native';
import metrics from '../Themes/Metrics.js';
import colors from '../Themes/Colors.js';
const moment = require('moment');
const axios = require('axios');
import AsyncStorage from '@react-native-community/async-storage';
import RBSheet from "react-native-raw-bottom-sheet";
import { camelCase } from '../Utils.js';
var Spinner = require('react-native-spinkit');

export default class TransactionsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            transactionData: [],
            subtotalValue: '',
            transaction_id: '',
            type: '',
            payment_method_name: '', user_id: this.props.navigation.state.params.user_id,
            token: this.props.navigation.state.params.token,
            visible: false,
            requestLoading: false,
            request_To_Status: "",
            userEndFullName: '',
            endUserEmail: '',
            available_Balance: this.props.navigation.state.params.available_Balance,
            txn_ref_id: '',
            txn_total_fees: '',
            symbol: this.props.navigation.state.params.symbol,
            fromAll: true, fromRecieve: false, fromsent: false,
            recieved_txn_Data: '',
            sent_txn_Data: '',
        }
    }

    componentDidMount() {
        console.log("response list --- >", this.state.symbol)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.setState({
            visible: true
        })
        let postData = {
            type: "allTransactions",
            user_id: this.state.user_id,
            currency_id: this.state.symbol == "USD" ? "1" : "5"
        }
        axios.get('https://sandbox.caribpayintl.com/api/activityall', { params: postData, headers: { 'Authorization': this.state.token } }).then((response) => {
            if (response.data.success.status == 200) {
                let txnlist = response.data.transactions
                console.log("txnlist --> ", txnlist)
                const txnId1 = txnlist.filter(roshan => roshan.transaction_type_id == 1);
                const txnId4 = txnlist.filter(roshan => roshan.transaction_type_id == 4);
                const txnId9 = txnlist.filter(roshan => roshan.transaction_type_id == 9);
                const txnId12 = txnlist.filter(roshan => roshan.transaction_type_id == 12);
                var total_txn_recieved = txnId1.concat(txnId4, txnId9, txnId12);
                const txnId2 = txnlist.filter(roshan => roshan.transaction_type_id == 2);
                const txnId3 = txnlist.filter(roshan => roshan.transaction_type_id == 3);
                const txnId10 = txnlist.filter(roshan => roshan.transaction_type_id == 10);
                const txnId11 = txnlist.filter(roshan => roshan.transaction_type_id == 11);
                const txnId15 = txnlist.filter(roshan => roshan.transaction_type_id == 15);
                var total_txn_sent = txnId2.concat(txnId3, txnId10, txnId11, txnId15);
                this.setState({
                    transactionData: txnlist,
                    recieved_txn_Data: total_txn_recieved,
                    sent_txn_Data: total_txn_sent,
                    visible: false
                })
            }
        }).catch((error) => {
            console.log(error);
        })









    }






    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null)
        return true;
    }

    onRequestMoney = (item) => {
        let postData = { trans_id: item.id }
        axios.get('https://sandbox.caribpayintl.com/api/transaction-details/request-payment/check-creator-status', { params: postData, headers: { 'Authorization': this.state.token } }).then((response) => {
            if (response.data.success.status == 200) {
                let user_name = camelCase(item.end_user_f_name) + " " + camelCase(item.end_user_l_name)
                this.setState({
                    userEndFullName: user_name,
                    endUserEmail: item.end_user_email
                })
                this.RBSheetRequestToMoney.open()
            }
        }).catch((error) => {
            console.log(error);
        })
    }



    onPressItems = async (item) => {

        let user_token = await AsyncStorage.getItem('token');
        let postData = {
            user_id: item.user_id,
            tr_id: item.id
        }
        await axios.get('https://sandbox.caribpayintl.com/api/transaction-details', { params: postData, headers: { 'Authorization': user_token } }).then((response) => {
            if (response.data.success.status == 200) {
                console.log("item onlick ", response.data.transaction)
                var txn_type;
                switch (response.data.transaction.transaction_type_id) {
                    case 1:
                        txn_type = "Reload"
                        break;
                    case 4:
                        txn_type = "Money Receive"
                        break;
                    case 9:
                        txn_type = "Request Sent"
                        break;
                    case 12:
                        txn_type = "Shopping"
                        break;
                    case 3:
                        txn_type = "Money Transfer"
                        break;
                    case 10:
                        txn_type = "Request Received"
                        break;
                    case 11:
                        txn_type = "Shopping"
                        break;
                    case 15:
                        txn_type = "Mobile Top-Up"
                        break;
                }
                this.setState({
                    subtotalValue: response.data.transaction.subtotal,
                    transaction_id: response.data.transaction.transaction_id,
                    type: txn_type,
                    payment_method_name: response.data.transaction.payment_method_name,
                    request_To_Status: response.data.transaction.status,
                    txn_ref_id: response.data.transaction.transaction_reference_id,
                    txn_total_fees: response.data.transaction.totalFees,
                    txn_detail_time: response.data.transaction.t_created_at
                })
                // console.log("info coming ,", this.state.subtotalValue, this.state.transaction_id, this.state.type, this.state.payment_method_name)
                { this.state.type == "Request Received" && this.state.request_To_Status == "Pending" ? this.onRequestMoney(item) : this.RBSheetAlert.open() }
            }
        }).catch((error) => {
            console.log(error);
        })
    }

    formatDate = (dateformat, date) => {
        try {
            return moment.utc(date).local().format(dateformat)
        } catch (error) {
            return 'Invalid Date'
        }
    }

    renderItem = (data, index) => {
        console.log("txn data ----->", data)
        var txn_type;

        let totalAmount = data.total
        let txn_amount
        if (totalAmount.charAt(0) == "-") {
            console.log("total amount -")
            txn_amount = totalAmount.slice(1)
        } else {
            txn_amount = data.total
        }

        switch (data.transaction_type_id) {
            case 1:
                txn_type = "Reload"
                break;
            case 4:
                txn_type = "Money Receive"
                break;
            case 9:
                txn_type = "Request Sent"
                break;
            case 12:
                txn_type = "Shopping"
                break;
            case 3:
                txn_type = "Money Transfer"
                break;
            case 10:
                txn_type = "Request Received"
                break;
            case 11:
                txn_type = "Shopping"
                break;
            case 15:
                txn_type = "Mobile Top-Up"
                break;
        }

        return (
            <TouchableOpacity onPress={() => this.onPressItems(data)}>
                <View style={styles.cellContainer}>
                    <View style={{
                        flexDirection: 'row', backgroundColor: 'white', height: 70, justifyContent: 'space-between', marginBottom: 10,
                        borderWidth: 1,
                        borderRadius: 7,
                        borderColor: '#ddd',
                        borderBottomWidth: 1,
                        shadowColor: 'black',
                        shadowOffset: { width: 1, height: 2 },
                        shadowOpacity: 0.9,
                        shadowRadius: 3,
                        elevation: 5,
                    }}>
                        <View style={{ flexDirection: 'row' }}>

                            {data.transaction_type_id == 4 && <Image style={{ height: 45, width: 45, alignSelf: 'center',  marginLeft: 20 }} source={require("../Images/money_rcv.png")}>
                            </Image>}
                            {data.transaction_type_id == 15 && <Image style={{ height: 45, width: 45, alignSelf: 'center', marginLeft: 20 }} source={require("../Images/topups.png")}>
                            </Image>
                            }
                            {data.transaction_type_id == 11 && <Image style={{ height: 45, width: 45, alignSelf: 'center',  marginLeft: 20 }} source={require("../Images/shopping.png")}>
                            </Image>}

                            {data.transaction_type_id == 1 && <Image style={{ height: 45, width: 45, alignSelf: 'center',  marginLeft: 20 }} source={require("../Images/reloads.png")}>
                            </Image>}


                            {data.transaction_type_id == 3 && <Image style={{ height: 45, width: 45, alignSelf: 'center', marginLeft: 20 }} source={require("../Images/transferamount.png")}>
                            </Image>}
                         
                            {data.transaction_type_id == 9 && <Image style={{ height: 45, width: 45, alignSelf: 'center', marginLeft: 20 }} source={require("../Images/send.png")}>
                            </Image>}
                            
                            {data.transaction_type_id == 10 && <Image style={{ height: 45, width: 45, alignSelf: 'center', marginLeft: 20 }} source={require("../Images/request_money.png")}>
                            </Image>}
                            
                            {/* {data.end_user_photo != "" && data.transaction_type_id != 1 && <Image style={{ height: 45, width: 45, alignSelf: 'center', borderRadius: 45 / 2, marginLeft: 20 }} source={{ uri: data.end_user_photo }}>
                            </Image>} */}

                            {/* {data.end_user_photo == "" && <Image style={{ height: 45, width: 45, alignSelf: 'center', borderRadius: 45 / 2, marginLeft: 20 }} source={require("../Images/txn_image.png")}>
                            </Image>
                            } */}


                            <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft:20 }}>
                                <Text style={{ fontSize: metrics.text_normal, color: colors.app_black_text,fontFamily:metrics.quicksand_bold }}>{txn_type}</Text>
                                <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray,fontFamily:metrics.quicksand_semibold }}>{this.formatDate('MMMM DD, YYYY', data.t_created_at)}</Text>
                            </View>
                        </View>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, fontFamily:metrics.quicksand_bold, textAlignVertical: 'center', marginRight: 20 }}>{data.curr_symbol + " " + txn_amount}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    gotoReviewSend = () => {
        this.RBSheetRequestToMoney.close();
        this.props.navigation.navigate("ReviewSend", { sendCurrency: "1", user_id: this.state.user_id, sendAmount: this.state.subtotalValue, balance: this.state.available_Balance, emailPhone: this.state.endUserEmail, fromTxnList: '1', txn_ref_id: this.state.txn_ref_id, totalFees: this.state.txn_total_fees })
    }


    renderRequestMoney = () => {
        return (
            <View style={{ flex: 1 }}>

                <View>
                    <View style={{ height: 60, backgroundColor: colors.carib_pay_blue, justifyContent: 'center' }}>
                        <Text style={{ fontSize: metrics.text_large, color: colors.white, fontWeight: 'bold', paddingHorizontal: 20, textAlign: 'center' }}>Request Money</Text>
                    </View>
                    <View style={{ margin: metrics.dimen_15, justifyContent: 'center', flexDirection: 'column' }}>
                        <Text style={{ fontSize: metrics.text_large, color: colors.carib_pay_blue, fontWeight: 'bold', textAlign: 'center' }}>{this.state.userEndFullName}</Text>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.heading_black_text, fontWeight: 'bold', textAlign: 'center', marginTop: metrics.dimen_5 }}>{"Requesting You for amount $" + this.state.subtotalValue}</Text>

                        <View style={{ flexDirection: 'row', flex: 1, marginTop: metrics.dimen_15 }}>

                            <View style={{ flex: 0.5, height: metrics.dimen_50, justifyContent: 'center', borderRadius: metrics.dimen_7, backgroundColor: 'red' }}>
                                <Text style={{ fontSize: metrics.text_header, color: colors.white, fontWeight: 'bold', textAlign: 'center' }}>{"Reject"}</Text>
                            </View>


                            <TouchableOpacity style={{ flex: 0.5, height: metrics.dimen_50, justifyContent: 'center', borderRadius: metrics.dimen_7, backgroundColor: colors.carib_pay_blue, marginLeft: metrics.dimen_10 }} onPress={() => this.gotoReviewSend()}>
                                <View style={{ flex: 0.5, height: metrics.dimen_50, justifyContent: 'center', borderRadius: metrics.dimen_7, backgroundColor: colors.carib_pay_blue, marginLeft: metrics.dimen_10 }}>
                                    <Text style={{ fontSize: metrics.text_header, color: colors.white, fontWeight: 'bold', textAlign: 'center' }}>{"Accept"}</Text>
                                </View>
                            </TouchableOpacity>

                        </View>

                    </View>

                </View>

            </View>
        )
    }





    renderTxnDetails = () => {
        return (
            <View>
                <View style={{ height: 60, backgroundColor: colors.theme_caribpay, justifyContent: 'center' }}>
                    <Text style={{ fontSize: metrics.text_large, color: colors.white, fontFamily:metrics.quicksand_bold, paddingHorizontal: 20 }}>Transaction Detail</Text>
                </View>

                <View style={{ flexDirection: 'column', margin: metrics.dimen_10, borderTopLeftRadius: 10, borderTopLeftRadius: 10 }}>
                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black,fontFamily:metrics.quicksand_bold }}>{"Transaction Amount"}</Text>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray,fontFamily:metrics.quicksand_semibold }}>{"$ " + this.state.subtotalValue}</Text>
                    </View>

                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black,fontFamily:metrics.quicksand_bold }}>Fees</Text>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray,fontFamily:metrics.quicksand_semibold }}>{this.state.txn_total_fees}</Text>
                    </View>

                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black,fontFamily:metrics.quicksand_bold }}>{"Date / Time"}</Text>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray,fontFamily:metrics.quicksand_semibold }}>{this.formatDate('DD/MM/YYYY hh:mm:ss', this.state.txn_detail_time)}</Text>
                    </View>


                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black,fontFamily:metrics.quicksand_bold }}>Transaction No.</Text>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray,fontFamily:metrics.quicksand_semibold }}>{this.state.transaction_id}</Text>
                    </View>

                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black,fontFamily:metrics.quicksand_bold }}>Type</Text>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray,fontFamily:metrics.quicksand_semibold }}>{this.state.type}</Text>
                    </View>

                    <View style={{ flexDirection: 'column', margin: 10 }}>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.black,fontFamily:metrics.quicksand_bold }}>Payment Method</Text>
                        <Text style={{ fontSize: metrics.text_normal, color: colors.app_gray, fontFamily:metrics.quicksand_semibold }}>{this.state.payment_method_name == undefined ? "Wallet" : this.state.payment_method_name == "Stripe" ? "Visa/Master Card" : this.state.payment_method_name}</Text>
                    </View>


                </View>
            </View>
        )
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Spinner style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />

                {!this.state.visible && <View style={{ flexDirection: 'row', backgroundColor: colors.theme_caribpay, height: metrics.dimen_40, alignSelf: 'center', width: '100%' }}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 3, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromAll ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromAll: true, fromRecieve: false, fromsent: false })}>
                        <View style={{ justifyContent: 'center', flex: 1 / 3, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromAll ? colors.light_grey_backgroud : colors.transparent }}>
                            <Text style={{ fontSize: metrics.text_normal, color: this.state.fromAll ? colors.black : colors.whitesmoke, textAlign: 'center',fontFamily:metrics.quicksand_bold }}>All</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 3, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromRecieve ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromRecieve: true, fromAll: false, fromsent: false })}>
                        <View style={{ justifyContent: 'center', flex: 1 / 3, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromRecieve ? colors.light_grey_backgroud : colors.transparent }}>
                            <Text style={{ fontSize: metrics.text_normal, color: this.state.fromRecieve ? colors.black : colors.whitesmoke, textAlign: 'center',fontFamily:metrics.quicksand_bold }}>Received</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1 / 3, borderRadius: metrics.dimen_10, margin: 2, backgroundColor: this.state.fromsent ? colors.light_grey_backgroud : colors.transparent }} onPress={() => this.setState({ fromRecieve: false, fromAll: false, fromsent: true })}>
                        <View style={{ justifyContent: 'center', flex: 1 / 3, borderRadius: metrics.dimen_10, backgroundColor: this.state.fromsent ? colors.light_grey_backgroud : colors.transparent }}>
                            <Text style={{ fontSize: metrics.text_normal, color: this.state.fromsent ? colors.black : colors.whitesmoke, textAlign: 'center',fontFamily:metrics.quicksand_bold }}>Sent</Text>
                        </View>
                    </TouchableOpacity>
                </View>}


                {/* {this.state.visible &&
                    <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                        <Spinner style={{ marginTop: 100 }} isVisible={this.state.visible} size={70} type={"ThreeBounce"} color={colors.black} />
                    </View>
                } */}

                {this.state.fromAll == false && this.state.fromRecieve == false && this.state.fromsent && this.state.sent_txn_Data.length == 0 &&
                    <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 1 }}>
                        <Image style={{ width: 100, height: 100, resizeMode: 'contain', alignSelf: 'center' }}
                            source={require("../Images/notxn.png")}></Image>
                        <Text style={{ fontSize: metrics.text_16, color: colors.app_gray, textAlign: 'center', alignSelf: 'center',fontFamily:metrics.quicksand_bold }}>Sorry, No Transactions!</Text>
                    </View>}

                {this.state.fromAll == false && this.state.fromRecieve && this.state.fromsent == false && this.state.recieved_txn_Data.length == 0 &&
                    <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 1 }}>
                        <Image style={{ width: 100, height: 100, resizeMode: 'contain', alignSelf: 'center' }}
                            source={require("../Images/notxn.png")}></Image>
                        <Text style={{ fontSize: metrics.text_16, color: colors.app_gray, textAlign: 'center', alignSelf: 'center',fontFamily:metrics.quicksand_bold }}>Sorry, No Transactions!</Text>
                    </View>}

                {this.state.visible == false && this.state.transactionData.length == 0 && this.state.fromsent == false && this.state.fromRecieve == false &&
                    <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 1 }}>
                        <Image style={{ width: 100, height: 100, resizeMode: 'contain', alignSelf: 'center' }}
                            source={require("../Images/notxn.png")}></Image>
                        <Text style={{ fontSize: metrics.text_16, color: colors.app_gray, textAlign: 'center', alignSelf: 'center',fontFamily:metrics.quicksand_bold }}>Sorry, No Transactions!</Text>
                    </View>
                }
                {this.state.visible == false && this.state.transactionData.length > 0 &&
                    <FlatList
                        style={{ marginTop: 10, marginBottom: 10 }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.fromAll ? this.state.transactionData : this.state.fromRecieve ? this.state.recieved_txn_Data : this.state.sent_txn_Data}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => this.renderItem(item)} />
                }
                <RBSheet
                    ref={ref => {
                        this.RBSheetAlert = ref;
                    }}
                    height={450}
                    duration={0}>
                    {this.renderTxnDetails()}
                </RBSheet>
                <RBSheet
                    ref={ref => {
                        this.RBSheetRequestToMoney = ref;
                    }}
                    height={230}
                    duration={0}>
                    {this.renderRequestMoney()}
                </RBSheet>
            </SafeAreaView>


        )
    }
}


const styles = StyleSheet.create({
    listStyle: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: 'transparent'
    },
    cellContainer: {
        width: '100%',
        flexDirection: 'column',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 7

    },
    cellSubViewContainer: {
        padding: 8,
        flexDirection: 'column',
        backgroundColor: 'white',
        borderWidth: 1,
        borderRadius: 5,
        marginHorizontal: 8,
        marginTop: 4,
        borderColor: colors.view_border_color,
    },
    cellSubViewTopContainer: {
        paddingLeft: 5,
        paddingRight: 5,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: 'white',
        paddingBottom: 0

    },
    SectionHeaderStyle: {
        backgroundColor: 'white',
        fontSize: metrics.text_16,
        padding: 10,
        color: colors.heading_color,
    },
    iconStyle: {
        marginTop: 2,
        width: metrics.dimen_27,
        height: metrics.dimen_27,

    },
    payment_iconStyle: {
        width: 18,
        height: 18,
        marginLeft: 10,
        marginRight: 10

    },
    textStyleBold: {
        fontFamily: metrics.proxima_nova,
        color: colors.gray,
        fontSize: metrics.text_normal,
        fontWeight: 'normal',
        // textAlign: 'center'
    },
    textStyleBoldBalance: {
        fontFamily: metrics.proxima_nova,
        // color: colors.gray,
        fontSize: metrics.text_normal,
        fontWeight: 'normal',
        textAlign: 'center'
    },
    textStyleExtBold: {
        fontFamily: metrics.proxima_nova,
        color: colors.gray,
        fontSize: metrics.text_normal,
        fontWeight: '400',
    },
    textStyleNormal: {
        fontFamily: metrics.proxima_nova,
        fontSize: metrics.text_normal,
        color: 'black',
        fontWeight: '400'
    },

    textStyleNormalwhite: {
        fontFamily: metrics.proxima_nova,
        fontSize: metrics.text_normal,
        color: 'white',
        fontWeight: '400'
    },
    textStyleRegular: {
        fontFamily: metrics.proxima_nova_regular,
        fontSize: metrics.text_medium,
        color: colors.white,
        textAlign: 'center',
        marginTop: metrics.dimen_2

    },
    textStyleRegular2: {
        fontFamily: metrics.proxima_nova_regular,
        fontSize: metrics.text_medium,
        textAlign: 'center',
        marginTop: metrics.dimen_2
    },
    seperatorStyle: {
        height: 1,
        width: '100%',
        backgroundColor: colors.view_border_color,
    },
    requestButtonStyle: {
        backgroundColor: colors.app_blueColor,
        height: metrics.dimen_40,
        paddingLeft: metrics.dimen_30,
        paddingRight: metrics.dimen_30,
        borderRadius: metrics.dimen_50 / 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20
    },
    bottomViewStyle: {
        position: 'absolute',
        width: '100%',
        paddingTop: 40,
        bottom: 0,
        left: 0,
        alignItems: 'center'
    }
})
