import React from 'react';
import { Text, FlatList, View, Image, TouchableOpacity, BackHandler } from 'react-native'
import metrics from '../Themes/Metrics';
import colors from '../Themes/Colors.js';
var Spinner = require('react-native-spinkit');
const axios = require('axios');
import Modal from 'react-native-modal';
const moment = require('moment');


export default class UserNotification extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            token: this.props.navigation.state.params.token,
            user_id: this.props.navigation.state.params.user_id,
            notifyData: []
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.setState({
            spinvisible: true
        })
        let postData = {
            user_id: this.state.user_id,
        }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/noticeboard',
            headers: { 'Authorization': this.state.token },
            data: postData
        }).then((response) => {
            console.log("notification response ---> ", response.data.success.data)
            this.setState({
                notifyData: response.data.success.data,
                spinvisible: false
            })
        }).catch((err) => {
            console.log("notification response  error   --- > ", err)
            this.setState({
                spinvisible: false
            })

        })

        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                this.reloadNotifications();
            },
        );
    }

    componentWillUnmount() {
        this.willFocusSubscription.remove();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    }


    reloadNotifications = () => {
        this.setState({
            spinvisible: true
        })
        let postData = {
            user_id: this.state.user_id,
        }
        axios({
            method: 'post',
            url: 'https://sandbox.caribpayintl.com/api/noticeboard',
            headers: { 'Authorization': this.state.token },
            data: postData
        }).then((response) => {
            console.log("notification response ---> ", response.data.success.data)
            this.setState({
                notifyData: response.data.success.data,
                spinvisible: false
            })
        }).catch((err) => {
            console.log("notification response  error   --- > ", err)
            this.setState({
                spinvisible: false
            })

        })
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.white }}>
                {!this.state.spinvisible && this.state.notifyData.length > 0 &&
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.notifyData}
                        ItemSeparatorComponent={() => <View style={{ borderBottomWidth: 1, borderBottomColor: colors.light_grey_backgroud }}></View>}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) =>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("NotificationDetails", { data: item, token: this.state.token })}>
                                <View style={{ flexDirection: 'column', margin: 20 }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <Text style={{ fontSize: 16, color: item.read_status == 0 ? colors.black : colors.app_gray,fontFamily:metrics.quicksand_bold}}>{item.title}</Text>
                                        <Text style={{ textAlign: 'right', fontSize: 13, color: item.read_status == 0 ? colors.black : colors.app_gray, fontWeight: '500', marginTop: 3, marginRight: 10 }}>{moment(item.created_at).format("h:mm a")}</Text>
                                    </View>
                                    <Text style={{ fontSize: 13, color: item.read_status == 0 ? colors.black : colors.app_gray, fontFamily:metrics.quicksand_semibold, marginTop: 3 }}>{item.content}</Text>
                                </View>
                            </TouchableOpacity>
                        }></FlatList>
                }
                {!this.state.spinvisible && this.state.notifyData.length == 0 &&
                    <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                        <Image source={require("../Images/nonoti.png")}
                            style={{ height: 180, width: 180, alignSelf: 'center' }}></Image>
                        <Text style={{ fontSize: 22, color: colors.black, fontWeight: 'bold', textAlign: 'center' }}>{"No Notifications"}</Text>
                        <Text style={{ fontSize: 13, color: colors.app_gray, fontWeight: '500', marginTop: 3 }}>{"We will notify you once something arrives."}</Text>
                    </View>}


                <Modal style={{ borderRadius: metrics.dimen_10 }} isVisible={this.state.spinvisible}>
                    <View style={{ width: "50%", alignSelf: 'center', justifyContent: 'center', backgroundColor: colors.white, borderRadius: metrics.dimen_5, margin: metrics.dimen_20, height: 100 }}>
                        <Spinner style={{ alignSelf: 'center' }} isVisible={this.state.spinvisible} size={70} type={"ThreeBounce"} color={colors.black} />
                    </View>
                </Modal>
            </View >
        )
    }
}