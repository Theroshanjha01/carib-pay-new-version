import { Alert } from 'react-native'
import NetInfo from '@react-native-community/netinfo';

export const showAlert = (title = '', message = '') => {
    setTimeout(() => Alert.alert(
        title,
        message,
        [{ text:'OK' }],
        //{cancelable: false},
    ), 100)
}

export const camelCase = (str) => {
    console.log("string received = ", str)
    let wordsArray = str.split(' ')
    return wordsArray.map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
  }



  export const environment = {
    // currentEndPoint: 'https://private-c533ef-cardx.apiary-mock.com/',
    currentEndPoint: 'https://sandbox.caribpayintl.com/api/',
    // currentEndPoint: 'http://373a97127e45.ngrok.io/api/v1/',
  };
  
  export const paypal_basictoken = 'Basic QWRFM0ZyRWdmQTktbVZoM1RiMTktNkhCRWN5SUU2cGp4X0wyMjB3SThnekk0SmhibGk0b3NZZDZvUkJRY1dFcml6eDlmOEtYLVlqWXZ1NEw6RUFxVWdFQmpFNmtFc0FHQ05RSldKMlNBSUtvalpseEhZMXE3RDRmMnVsR2pyS0FwSV9jSnByX2lHanhQak9nSlY5VmNzb0JVQjdJd1FfOWQ=';
  export const paypal_gettoken = 'https://api.sandbox.paypal.com/v1/oauth2/token';
  export const paypal_gettoken_live = 'https://api.paypal.com/v1/oauth2/token';
  export const paypal_payment = 'https://api.sandbox.paypal.com/v1/payments/payment';
  export const paypal_payment_live = 'https://api.paypal.com/v1/payments/payment';

export default class NetworkUtils {
  static async isNetworkAvailable() {
    const response = await NetInfo.fetch();
    return response.isConnected;
  }
}