import AsyncStorage from '@react-native-community/async-storage';
import NetworkUtils from './Utils';

export default async function fetchGet(url) {
  const isConnected = await NetworkUtils.isNetworkAvailable();
  if (!isConnected) {
    var response = {
      status: 'failed',
      message: 'Please check your Internet connection',
    };
    return response;
  }
  let objectReq = {
    method: 'GET',
    headers: {
      Accept: 'application/json',
    },
  };
//   let userId = await AsyncStorage.getItem('userId');

//   if (userId) {
//     let id = userId.replace(/^"|"$/g, '');
//     objectReq.headers.Authorization = 'Bearer ' + id;
//   }
  const res = await fetch(url, objectReq);
  console.log(res.status);
  if (res.status === 404) {
    return {status: 'failed', message: 'Page not available'};
  }
  const jsonRes = await res.json();
  return jsonRes;
}
