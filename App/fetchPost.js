// import AsyncStorage from '@react-native-community/async-storage';
import NetworkUtils from './Utils';

export default async function fetchPost(url, body, token, json) {
  const isConnected = await NetworkUtils.isNetworkAvailable();
  if (!isConnected) {
    var response = {
      status: 'error',
      message: 'Please check your Internet connection',
    };
    return response;
  }

  let objectReq = {
    method: 'POST',
    body: json ? JSON.stringify(body) : body,
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: token
    },
  };
  
    // objectReq.headers.Authorization = token;
  
    try {
    const res = await fetch(url, objectReq);
    const jsonRes = await res.json();
    return jsonRes;
  } catch (error) {
    return false;
  }
}
